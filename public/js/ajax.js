$('form.ajax button[type=submit]').click(function(e){
    e.preventDefault();

var form = jQuery(this).parents("form:first");
var dataString = form.serialize();

var formAction = form.attr('action');

$.ajax({
    type: "POST",
    url : formAction,
    data : dataString,
    success : function(data){
        alert("Testing ajax forms: " + data);
    }
    },"json");
});