<?php

class ExampleTest extends TestCase {

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
	public function testBasicExample()
	{
		$crawler = $this->call('GET', '/inventories/product/totes');

		$this->assertEquals('Area', $crawler->getContent());
		//
		// $this->visit('/products/totes')
		// 		 ->see('Area');
	}

}
