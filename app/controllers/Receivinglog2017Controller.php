<?php

class Receivinglog2017Controller extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /receivinglog2016
	 *
	 * @return Response
	 */
	public function index()
	{
		// $receivinglogs = Lotlog::all();

		// $receivinglogs = DB::raw("select date from receivingappointmentlogs where date like '%2016'");

		

		$receivinglogs = DB::table('receivingappointmentlogs')
								->where('date', 'LIKE', '%2017')
								->where('active', '=', '2')
								->orderBy('date', 'asc')
								->get();

		return View::make('receivinglogs2017.index', compact('receivinglogs'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /receivinglog2016/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /receivinglog2016
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Lotlog::$rules);

		if ($validator->fails()) {
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Receivinglog::create($data);
		Session::flash('message', 'Your Receiving Log Was Created');
		Session::flash('alert-class', 'alert-success');
		return Redirect::route('receivinglogs2017.index');
	}

	/**
	 * Display the specified resource.
	 * GET /receivinglog2016/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$receivinglog = Receivingappointmentlog::findOrFail($id);

		return View::make('receivinglogs2017.show', compact('receivinglog'));
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /receivinglog2016/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$receivinglog = Receivingappointmentlog::findOrFail($id);

		return View::make('receivinglogs2017.edit', compact('receivinglog'));
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /receivinglog2016/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$receivinglog = Receivingappointmentlog::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Receivingappointmentlog::$rules);

		if ($validator->fails()) {
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$receivinglog->update($data);
		Session::flash('message', 'Your Receiving Log Was Updated Successfully');
		Session::flash('alert-class', 'alert-success');
		return Redirect::route('receivinglogs2017.index');
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /receivinglog2016/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Receivingappointmentlog::destroy($id);
		Session::flash('message', 'Your Receiving Log Was Deleted');
		Session::flash('alert-class', 'alert-danger');
		return Redirect::route('receivinglogs2017.index');
	}

	// public function label($id)
	// {
	// 	$lotlog = Lotlog::find($id);
	// 	return View::make('receivinglogs2017.label', compact('lotlog'));
	// }

	/**
	 * Print all of Recievinglog2015 from storage
	 *
	 * @return Response
	 */

	public function print_page() {
		$receivinglogs = DB::table('receivingappointmentlogs')
								->where('date', 'LIKE', '%2017')
								->where('active', '=', '2')
								->get();

		return View::make('receivinglogs2017.print', compact('receivinglogs'));
	}


}