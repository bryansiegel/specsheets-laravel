<?php

class NutritionfactsController extends \BaseController {

	/**
	 * Display a listing of nutritionfacts
	 *
	 * @return Response
	 */
	public function index()
	{
		$nutritionfacts = Nutritionfact::all();

		return View::make('nutritionfacts.index', compact('nutritionfacts'));
	}

	/**
	 * Show the form for creating a new nutritionfact
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('nutritionfacts.create');
	}

	/**
	 * Store a newly created nutritionfact in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Nutritionfact::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Nutritionfact::create($data);
        Session::flash('message', 'Your Nutrition Fact Was Created');
        Session::flash('alert-class', 'alert-success');
		return Redirect::route('nutritionfacts.index');
	}

	/**
	 * Display the specified nutritionfact.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$nutritionfact = Nutritionfact::findOrFail($id);

		return View::make('nutritionfacts.show', compact('nutritionfact'));
	}

	/**
	 * Show the form for editing the specified nutritionfact.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$nutritionfact = Nutritionfact::find($id);

		return View::make('nutritionfacts.edit', compact('nutritionfact'));
	}

	/**
	 * Update the specified nutritionfact in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$nutritionfact = Nutritionfact::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Nutritionfact::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$nutritionfact->update($data);
        Session::flash('message', 'Your Nutrition Fact Was Updated Successfully');
        Session::flash('alert-class', 'alert-success');
		return Redirect::route('nutritionfacts.index');
	}

	/**
	 * Remove the specified nutritionfact from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Nutritionfact::destroy($id);
        Session::flash('message', 'Your Nutrition Fact Was Deleted');
        Session::flash('alert-class', 'alert-danger');
		return Redirect::route('nutritionfacts.index');
	}

	public function download()
	{
		return "hi";
	}

}
