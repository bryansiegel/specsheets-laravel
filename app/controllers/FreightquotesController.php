<?php

class FreightquotesController extends \BaseController {

	/**
	 * Display a listing of freightquotes
	 *
	 * @return Response
	 */
	public function index()
	{
		$freightquotes = Freightquote::all();

		return View::make('freightquotes.index', compact('freightquotes'));
	}

	/**
	 * Show the form for creating a new freightquote
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('freightquotes.create');
	}

	/**
	 * Store a newly created freightquote in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Freightquote::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Freightquote::create($data);

		return Redirect::route('freightquotes.index');
	}

	/**
	 * Display the specified freightquote.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$freightquote = Freightquote::findOrFail($id);

		return View::make('freightquotes.show', compact('freightquote'));
	}

	/**
	 * Show the form for editing the specified freightquote.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$freightquote = Freightquote::find($id);

		return View::make('freightquotes.edit', compact('freightquote'));
	}

	/**
	 * Update the specified freightquote in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$freightquote = Freightquote::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Freightquote::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$freightquote->update($data);

		return Redirect::route('freightquotes.index');
	}

	/**
	 * Remove the specified freightquote from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Freightquote::destroy($id);

		return Redirect::route('freightquotes.index');
	}

}
