<?php

class Recievinglog2015sController extends BaseController {

	/**
	 * Display a listing of Recievinglog2015s
	 *
	 * @return Response
	 */
	public function index() {
		$recievinglog2015s = DB::table('recievinglogs2015s')
			->orderBy('id', 'desc')
			->get();

		return View::make('recievinglog2015s.index', compact('recievinglog2015s'));
	}

	/**
	 * Show the form for creating a new Recievinglog2015
	 *
	 * @return Response
	 */
	public function create() {
		return View::make('recievinglog2015s.create');
	}

	/**
	 * Store a newly created Recievinglog2015 in storage.
	 *
	 * @return Response
	 */
	public function store() {
		$validator = Validator::make($data = Input::all(), Recievinglog2015::$rules);

		if ($validator->fails()) {
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Recievinglog2015::create($data);
		Session::flash('message', 'Your Receiving Log Was Created');
		Session::flash('alert-class', 'alert-success');
		return Redirect::route('recievinglog2015s.index');
	}

	/**
	 * Display the specified Recievinglog2015.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) {
		$recievinglog2015 = Recievinglog2015::findOrFail($id);

		return View::make('recievinglog2015s.show', compact('recievinglog2015'));
	}

	/**
	 * Show the form for editing the specified Recievinglog2015.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id) {
		$recievinglog2015 = Recievinglog2015::find($id);

		return View::make('recievinglog2015s.edit', compact('recievinglog2015'));
	}

	/**
	 * Update the specified Recievinglog2015 in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id) {
		$recievinglog2015 = Recievinglog2015::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Recievinglog2015::$rules);

		if ($validator->fails()) {
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$recievinglog2015->update($data);
		Session::flash('message', 'Your Receiving Log Was Updated Successfully');
		Session::flash('alert-class', 'alert-success');
		return Redirect::route('recievinglog2015s.index');
	}

	/**
	 * Remove the specified Recievinglog2015 from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		Recievinglog2015::destroy($id);
		Session::flash('message', 'Your Receiving Log Was Deleted');
		Session::flash('alert-class', 'alert-danger');
		return Redirect::route('recievinglog2015s.index');
	}

	/**
	 * Print all of Recievinglog2015 from storage
	 *
	 * @return Response
	 */

	public function print_page() {
		$receivinglogs = Recievinglog2015::all();

		return View::make('recievinglog2015s.print', compact('receivinglogs'));
	}

}
