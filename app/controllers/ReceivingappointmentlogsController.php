<?php

class ReceivingappointmentlogsController extends \BaseController {

	/**
	 * Display a listing of receivingappointmentlogs
	 *
	 * @return Response
	 */
	public function index() {

		//current year
		// $current_year = '%' . Date('y');

		// $receivingappointmentlogs = DB::table('receivingappointmentlogs')
		// 	->where('date' , 'LIKE' , $current_year)
		// 	->orderBy('date', 'desc')
		// 	->get();

		$receivingappointmentlogs = Receivingappointmentlog::all();

		return View::make('receivingappointmentlogs.index', compact('receivingappointmentlogs'));
	}

	/**
	 * if receiving appointment log is not active
	 *
	 * @return Response
	 */

	public function received() {

		$receivingappointmentlogs = DB::table('receivingappointmentlogs')
			->orderBy('id', 'desc')
			->get();

		return View::make('receivingappointmentlogs.received', compact('receivingappointmentlogs'));
	}

	/**
	 * Show the form for creating a new receivingappointmentlog
	 *
	 * @return Response
	 */
	public function create() {
		return View::make('receivingappointmentlogs.create');
	}

	/**
	 * Store a newly created receivingappointmentlog in storage.
	 *
	 * @return Response
	 */
	public function store() {
		$validator = Validator::make($data = Input::all(), Receivingappointmentlog::$rules);

		if ($validator->fails()) {
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Receivingappointmentlog::create($data);
		Session::flash('message', 'Your Receiving Appointment Log Was Created');
		Session::flash('alert-class', 'alert-success');
		return Redirect::route('receivingappointmentlogs.index');
	}

	/**
	 * Display the specified receivingappointmentlog.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) {
		$receivingappointmentlog = Receivingappointmentlog::findOrFail($id);

		return View::make('receivingappointmentlogs.show', compact('receivingappointmentlog'));
	}

	/**
	 * Show the form for editing the specified receivingappointmentlog.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id) {
		$receivingappointmentlog = Receivingappointmentlog::find($id);

		return View::make('receivingappointmentlogs.edit', compact('receivingappointmentlog'));
	}

	/**
	 * Update the specified receivingappointmentlog in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id) {
		$receivingappointmentlog = Receivingappointmentlog::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Receivingappointmentlog::$rules);

		if ($validator->fails()) {
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$receivingappointmentlog->update($data);
		Session::flash('message', 'Your Receiving Appointment Log Was Updated Successfully');
		Session::flash('alert-class', 'alert-success');
		return Redirect::route('receivingappointmentlogs.index');
	}

	/**
	 * Remove the specified receivingappointmentlog from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		Receivingappointmentlog::destroy($id);
		Session::flash('message', 'Your Receiving Appointment Log Was Deleted');
		Session::flash('alert-class', 'alert-danger');
		return Redirect::route('receivingappointmentlogs.index');
	}

	/*
	 * Warehouse view
	 *
	 * @return Response
	 *
	 */

	public function warehouse() {
		$receivingappointmentlogs = Receivingappointmentlog::all();
		return View::make('receivingappointmentlogs.warehouse', compact('receivingappointmentlogs'));
	}

}
