<?php

class SpecsheetsController extends \BaseController {

	/**
	 * Display a listing of specsheets
	 *
	 * @return Response
	 */
	public function index() {
		$specsheets = Specsheet::all();

		return View::make('specsheets.index', compact('specsheets'));
	}

	/**
	 * Show the form for creating a new specsheet
	 *
	 * @return Response
	 */
	public function create() {
		return View::make('specsheets.create');
	}

	/**
	 * Store a newly created specsheet in storage.
	 *
	 * @return Response
	 */
	public function store() {

		//get the values
		$data = new Specsheet();

		//file upload
		if (Input::hasFile('nutrition')) {
			$file = Input::file('nutrition');

			//production
			// $destinationPath = '/images/nutrition/';
			//local
			$destinationPath = '/images/nutrition/';
			$filename = rand() . $file->getClientOriginalName();

			//production
			// $success = Input::file('nutrition')->move("../public_html" . $destinationPath, $filename);
			//local
			$success = Input::file('nutrition')->move(public_path() . $destinationPath, $filename);

			if ($success) {
				$data->nutrition = $filename;
			}
		}

		//$_GET
		$data->title = Input::get('title');
		$data->sku = Input::get('sku');
		$data->revised = Input::get('revised');
		$data->description = Input::get('description');
		$data->bullet_point1 = Input::get('bullet_point1');
		$data->bullet_point2 = Input::get('bullet_point2');
		$data->bullet_point3 = Input::get('bullet_point3');
		$data->bullet_point4 = Input::get('bullet_point4');
		$data->bullet_point5 = Input::get('bullet_point5');
		$data->bullet_point6 = Input::get('bullet_point6');
		$data->bullet_point7 = Input::get('bullet_point7');
		$data->bullet_point8 = Input::get('bullet_point8');
		$data->storage = Input::get('storage');
		$data->shelf_life = Input::get('shelf_life');
		$data->sewer = Input::get('sewer');
		$data->applications = Input::get('applications');
		$data->country = Input::get('country');
		$data->nutrition_fact1 = Input::get('nutrition_fact1');
		$data->nutrition_fact2 = Input::get('nutrition_fact2');
		$data->nutrition_fact3 = Input::get('nutrition_fact3');
		$data->certifications_cas = Input::get('certifications_cas');
		$data->certifications_einecs = Input::get('certifications_einecs');
		$data->certifications_incl = Input::get('certifications_incl');
		$data->certifications_notes = Input::get('certifications_notes');
        $data->typical_additives = Input::get('typical_additives');
        $data->typical_max = Input::get('typical_max');
		$data->typical_acidity = Input::get('typical_acidity');
		$data->typical_270 = Input::get('typical_270');
		$data->typical_delta = Input::get('typical_delta');
		$data->typical_270after = Input::get('typical_270after');
		$data->typical_passage = Input::get('typical_passage');
		$data->typical_peroxide = Input::get('typical_peroxide');
		$data->typical_gravity = Input::get('typical_gravity');
		$data->typical_myristic = Input::get('typical_myristic');
		$data->typical_palmitic = Input::get('typical_palmitic');
		$data->typical_palmitoleic = Input::get('typical_palmitoleic');
		$data->typical_heptadecanoic = Input::get('typical_heptadecanoic');
		$data->typical_heptadecenoic = Input::get('typical_heptadecenoic');
		$data->typical_stearic = Input::get('typical_stearic');
		$data->typical_oleic = Input::get('typical_oleic');
		$data->typical_linoleic = Input::get('typical_linoleic');
		$data->typical_linolenic = Input::get('typical_linolenic');
		$data->typical_arachidic = Input::get('typical_arachidic');
		$data->typical_gadoleic = Input::get('typical_gadoleic');
		$data->typical_palmitic = Input::get('typical_palmitic');
		$data->typical_behenic = Input::get('typical_behenic');
		$data->typical_erucic = Input::get('typical_erucic');
		$data->typical_lignoceric = Input::get('typical_lignoceric');
		// 'energy_unit' => Input::get('energy_unit');
		// 'water_unit' => Input::get('water_unit');
		$data->water_value = Input::get('water_value');
		$data->water_tbsp = Input::get('water_tbsp');
		$data->energy_value = Input::get('energy_value');
		$data->energy_tbsp = Input::get('energy_tbsp');
		// 'protein_unit' => Input::get('protein_unit');
		$data->protein_value = Input::get('protein_value');
		$data->protein_tbsp = Input::get('protein_tbsp');
		// 'totallipid_unit' => Input::get('totallipid_unit');
		$data->totallipid_value = Input::get('totallipid_value');
		$data->totallipid_tbsp = Input::get('totallipid_tbsp');
		// 'carbohydrate_unit' => Input::get('carbohydrate_unit');
		$data->carbohydrate_value = Input::get('carbohydrate_value');
		$data->carbohydrate_tbsp = Input::get('carbohydrate_tbsp');
		// 'fiber_unit' => Input::get('fiber_unit');
		$data->fiber_value = Input::get('fiber_value');
		$data->fiber_tbsp = Input::get('fiber_tbsp');
		// 'sugars_unit' => Input::get('sugars_unit');
		$data->sugars_value = Input::get('sugars_value');
		$data->sugars_tbsp = Input::get('sugars_tbsp');
		// 'calcium_unit' => Input::get('calcium_unit');
		$data->calcium_value = Input::get('calcium_value');
		$data->calcium_tbsp = Input::get('calcium_tbsp');
		// 'iron_unit' => Input::get('iron_unit');
		$data->iron_value = Input::get('iron_value');
		$data->iron_tbsp = Input::get('iron_tbsp');
		// 'magnesium_unit' => Input::get('magnesium_unit');
		$data->magnesium_value = Input::get('magnesium_value');
		$data->magnesium_tbsp = Input::get('magnesium_tbsp');
		// 'phosphorus_unit' => Input::get('phosphorus_unit');
		$data->phosphorus_value = Input::get('phosphorus_value');
		$data->phosphorus_tbsp = Input::get('phosphorus_tbsp');
		// 'potassium_unit' => Input::get('potassium_unit');
		$data->potassium_value = Input::get('potassium_value');
		$data->potassium_tbsp = Input::get('potassium_tbsp');
		// 'sodium_unit' => Input::get('sodium_unit');
		$data->sodium_value = Input::get('sodium_value');
		$data->sodium_tbsp = Input::get('sodium_tbsp');
		// 'zinc_unit' => Input::get('zinc_unit');
		$data->zinc_value = Input::get('zinc_value');
		$data->zinc_tbsp = Input::get('zinc_tbsp');
		// 'vitaminc_unit' => Input::get('vitaminc_unit');
		$data->vitaminc_value = Input::get('vitaminc_value');
		$data->vitaminc_tbsp = Input::get('vitaminc_tbsp');
		// 'thiamin_unit' => Iput::get('thiamin_unit');
		$data->thiamin_value = Input::get('thiamin_value');
		$data->thiamin_tbsp = Input::get('thiamin_tbsp');
		// 'riboflavin_unit' => Input::get('riboflavin_unit');
		$data->riboflavin_value = Input::get('riboflavin_value');
		$data->riboflavin_tbsp = Input::get('riboflavin_tbsp');
		// 'niacin_unit' => Input::get('niacin_unit');
		$data->niacin_value = Input::get('niacin_value');
		$data->niacin_tbsp = Input::get('niacin_tbsp');
		$data->riboflavin_tbsp = Input::get('riboflavin_tbsp');
		// 'vitaminb6_unit' => Input::get('vitaminb6_unit');
		$data->vitaminb6_value = Input::get('vitaminb6_value');
		$data->vitaminb6_tbsp = Input::get('vitaminb6_tbsp');
		// 'folate_unit' => Input::get('folate_unit');
		$data->folate_value = Input::get('folate_value');
		$data->folate_tbsp = Input::get('folate_tbsp');
		// 'vitaminb12_unit' => Input::get('vitaminb12_unit');
		$data->vitaminb12_value = Input::get('vitaminb12_value');
		$data->vitaminb12_tbsp = Input::get('vitaminb12_tbsp');
		// 'vitaminar_unit' => Input::get('vitaminar_unit');
		$data->vitaminar_value = Input::get('vitaminar_value');
		$data->vitaminar_tbsp = Input::get('vitaminar_tbsp');
		// 'vitaminai_unit' => Input::get('vitaminai_unit');
		$data->vitaminai_value = Input::get('vitaminai_value');
		$data->vitaminai_tbsp = Input::get('vitaminai_tbsp');
		// 'vitamine_unit' => Input::get('vitamine_unit');
		$data->vitamine_value = Input::get('vitamine_value');
		$data->vitamine_tbsp = Input::get('vitamine_tbsp');
		// 'vitamind2_unit' => Input::get('vitamind2_unit');
		$data->vitamind2_value = Input::get('vitamind2_value');
		$data->vitamind2_tbsp = Input::get('vitamind2_tbsp');
		// 'vitamind_unit' => Input::get('vitamind_unit');
		$data->vitamind_value = Input::get('vitamind_value');
		$data->vitamind_tbsp = Input::get('vitamind_tbsp');
		// 'vitamink_unit' => Input::get('vitamink_unit');
		$data->vitamink_value = Input::get('vitamink_value');
		$data->vitamink_tbsp = Input::get('vitamink_tbsp');
		// 'fattysaturated_unit' => Input::get('fattysaturated_unit');
		$data->fattysaturated_value = Input::get('fattysaturated_value');
		$data->fattysaturated_tbsp = Input::get('fattysaturated_tbsp');
		// 'fattymonoun_unit' => Input::get('fattymonoun_unit');
		$data->fattymonoun_value = Input::get('fattymonoun_value');
		$data->fattymonoun_tbsp = Input::get('fattymonoun_tbsp');
		// 'fattypoly_unit' => Input::get('fattypoly_unit');
		$data->fattypoly_value = Input::get('fattypoly_value');
		$data->fattypoly_tbsp = Input::get('fattypoly_tbsp');
		// 'cholesterol_unit' => Input::get('cholesterol_unit');
		$data->cholesterol_value = Input::get('cholesterol_value');
		$data->cholesterol_tbsp = Input::get('cholesterol_tbsp');
		// 'caffeine_unit' => Input::get('caffeine_unit');
		$data->caffeine_value = Input::get('caffeine_value');
		$data->caffeine_tbsp = Input::get('caffeine_tbsp');
//                    'msds_tlv' => Input::get('msds_tlv');
		//                    'msds_flash_point' => Input::get('msds_flash_point');
		//                    'msds_ph' => Input::get('msds_ph');
		//                    'msds_vapor_density' => Input::get('msds_vapor_density');
		//                    'msds_solubility_water' => Input::get('msds_solubility_water');
		//                    'msds_appearance_odor' => Input::get('msds_appearance_odor');
		//                    'msds_gravity' => Input::get('msds_gravity');
		//                    'msds_percentage_volatile' => Input::get('msds_percentage_volatile');
		//                    'msds_vapor_pressure' => Input::get('msds_vapor_pressure');
		//                    'msds_flame_extension' => Input::get('msds_flame_extension');
		//                    'msds_fire_explosion_hazards' => Input::get('msds_fire_explosion_hazards');
		//                    'msds_precautions' => Input::get('msds_precautions');
		//                    'msds_extinguishing_media' => Input::get('msds_extinguishing_media');
		//                    'msds_threshold_limit' => Input::get('msds_threshold_limit');
		//                    'msds_eo_ingestion' => Input::get('msds_eo_ingestion');
		//                    'msds_eo_eyes' => Input::get('msds_eo_eyes');
		//                    'msds_eo_inhalation' => Input::get('msds_eo_inhalation');
		//                    'msds_eo_skin' => Input::get('msds_eo_skin');
		//                    'msds_ep_ingestion' => Input::get('msds_ep_ingestion');
		//                    'msds_ep_eyes' => Input::get('msds_ep_eyes');
		//                    'msds_ep_inhalation' => Input::get('msds_ep_inhalation');
		//                    'msds_ep_skin' => Input::get('msds_ep_skin');
		//                    'msds_conditions_avoid' => Input::get('msds_conditions_avoid');
		//                    'msds_large_amounts_released' => Input::get('msds_large_amounts_released');
		//                    'msds_waste_disposal_method' => Input::get('msds_waste_disposal_method');
		//                    'msds_respiratory' => Input::get('msds_respiratory');
		//                    'msds_ventilation' => Input::get('msds_ventilation');
		//                    'msds_mechanical' => Input::get('msds_mechanical');
		//                    'msds_protective_gloves' => Input::get('msds_protective_gloves');
		//                    'msds_other_protective_equipment' => Input::get('msds_other_protective_equipment');
		//                    'msds_precautions_storage' => Input::get('msds_precautions_storage');
		//                    'msds_other_precautions' => Input::get('msds_other_precautions');
		//                    'msds_trade_name' => Input::get('msds_trade_name');
		//                    'msds_chemical_name' => Input::get('msds_chemical_name');
		//                    'msds_hazard' => Input::get('msds_hazard');
		//                    'msds_weight' => Input::get('msds_weight');
		//                    'stability_reactivity1' => Input::get('stability_reactivity1');
		$data->stability_reactivity2 = Input::get('stability_reactivity2');
		$data->stability_reactivity3 = Input::get('stability_reactivity3');
		$data->stability_reactivity4 = Input::get('stability_reactivity4');
		$data->stability_reactivity5 = Input::get('stability_reactivity5');
		$data->stability_reactivity6 = Input::get('stability_reactivity6');
		$data->disposal_considerations1 = Input::get('disposal_considerations1');
		$data->disposal_considerations2 = Input::get('disposal_considerations2');
		$data->disposal_considerations3 = Input::get('disposal_considerations3');
		$data->disposal_considerations4 = Input::get('disposal_considerations4');
		$data->disposal_considerations5 = Input::get('disposal_considerations5');
		$data->disposal_considerations6 = Input::get('disposal_considerations6');
		$data->transport_info1 = Input::get('transport_info1');
		$data->transport_info2 = Input::get('transport_info2');
		$data->transport_info3 = Input::get('transport_info3');
		$data->transport_info4 = Input::get('transport_info4');
		$data->transport_info5 = Input::get('transport_info5');
		$data->transport_info6 = Input::get('transport_info6');
		$data->tox_info1 = Input::get('tox_info1');
		$data->tox_info2 = Input::get('tox_info2');
		$data->tox_info3 = Input::get('tox_info3');
		$data->tox_info4 = Input::get('tox_info4');
		$data->tox_info5 = Input::get('tox_info5');
		$data->tox_info6 = Input::get('tox_info6');
		$data->eco_info1 = Input::get('eco_info1');
		$data->eco_info2 = Input::get('eco_info2');
		$data->eco_info3 = Input::get('eco_info3');
		$data->eco_info4 = Input::get('eco_info4');
		$data->eco_info5 = Input::get('eco_info5');
		$data->eco_info6 = Input::get('eco_info6');
		$data->tox_info1 = Input::get('tox_info1');
		$data->tox_info2 = Input::get('tox_info2');
		$data->tox_info3 = Input::get('tox_info3');
		$data->tox_info4 = Input::get('tox_info4');
		$data->tox_info5 = Input::get('tox_info5');
		$data->tox_info6 = Input::get('tox_info6');
		$data->reg_info1 = Input::get('reg_info1');
		$data->reg_info2 = Input::get('reg_info2');
		$data->reg_info3 = Input::get('reg_info3');
		$data->reg_info4 = Input::get('reg_info4');
		$data->reg_info5 = Input::get('reg_info5');
		$data->reg_info6 = Input::get('reg_info6');
		$data->other_info1 = Input::get('other_info1');
		$data->other_info2 = Input::get('other_info2');
		$data->other_info3 = Input::get('other_info3');
		$data->other_info4 = Input::get('other_info4');
		$data->other_info5 = Input::get('other_info5');
		$data->other_info6 = Input::get('other_info6');
		$data->stability_and_reactivity = Input::get('stability_and_reactivity');
		$data->toxilogical_information = Input::get('toxilogical_information');
		$data->disposal_considerations = Input::get('disposal_considerations');
		$data->ecological_information = Input::get('ecological_information');
		$data->transport_information = Input::get('transport_information');
		$data->regulatory_information = Input::get('regulatory_information');
		$data->other_information = Input::get('other_information');
		$data->isdone = Input::get('isdone');
		$data->vinegar_acidity = Input::get('vinegar_acidity');
		$data->vinegar_dry = Input::get('vinegar_dry');
		$data->vinegar_gravity = Input::get('vinegar_gravity');
		$data->vinegar_density = Input::get('vinegar_density');
		$data->vinegar_ph = Input::get('vinegar_ph');
		$data->vinegar_sulphurous = Input::get('vinegar_sulphurous');
		$data->vinegar_metals = Input::get('vinegar_metals');
		$data->vinegar_matters = Input::get('vinegar_matters');
		$data->vinegar_color = Input::get('vinegar_color');
		$data->vinegar_flavor = Input::get('vinegar_flavor');
		$data->vinegar_odour = Input::get('vinegar_odour');
		$data->vinegar_appearance = Input::get('vinegar_appearance');
		$data->vinegar_microbiological = Input::get('vinegar_microbiological');
		$data->vinegar_microbiological1 = Input::get('vinegar_microbiological1');
		$data->is_oil_vinegar = Input::get('is_oil_vinegar');
		$data->organoleptic_appearance = Input::get('organoleptic_appearance');
		$data->organoleptic_flavor = Input::get('organoleptic_flavor');
		$data->organoleptic_color_red = Input::get('organoleptic_color_red');
		$data->organoleptic_color_yellow = Input::get('organoleptic_color_yellow');
		$data->typical_fatty_acid = Input::get('typical_fatty_acid');
		$data->typical_moisture = Input::get('typical_moisture');
		$data->typical_peroxide = Input::get('typical_peroxide');
		$data->typical_iodine = Input::get('typical_iodine');
		$data->typical_saponification = Input::get('typical_saponification');
		$data->typical_anisidine = Input::get('typical_anisidine');
		$data->typical_cold = Input::get('typical_cold');
		$data->typical_refractive = Input::get('typical_refractive');
		$data->typical_gravity = Input::get('typical_gravity');
		$data->typical_stability = Input::get('typical_stability');
		$data->typical_smoke = Input::get('typical_smoke');
		$data->typical_moisture2 = Input::get('typical_moisture2');
		$data->developed_alcohol_degree = Input::get('developed_alcohol_degree');
		$data->reduced_dry_extract = Input::get('reduced_dry_extract');
		$data->ashes = Input::get('ashes');
		$data->reducing_sugars = Input::get('reducing_sugars');
		$data->antioxidant = Input::get('antioxidant');
		$data->vinegar_nutrition_fat_100g = Input::get('vinegar_nutrition_fat_100g');
		$data->vinegar_nutrition_fat_100ml = Input::get('vinegar_nutrition_fat_100ml');
		$data->vinegar_nutrition_sodium_100g = Input::get('vinegar_nutrition_sodium_100g');
		$data->vinegar_nutrition_sodium_100ml = Input::get('vinegar_nutrition_sodium_100ml');
		$data->vinegar_nutrition_carb_100g = Input::get('vinegar_nutrition_carb_100g');
		$data->vinegar_nutrition_carb_100ml = Input::get('vinegar_nutrition_carb_100ml');
		$data->vinegar_nutrition_sugars_100g = Input::get('vinegar_nutrition_sugars_100g');
		$data->vinegar_nutrition_sugars_100ml = Input::get('vinegar_nutrition_sugars_100ml');
		$data->vinegar_nutrition_protein_100g = Input::get('vinegar_nutrition_protein_100g');
		$data->vinegar_nutrition_protein_100ml = Input::get('vinegar_nutrition_protein_100ml');
		$data->vinegar_nutrition_kcal_100g = Input::get('vinegar_nutrition_kcal_100g');
		$data->vinegar_nutrition_kcal_100ml = Input::get('vinegar_nutrition_kcal_100ml');
		$data->vinegar_nutrition_calories_100g = Input::get('vinegar_nutrition_calories_100g');
		$data->vinegar_nutrition_calories_100ml = Input::get('vinegar_nutrition_calories_100ml');
		$data->vinegar_nutrition_vitamin_a_100g = Input::get('vinegar_nutrition_vitamin_a_100g');
		$data->vinegar_nutrition_vitamin_a_100ml = Input::get('vinegar_nutrition_vitamin_a_100ml');
		$data->vinegar_nutrition_vitamin_c_100g = Input::get('vinegar_nutrition_vitamin_c_100g');
		$data->vinegar_nutrition_vitamin_c_100ml = Input::get('vinegar_nutrition_vitamin_c_100ml');
		$data->vinegar_nutrition_calcium_100g = Input::get('vinegar_nutrition_calcium_100g');
		$data->vinegar_nutrition_calcium_100ml = Input::get('vinegar_nutrition_calcium_100ml');
		$data->vinegar_nutrition_iron_100g = Input::get('vinegar_nutrition_iron_100g');
		$data->vinegar_nutrition_iron_100ml = Input::get('vinegar_nutrition_iron_100ml');
		$data->vinegar_nutrition_cholesterol_100g = Input::get('vinegar_nutrition_cholesterol_100g');
		$data->vinegar_nutrition_cholesterol_100ml = Input::get('vinegar_nutrition_cholesterol_100ml');
		$data->vinegar_nutrition_fiber_100g = Input::get('vinegar_nutrition_fiber_100g');
		$data->vinegar_nutrition_fiber_100ml = Input::get('vinegar_nutrition_fiber_100ml');
		$data->vinegar_nutrition_moisture_100g = Input::get('vinegar_nutrition_moisture_100g');
		$data->vinegar_nutrition_moisture_100ml = Input::get('vinegar_nutrition_moisture_100ml');
		$data->vinegar_nutrition_calories_fat_100g = Input::get('vinegar_nutrition_calories_fat_100g');
		$data->vinegar_nutrition_calories_fat_100ml = Input::get('vinegar_nutrition_calories_fat_100ml');
		$data->organoleptic_color = Input::get('organoleptic_color');
		$data->organoleptic_odor = Input::get('organoleptic_odor');
		$data->organoleptic_flavor = Input::get('organoleptic_flavor');
		$data->organoleptic_appearance = Input::get('organoleptic_appearance');
		$data->chemical_acidity = Input::get('chemical_acidity');
		$data->chemical_ph = Input::get('chemical_ph');
		$data->chemical_gravity = Input::get('chemical_gravity');
		$data->chemical_matters = Input::get('chemical_matters');
		$data->chemical_metals = Input::get('chemical_metals');
		$data->chemical_extract = Input::get('chemical_extract');
		$data->chemical_anhydride = Input::get('chemical_anhydride');
		$data->chemical_ash = Input::get('chemical_ash');
		$data->chemical_grain = Input::get('chemical_grain');
		$data->chemical_alcohol = Input::get('chemical_alcohol');
		$data->chemical_sugar_free_extract = Input::get('chemical_sugar_free_extract');
		$data->chemical_density = Input::get('chemical_density');
		$data->chemical_brix = Input::get('chemical_brix');
		$data->chemical_alcohol_degree = Input::get('chemical_alcohol_degree');
		$data->chemical_reduced_dry_extract = Input::get('chemical_reduced_dry_extract');
		$data->chemical_sugars = Input::get('chemical_sugars');
		$data->chemical_rif = Input::get('chemical_rif');
		$data->other_product_info1 = Input::get('other_product_info1');
		$data->other_product_info2 = Input::get('other_product_info2');
		$data->other_product_info3 = Input::get('other_product_info3');
		$data->other_product_info4 = Input::get('other_product_info4');
		$data->other_product_info5 = Input::get('other_product_info5');
		$data->other_product_info6 = Input::get('other_product_info6');
		$data->under_allergen = Input::get('under_allergen');
		$data->website = Input::get('website');
		$data->prop_65_statement = Input::get('prop_65_statement');
	//sds
			$data->sds_active = Input::get('sds_active');
			$data->sds_name = Input::get('sds_name');
			$data->sds_special_hazard_for_personal = Input::get('sds_special_hazard_for_personal');
			$data->sds_cas_number = Input::get('sds_cas_number');
			$data->sds_upon_inhalation = Input::get('sds_upon_inhalation');
			$data->sds_upon_skin_contact = Input::get('sds_upon_skin_contact');
			$data->sds_extinguishing_media = Input::get('sds_extinguishing_media');
			$data->sds_not_to_be_used_extinguishing_media = Input::get('sds_not_to_be_used_extinguishing_media');
			$data->sds_special_exposure_hazard = Input::get('sds_special_exposure_hazard');
			$data->sds_special_protection = Input::get('sds_special_protection');
			$data->sds_precautions_related_to_personnel = Input::get('sds_precautions_related_to_personnel');
			$data->sds_procedure_for_cleanup = Input::get('sds_procedure_for_cleanup');
			$data->sds_small_spill = Input::get('sds_small_spill');
			$data->sds_large_spill = Input::get('sds_large_spill');
			$data->sds_handling = Input::get('sds_handling');
			$data->sds_storage = Input::get('sds_storage');
			$data->sds_threshold_limit_value = Input::get('sds_threshold_limit_value');
			$data->sds_inhalation_health_risks = Input::get('sds_inhalation_health_risks');
			$data->sds_skin_absorption = Input::get('sds_skin_absorption');
			$data->sds_appearance = Input::get('sds_appearance');
			$data->sds_form = Input::get('sds_form');
			$data->sds_color = Input::get('sds_color');
			$data->sds_odor = Input::get('sds_odor');
			$data->sds_melting_point = Input::get('sds_melting_point');
			$data->sds_flash_point = Input::get('sds_flash_point');
			$data->sds_ignition_temperature = Input::get('sds_ignition_temperature');
			$data->sds_density = Input::get('sds_density');
			$data->sds_water_solubility = Input::get('sds_water_solubility');
			$data->sds_general_information = Input::get('sds_general_information');
			$data->sds_conditions_to_be_avoided = Input::get('sds_conditions_to_be_avoided');
			$data->sds_dangerous_decomposition_product = Input::get('sds_dangerous_decomposition_product');
			$data->sds_toxicological_testing = Input::get('sds_toxicological_testing');
			$data->sds_practical_experience = Input::get('sds_practical_experience');
			$data->sds_general_considerations = Input::get('sds_general_considerations');
			$data->sds_information_on_product = Input::get('sds_information_on_product');
			$data->sds_behavior_in_ecological_setting = Input::get('sds_behavior_in_ecological_setting');
			$data->sds_toxic_effects_on_the_ecosystem = Input::get('sds_toxic_effects_on_the_ecosystem');
			$data->sds_additional_ecological_information = Input::get('sds_additional_ecological_information');
			$data->sds_disposal_considerations = Input::get('sds_disposal_considerations');
			$data->sds_transportation_information = Input::get('sds_transportation_information');
			$data->sds_regulatory_information = Input::get('sds_regulatory_information');

		

		$data->save();


		Session::flash('message', 'Your Specsheet Was Created');
		Session::flash('alert-class', 'alert-success');
		return Redirect::route('specsheets.index');
	}

	/**
	 * Display the specified specsheet.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) {
		$specsheet = Specsheet::findOrFail($id);
        //sds
        $sds = $specsheet->sds;
		return View::make('specsheets.show', compact(['specsheet', 'sds']));
	}

	/**
	 * Show the form for editing the specified specsheet.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id) {
		$specsheet = Specsheet::find($id);

		return View::make('specsheets.edit', compact('specsheet'));
	}

	/**
	 * Update the specified specsheet in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id) {
		$data = Specsheet::findOrFail($id);

		if (Input::hasFile('nutrition')) {
			//file upload
			$file = Input::file('nutrition');
			//production
			// $destinationPath = '/images/nutrition/';
			//local
			$destinationPath = '/images/nutrition/';
			$filename = rand() . $file->getClientOriginalName();

			//production
			// $success = Input::file('nutrition')->move("../public_html" . $destinationPath, $filename);
			//local
			$success = Input::file('nutrition')->move(public_path() . $destinationPath, $filename);
			if ($success) {
				$data->nutrition = $filename;
			}
		}

		//$_GET
		$data->title = Input::get('title');
		$data->sku = Input::get('sku');
		$data->revised = Input::get('revised');
		$data->description = Input::get('description');
		$data->bullet_point1 = Input::get('bullet_point1');
		$data->bullet_point2 = Input::get('bullet_point2');
		$data->bullet_point3 = Input::get('bullet_point3');
		$data->bullet_point4 = Input::get('bullet_point4');
		$data->bullet_point5 = Input::get('bullet_point5');
		$data->bullet_point6 = Input::get('bullet_point6');
		$data->bullet_point7 = Input::get('bullet_point7');
		$data->bullet_point8 = Input::get('bullet_point8');
		$data->storage = Input::get('storage');
		$data->shelf_life = Input::get('shelf_life');
		$data->sewer = Input::get('sewer');
		$data->applications = Input::get('applications');
		$data->country = Input::get('country');
		$data->nutrition_fact1 = Input::get('nutrition_fact1');
		$data->nutrition_fact2 = Input::get('nutrition_fact2');
		$data->nutrition_fact3 = Input::get('nutrition_fact3');
		$data->certifications_cas = Input::get('certifications_cas');
		$data->certifications_einecs = Input::get('certifications_einecs');
		$data->certifications_incl = Input::get('certifications_incl');
		$data->certifications_notes = Input::get('certifications_notes');
		$data->typical_max = Input::get('typical_max');
		$data->typical_acidity = Input::get('typical_acidity');
        $data->typical_additives = Input::get('typical_additives');
		$data->typical_270 = Input::get('typical_270');
		$data->typical_delta = Input::get('typical_delta');
		$data->typical_270after = Input::get('typical_270after');
		$data->typical_passage = Input::get('typical_passage');
		$data->typical_peroxide = Input::get('typical_peroxide');
		$data->typical_gravity = Input::get('typical_gravity');
		$data->typical_myristic = Input::get('typical_myristic');
		$data->typical_palmitic = Input::get('typical_palmitic');
		$data->typical_palmitoleic = Input::get('typical_palmitoleic');
		$data->typical_heptadecanoic = Input::get('typical_heptadecanoic');
		$data->typical_heptadecenoic = Input::get('typical_heptadecenoic');
		$data->typical_stearic = Input::get('typical_stearic');
		$data->typical_oleic = Input::get('typical_oleic');
		$data->typical_linoleic = Input::get('typical_linoleic');
		$data->typical_linolenic = Input::get('typical_linolenic');
		$data->typical_arachidic = Input::get('typical_arachidic');
		$data->typical_gadoleic = Input::get('typical_gadoleic');
		$data->typical_palmitic = Input::get('typical_palmitic');
		$data->typical_behenic = Input::get('typical_behenic');
		$data->typical_erucic = Input::get('typical_erucic');
		$data->typical_lignoceric = Input::get('typical_lignoceric');
		// 'energy_unit' => Input::get('energy_unit');
		// 'water_unit' => Input::get('water_unit');
		$data->water_value = Input::get('water_value');
		$data->water_tbsp = Input::get('water_tbsp');
		$data->energy_value = Input::get('energy_value');
		$data->energy_tbsp = Input::get('energy_tbsp');
		// 'protein_unit' => Input::get('protein_unit');
		$data->protein_value = Input::get('protein_value');
		$data->protein_tbsp = Input::get('protein_tbsp');
		// 'totallipid_unit' => Input::get('totallipid_unit');
		$data->totallipid_value = Input::get('totallipid_value');
		$data->totallipid_tbsp = Input::get('totallipid_tbsp');
		// 'carbohydrate_unit' => Input::get('carbohydrate_unit');
		$data->carbohydrate_value = Input::get('carbohydrate_value');
		$data->carbohydrate_tbsp = Input::get('carbohydrate_tbsp');
		// 'fiber_unit' => Input::get('fiber_unit');
		$data->fiber_value = Input::get('fiber_value');
		$data->fiber_tbsp = Input::get('fiber_tbsp');
		// 'sugars_unit' => Input::get('sugars_unit');
		$data->sugars_value = Input::get('sugars_value');
		$data->sugars_tbsp = Input::get('sugars_tbsp');
		// 'calcium_unit' => Input::get('calcium_unit');
		$data->calcium_value = Input::get('calcium_value');
		$data->calcium_tbsp = Input::get('calcium_tbsp');
		// 'iron_unit' => Input::get('iron_unit');
		$data->iron_value = Input::get('iron_value');
		$data->iron_tbsp = Input::get('iron_tbsp');
		// 'magnesium_unit' => Input::get('magnesium_unit');
		$data->magnesium_value = Input::get('magnesium_value');
		$data->magnesium_tbsp = Input::get('magnesium_tbsp');
		// 'phosphorus_unit' => Input::get('phosphorus_unit');
		$data->phosphorus_value = Input::get('phosphorus_value');
		$data->phosphorus_tbsp = Input::get('phosphorus_tbsp');
		// 'potassium_unit' => Input::get('potassium_unit');
		$data->potassium_value = Input::get('potassium_value');
		$data->potassium_tbsp = Input::get('potassium_tbsp');
		// 'sodium_unit' => Input::get('sodium_unit');
		$data->sodium_value = Input::get('sodium_value');
		$data->sodium_tbsp = Input::get('sodium_tbsp');
		// 'zinc_unit' => Input::get('zinc_unit');
		$data->zinc_value = Input::get('zinc_value');
		$data->zinc_tbsp = Input::get('zinc_tbsp');
		// 'vitaminc_unit' => Input::get('vitaminc_unit');
		$data->vitaminc_value = Input::get('vitaminc_value');
		$data->vitaminc_tbsp = Input::get('vitaminc_tbsp');
		// 'thiamin_unit' => Iput::get('thiamin_unit');
		$data->thiamin_value = Input::get('thiamin_value');
		$data->thiamin_tbsp = Input::get('thiamin_tbsp');
		// 'riboflavin_unit' => Input::get('riboflavin_unit');
		$data->riboflavin_value = Input::get('riboflavin_value');
		$data->riboflavin_tbsp = Input::get('riboflavin_tbsp');
		// 'niacin_unit' => Input::get('niacin_unit');
		$data->niacin_value = Input::get('niacin_value');
		$data->niacin_tbsp = Input::get('niacin_tbsp');
		$data->riboflavin_tbsp = Input::get('riboflavin_tbsp');
		// 'vitaminb6_unit' => Input::get('vitaminb6_unit');
		$data->vitaminb6_value = Input::get('vitaminb6_value');
		$data->vitaminb6_tbsp = Input::get('vitaminb6_tbsp');
		// 'folate_unit' => Input::get('folate_unit');
		$data->folate_value = Input::get('folate_value');
		$data->folate_tbsp = Input::get('folate_tbsp');
		// 'vitaminb12_unit' => Input::get('vitaminb12_unit');
		$data->vitaminb12_value = Input::get('vitaminb12_value');
		$data->vitaminb12_tbsp = Input::get('vitaminb12_tbsp');
		// 'vitaminar_unit' => Input::get('vitaminar_unit');
		$data->vitaminar_value = Input::get('vitaminar_value');
		$data->vitaminar_tbsp = Input::get('vitaminar_tbsp');
		// 'vitaminai_unit' => Input::get('vitaminai_unit');
		$data->vitaminai_value = Input::get('vitaminai_value');
		$data->vitaminai_tbsp = Input::get('vitaminai_tbsp');
		// 'vitamine_unit' => Input::get('vitamine_unit');
		$data->vitamine_value = Input::get('vitamine_value');
		$data->vitamine_tbsp = Input::get('vitamine_tbsp');
		// 'vitamind2_unit' => Input::get('vitamind2_unit');
		$data->vitamind2_value = Input::get('vitamind2_value');
		$data->vitamind2_tbsp = Input::get('vitamind2_tbsp');
		// 'vitamind_unit' => Input::get('vitamind_unit');
		$data->vitamind_value = Input::get('vitamind_value');
		$data->vitamind_tbsp = Input::get('vitamind_tbsp');
		// 'vitamink_unit' => Input::get('vitamink_unit');
		$data->vitamink_value = Input::get('vitamink_value');
		$data->vitamink_tbsp = Input::get('vitamink_tbsp');
		// 'fattysaturated_unit' => Input::get('fattysaturated_unit');
		$data->fattysaturated_value = Input::get('fattysaturated_value');
		$data->fattysaturated_tbsp = Input::get('fattysaturated_tbsp');
		// 'fattymonoun_unit' => Input::get('fattymonoun_unit');
		$data->fattymonoun_value = Input::get('fattymonoun_value');
		$data->fattymonoun_tbsp = Input::get('fattymonoun_tbsp');
		// 'fattypoly_unit' => Input::get('fattypoly_unit');
		$data->fattypoly_value = Input::get('fattypoly_value');
		$data->fattypoly_tbsp = Input::get('fattypoly_tbsp');
		// 'cholesterol_unit' => Input::get('cholesterol_unit');
		$data->cholesterol_value = Input::get('cholesterol_value');
		$data->cholesterol_tbsp = Input::get('cholesterol_tbsp');
		// 'caffeine_unit' => Input::get('caffeine_unit');
		$data->caffeine_value = Input::get('caffeine_value');
		$data->caffeine_tbsp = Input::get('caffeine_tbsp');
//                    'msds_tlv' => Input::get('msds_tlv');
		//                    'msds_flash_point' => Input::get('msds_flash_point');
		//                    'msds_ph' => Input::get('msds_ph');
		//                    'msds_vapor_density' => Input::get('msds_vapor_density');
		//                    'msds_solubility_water' => Input::get('msds_solubility_water');
		//                    'msds_appearance_odor' => Input::get('msds_appearance_odor');
		//                    'msds_gravity' => Input::get('msds_gravity');
		//                    'msds_percentage_volatile' => Input::get('msds_percentage_volatile');
		//                    'msds_vapor_pressure' => Input::get('msds_vapor_pressure');
		//                    'msds_flame_extension' => Input::get('msds_flame_extension');
		//                    'msds_fire_explosion_hazards' => Input::get('msds_fire_explosion_hazards');
		//                    'msds_precautions' => Input::get('msds_precautions');
		//                    'msds_extinguishing_media' => Input::get('msds_extinguishing_media');
		//                    'msds_threshold_limit' => Input::get('msds_threshold_limit');
		//                    'msds_eo_ingestion' => Input::get('msds_eo_ingestion');
		//                    'msds_eo_eyes' => Input::get('msds_eo_eyes');
		//                    'msds_eo_inhalation' => Input::get('msds_eo_inhalation');
		//                    'msds_eo_skin' => Input::get('msds_eo_skin');
		//                    'msds_ep_ingestion' => Input::get('msds_ep_ingestion');
		//                    'msds_ep_eyes' => Input::get('msds_ep_eyes');
		//                    'msds_ep_inhalation' => Input::get('msds_ep_inhalation');
		//                    'msds_ep_skin' => Input::get('msds_ep_skin');
		//                    'msds_conditions_avoid' => Input::get('msds_conditions_avoid');
		//                    'msds_large_amounts_released' => Input::get('msds_large_amounts_released');
		//                    'msds_waste_disposal_method' => Input::get('msds_waste_disposal_method');
		//                    'msds_respiratory' => Input::get('msds_respiratory');
		//                    'msds_ventilation' => Input::get('msds_ventilation');
		//                    'msds_mechanical' => Input::get('msds_mechanical');
		//                    'msds_protective_gloves' => Input::get('msds_protective_gloves');
		//                    'msds_other_protective_equipment' => Input::get('msds_other_protective_equipment');
		//                    'msds_precautions_storage' => Input::get('msds_precautions_storage');
		//                    'msds_other_precautions' => Input::get('msds_other_precautions');
		//                    'msds_trade_name' => Input::get('msds_trade_name');
		//                    'msds_chemical_name' => Input::get('msds_chemical_name');
		//                    'msds_hazard' => Input::get('msds_hazard');
		//                    'msds_weight' => Input::get('msds_weight');
		//                    'stability_reactivity1' => Input::get('stability_reactivity1');
		$data->stability_reactivity2 = Input::get('stability_reactivity2');
		$data->stability_reactivity3 = Input::get('stability_reactivity3');
		$data->stability_reactivity4 = Input::get('stability_reactivity4');
		$data->stability_reactivity5 = Input::get('stability_reactivity5');
		$data->stability_reactivity6 = Input::get('stability_reactivity6');
		$data->disposal_considerations1 = Input::get('disposal_considerations1');
		$data->disposal_considerations2 = Input::get('disposal_considerations2');
		$data->disposal_considerations3 = Input::get('disposal_considerations3');
		$data->disposal_considerations4 = Input::get('disposal_considerations4');
		$data->disposal_considerations5 = Input::get('disposal_considerations5');
		$data->disposal_considerations6 = Input::get('disposal_considerations6');
		$data->transport_info1 = Input::get('transport_info1');
		$data->transport_info2 = Input::get('transport_info2');
		$data->transport_info3 = Input::get('transport_info3');
		$data->transport_info4 = Input::get('transport_info4');
		$data->transport_info5 = Input::get('transport_info5');
		$data->transport_info6 = Input::get('transport_info6');
		$data->tox_info1 = Input::get('tox_info1');
		$data->tox_info2 = Input::get('tox_info2');
		$data->tox_info3 = Input::get('tox_info3');
		$data->tox_info4 = Input::get('tox_info4');
		$data->tox_info5 = Input::get('tox_info5');
		$data->tox_info6 = Input::get('tox_info6');
		$data->eco_info1 = Input::get('eco_info1');
		$data->eco_info2 = Input::get('eco_info2');
		$data->eco_info3 = Input::get('eco_info3');
		$data->eco_info4 = Input::get('eco_info4');
		$data->eco_info5 = Input::get('eco_info5');
		$data->eco_info6 = Input::get('eco_info6');
		$data->tox_info1 = Input::get('tox_info1');
		$data->tox_info2 = Input::get('tox_info2');
		$data->tox_info3 = Input::get('tox_info3');
		$data->tox_info4 = Input::get('tox_info4');
		$data->tox_info5 = Input::get('tox_info5');
		$data->tox_info6 = Input::get('tox_info6');
		$data->reg_info1 = Input::get('reg_info1');
		$data->reg_info2 = Input::get('reg_info2');
		$data->reg_info3 = Input::get('reg_info3');
		$data->reg_info4 = Input::get('reg_info4');
		$data->reg_info5 = Input::get('reg_info5');
		$data->reg_info6 = Input::get('reg_info6');
		$data->other_info1 = Input::get('other_info1');
		$data->other_info2 = Input::get('other_info2');
		$data->other_info3 = Input::get('other_info3');
		$data->other_info4 = Input::get('other_info4');
		$data->other_info5 = Input::get('other_info5');
		$data->other_info6 = Input::get('other_info6');
		$data->stability_and_reactivity = Input::get('stability_and_reactivity');
		$data->toxilogical_information = Input::get('toxilogical_information');
		$data->disposal_considerations = Input::get('disposal_considerations');
		$data->ecological_information = Input::get('ecological_information');
		$data->transport_information = Input::get('transport_information');
		$data->regulatory_information = Input::get('regulatory_information');
		$data->other_information = Input::get('other_information');
		$data->isdone = Input::get('isdone');
		$data->vinegar_acidity = Input::get('vinegar_acidity');
		$data->vinegar_dry = Input::get('vinegar_dry');
		$data->vinegar_gravity = Input::get('vinegar_gravity');
		$data->vinegar_density = Input::get('vinegar_density');
		$data->vinegar_ph = Input::get('vinegar_ph');
		$data->vinegar_sulphurous = Input::get('vinegar_sulphurous');
		$data->vinegar_metals = Input::get('vinegar_metals');
		$data->vinegar_matters = Input::get('vinegar_matters');
		$data->vinegar_color = Input::get('vinegar_color');
		$data->vinegar_flavor = Input::get('vinegar_flavor');
		$data->vinegar_odour = Input::get('vinegar_odour');
		$data->vinegar_appearance = Input::get('vinegar_appearance');
		$data->vinegar_microbiological = Input::get('vinegar_microbiological');
		$data->vinegar_microbiological1 = Input::get('vinegar_microbiological1');
		$data->is_oil_vinegar = Input::get('is_oil_vinegar');
		$data->organoleptic_appearance = Input::get('organoleptic_appearance');
		$data->organoleptic_flavor = Input::get('organoleptic_flavor');
		$data->organoleptic_color_red = Input::get('organoleptic_color_red');
		$data->organoleptic_color_yellow = Input::get('organoleptic_color_yellow');
		$data->typical_fatty_acid = Input::get('typical_fatty_acid');
		$data->typical_moisture = Input::get('typical_moisture');
		$data->typical_peroxide = Input::get('typical_peroxide');
		$data->typical_iodine = Input::get('typical_iodine');
		$data->typical_saponification = Input::get('typical_saponification');
		$data->typical_anisidine = Input::get('typical_anisidine');
		$data->typical_cold = Input::get('typical_cold');
		$data->typical_refractive = Input::get('typical_refractive');
		$data->typical_gravity = Input::get('typical_gravity');
		$data->typical_stability = Input::get('typical_stability');
		$data->typical_smoke = Input::get('typical_smoke');
		$data->typical_moisture2 = Input::get('typical_moisture2');
		$data->developed_alcohol_degree = Input::get('developed_alcohol_degree');
		$data->reduced_dry_extract = Input::get('reduced_dry_extract');
		$data->ashes = Input::get('ashes');
		$data->reducing_sugars = Input::get('reducing_sugars');
		$data->antioxidant = Input::get('antioxidant');
		$data->vinegar_nutrition_fat_100g = Input::get('vinegar_nutrition_fat_100g');
		$data->vinegar_nutrition_fat_100ml = Input::get('vinegar_nutrition_fat_100ml');
		$data->vinegar_nutrition_sodium_100g = Input::get('vinegar_nutrition_sodium_100g');
		$data->vinegar_nutrition_sodium_100ml = Input::get('vinegar_nutrition_sodium_100ml');
		$data->vinegar_nutrition_carb_100g = Input::get('vinegar_nutrition_carb_100g');
		$data->vinegar_nutrition_carb_100ml = Input::get('vinegar_nutrition_carb_100ml');
		$data->vinegar_nutrition_sugars_100g = Input::get('vinegar_nutrition_sugars_100g');
		$data->vinegar_nutrition_sugars_100ml = Input::get('vinegar_nutrition_sugars_100ml');
		$data->vinegar_nutrition_protein_100g = Input::get('vinegar_nutrition_protein_100g');
		$data->vinegar_nutrition_protein_100ml = Input::get('vinegar_nutrition_protein_100ml');
		$data->vinegar_nutrition_kcal_100g = Input::get('vinegar_nutrition_kcal_100g');
		$data->vinegar_nutrition_kcal_100ml = Input::get('vinegar_nutrition_kcal_100ml');
		$data->vinegar_nutrition_calories_100g = Input::get('vinegar_nutrition_calories_100g');
		$data->vinegar_nutrition_calories_100ml = Input::get('vinegar_nutrition_calories_100ml');
		$data->vinegar_nutrition_vitamin_a_100g = Input::get('vinegar_nutrition_vitamin_a_100g');
		$data->vinegar_nutrition_vitamin_a_100ml = Input::get('vinegar_nutrition_vitamin_a_100ml');
		$data->vinegar_nutrition_vitamin_c_100g = Input::get('vinegar_nutrition_vitamin_c_100g');
		$data->vinegar_nutrition_vitamin_c_100ml = Input::get('vinegar_nutrition_vitamin_c_100ml');
		$data->vinegar_nutrition_calcium_100g = Input::get('vinegar_nutrition_calcium_100g');
		$data->vinegar_nutrition_calcium_100ml = Input::get('vinegar_nutrition_calcium_100ml');
		$data->vinegar_nutrition_iron_100g = Input::get('vinegar_nutrition_iron_100g');
		$data->vinegar_nutrition_iron_100ml = Input::get('vinegar_nutrition_iron_100ml');
		$data->vinegar_nutrition_cholesterol_100g = Input::get('vinegar_nutrition_cholesterol_100g');
		$data->vinegar_nutrition_cholesterol_100ml = Input::get('vinegar_nutrition_cholesterol_100ml');
		$data->vinegar_nutrition_fiber_100g = Input::get('vinegar_nutrition_fiber_100g');
		$data->vinegar_nutrition_fiber_100ml = Input::get('vinegar_nutrition_fiber_100ml');
		$data->vinegar_nutrition_moisture_100g = Input::get('vinegar_nutrition_moisture_100g');
		$data->vinegar_nutrition_moisture_100ml = Input::get('vinegar_nutrition_moisture_100ml');
		$data->vinegar_nutrition_calories_fat_100g = Input::get('vinegar_nutrition_calories_fat_100g');
		$data->vinegar_nutrition_calories_fat_100ml = Input::get('vinegar_nutrition_calories_fat_100ml');
		$data->organoleptic_color = Input::get('organoleptic_color');
		$data->organoleptic_odor = Input::get('organoleptic_odor');
		$data->organoleptic_flavor = Input::get('organoleptic_flavor');
		$data->organoleptic_appearance = Input::get('organoleptic_appearance');
		$data->chemical_acidity = Input::get('chemical_acidity');
		$data->chemical_ph = Input::get('chemical_ph');
		$data->chemical_gravity = Input::get('chemical_gravity');
		$data->chemical_matters = Input::get('chemical_matters');
		$data->chemical_metals = Input::get('chemical_metals');
		$data->chemical_extract = Input::get('chemical_extract');
		$data->chemical_anhydride = Input::get('chemical_anhydride');
		$data->chemical_ash = Input::get('chemical_ash');
		$data->chemical_grain = Input::get('chemical_grain');
		$data->chemical_alcohol = Input::get('chemical_alcohol');
		$data->chemical_sugar_free_extract = Input::get('chemical_sugar_free_extract');
		$data->chemical_density = Input::get('chemical_density');
		$data->chemical_brix = Input::get('chemical_brix');
		$data->chemical_alcohol_degree = Input::get('chemical_alcohol_degree');
		$data->chemical_reduced_dry_extract = Input::get('chemical_reduced_dry_extract');
		$data->chemical_sugars = Input::get('chemical_sugars');
		$data->chemical_rif = Input::get('chemical_rif');
		$data->other_product_info1 = Input::get('other_product_info1');
		$data->other_product_info2 = Input::get('other_product_info2');
		$data->other_product_info3 = Input::get('other_product_info3');
		$data->other_product_info4 = Input::get('other_product_info4');
		$data->other_product_info5 = Input::get('other_product_info5');
		$data->other_product_info6 = Input::get('other_product_info6');
		$data->under_allergen = Input::get('under_allergen');
		$data->website = Input::get('website');
		$data->prop_65_statement = Input::get('prop_65_statement');
		
		//sds
			$data->sds_active = Input::get('sds_active');
			$data->sds_name = Input::get('sds_name');
			$data->sds_special_hazard_for_personal = Input::get('sds_special_hazard_for_personal');
			$data->sds_cas_number = Input::get('sds_cas_number');
			$data->sds_upon_inhalation = Input::get('sds_upon_inhalation');
			$data->sds_upon_skin_contact = Input::get('sds_upon_skin_contact');
			$data->sds_extinguishing_media = Input::get('sds_extinguishing_media');
			$data->sds_not_to_be_used_extinguishing_media = Input::get('sds_not_to_be_used_extinguishing_media');
			$data->sds_special_exposure_hazard = Input::get('sds_special_exposure_hazard');
			$data->sds_special_protection = Input::get('sds_special_protection');
			$data->sds_precautions_related_to_personnel = Input::get('sds_precautions_related_to_personnel');
			$data->sds_procedure_for_cleanup = Input::get('sds_procedure_for_cleanup');
			$data->sds_small_spill = Input::get('sds_small_spill');
			$data->sds_large_spill = Input::get('sds_large_spill');
			$data->sds_handling = Input::get('sds_handling');
			$data->sds_storage = Input::get('sds_storage');
			$data->sds_threshold_limit_value = Input::get('sds_threshold_limit_value');
			$data->sds_inhalation_health_risks = Input::get('sds_inhalation_health_risks');
			$data->sds_skin_absorption = Input::get('sds_skin_absorption');
			$data->sds_appearance = Input::get('sds_appearance');
			$data->sds_form = Input::get('sds_form');
			$data->sds_color = Input::get('sds_color');
			$data->sds_odor = Input::get('sds_odor');
			$data->sds_melting_point = Input::get('sds_melting_point');
			$data->sds_flash_point = Input::get('sds_flash_point');
			$data->sds_ignition_temperature = Input::get('sds_ignition_temperature');
			$data->sds_density = Input::get('sds_density');
			$data->sds_water_solubility = Input::get('sds_water_solubility');
			$data->sds_general_information = Input::get('sds_general_information');
			$data->sds_conditions_to_be_avoided = Input::get('sds_conditions_to_be_avoided');
			$data->sds_dangerous_decomposition_product = Input::get('sds_dangerous_decomposition_product');
			$data->sds_toxicological_testing = Input::get('sds_toxicological_testing');
			$data->sds_practical_experience = Input::get('sds_practical_experience');
			$data->sds_general_considerations = Input::get('sds_general_considerations');
			$data->sds_information_on_product = Input::get('sds_information_on_product');
			$data->sds_behavior_in_ecological_setting = Input::get('sds_behavior_in_ecological_setting');
			$data->sds_toxic_effects_on_the_ecosystem = Input::get('sds_toxic_effects_on_the_ecosystem');
			$data->sds_additional_ecological_information = Input::get('sds_additional_ecological_information');
			$data->sds_disposal_considerations = Input::get('sds_disposal_considerations');
			$data->sds_transportation_information = Input::get('sds_transportation_information');
			$data->sds_regulatory_information = Input::get('sds_regulatory_information');

		$data->save();

		Session::flash('message', 'Your Specsheet Was Updated Successfully');
		Session::flash('alert-class', 'alert-success');
		return Redirect::route('specsheets.index');
	}

	/**
	 * Remove the specified specsheet from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		Specsheet::destroy($id);
		Session::flash('message', 'Your Specsheet Was Deleted');
		Session::flash('alert-class', 'alert-danger');
		return Redirect::route('specsheets.index');
	}

	//pdf

	public function pdf_specsheets($id)
	{
		$specsheet = Specsheet::findOrFail($id);

		$pdf = PDF::loadView('specsheets.form.pdf.specsheets', compact('specsheet'));
		return $pdf->download('specsheet-' . Date("d/m/y") . '.pdf');
	}

}
