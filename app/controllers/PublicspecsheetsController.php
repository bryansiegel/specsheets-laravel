<?php

class PublicspecsheetsController extends \BaseController {

    public function show($id){
        $specsheet = Specsheet::findOrFail($id);
        

        return View::make('public.specsheets.show', compact('specsheet'));
    }

}