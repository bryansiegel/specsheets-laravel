<?php

class BottlesController extends \BaseController
{





    public function update($id)
    {

        $update = Input::get('update');

        if($update == "true" || Input::get('discontinued') === "1") {

        $data = Lotlog::findOrFail($id);

        //redirect to main product page
        //inventories/product/drums
        $data->discontinued = Input::get('discontinued');
        $data->redirect_to_product = Input::get('redirect_to_product');
        $data->bottles_cost_per = Input::get('bottles_cost_per');
        // $data->id = Input::get('id');
        $historyid = Input::get('historyid');

        $count = Input::get('count');
        $username = Input::get('username');
        $data->expiration_date = Input::get('expiration_date');

        //add history information
        
            if(!empty($count))
        {
            Lotlog::find($id)->history()->where('id', $historyid)->update([
                'count' => $count,
                'lotlog_id' => $id,
                // 'month' => Carbon::now()->month,
                // 'year' => Carbon::now()->year,
                'date' => Carbon::now(),
                'username' => $username, 
                ]);
        } 
        
        
        // $data->qty = Input::get('qty');
        $data->size = Input::get('size');
        $data->oil_type = Input::get('oil_type');
        $data->lot = Input::get('lot');
        $data->in_bulk = Input::get('in_bulk');
        $data->in_carton = Input::get('in_carton');
        $data->pcs_plt = Input::get('pcs_plt');
        $data->supplier = Input::get('supplier');
        $data->manufacturer_part_number = Input::get('manufacturer_part_number');
        
        // return $count;

        $data->update();

     

     

        } 

        else {

            
 $data = Lotlog::findOrFail($id);

        //redirect to main product page
        //inventories/product/drums
        $data->discontinued = Input::get('discontinued');
        $data->redirect_to_product = Input::get('redirect_to_product');
        $data->bottles_cost_per = Input::get('bottles_cost_per');
        $data->id = Input::get('id');
        $count = Input::get('count');
        $username = Input::get('username');

        //add history information
        
            if(!empty($count))
        {
            Lotlog::find($id)->history()->insert([
                'count' => $count,
                'lotlog_id' => $id,
                'month' => Carbon::now()->month,
                'year' => Carbon::now()->year,
                'date' => Carbon::now(),
                'username' => $username, 
                ]);
        } 
        
        
        // $data->qty = Input::get('qty');
        $data->size = Input::get('size');
        $data->oil_type = Input::get('oil_type');
        $data->lot = Input::get('lot');
        $data->in_bulk = Input::get('in_bulk');
        $data->in_carton = Input::get('in_carton');
        $data->pcs_plt = Input::get('pcs_plt');
        $data->supplier = Input::get('supplier');
        $data->manufacturer_part_number = Input::get('manufacturer_part_number');
        
 // return $count;

        $data->save();

        } 


if (Input::get('redirect_to_product') === "1" || Input::get('discontinued') === "1") {
            return Redirect::to('inventories/product/bottles');

        } else {
            return Redirect::back();
        
        }

    }


    public function edit($id)
    {
        $count = Input::get('count');

        return $count;
    }

    // public function update($id)
    // {
    //     $data = Lotlog::findOrFail($id);

    //     //redirect to main product page
    //     //inventories/product/drums
    //     $data->discontinued = Input::get('discontinued');
    //     $data->redirect_to_product = Input::get('redirect_to_product');
    //     $data->bottles_cost_per = Input::get('bottles_cost_per');
    //     $data->id = Input::get('id');
    //     $count = Input::get('count');
    //     $username = Input::get('username');

    //     //add history information
    //     if(!empty($count))
    //     {
    //         Lotlog::find($id)->history()->insert([
    //             'count' => $count,
    //             'lotlog_id' => $id,
    //             'month' => Carbon::now()->month,
    //             'year' => Carbon::now()->year,
    //             'date' => Carbon::now(),
    //             'username' => $username, 
    //             ]);
    //     }
    //     $data->qty = Input::get('qty');
    //     $data->size = Input::get('size');
    //     $data->oil_type = Input::get('oil_type');
    //     $data->lot = Input::get('lot');
    //     $data->in_bulk = Input::get('in_bulk');
    //     $data->in_carton = Input::get('in_carton');
    //     $data->pcs_plt = Input::get('pcs_plt');
    //     $data->save();

    //     if (Input::get('redirect_to_product') === "1" || Input::get('discontinued') === "1") {
    //         return Redirect::to('inventories/product/bottles');

    //     } else {
    //         return Redirect::back();
        
    //     }



}
