<?php

class BlendsController extends \BaseController {

	/**
	 * Display a listing of blends
	 *
	 * @return Response
	 */
	public function index() {
		$blends = DB::table('blends')
			->orderBy('id', 'desc')
			->get();

		return View::make('blends.index', compact('blends'));
	}

	/**
	 * Show the form for creating a new blend
	 *
	 * @return Response
	 */
	public function create() {
		return View::make('blends.create');
	}

	/**
	 * Store a newly created blend in storage.
	 *
	 * @return Response
	 */
	public function store() {
		$validator = Validator::make($data = Input::all(), Blend::$rules);

		if ($validator->fails()) {

			return Redirect::back()->withErrors($validator)->withInput();
		}

		Blend::create($data);

		Session::flash('message', 'Your Blend Was Created');
		Session::flash('alert-class', 'alert-success');
		return Redirect::route('blends.index');
	}

	/**
	 * Display the specified blend.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) {
		$blend = Blend::findOrFail($id);

		return View::make('blends.show', compact('blend'));
	}

	/**
	 * Show the form for editing the specified blend.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id) {
		$blend = Blend::find($id);
		return View::make('blends.edit', compact('blend'));
	}

	/**
	 * Update the specified blend in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id) {
		$blend = Blend::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Blend::$rules);

		if ($validator->fails()) {
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$blend->update($data);
		Session::flash('message', 'Your Blend Was Updated Successfully');
		Session::flash('alert-class', 'alert-success');
		return Redirect::route('blends.index');
	}

	/**
	 * Remove the specified blend from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		Blend::destroy($id);
		Session::flash('message', 'Your Blend Was Deleted');
		Session::flash('alert-class', 'alert-danger');
		return Redirect::route('blends.index');
	}

}
