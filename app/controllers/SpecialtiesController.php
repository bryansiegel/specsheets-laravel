<?php

class SpecialtiesController extends \BaseController
{



    public function update($id)
    {

        $update = Input::get('update');

        if($update == "true") {

        $data = Lotlog::findOrFail($id);

        //redirect to main product page
        //inventories/product/tanks
        $data->discontinued = Input::get('discontinued');
        $data->redirect_to_product = Input::get('redirect_to_product');
        $data->specialty_cost_per = Input::get('specialty_cost_per');
        $historyid = Input::get('historyid');

         // $data->id = Input::get('id');

        $count = Input::get('count');

        $username = Input::get('username');

        //add history information
        if(!empty($count))
        {
            Lotlog::find($id)->history()->where('id', $historyid)->update([
                'count' => $count,
                'lotlog_id' => $id,
                // 'month' => Carbon::now()->month,
                // 'year' => Carbon::now()->year,
                'date' => Carbon::now(),
                'username' => $username, 
                ]);
        }
        
        // $data->qty = Input::get('qty');
        $data->size = Input::get('size');
        $data->oil_type = Input::get('oil_type');
        $data->lot = Input::get('lot');
        $data->expiration_date = Input::get('expiration_date');
        $data->sub_product_category = Input::get('sub_product_category');

        $data->update();
    } else {
        $data = Lotlog::findOrFail($id);

        //redirect to main product page
        //inventories/product/tanks
        $data->discontinued = Input::get('discontinued');
        $data->redirect_to_product = Input::get('redirect_to_product');
        $data->specialty_cost_per = Input::get('specialty_cost_per');

         $data->id = Input::get('id');

        $count = Input::get('count');

        $username = Input::get('username');

        //add history information
        if(!empty($count))
        {
            Lotlog::find($id)->history()->insert([
                'count' => $count,
                'lotlog_id' => $id,
                'month' => Carbon::now()->month,
                'year' => Carbon::now()->year,
                'date' => Carbon::now(),
                'username' => $username, 
                ]);
        }
        
        // $data->qty = Input::get('qty');
        $data->size = Input::get('size');
        $data->oil_type = Input::get('oil_type');
        $data->lot = Input::get('lot');
        $data->expiration_date = Input::get('expiration_date');
        $data->sub_product_category = Input::get('sub_product_category');

        $data->save();  
    }

        if(Input::get('redirect_to_product') === "1" || Input::get('discontinued') === "1")
        {

        return Redirect::to('inventories/product/specialty');

    } else
    {
        return Redirect::back();
        
    }


    }

    public function truffle_oil()
    {
        $data = Lotlog::where('sub_product_category', 'truffle_oil_black')
                    ->orWhere('sub_product_category', 'truffle_oil_white')
                    ->orderBy('oil_type', 'asc')
                    ->get();
        $title = 'Truffle Oil';
        //Inventory Category
        $category = 'specialty';


    return View::make('inventories/product/sub_category/specialty/truffle_oil', compact('data', 'title', 'category'));
  
    }

    public function truffle_oil_black()
    {
        $data = Lotlog::where('sub_product_category', 'truffle_oil_black')
                    ->orderBy('oil_type', 'asc')
                    ->get();
        $title = 'Truffle Oil, Black';
        //Inventory Category
        $category = 'specialty';


    return View::make('inventories/product/sub_category/specialty/truffle_oil_black', compact('data', 'title', 'category'));
        
    }

    public function truffle_oil_white()
    {
      $data = Lotlog::where('sub_product_category', 'truffle_oil_white')
                    ->orderBy('oil_type', 'asc')
                    ->get();
        $title = 'Truffle Oil, White';
        //Inventory Category
        $category = 'specialty';


    return View::make('inventories/product/sub_category/specialty/truffle_oil_white', compact('data', 'title', 'category'));
  
    }

    public function cocoa_butter()
    {
        $data = Lotlog::where('sub_product_category', 'cocoa_butter_deodorized')
                    ->orWhere('sub_product_category', 'cocoa_butter_natural')
                    ->orderBy('oil_type', 'asc')
                    ->get();
        $title = 'Cocoa Butter';
        //Inventory Category
        $category = 'specialty';


    return View::make('inventories/product/sub_category/specialty/cocoa_butter', compact('data', 'title', 'category'));
        
    }

    public function cocoa_butter_deodorized()
    {
        $data = Lotlog::where('sub_product_category', 'cocoa_butter_deodorized')
                    ->orderBy('oil_type', 'asc')
                    ->get();
        $title = 'Cocoa Butter, Deodorized';
        //Inventory Category
        $category = 'specialty';


    return View::make('inventories/product/sub_category/specialty/cocoa_butter_deodorized', compact('data', 'title', 'category'));
        
    }

    public function cocoa_butter_natural()
    {
        $data = Lotlog::where('sub_product_category', 'cocoa_butter_natural')
                    ->orderBy('oil_type', 'asc')
                    ->get();
        $title = 'Cocoa Butter, Natural';
        //Inventory Category
        $category = 'specialty';


    return View::make('inventories/product/sub_category/specialty/cocoa_butter_natural', compact('data', 'title', 'category'));
        
    }

    public function sesame_oil()
    {
        $data = Lotlog::where('sub_product_category', 'sesame_clear')
                    ->orWhere('sub_product_category', 'sesame_toasted')
                    ->orderBy('oil_type', 'asc')
                    ->get();
        $title = 'Sesame Oil';
        //Inventory Category
        $category = 'specialty';


    return View::make('inventories/product/sub_category/specialty/sesame_oil', compact('data', 'title', 'category'));
        
    }

    public function sesame_clear()
    {
        $data = Lotlog::where('sub_product_category', 'sesame_clear')
                    ->orderBy('oil_type', 'asc')
                    ->get();
        $title = 'Sesame, Clear';
        //Inventory Category
        $category = 'specialty';


    return View::make('inventories/product/sub_category/specialty/sesame_clear', compact('data', 'title', 'category'));
        
    }

    public function sesame_toasted()
    {
        $data = Lotlog::where('sub_product_category', 'sesame_toasted')
                    ->orderBy('oil_type', 'asc')
                    ->get();
        $title = 'Sesame, Toasted';
        //Inventory Category
        $category = 'specialty';


    return View::make('inventories/product/sub_category/specialty/sesame_toasted', compact('data', 'title', 'category'));
        
    }

//end class
}
