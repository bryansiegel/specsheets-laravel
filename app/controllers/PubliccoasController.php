<?php

class PubliccoasController extends \BaseController {

	
	/**
	 * Display the specified resource.
	 * GET /publiccoascontroller/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$coa = Coa::findOrFail($id);

		return View::make('public.coas.show', compact('coa'));
	}

	

}