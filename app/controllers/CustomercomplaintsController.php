<?php

class CustomercomplaintsController extends \BaseController {

	/**
	 * Display a listing of customercomplaints
	 *
	 * @return Response
	 */
	public function index()
	{
	 //Current Year
     //ex. 2018
     $current_year = '%/' . Date('Y');
     //ex. 18
     $current_year_simple = '%/' . Date('y');;

        $customercomplaints = DB::table('customercomplaints')
            ->where('date_of_complaint', 'LIKE', $current_year )
            ->orWhere('date_of_complaint', 'LIKE', $current_year_simple )
            ->orderBy('id', 'desc')
            ->get();

		return View::make('customercomplaints.index', compact('customercomplaints'));
	}

	/**
	 * Show the form for creating a new customercomplaint
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('customercomplaints.create');
	}

	/**
	 * Store a newly created customercomplaint in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Customercomplaint::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Customercomplaint::create($data);

		return Redirect::route('customercomplaints.index');
	}

	//customer complaint form
	public function api_store()
	{
		$customer_complaint = new Customercomplaint;
		$customer_complaint->status = 'in_progress';
		$customer_complaint->date_of_complaint = Date("m/d/Y");
		$customer_complaint->complaint_received_via = 'email';
		$customer_complaint->lot_number = Input::get('lot_number');
		$customer_complaint->customer_name = Input::get('customer_name');
		$customer_complaint->order_number = Input::get('order_number');
		$customer_complaint->customer_contact = Input::get('customer_contact');
		$customer_complaint->customer_email = Input::get('customer_email');
		$customer_complaint->customer_phone = Input::get('customer_phone');
		$customer_complaint->details_of_complaint = Input::get('details_of_complaint');

		$customer_complaint->save();

	/**** EMAIL *****/
		//email customer service of the complaint
 
    // $email_to = "bsiegel@cibaria-intl.com";
	   $email_to = "customerservice@cibaria-intl.com";

    $email_subject = "Cibaria Store Supply Customer Concern";

		$status = $customer_complaint->status;
		$lot_number = $customer_complaint->lot_number;
		$date_of_complaint = $customer_complaint->date_of_complaint;
		$complaint_received_via = $customer_complaint->complaint_received_via;
		$company_name = $customer_complaint->customer_name;
		$order_number = $customer_complaint->order_number;
		$customer_contact = $customer_complaint->customer_contact;
		$customer_email = $customer_complaint->customer_email;
		$customer_phone = $customer_complaint->customer_phone;
		$details_of_complaint = $customer_complaint->details_of_complaint;
		
    $email_message = "Form details below.\n\n";
     
 
    function clean_string($string) {
 
      $bad = array("content-type","bcc:","to:","cc:","href");
 
      return str_replace($bad,"",$string);
 
    }

    $email_message .= "Order Number: " . clean_string($order_number)."\n"; 	
    $email_message .= "Lot Number: " . clean_string($lot_number)."\n"; 	
    $email_message .= "Date Of Complaint: " . clean_string($date_of_complaint)."\n";	
    $email_message .= "Company Name: " . clean_string($company_name)."\n";	
    $email_message .= "Customer Contact: " . clean_string($customer_contact)."\n";	
    $email_message .= "Customer Email: " . clean_string($customer_email)."\n";	
    $email_message .= "Customer Phone: " . clean_string($customer_phone)."\n";	
    $email_message .= "Details Of Complaint: " . clean_string($details_of_complaint)."\n";	

// create email headers
 
$headers = 'From: customerservice@cibaria-intl.com'."\r\n".
 
'Reply-To: '.$customer_email."\r\n" .
 
'X-Mailer: PHP/' . phpversion();
 
@mail($email_to, $email_subject, $email_message, $headers);  
 

 //return to cibariastoresupply thank you page
		return Redirect::to('http://www.cibariastoresupply.com/shop/customer-concerns/thank-you.php');

//
	}


	/**
	 * Display the specified customercomplaint.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$customercomplaint = Customercomplaint::findOrFail($id);

		return View::make('customercomplaints.show', compact('customercomplaint'));
	}

	/**
	 * Show the form for editing the specified customercomplaint.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$customercomplaint = Customercomplaint::find($id);

		return View::make('customercomplaints.edit', compact('customercomplaint'));
	}

	/**
	 * Update the specified customercomplaint in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$customercomplaint = Customercomplaint::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Customercomplaint::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$customercomplaint->update($data);

		return Redirect::route('customercomplaints.index');
	}

	/**
	 * Remove the specified customercomplaint from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Customercomplaint::destroy($id);

		return Redirect::back();
	}

	/** In Progres
	** Filter customer complaints via status
	**/

	public function in_progress()
	{

	 //Current Year
     //ex. 2018
     $current_year = '%/' . Date('Y');
     //ex. 18
     $current_year_simple = '%/' . Date('y');;

        $customercomplaints = DB::table('customercomplaints')
            ->where('date_of_complaint', 'LIKE', $current_year )
            ->orWhere('date_of_complaint', 'LIKE', $current_year_simple )
            ->orderBy('case_number', 'desc')
            ->get();

		// $customercomplaints = Customercomplaint::where('status', 'in_progress')
		// 					->orderBy('case_number','desc')
		// 					->get();
																		;

		return View::make('customercomplaints.in_progress', compact('customercomplaints'));
	}

	/**$comp-
	** filter customer compolaints completed via status
	**/

	public function complete()
	{
	 //Current Year
     //ex. 2018
     $current_year = '%/' . Date('Y');
     //ex. 18
     $current_year_simple = '%/' . Date('y');;

        $customercomplaints = DB::table('customercomplaints')
            ->where('date_of_complaint', 'LIKE', $current_year )
            ->orWhere('date_of_complaint', 'LIKE', $current_year_simple )
            ->orderBy('case_number', 'desc')
            ->get();

		return View::make('customercomplaints.complete', compact('customercomplaints'));
	}

	public function products($id)
	{
		if(Input::get('id') != NULL)
		{
		$customercomplaint = Customercomplaint::find(Input::get('id'));

            $customercomplaint->products()->insert([
                    'product_description' => Input::get('product_description'),
                    'product_quantity' => Input::get('product_quantity'),
                    'credit_amount' => Input::get('credit_amount'),
                    'customercomplaint_id' => $customercomplaint->id
                ]);	
		} else {
			$customercomplaint = Customercomplaint::findOrFail($id);
			$products = '';
		}
		

		return View::make('customercomplaints.products', compact(['products', 'customercomplaint']));
	}


	public function qc()
	{
							 //Current Year
     //ex. 2018
     $current_year = '%/' . Date('Y');
     //ex. 18
     $current_year_simple = '%/' . Date('y');;

        $qc = DB::table('customercomplaints')
            ->where('date_of_complaint', 'LIKE', $current_year )
            ->orWhere('date_of_complaint', 'LIKE', $current_year_simple )
            ->orderBy('case_number', 'desc')
            ->get();

		return View::make('customercomplaints.qc', compact('qc'));
	}

		public function customer_service()
	{
			 //Current Year
     //ex. 2018
     $current_year = '%/' . Date('Y');
     //ex. 18
     $current_year_simple = '%/' . Date('y');;

        $customer_service = DB::table('customercomplaints')
            ->where('date_of_complaint', 'LIKE', $current_year )
            ->orWhere('date_of_complaint', 'LIKE', $current_year_simple )
            ->orderBy('case_number', 'desc')
            ->get();
            

		return View::make('customercomplaints.customer_service', compact('customer_service'));
		
	}

		public function production()
	{
					 //Current Year
     //ex. 2018
     $current_year = '%/' . Date('Y');
     //ex. 18
     $current_year_simple = '%/' . Date('y');;

        $production = DB::table('customercomplaints')
            ->where('date_of_complaint', 'LIKE', $current_year )
            ->orWhere('date_of_complaint', 'LIKE', $current_year_simple )
            ->orderBy('case_number', 'desc')
            ->get();
            

		return View::make('customercomplaints.production', compact('production'));
		
	}

		public function technology()
	{
									 //Current Year
     //ex. 2018
     $current_year = '%/' . Date('Y');
     //ex. 18
     $current_year_simple = '%/' . Date('y');;

        $technology = DB::table('customercomplaints')
            ->where('date_of_complaint', 'LIKE', $current_year )
            ->orWhere('date_of_complaint', 'LIKE', $current_year_simple )
            ->orderBy('case_number', 'desc')
            ->get();

		return View::make('customercomplaints.technology', compact('technology'));
		
	}

	public function reports()
	{
			 //Current Year Report
     //ex. 2018
     $current_year = '%/' . Date('Y');
     //ex. 18
     $current_year_simple = '%/' . Date('y');;

        $report = DB::table('customercomplaints')
            ->where('date_of_complaint', 'LIKE', $current_year )
            ->orWhere('date_of_complaint', 'LIKE', $current_year_simple )
            ->orderBy('id', 'desc')
            ->get();

       //last year report

//ex. 2018
     $last_year_date = Date("Y") -1;
     $last_year = '%/' . $last_year_date;
     //ex. 18
     $last_year_simple_date = Date("y") -1;
     $last_year_simple = '%/' . $last_year_simple_date;

        $last_year_report = DB::table('customercomplaints')
            ->where('date_of_complaint', 'LIKE', $last_year )
            ->orWhere('date_of_complaint', 'LIKE', $last_year_simple )
            ->orderBy('id', 'desc')
            ->get();
		
		return View::make('customercomplaints.reports', compact('report', 'last_year_report'));
	}

	//qc report
	public function concern_report($id)
	{
		$concern_report = Customercomplaint::find($id);


	$pdf = PDF::loadView('customercomplaints.pdf.concern_report', compact('concern_report'));
		return $pdf->download('customer-concern-' . Date("d/m/y") . '.pdf');


		// return View::make('customercomplaints.pdf.concern_report', compact('concern_report'));
	}

	//last year customer complaint

	public function last_year()
	{
	$last_year_date = Date("Y") -1;
     $last_year = '%/' . $last_year_date;
     //ex. 18
     $last_year_simple_date = Date("y") -1;
     $last_year_simple = '%/' . $last_year_simple_date;

        $last_year = DB::table('customercomplaints')
            ->where('date_of_complaint', 'LIKE', $last_year )
            ->orWhere('date_of_complaint', 'LIKE', $last_year_simple )
            ->orderBy('id', 'desc')
            ->get();

     return View::make('customercomplaints.last_year', compact('last_year'));
	}
}
