<?php

class PoetasController extends \BaseController {

	/**
	 * Display a listing of poetas
	 *
	 * @return Response
	 */
	public function index()
	{
		$poetas = DB::table('poetas')
					->orderBy('id', 'desc')
					->get();

		return View::make('poetas.index', compact('poetas'));
	}

		public function archive()
	{
		$poetas = Poeta::all();

		return View::make('poetas.archive', compact('poetas'));
	}

	/**
	 * Show the form for creating a new poeta
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('poetas.create');
	}

	/**
	 * Store a newly created poeta in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Poeta::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Poeta::create($data);

		return Redirect::route('poetas.index');
	}

	/**
	 * Display the specified poeta.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$poeta = Poeta::findOrFail($id);

		return View::make('poetas.show', compact('poeta'));
	}

	/**
	 * Show the form for editing the specified poeta.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$poeta = Poeta::find($id);

		return View::make('poetas.edit', compact('poeta'));
	}

	/**
	 * Update the specified poeta in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$poeta = Poeta::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Poeta::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$poeta->update($data);

		return Redirect::route('poetas.index');
	}

	/**
	 * Remove the specified poeta from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Poeta::destroy($id);

		return Redirect::route('poetas.index');
	}

}
