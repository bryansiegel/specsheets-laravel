<?php

class InventoriesController extends \BaseController
{

    /**
     * Display a listing of inventories
     *
     * @return Response
     */
    public function index()
    {


        include('services/totals.php');
        include('services/counts.php');

        $total = new Totals();
        $count = new Counts();

        $drum_count = Lotlog::whereHas('history', function($q){
            $q->where('product_category', 'drums', function($q){
                $q->with('history');
            });
        })->get();



        return View::make('inventories.index', compact(
            //@include counts.php
            'count',
            //@include totals.php
            'total'
            // 'drum_count'
        ));

    }

    /*
    *Cost
    *Drum cost
    */

    public function drum_cost()
    {
        $drums = Lotlog::where('product_category', 'drums')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.cost.drums', compact('drums'));
    }

    /*
    *Cost
    *Flavors for samples cost
    */

    public function flavors_for_samples_cost()
    {
        $flavors_for_samples = Lotlog::where('product_category', 'flavors_for_samples')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.cost.flavors_for_samples', compact('flavors_for_samples'));
    }


    /*
    *Cost
    *Flavors cost
    */

    public function flavors_cost()
    {
        $flavors = Lotlog::where('product_category', 'flavors')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.cost.flavors', compact('flavors'));
    }


    /*
    *Cost
    *Follmer cost
    */

    public function follmer_cost()
    {
        $follmer = Lotlog::where('product_category', 'follmer')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.cost.follmer', compact('follmer'));
    }


    /*
    *Cost
    *Materials cost
    */

    public function materials_cost()
    {
        $materials = Lotlog::where('product_category', 'materials')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.cost.materials', compact('materials'));
    }


    /*
    *Cost
    *Supplies cost
    */

    public function supplies_cost()
    {
        $supplies = Lotlog::where('product_category', 'supplies')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.cost.supplies', compact('supplies'));
    }


    /*
    *Cost
    *Drum cost
    */

    public function cardboard_printed_cost()
    {
        $cardboard_printed = Lotlog::where('product_category', 'cardboard_printed')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.cost.cardboard_printed', compact('cardboard_printed'));
    }


    /*
    *Cost
    *Drum cost
    */

    public function cardboard_blank_cost()
    {
        $cardboard_blank = Lotlog::where('product_category', 'cardboard_blank')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.cost.cardboard_blank', compact('cardboard_blank'));
    }


    /*
    *Cost
    *Caps cost
    */

    public function caps_cost()
    {
        $caps = Lotlog::where('product_category', 'caps')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.cost.caps', compact('caps'));
    }


    /*
    *Cost
    *Drum cost
    */

    public function fustis_cost()
    {
        $fustis = Lotlog::where('product_category', 'fustis')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.cost.fustis', compact('fustis'));
    }


    /*
    *Cost
    *Drum cost
    */

    public function etched_bottles_cost()
    {
        $etched_bottles = Lotlog::where('product_category', 'etched_bottles')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.cost.etched_bottles', compact('etched_bottles'));
    }

    /*
    *Cost
    *Bottles cost
    */

    public function bottles_cost()
    {
        $bottles = Lotlog::where('product_category', 'bottles')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.cost.bottles', compact('bottles'));
    }

    /*
    *Cost
    *customers labels cost
    */

    public function customers_labels_cost()
    {
        $customer_labels = Lotlog::where('product_category', 'customers_labels')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.cost.customers_labels', compact('customer_labels'));
    }


    /*
    *Cost
    *Labels in labelroom cost
    */

    public function labels_in_labelroom_cost()
    {
        $labels_in_labelroom = Lotlog::where('product_category', 'labels_in_labelroom')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.cost.labels_in_labelroom', compact('labels_in_labelroom'));
    }


    /*
    *Cost
    *Labels in house cost
    */

    public function labels_in_house_cost()
    {
        $labels_in_house = Lotlog::where('product_category', 'labels_in_house')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.cost.labels_in_house', compact('labels_in_house'));
    }

    /*
    *Cost
    *Rework cost
    */

    public function rework_cost()
    {
        $rework = Lotlog::where('product_category', 'rework')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.cost.rework', compact('rework'));
    }


    /*
    *Cost
    *Ready To ship cost
    */

    public function ready_to_ship_cost()
    {
        $ready_to_ship = Lotlog::where('product_category', 'ready_to_ship')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.cost.ready_to_ship', compact('ready_to_ship'));
    }

    /*
    *Cost
    *Spray cost
    */

    public function sprays_cost()
    {
        $sprays = Lotlog::where('product_category', 'sprays')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.cost.sprays', compact('sprays'));
    }


    /*
    *Cost
    *Finished Product cost
    */

    public function finished_product_cost()
    {
        $finished_products = Lotlog::where('product_category', 'finished_product')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.cost.finished_product', compact('finished_products'));
    }


    /*
    *Cost
    *Drum cost
    */

    public function website_other_cost()
    {
        $website_other = Lotlog::where('product_category', 'website_other')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.cost.website_other', compact('website_other'));
    }


    /*
    *Cost
    *Website Vinegars cost
    */

    public function website_vinegars_cost()
    {
        $website_vinegar = Lotlog::where('product_category', 'website_vinegars')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.cost.website_vinegars', compact('website_vinegar'));
    }


     /*
    *Cost
    *Web Flavor and infused cost
    */

    public function web_flavor_and_infused_cost()
    {
        $web_flavor_and_infused = Lotlog::where('product_category', 'web_flavor_and_infused')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.cost.web_flavor_and_infused', compact('web_flavor_and_infused'));
    }


    /*
    *Cost
    *Website Olive Oils cost
    */

    public function website_olive_oils_cost()
    {
        $website_olive_oils = Lotlog::where('product_category', 'website_olive_oils')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.cost.website_olive_oils', compact('website_olive_oils'));
    }


    /*
    *Cost
    *Spreads and olives cost
    */

    public function spreads_and_olives_cost()
    {
        $spreads_and_olives = Lotlog::where('product_category', 'spreads_and_olives')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.cost.spreads_and_olives', compact('spreads_and_olives'));
    }

    /*
    *Cost
    *Infused Oil Drums cost
    */

    public function infused_oil_drums_cost()
    {
        $infused_oil_drums = Lotlog::where('product_category', 'infused_oil_drums')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.cost.infused_oil_drums', compact('infused_oil_drums'));
    }

    /*
    *Cost
    *Drum cost
    */

    public function specialty_cost()
    {
        $specialties = Lotlog::where('product_category', 'specialty')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.cost.specialties', compact('specialties'));
    }


    /*
    *Cost
    *Drum cost
    */

    public function railcar_cost()
    {
        $railcars = Lotlog::where('product_category', 'railcars')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.cost.railcars', compact('railcars'));
    }
    /*
    *Cost
    *Drum cost
    */

    public function vinegar_cost()
    {
        $vinegars = Lotlog::where('product_category', 'vinegars')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.cost.vinegars', compact('vinegars'));
    }
    /*
    *Cost
    *Tank cost
    */

    public function tank_cost()
    {
        $tanks = Lotlog::where('product_category', 'tanks')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.cost.tanks', compact('tanks'));
    }
    /*
    *Cost
    *Organic cost
    */

    public function organic_cost()
    {
        $organics = Lotlog::where('product_category', 'organic')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.cost.organics', compact('organics'));
    }
 /*
    *Cost
    *Drum cost
    */

    public function tote_cost()
    {
        $totes = Lotlog::where('product_category', 'totes')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.cost.totes', compact('totes'));
    }

 /*
    *Cost
    *Drum cost
    */

    public function customers_oils_cost()
    {
        $customers_oils = Lotlog::where('product_category', 'customers_oils')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.cost.customers_oils', compact('customers_oils'));
    }


    /*
    *Count
    *Drum count
    */

    public function drum_count()
    {
        $drums = Lotlog::where('product_category', 'drums')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.count.drums', compact('drums'));
    }

    /*
    *Count
    *Tote count
    */

    public function tote_count()
    {
        $totes = Lotlog::where('product_category', 'totes')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.count.totes', compact('totes'));
    }

    /*
	*Organics
	*Organics count
	*/

    public function organics_count()
    {
        $organics = Lotlog::where('product_category', 'organic')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.count.organics', compact('organics'));
    }

    /*
	*Vinegars
	*Vinegars count
	*/

    public function vinegars_count()
    {
        $vinegars = Lotlog::where('product_category', 'vinegars')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.count.vinegars', compact('vinegars'));
    }

    /*
	*Railcars
	*Railcars count
	*/

    public function railcars_count()
    {
        $railcars = Lotlog::where('product_category', 'railcars')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.count.railcars', compact('railcars'));
    }

    /*
	*Specialty
	*Specialty count
	*/

    public function specialty_count()
    {
        $specialties = Lotlog::where('product_category', 'specialty')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.count.specialties', compact('specialties'));
    }

    /*
	*Infused Oil Drums
	*Infused Oil Drum count
	*/

    public function infused_oil_drums_count()
    {
        $infused_oil_drums = Lotlog::where('product_category', 'infused_oil_drums')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.count.infused_oil_drums', compact('infused_oil_drums'));
    }

    /*
	*Spreads and Olives
	*Spreads and Olives count
	*/

    public function spreads_and_olives_count()
    {
        $spreads_and_olives = Lotlog::where('product_category', 'spreads_and_olives')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.count.spreads_and_olives', compact('spreads_and_olives'));
    }

    /*
	*Flavor and Infused Oil Drums
	*Flavor and Infused Oil Drums count
	*/

    public function flavor_and_infused_oil_drums_count()
    {
        $flavor_and_infused_oil_drums = Lotlog::where('product_category', 'flavor_and_infused_oil_drums')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.count.flavor_and_infused_oil_drums', compact('flavor_and_infused_oil_drums'));
    }


    /*
	*Website Vinegars
	*Website Vinegars count
	*/

    public function website_vinegars_count()
    {
        $website_vinegars = Lotlog::where('product_category', 'website_vinegars')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.count.website_vinegars', compact('website_vinegars'));
    }

    /*
	*Website Other
	*Website Other count
	*/

    public function website_other_count()
    {
        $website_others = Lotlog::where('product_category', 'website_other')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.count.website_other', compact('website_others'));
    }

    /*
	*Finished Product
	*Finished Product count
	*/

    public function finished_product_count()
    {
        $finished_products = Lotlog::where('product_category', 'finished_product')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.count.finished_product', compact('finished_products'));
    }

    /*
	*Sprays
	*Sprays count
	*/

    public function sprays_count()
    {
        $sprays = Lotlog::where('product_category', 'sprays')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.count.sprays', compact('sprays'));
    }

    /*
	*Ready To Ship
	*Ready To Ship count
	*/

    public function ready_to_ship_count()
    {
        $ready_to_ships = Lotlog::where('product_category', 'ready_to_ship')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.count.ready_to_ship', compact('ready_to_ships'));
    }

    /*
	*Rework
	*Rework count
	*/

    public function rework_count()
    {
        $reworks = Lotlog::where('product_category', 'rework')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.count.rework', compact('reworks'));
    }

    /*
	*Labels In House
	*Labels In House count
	*/

    public function labels_in_houses_count()
    {
        $labels_in_house = Lotlog::where('product_category', 'labels_in_house')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.count.labels_in_house', compact('labels_in_house'));
    }


    /*
	*Labels In Labelroom
	*Labels In Labelroom count
	*/

    public function labels_in_labelroom_count()
    {
        $labels_in_labelroom = Lotlog::where('product_category', 'labels_in_labelroom')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.count.labels_in_labelroom', compact('labels_in_labelroom'));
    }


    /*
	*Customer Labels
	*Customer Labels count
	*/

    public function customer_labels_count()
    {
        $customer_labels = Lotlog::where('product_category', 'customers_labels')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.count.customer_labels', compact('customer_labels'));
    }


    /*
	*Bottles
	*Bottles count
	*/

    public function bottles_count()
    {
        $bottles = Lotlog::where('product_category', 'bottles')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.count.bottles', compact('bottles'));
    }


    /*
	*Ethced Bottles
	*Etched Bottles count
	*/

    public function etched_bottles_count()
    {
        $etched_bottles = Lotlog::where('product_category', 'etched_bottles')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.count.etched_bottles', compact('etched_bottles'));
    }


    /*
	*Fustis
	*Fustis count
	*/

    public function fustis_count()
    {
        $fustis = Lotlog::where('product_category', 'fustis')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.count.fustis', compact('fustis'));
    }


    /*
	*Caps
	*Caps count
	*/

    public function caps_count()
    {
        $caps = Lotlog::where('product_category', 'caps')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.count.caps', compact('caps'));
    }


    /*
	*Cardboard Blank
	*Cardboard Blank count
	*/

    public function cardboard_blank_count()
    {
        $cardboard_blank = Lotlog::where('product_category', 'cardboard_blank')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.count.cardboard_blank', compact('cardboard_blank'));
    }

    /*
	*Cardboard Printed
	*Cardboard Printed count
	*/

    public function cardboard_printed_count()
    {
        $cardboard_printed = Lotlog::where('product_category', 'cardboard_printed')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.count.cardboard_printed', compact('cardboard_printed'));
    }

    /*
	*Supplies
	*Supplies count
	*/

    public function supplies_count()
    {
        $supplies = Lotlog::where('product_category', 'supplies')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.count.supplies', compact('supplies'));
    }


    /*
	*Materials
	*Materials count
	*/

    public function materials_count()
    {
        $materials = Lotlog::where('product_category', 'materials')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.count.materials', compact('materials'));
    }


    /*
	*Follmer
	*Follmer count
	*/

    public function follmer_count()
    {
        $follmer = Lotlog::where('product_category', 'follmer')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.count.follmer', compact('follmer'));
    }


    /*
	*Flavors
	*Flavors count
	*/

    public function flavors_count()
    {
        $flavors = Lotlog::where('product_category', 'flavors')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.count.flavors', compact('flavors'));
    }


    /*
	*Flavors For Samples
	*Flavors For Samples count
	*/

    public function flavors_for_samples_count()
    {
        $flavors_for_samples = Lotlog::where('product_category', 'flavors_for_samples')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.count.flavors_for_samples', compact('flavors_for_samples'));
    }

    /*
	*Web Flavored And Infused
	*Web Flavored And Infused count
	*/

    public function web_flavor_and_infused_count()
    {
        $web_flavor_and_infused = Lotlog::where('product_category', 'web_flavor_and_infused')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.count.web_flavor_and_infused', compact('web_flavor_and_infused'));
    }



    /*
	*Tanks
	*Tote count
	*/

    public function tanks_count()
    {
        $tanks = Lotlog::where('product_category', 'tanks')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.count.tanks', compact('tanks'));
    }

    /*
	*Website Olive Oils
	*Website Olive Oils count
	*/

    public function website_olive_oils_count()
    {
        $website_olive_oils = Lotlog::where('product_category', 'website_olive_oils')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.count.website_olive_oils', compact('website_olive_oils'));
    }

       /*
    *Customers Oils
    *Customers Oils count
    */

    public function customers_oils_count()
    {
        $customers_oils = Lotlog::where('product_category', 'customers_oils')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.count.customers_oils', compact('customers_oils'));
    }


    /**
    *Drums
    *@return Response
    *
    */
    public function drums()
    {
      include('services/totals.php');
      $total = new Totals();

        $drums = Lotlog::where('product_category', 'drums')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.product.drums', compact('drums', 'total'));
    }

    /**
    *Totes
    *@return Response
    *
    */
    public function totes()
    {
      include('services/totals.php');
      $total = new Totals();
      //Inventory Category
      $category = 'totes';

        $totes = Lotlog::where('product_category', 'totes')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.product.totes', compact('totes', 'total', 'category'));
    }

    /**
    *Organic
    *@return Response
    *
    */
    public function organic()
    {
      include('services/totals.php');
      $total = new Totals();

        $organics = Lotlog::where('product_category', 'organic')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.product.organic', compact('organics', 'total'));
    }

    /**
    *Tanks
    *@return Response
    *
    */
    public function tanks()
    {
        include('services/totals.php');
        $total = new Totals();
        $tanks = Lotlog::where('product_category', 'tanks')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.product.tanks', compact('tanks','total'));
    }

    /**
    *Vinegar
    *@return Response
    *
    */
    public function vinegars()
    {
      include('services/totals.php');
      $total = new Totals();

      //Inventory Category
      $category = 'vinegars';

        $vinegars = Lotlog::where('product_category', 'vinegars')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.product.vinegars', compact('vinegars', 'total', 'category'));
    }

    /**
    *Rail Cars
    *@return Response
    *
    */
    public function railcars()
    {
      include('services/totals.php');
      $total = new Totals();

        $railcars = Lotlog::where('product_category', 'railcars')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.product.railcars', compact('railcars', 'total'));
    }

    /**
    *Specialty
    *@return Response
    *
    */
    public function specialty()
    {
      include('services/totals.php');
      $total = new Totals();
      //Inventory Category
      $category = 'specialty';

        $specialties = Lotlog::where('product_category', 'specialty')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.product.specialty', compact('specialties', 'total', 'category'));
    }

    /**
    *Infused Oil Drums
    *@return Response
    *
    */
    public function infused_oil_drums()
    {
      include('services/totals.php');
      $total = new Totals();

        $infused_oil_drums = Lotlog::where('product_category', 'infused_oil_drums')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.product.infused_oil_drums', compact('infused_oil_drums', 'total'));
    }

    /**
     *Spreads and Olives
     *@return Response
     *
     */
    public function spreads_and_olives()
    {
      include('services/totals.php');
      $total = new Totals();

        $spreads_and_olives = Lotlog::where('product_category', 'spreads_and_olives')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.product.spreads_and_olives', compact('spreads_and_olives', 'total'));
    }

    /**
     *Ready To Ship
     *@return Response
     *
     */
    public function ready_to_ship()
    {
      include('services/totals.php');
      $total = new Totals();

        $ready_to_ship = Lotlog::where('product_category', 'ready_to_ship')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.product.ready_to_ship', compact('ready_to_ship', 'total'));
    }

    /**
    *Website Olive Oils
    *@return Response
    *
    */
    public function website_olive_oils()
    {
      include('services/totals.php');
      $total = new Totals();

        $website_olive_oils = Lotlog::where('product_category', 'website_olive_oils')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.product.website_olive_oils', compact('website_olive_oils', 'total'));
    }

    /**
    *Web Flavor and Infused
    *@return Response
    *
    */
    public function web_flavor_and_infused()
    {
      include('services/totals.php');
      $total = new Totals();

        $web_flavor_and_infused = Lotlog::where('product_category', 'web_flavor_and_infused')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.product.web_flavor_and_infused', compact('web_flavor_and_infused', 'total'));
    }

    /**
    *Web Flavor and Infused
    *@return Response
    *
    */
    public function website_vinegars()
    {
      include('services/totals.php');
      $total = new Totals();

        $website_vinegars = Lotlog::where('product_category', 'website_vinegars')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.product.website_vinegars', compact('website_vinegars', 'total'));
    }

    /**
    *Website Other
    *@return Response
    *
    */
    public function website_other()
    {
      include('services/totals.php');
      $total = new Totals();

        $website_other = Lotlog::where('product_category', 'website_other')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.product.website_other', compact('website_other', 'total'));
    }

    /**
    *Website totals
    *@return Response
    *
    */
    public function website_totals()
    {

        include('services/totals.php');

        $total = new Totals();

        return View::make('inventories.product.website_totals', compact('total'));
    }

    /**
    *Inventory totals
    *@return Response
    *
    */
    public function inventory_totals()
    {

        include('services/totals.php');

        $total = new Totals();

        return View::make('inventories.print.inventory_totals', compact('total'));
    }

    /**
     *Website totals print
     *@return Response
     *
     */
    public function website_totals_print()
    {

        include('services/totals.php');

        $total = new Totals();

        return View::make('inventories.print.website_totals', compact('total'));
    }

    /**
     *Lotlog Totals Print
     *@return Response
     *
     */
    public function Lotlog_totals()
    {

        include('services/totals.php');

        $total = new Totals();

        return View::make('inventories.print.Lotlog_totals', compact('total'));
    }

    /**
     *Totals For Bank
     *@return Response
     *
     */
    public function totals_for_bank()
    {

        include('services/totals.php');

        $total = new Totals();

        return View::make('inventories.print.totals_for_bank', compact('total'));
    }


    /**
    *totals
    *@return Response
    *
    */
    public function totals()
    {

        include('services/totals.php');
        $total = new Totals();

        // $totals = Lotlog::all();
        return View::make('inventories.product.totals', compact('total'));
    }

    /**
    *Finished Product
    *@return Response
    *
    */
    public function finished_product()
    {
      include('services/totals.php');
      $total = new Totals();

        $finished_product = Lotlog::where('product_category', 'finished_product')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.product.finished_product', compact('finished_product', 'total'));
    }

    /**
    *Sprays
    *@return Response
    *
    */
    public function sprays()
    {
      include('services/totals.php');
      $total = new Totals();

        $sprays = Lotlog::where('product_category', 'sprays')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.product.sprays', compact('sprays', 'total'));
    }

    /**
    *Rework
    *@return Response
    *
    */
    public function rework()
    {
      include('services/totals.php');
      $total = new Totals();

        $rework = Lotlog::where('product_category', 'rework')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.product.rework', compact('rework', 'total'));
    }

    /**
    *Labels in house
    *@return Response
    *
    */
    public function labels_in_house()
    {
      include('services/totals.php');
      $total = new Totals();

        $labels_in_house = Lotlog::where('product_category', 'labels_in_house')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.product.labels_in_house', compact('labels_in_house', 'total'));
    }

    /**
    *Labels in labelroom
    *@return Response
    *
    */
    public function labels_in_labelroom()
    {
      include('services/totals.php');
      $total = new Totals();

        $labels_in_labelroom = Lotlog::where('product_category', 'labels_in_labelroom')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.product.labels_in_labelroom', compact('labels_in_labelroom', 'total'));
    }

    /**
    *Customers Labels
    *@return Response
    *
    */
    public function customers_labels()
    {
      include('services/totals.php');
      $total = new Totals();

        $customers_labels = Lotlog::where('product_category', 'customers_labels')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.product.customers_labels', compact('customers_labels', 'total'));
    }

    /**
    *Bottles
    *@return Response
    *
    */
    public function bottles()
    {
      include('services/totals.php');
      $total = new Totals();

        $bottles = Lotlog::where('product_category', 'bottles')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.product.bottles', compact('bottles', 'total'));
    }

    /**
    *Etched Bottles
    *@return Response
    *
    */
    public function etched_bottles()
    {
      include('services/totals.php');
      $total = new Totals();

        $etched_bottles = Lotlog::where('product_category', 'etched_bottles')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.product.etched_bottles', compact('etched_bottles', 'total'));
    }

    /**
    *Fustis
    *@return Response
    *
    */
    public function fustis()
    {
      include('services/totals.php');
      $total = new Totals();

        $fustis = Lotlog::where('product_category', 'fustis')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.product.fustis', compact('fustis', 'total'));
    }

    /**
    *Caps
    *@return Response
    *
    */
    public function caps()
    {
      include('services/totals.php');
      $total = new Totals();

        $caps = Lotlog::where('product_category', 'caps')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.product.caps', compact('caps', 'total'));
    }

    /**
    *Cardboard Blank
    *@return Response
    *
    */
    public function cardboard_blank()
    {
      include('services/totals.php');
      $total = new Totals();

        $cardboard_blank = Lotlog::where('product_category', 'cardboard_blank')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.product.cardboard_blank', compact('cardboard_blank', 'total'));
    }

    /**
     *Cardboard Printed
     *@return Response
     *
     */
    public function cardboard_printed()
    {
      include('services/totals.php');
      $total = new Totals();

        $cardboard_printed = Lotlog::where('product_category', 'cardboard_printed')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.product.cardboard_printed', compact('cardboard_printed', 'total'));
    }


    /**
    *Supplies
    *@return Response
    *
    */
    public function supplies()
    {
      include('services/totals.php');
      $total = new Totals();

        $supplies = Lotlog::where('product_category', 'supplies')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.product.supplies', compact('supplies', 'total'));
    }

    /**
    *materials
    *@return Response
    *
    */
    public function materials()
    {
      include('services/totals.php');
      $total = new Totals();

        $materials = Lotlog::where('product_category', 'materials')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.product.materials', compact('materials', 'total'));
    }

    /**
    *follmer
    *@return Response
    *
    */
    public function follmer()
    {
      include('services/totals.php');
      $total = new Totals();

        $follmer = Lotlog::where('product_category', 'follmer')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.product.follmer', compact('follmer', 'total'));
    }

    /**
    *flavors
    *@return Response
    *
    */
    public function flavors()
    {
      include('services/totals.php');
      $total = new Totals();

        $flavors = Lotlog::where('product_category', 'flavors')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.product.flavors', compact('flavors', 'total'));
    }

    /**
    *flavors for samples
    *@return Response
    *
    */
    public function flavors_for_samples()
    {
      include('services/totals.php');
      $total = new Totals();

        $flavors_for_samples = Lotlog::where('product_category', 'flavors_for_samples')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.product.flavors_for_samples', compact('flavors_for_samples', 'total'));
    }

      /**
    *flavors for samples
    *@return Response
    *
    */
    public function customers_oils()
    {
      include('services/totals.php');
      $total = new Totals();

        $customers_oils = Lotlog::where('product_category', 'customers_oils')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.product.customers_oils', compact('customers_oils', 'total'));
    }


    /**
     *Product Totals
     *@return Response
     *
     */
    public function product_totals()
    {
        include('services/totals.php');
        include('services/counts.php');

        $total = new Totals();
        $count = new Counts();
        return View::make('inventories.product.product_totals', compact('total', 'count'));
    }


//discontinued
    public function drum_discontinued()
    {
        $drums = Lotlog::where('product_category', 'drums')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.discontinued.drums', compact('drums'));
    }

    public function tote_discontinued()
    {
        $totes = Lotlog::where('product_category', 'totes')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.discontinued.totes', compact('totes'));
    }

    public function organic_discontinued()
    {
        $organics = Lotlog::where('product_category', 'organic')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.discontinued.organic', compact('organics'));
    }

    public function tank_discontinued()
    {
        $tanks = Lotlog::where('product_category', 'tanks')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.discontinued.tanks', compact('tanks'));
    }

    public function vinegar_discontinued()
    {
        $vinegars = Lotlog::where('product_category', 'vinegars')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.discontinued.vinegars', compact('vinegars'));
    }

    public function railcar_discontinued()
    {
        $railcars = Lotlog::where('product_category', 'railcars')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.discontinued.railcars', compact('railcars'));
    }

    public function specialty_discontinued()
    {
        $specialties = Lotlog::where('product_category', 'specialty')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.discontinued.specialty', compact('specialties'));
    }

    public function infused_oil_drums_discontinued()
    {
        $infused_oil_drums = Lotlog::where('product_category', 'infused_oil_drums')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.discontinued.infused_oil_drums', compact('infused_oil_drums'));
    }

    public function spreads_and_olives_discontinued()
    {
        $spreads_and_olives = Lotlog::where('product_category', 'spreads_and_olives')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.discontinued.spreads_and_olives', compact('spreads_and_olives'));
    }

    // public function website_olive_oils_discontinued()
    // {
    //     $website_olive_oils = Lotlog::where('product_category', 'website_olive_oils')->get();
    //     return View::make('inventories.discontinued.website_olive_oils', compact('website_olive_oils'));
    // }

    // public function web_flavor_and_infused_discontinued()
    // {
    //     $web_flavor_and_infused = Lotlog::where('product_category', 'web_flavor_and_infused')->get();
    //     return View::make('inventories.discontinued.web_flavor_and_infused', compact('web_flavor_and_infused'));
    // }

    public function website_vinegars_discontinued()
    {
        $website_vinegars = Lotlog::where('product_category', 'website_vinegars')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.discontinued.website_vinegars', compact('website_vinegars'));
    }

    public function website_other_discontinued()
    {
        $website_other = Lotlog::where('product_category', 'website_other')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.discontinued.website_other', compact('website_other'));
    }

    public function finished_product_discontinued()
    {
        $finished_product = Lotlog::where('product_category', 'finished_product')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.discontinued.finished_product', compact('finished_product'));
    }

    public function sprays_discontinued()
    {
        $sprays = Lotlog::where('product_category', 'sprays')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.discontinued.sprays', compact('sprays'));
    }

    public function ready_to_ship_discontinued()
    {
        $ready_to_ship = Lotlog::where('product_category', 'ready_to_ship')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.discontinued.ready_to_ship', compact('ready_to_ship'));
    }

    public function rework_discontinued()
    {
        $rework = Lotlog::where('product_category', 'rework')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.discontinued.rework', compact('rework'));
    }

    public function labels_in_house_discontinued()
    {
        $labels_in_house = Lotlog::where('product_category', 'labels_in_house')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.discontinued.labels_in_house', compact('labels_in_house'));
    }

    public function labels_in_labelroom_discontinued()
    {
        $labels_in_labelroom = Lotlog::where('product_category', 'labels_in_labelroom')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.discontinued.labels_in_labelroom', compact('labels_in_labelroom'));
    }

    public function customers_labels_discontinued()
    {
        $customers_labels = Lotlog::where('product_category', 'customers_labels')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.discontinued.customers_labels', compact('customers_labels'));
    }

    public function bottles_discontinued()
    {
        $bottles = Lotlog::where('product_category', 'bottles')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.discontinued.bottles', compact('bottles'));
    }

    public function etched_bottles_discontinued()
    {
        $etched_bottles = Lotlog::where('product_category', 'etched_bottles')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.discontinued.etched_bottles', compact('etched_bottles'));
    }

    public function fustis_discontinued()
    {
        $fustis = Lotlog::where('product_category', 'fustis')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.discontinued.fustis', compact('fustis'));
    }

    public function caps_discontinued()
    {
        $caps = Lotlog::where('product_category', 'caps')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.discontinued.caps', compact('caps'));
    }

    public function cardboard_blank_discontinued()
    {
        $cardboard_blank = Lotlog::where('product_category', 'cardboard_blank')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.discontinued.cardboard_blank', compact('cardboard_blank'));
    }

    public function cardboard_printed_discontinued()
    {
        $cardboard_printed = Lotlog::where('product_category', 'cardboard_printed')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.discontinued.cardboard_printed', compact('cardboard_printed'));
    }

    public function supplies_discontinued()
    {
        $supplies = Lotlog::where('product_category', 'supplies')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.discontinued.supplies', compact('supplies'));
    }

    public function materials_discontinued()
    {
        $materials = Lotlog::where('product_category', 'materials')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.discontinued.materials', compact('materials'));
    }

    public function follmer_discontinued()
    {
        $follmer = Lotlog::where('product_category', 'follmer')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.discontinued.follmer', compact('follmer'));
    }

    public function flavors_discontinued()
    {
        $flavors = Lotlog::where('product_category', 'flavors')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.discontinued.flavors', compact('flavors'));
    }

    public function flavors_for_samples_discontinued()
    {
        $flavors_for_samples = Lotlog::where('product_category', 'flavors_for_samples')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.discontinued.flavors_for_samples', compact('flavors_for_samples'));
    }

    public function website_olive_oils_discontinued()
    {
        $website_olive_oils = Lotlog::where('product_category', 'website_olive_oils')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.discontinued.website_olive_oils', compact('website_olive_oils'));
    }

    public function web_flavor_and_infused_discontinued()
    {
        $web_flavor_and_infused = Lotlog::where('product_category', 'web_flavor_and_infused')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.discontinued.web_flavor_and_infused', compact('web_flavor_and_infused'));
    }

    public function customers_oils_discontinued()
    {
        $customers_oils = Lotlog::where('product_category', 'customers_oils')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories.discontinued.customers_oils', compact('customers_oils'));
    }



    /**
     * Show the form for creating a new Lotlog
     *
     * @return Response
     */
    public function create()
    {
        return View::make('inventories.create');
    }

    /**
     * Store a newly created Lotlog in storage.
     *
     * @return Response
     */
    public function store()
    {

        $data = new Lotlog();


        //$_GET
        $data->active = Input::get('active');
        $data->lot = Input::get('lot');
        $data->so = Input::get('so');
        $data->oil_type = Input::get('oil_type');
        $data->qty = Input::get('qty');
        $data->size = Input::get('size');
        $data->date = Input::get('date');
        $data->created_at = Input::get('created_at');
        $data->updated_at = Input::get('updated_at');
        $data->expiration_date = Input::get('expiration_date');
        $data->product_contract = Input::get('product_contract');
        $data->year = Input::get('year');
        // $data->spotchecked = Input::get('spotchecked');
        $data->lotlogtype = Input::get('lotlogtype');
        $data->progress = Input::get('progress');
        $data->product_category = Input::get('product_category');

        //cost per
            $data->drums_cost_per = Input::get('drums_cost_per');
            $data->railcars_per_lb_price = Input::get('railcars_per_lb_price');
            $data->totes_cost_per = Input::get('totes_cost_per');
            $data->organic_cost_per = Input::get('organic_cost_per');
            $data->tanks_cost_per = Input::get('tanks_cost_per');
            $data->vinegars_cost_per = Input::get('vinegars_cost_per');
            $data->specialty_cost_per = Input::get('specialty_cost_per');
            $data->infused_oil_drums_cost_per = Input::get('infused_oil_drums_cost_per');
            $data->spreads_and_olives_cost_per = Input::get('spreads_and_olives_cost_per');
            $data->website_olive_oils_cost_per = Input::get('website_olive_oils_cost_per');
            $data->web_flavor_and_infused_cost_per = Input::get('web_flavor_and_infused_cost_per');
            $data->website_vinegars_cost = Input::get('website_vinegars_cost');
            $data->website_other_cost_per = Input::get('website_other_cost_per');
            $data->finished_product_cost_per = Input::get('finished_product_cost_per');
            $data->sprays_cost_per = Input::get('sprays_cost_per');
            $data->ready_to_ship_cost_per = Input::get('ready_to_ship_cost_per');
            $data->rework_cost_per = Input::get('rework_cost_per');
            $data->labels_in_house_cost_per = Input::get('labels_in_house_cost_per');
            $data->labels_in_labelroom = Input::get('labels_in_labelroom');
            $data->labels_in_labelroom_cost_per = Input::get('labels_in_labelroom_cost_per');
            $data->customers_labels_cost_per = Input::get('customers_labels_cost_per');
            $data->bottles_cost_per = Input::get('bottles_cost_per');
            $data->etched_bottles_cost_per = Input::get('etched_bottles_cost_per');
            $data->fustis_cost_per = Input::get('fustis_cost_per');
            $data->caps_price = Input::get('caps_price');
            $data->cardboard_blank_cost_per = Input::get('cardboard_blank_cost_per');
            $data->cardboard_printed = Input::get('cardboard_printed');
            $data->cardboard_printed_cost_per = Input::get('cardboard_printed_cost_per');
            $data->supplies_cost_per = Input::get('supplies_cost_per');
            $data->materials_cost_per = Input::get('materials_cost_per');
            $data->follmer_cost_per = Input::get('follmer_cost_per');
            $data->flavors_cost_per = Input::get('flavors_cost_per');
            $data->flavors_for_samples = Input::get('flavors_for_samples');
            $data->flavors_for_samples_cost_per = Input::get('flavors_for_samples_cost_per');
        //tanks
        $data->gal_inch = Input::get('gal_inch');
        $data->tank_total = Input::get('tank_total');
        $data->extension = Input::get('extension');
        $data->lot1 = Input::get('lot1');
        $data->lot2 = Input::get('lot2');
        //spreads and olives
        $data->cases = Input::get('cases');
        $data->jars = Input::get('jars');
        //railcars
        $data->loads_taken = Input::get('loads_taken');
        //ready to ship
        $data->order_number = Input::get('order_number');
        //bottles
        $data->in_bulk = Input::get('in_bulk');
        $data->in_carton = Input::get('in_carton');
        $data->pcs_plt = Input::get('pcs_plt');
        //customers labels
        $data->placement = Input::get('placement');
        //caps
        $data->per_case = Input::get('per_case');
        $data->supplier = Input::get('supplier');
        $data->manufacturer_part_number = Input::get('manufacturer_part_number');
        $data->sub_product_category = Input::get('sub_product_category');


$data->save();

        $data->history()->insert([
          // 'count' => '0.00',
          'date' => Carbon::now(),
          'month' => Carbon::now()->month,
          'year' => Carbon::now()->year,
          'lotlog_id' => $data->id,
          'username' => Auth::user()->username,
        ]);










        Session::flash('message', 'Your Product Was Created');
        Session::flash('alert-class', 'alert-success');
        return Redirect::route('inventories.index');
    }

    /**
     * Display the specified Lotlog.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $Lotlog = Lotlog::findOrFail($id);

        return View::make('inventories.show', compact('Lotlog'));
    }

    /**
     * Show the form for editing the specified Lotlog.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $Lotlog = Lotlog::find($id);

        return View::make('inventories.edit', compact('Lotlog'));
    }




    /**
     * Update the specified Lotlog in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function drum_edit($id)
    {
        $drum = Lotlog::find($id);

        return View::make('inventories.edit.drums', compact('drum'));

    }

    /**
     * Update the specified Lotlog in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function flavors_for_samples_edit($id)
    {
        $flavors_for_samples = Lotlog::find($id);

        return View::make('inventories.edit.flavors_for_samples', compact('flavors_for_samples'));

    }


    /**
     * Update the specified Lotlog in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function flavors_edit($id)
    {
        $flavors = Lotlog::find($id);

        return View::make('inventories.edit.flavors', compact('flavors'));

    }


        /**
     * Update the specified Lotlog in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function follmer_edit($id)
    {
        $follmer = Lotlog::find($id);

        return View::make('inventories.edit.follmer', compact('follmer'));

    }


        /**
     * Update the specified Lotlog in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function materials_edit($id)
    {
        $materials = Lotlog::find($id);

        return View::make('inventories.edit.materials', compact('materials'));

    }


    /**
     * Update the specified Lotlog in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function supplies_edit($id)
    {
        $supplies = Lotlog::find($id);

        return View::make('inventories.edit.supplies', compact('supplies'));

    }


    /**
     * Update the specified Lotlog in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function cardboard_printed_edit($id)
    {
        $cardboard_printed = Lotlog::find($id);

        return View::make('inventories.edit.cardboard_printed', compact('cardboard_printed'));

    }


    /**
     * Update the specified Lotlog in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function cardboard_blank_edit($id)
    {
        $cardboard_blank = Lotlog::find($id);

        return View::make('inventories.edit.cardboard_blank', compact('cardboard_blank'));

    }


    /**
     * Update the specified Lotlog in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function caps_edit($id)
    {
        $cap = Lotlog::find($id);

        return View::make('inventories.edit.caps', compact('cap'));

    }


    /**
     * Update the specified Lotlog in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function fustis_edit($id)
    {
        $fustis = Lotlog::find($id);

        return View::make('inventories.edit.fustis', compact('fustis'));

    }


     /**
     * Update the specified Lotlog in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function etched_bottles_edit($id)
    {
        $etched_bottles = Lotlog::find($id);

        return View::make('inventories.edit.etched_bottles', compact('etched_bottles'));

    }

    /**
     * Update the specified Lotlog in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function bottles_edit($id)
    {
        $bottle = Lotlog::find($id);

        return View::make('inventories.edit.bottles', compact('bottle'));

    }

    /**
     * Update the specified Lotlog in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function customers_labels_edit($id)
    {
        $customers_labels = Lotlog::find($id);

        return View::make('inventories.edit.customers_labels', compact('customers_labels'));

    }


    /**
     * Update the specified Lotlog in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function labels_in_labelroom_edit($id)
    {
        $labels_in_labelroom = Lotlog::find($id);

        return View::make('inventories.edit.labels_in_labelroom', compact('labels_in_labelroom'));

    }


     /**
     * Update the specified Lotlog in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function labels_in_house_edit($id)
    {
        $labels_in_house = Lotlog::find($id);

        return View::make('inventories.edit.labels_in_house', compact('labels_in_house'));

    }

        /**
     * Update the specified Lotlog in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function rework_edit($id)
    {
        $rework = Lotlog::find($id);

        return View::make('inventories.edit.rework', compact('rework'));

    }


    /**
     * Update the specified Lotlog in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function ready_to_ship_edit($id)
    {
        $ready_to_ship = Lotlog::find($id);

        return View::make('inventories.edit.ready_to_ship', compact('ready_to_ship'));

    }

    /**
     * Update the specified Lotlog in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function sprays_edit($id)
    {
        $spray = Lotlog::find($id);

        return View::make('inventories.edit.sprays', compact('spray'));

    }


      /**
     * Update the specified Lotlog in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function finished_product_edit($id)
    {
        $finished_product = Lotlog::find($id);

        return View::make('inventories.edit.finished_product', compact('finished_product'));

    }

    /**
     * Update the specified Lotlog in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function website_other_edit($id)
    {
        $website_other = Lotlog::find($id);

        return View::make('inventories.edit.website_other', compact('website_other'));

    }


    /**
     * Update the specified Lotlog in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function website_vinegars_edit($id)
    {
        $website_vinegar = Lotlog::find($id);

        return View::make('inventories.edit.website_vinegars', compact('website_vinegar'));

    }


    /**
     * Update the specified Lotlog in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function web_flavor_and_infused_edit($id)
    {
        $web_flavor_and_infused = Lotlog::find($id);

        return View::make('inventories.edit.web_flavor_and_infused', compact('web_flavor_and_infused'));

    }


    /**
     * Update the specified Lotlog in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function website_olive_oils_edit($id)
    {
        $website_olive_oil = Lotlog::find($id);

        return View::make('inventories.edit.website_olive_oils', compact('website_olive_oil'));

    }


    /**
     * Update the specified Lotlog in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function spreads_and_olives_edit($id)
    {
        $spreads_and_olive = Lotlog::find($id);

        return View::make('inventories.edit.spreads_and_olives', compact('spreads_and_olive'));

    }

    /**
     * Update the specified Lotlog in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function infused_oil_drums_edit($id)
    {
        $infused_oil_drum = Lotlog::find($id);

        return View::make('inventories.edit.infused_oil_drums', compact('infused_oil_drum'));

    }
    /**
     * Update the specified Lotlog in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function specialty_edit($id)
    {
        $specialty = Lotlog::find($id);

        return View::make('inventories.edit.specialties', compact('specialty'));

    }
    /**
     * Update the specified Lotlog in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function railcar_edit($id)
    {
        $railcar = Lotlog::find($id);

        return View::make('inventories.edit.railcars', compact('railcar'));

    }
      /**
     * Update the specified Lotlog in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function vinegar_edit($id)
    {
        $vinegar = Lotlog::find($id);

        return View::make('inventories.edit.vinegars', compact('vinegar'));

    }
    /**
     * Update the specified Lotlog in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function tank_edit($id)
    {
        $tank = Lotlog::find($id);

        return View::make('inventories.edit.tanks', compact('tank'));

    }
     /**
     * Update the specified Lotlog in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function tote_edit($id)
    {
        $tote = Lotlog::find($id);

        return View::make('inventories.edit.totes', compact('tote'));

    }

 /**
     * Update the specified Lotlog in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function organic_edit($id)
    {
        $organic = Lotlog::find($id);

        return View::make('inventories.edit.organics', compact('organic'));

    }

    /**
     * Update the specified Lotlog in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function customers_oils_edit($id)
    {
        $customers_oil = Lotlog::find($id);

        return View::make('inventories.edit.customers_oils', compact('customers_oil'));

    }



    /**
     * Update the specified Lotlog in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $data->active = Input::get('active');
        $data->lot = Input::get('lot');
        $data->oil_type = Input::get('oil_type');
        $data->qty = Input::get('qty');
        $data->size = Input::get('size');
        $data->date = Input::get('date');
        $data->created_at = Input::get('created_at');
        $data->updated_at = Input::get('updated_at');
        $data->expiration_date = Input::get('expiration_date');
        $data->year = Input::get('year');
        $data->lotlogtype = Input::get('lotlogtype');


            $data->approved_by = Input::get('approved_by');
            $data->product_category = Input::get('product_category');

        //cost per
            $data->drums_cost_per = Input::get('drums_cost_per');
            $data->railcars_per_lb_price = Input::get('railcars_per_lb_price');
            $data->totes_cost_per = Input::get('totes_cost_per');
            $data->organic_cost_per = Input::get('organic_cost_per');
            $data->tanks_cost_per = Input::get('tanks_cost_per');
            $data->vinegars_cost_per = Input::get('vinegars_cost_per');
            $data->specialty_cost_per = Input::get('specialty_cost_per');
            $data->infused_oil_drums_cost_per = Input::get('infused_oil_drums_cost_per');
            $data->spreads_and_olives_cost_per = Input::get('spreads_and_olives_cost_per');
            $data->website_olive_oils_cost_per = Input::get('website_olive_oils_cost_per');
            $data->web_flavor_and_infused_cost_per = Input::get('web_flavor_and_infused_cost_per');
            $data->website_vinegars_cost = Input::get('website_vinegars_cost');
            $data->website_other_cost_per = Input::get('website_other_cost_per');
            $data->finished_product_cost_per = Input::get('finished_product_cost_per');
            $data->sprays_cost_per = Input::get('sprays_cost_per');
            $data->ready_to_ship_cost_per = Input::get('ready_to_ship_cost_per');
            $data->rework_cost_per = Input::get('rework_cost_per');
            $data->labels_in_house_cost_per = Input::get('labels_in_house_cost_per');
            $data->labels_in_labelroom = Input::get('labels_in_labelroom');
            $data->customers_labels_cost_per = Input::get('customers_labels_cost_per');
            $data->bottles_cost_per = Input::get('bottles_cost_per');
            $data->etched_bottles_cost_per = Input::get('etched_bottles_cost_per');
            $data->fustis_cost_per = Input::get('fustis_cost_per');
            $data->caps_price = Input::get('caps_price');
            $data->cardboard_blank_cost_per = Input::get('cardboard_blank_cost_per');
            $data->cardboard_printed = Input::get('cardboard_printed');
            $data->supplies_cost_per = Input::get('supplies_cost_per');
            $data->materials_cost_per = Input::get('materials_cost_per');
            $data->follmer_cost_per = Input::get('follmer_cost_per');
            $data->flavors_cost_per = Input::get('flavors_cost_per');
            $data->flavors_for_samples = Input::get('flavors_for_samples');
            $data->flavors_for_samples_cost_per = Input::get('flavors_for_samples_cost_per');
        //tanks
        $data->gal_inch = Input::get('gal_inch');
        $data->tank_total = Input::get('tank_total');
        $data->extension = Input::get('extension');
        $data->lot1 = Input::get('lot1');
        $data->lot2 = Input::get('lot2');
        //spreads and olives
        $data->cases = Input::get('cases');
        $data->jars = Input::get('jars');
        //railcars
        $data->loads_taken = Input::get('loads_taken');
        //ready to ship
        $data->order_number = Input::get('order_number');
        //bottles
        $data->in_bulk = Input::get('in_bulk');
        $data->in_carton = Input::get('in_carton');
        $data->pcs_plt = Input::get('pcs_plt');
        //customers labels
        $data->placement = Input::get('placement');
        //caps
        $data->per_case = Input::get('per_case');
        $data->supplier = Input::get('supplier');
        $data->save();

        Session::flash('message', 'Your Product Was Updated Successfully');
        Session::flash('alert-class', 'alert-success');

            return Redirect::route('inventories.index');
    }

    /**
     * Remove the specified Lotlog from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Lotlog::destroy($id);

//		return Redirect::route('inventories.index');
        return Redirect::back();
    }

    //pdf

    public function pdf_product_totals()
    {
        include('services/totals.php');
        include('services/counts.php');

        $total = new Totals();
        $count = new Counts();

        // $pdf = PDF::loadView('inventories.pdf.product_totals',compact('total') );
        // return $pdf->download('product_totals.pdf');

        $pdf = App::make('dompdf');

        $pdf->loadHTML('

<div>
    <div style="text-align:center">
<img src="http://cibariaspecsheets.com/images/logo.jpg" width="200px" height="200px">
<br />
        <strong>705 Columbia Ave<br/>
            Riverside, CA 92507<br/>
            (P) (951) 823-8490<br/>
            (F) (951) 823-8495<br/>
            (E) <a href="mailto:info@cibaria-intl.com">info@cibaria-intl.com</a>
        </strong>
    </div>
    <br />
    <h1 style="text-align:center;">Inventory Totals</h1>
    <br />
            <table border="1" width="100%" cellpadding="0" cellspacing="0" >
                            <thead>
                            <tr>
                                <td><strong>Product</strong></td>
                                <td><strong>Product Totals</strong></td>
                                <td><strong>Product Counts</strong></td>
                            </tr>
                            </thead>
                            <tbody class="list">

                            <tr>
                                <td><strong>Drums</strong></td>
                                <td class="lot">' . number_format($total->get_total_drums(), 3) . '</td>
                                <td>' .  number_format($count->count_drums(), 3) .'</td>
                            </tr>
                            <tr>
                                <td><strong>Totes</strong></td>
                                <td>' . number_format($total->get_total_totes(), 3) . '</td>
                                <td>' . number_format($count->count_totes(), 3) .'</td>
                            </tr>
                            <tr>
                                <td><strong>Organic</strong></td>
                                <td>' . number_format($total->get_total_organics(), 3) . '</td>
                                <td>' . number_format($count->count_organics(), 3) . '</td>
                            </tr>
                            <tr>
                                <td><strong>Vinegar</strong></td>
                                <td>' . number_format($total->get_total_vinegars(), 3) . '</td>
                                <td>' . number_format($count->count_vinegars(), 3) . '</td>
                            </tr>
                            <tr>
                                <td><strong>Tanks</strong></td>
                                <td>' . number_format($total->get_total_tanks(), 3) . '</td>
                                <td>' . number_format($count->count_tanks(), 3) . '</td>
                            </tr>
                            <tr>
                                <td><strong>Railcars</strong></td>
                                <td>' . number_format($total->get_railcars_per_lb_price(), 3) . '</td>
                                <td>' . number_format($count->count_railcars(), 3) . '</td>
                            </tr>
                            <tr>
                                <td><strong>Specialty</strong></td>
                                <td>' . number_format($total->get_total_specialties(), 3) . '</td>
                                <td>' . number_format($count->count_specialties(), 3) . '</td>
                            </tr>
                            <tr>
                                <td><strong>Infused</strong></td>
                                <td>' . number_format($total->get_total_infused_oil_drums(), 3) . '</td>
                                <td>' .  number_format($count->count_infused_oil_drums(), 3) . '</td>
                            </tr>
                            <tr>
                                <td><strong>Website</strong></td>
                                <td>' . number_format($total->get_website_totals(), 3) . '</td>
                                <td>' . number_format($count->count_website_totals(), 3) . '</td>
                            </tr>
                            <tr>
                                <td><strong>Labels</strong></td>
                                <td>' . number_format($total->get_label_totals(), 3) . '</td>
                                <td>' . number_format($count->count_labels(), 3) . '</td>
                            </tr>
                            <tr>
                                <td><strong>Bottles</strong></td>
                                <td>' . number_format($total->get_total_bottles(), 3) . '</td>
                                <td>' . number_format($count->count_bottles(), 3) . '</td>
                            </tr>
                            <tr>
                                <td><strong>Caps</strong></td>
                                <td>' . number_format($total->get_total_caps(), 3) . '</td>
                                <td>' . number_format($count->count_caps(), 3) . '</td>
                            </tr>
                            <tr>
                                <td><strong>Cardboard Blank</strong></td>
                                <td>' . number_format($total->get_total_cardboard_blank(), 3) . '</td>
                                <td>' . number_format($count->count_cardboard_blank(), 3) . '</td>
                            </tr>
                            <tr>
                                <td><strong>Cardboard Printed</strong></td>
                                <td>' . number_format($total->get_total_cardboard_printed(), 3) . '</td>
                                <td>' . number_format($count->count_cardboard_printed(), 3) . '</td>
                            </tr>
                            <tr>
                                <td><strong>Ready To Ship</strong></td>
                                <td>' . number_format($total->get_total_ready_to_ship(), 3) . '</td>
                                <td>' . number_format($count->count_ready_to_ship(), 3) . '</td>
                            </tr>
                            <tr>
                                <td><strong>Finished Product</strong></td>
                                <td>' . number_format($total->get_total_finished_product(), 3) . '</td>
                                <td>' . number_format($count->count_finished_product(), 3).  '</td>
                            </tr>
                            <tr>
                                <td><strong>Spray</strong></td>
                                <td>' . number_format($total->get_total_sprays(), 3) . '</td>
                                <td>' . number_format($count->count_sprays(), 3) . '</td>
                            </tr>
                            <tr>
                                <td><strong>Rework</strong></td>
                                <td>' .  number_format($total->get_total_rework(), 3) . '</td>
                                <td>' . number_format($count->count_rework(), 3) . '</td>
                            </tr>
                            <tr>
                                <td><strong>Etched Bottles</strong></td>
                                <td>' .  number_format($total->get_total_etched_bottles(), 3) . '</td>
                                <td>' .  number_format($count->count_etched_bottles(), 3) . '</td>
                            </tr>
                            <tr>
                                <td><strong>Flavors</strong></td>
                                <td>' . number_format($total->get_flavor_totals(), 3) . '</td>
                                <td>' .  number_format($count->count_flavors(), 3) . '</td>
                            </tr>
                            <tr>
                                <td><strong>Materials</strong></td>
                                <td>' . number_format($total->get_total_materials(), 3) . '</td>
                                <td>' . number_format($count->count_materials(), 3) . '</td>
                            </tr>
                            <tr>
                                <td><strong>Follmer</strong></td>
                                <td>' . number_format($total->get_total_follmer(), 3) . '</td>
                                <td>' .  number_format($count->count_follmer(), 3) . '</td>
                            </tr>
                            <tr>
                                <td><strong>Pantry Items</strong></td>
                                <td>' . number_format($total->get_total_spreads_and_olives(), 3) . '</td>
                                <td>' . number_format($count->count_spreads_and_olives(), 3) . '</td>
                            </tr>
                            <tr>
                                <td><strong>Fustis</strong></td>
                                <td>' .  number_format($total->get_total_fustis(), 3) . '</td>
                                <td>' .  number_format($count->count_fustis(), 3) . '</td>
                            </tr>
                            </tbody>
                        </table>
            ');

        return $pdf->download('cibaria-inventory-' . Date("d/m/y") .  '.pdf');
    }

    public function pdf_drum()
    {
        include('services/totals.php');
        $total = new Totals();

        $drums = Lotlog::where('product_category', 'drums')
            ->orderBy('oil_type','asc')
            ->get();
         $pdf = PDF::loadView('inventories.pdf.drums', compact('drums', 'total'));
         return $pdf->download('drums-inventory-' . Date("d/m/y") . '.pdf');
    }

    public function pdf_totes()
    {
        include('services/totals.php');
        $total = new Totals();

        $totes = Lotlog::where('product_category', 'totes')
            ->orderBy('oil_type','asc')
            ->get();
         $pdf = PDF::loadView('inventories.pdf.totes', compact('totes', 'total'));
         return $pdf->download('totes-inventory-' . Date("d/m/y") . '.pdf');
    }

    public function pdf_organic()
    {
        include('services/totals.php');
        $total = new Totals();

        $organics = Lotlog::where('product_category', 'organic')
            ->orderBy('oil_type','asc')
            ->get();
         $pdf = PDF::loadView('inventories.pdf.organics', compact('organics', 'total'));
         return $pdf->download('organics-inventory-' . Date("d/m/y") . '.pdf');
    }

    public function pdf_tanks()
    {
        include('services/totals.php');
        $total = new Totals();

        $tanks = Lotlog::where('product_category', 'tanks')
            ->orderBy('oil_type','asc')
            ->get();
         $pdf = PDF::loadView('inventories.pdf.tanks', compact('tanks', 'total'));
         return $pdf->download('tanks-inventory-' . Date("d/m/y") . '.pdf');
    }

    public function pdf_vinegars()
    {
        include('services/totals.php');
        $total = new Totals();

        $vinegars = Lotlog::where('product_category', 'vinegars')
            ->orderBy('oil_type','asc')
            ->get();
         $pdf = PDF::loadView('inventories.pdf.vinegars', compact('vinegars', 'total'));
         return $pdf->download('vinegars-inventory-' . Date("d/m/y") . '.pdf');
    }

    public function pdf_railcars()
    {
        include('services/totals.php');
        $total = new Totals();

        $railcars = Lotlog::where('product_category', 'railcars')
            ->orderBy('oil_type','asc')
            ->get();
         $pdf = PDF::loadView('inventories.pdf.railcars', compact('railcars', 'total'));
         return $pdf->download('railcars-inventory-' . Date("d/m/y") . '.pdf');
    }

     public function pdf_specialty()
    {
        
        include('services/totals.php');
        $total = new Totals();

        $specialties = Lotlog::where('product_category', 'specialty')
            ->orderBy('oil_type','asc')
            ->get();
         $pdf = PDF::loadView('inventories.pdf.specialty', compact('specialties', 'total'));
         return $pdf->download('specialties-inventory-' . Date("d/m/y") . '.pdf');
    }

    public function pdf_infused_oil_drum()
    {
        include('services/totals.php');
        $total = new Totals();

        $infused_oil_drums = Lotlog::where('product_category', 'infused_oil_drums')
            ->orderBy('oil_type','asc')
            ->get();
         $pdf = PDF::loadView('inventories.pdf.infused_oil_drums', compact('infused_oil_drums', 'total'));
         return $pdf->download('infused_oil_drum-inventory-' . Date("d/m/y") . '.pdf');
    }

    public function pdf_spreads_and_olive_oils()
    {
        include('services/totals.php');

        $total = new Totals();
        $spreads_and_olive_oils = Lotlog::where('product_category', 'spreads_and_olives')
            ->orderBy('oil_type','asc')
            ->get();
         $pdf = PDF::loadView('inventories.pdf.spreads_and_olive_oils', compact('spreads_and_olive_oils', 'total'));
         return $pdf->download('spreads_and_olive_oils-inventory-' . Date("d/m/y") . '.pdf');
    }

    public function pdf_website_olive_oil()
    {
        include('services/totals.php');
        $total = new Totals();
        
        $website_olive_oils = Lotlog::where('product_category', 'website_olive_oils')
            ->orderBy('oil_type','asc')
            ->get();
         $pdf = PDF::loadView('inventories.pdf.website_olive_oils', compact('website_olive_oils', 'total'));
         return $pdf->download('website_olive_oils-inventory-' . Date("d/m/y") . '.pdf');
    }

    public function pdf_web_flavor_and_infused()
    {
        include('services/totals.php');
        $total = new Totals();

        $web_flavor_and_infused = Lotlog::where('product_category', 'web_flavor_and_infused')
            ->orderBy('oil_type','asc')
            ->get();
         $pdf = PDF::loadView('inventories.pdf.web_flavor_and_infused', compact('web_flavor_and_infused', 'total'));
         return $pdf->download('web_flavor_and_infused-inventory-' . Date("d/m/y") . '.pdf');
    }

    public function pdf_website_vinegar()
    {
        include('services/totals.php');
        $total = new Totals();
        
        $website_vinegars = Lotlog::where('product_category', 'website_vinegars')
            ->orderBy('oil_type','asc')
            ->get();
         $pdf = PDF::loadView('inventories.pdf.website_vinegars', compact('website_vinegars', 'total'));
         return $pdf->download('website_vinegars-inventory-' . Date("d/m/y") . '.pdf');
    }

    public function pdf_website_other()
    {
        include('services/totals.php');
        $total = new Totals();

        $website_others = Lotlog::where('product_category', 'website_other')
            ->orderBy('oil_type','asc')
            ->get();
         $pdf = PDF::loadView('inventories.pdf.website_others', compact('website_others', 'total'));
         return $pdf->download('website_others-inventory-' . Date("d/m/y") . '.pdf');
    }

    public function pdf_website_totals()
    {
        include('services/totals.php');

        $website_totals = new Totals();
         $pdf = PDF::loadView('inventories.pdf.website_totals', compact('website_totals', 'total'));
         return $pdf->download('website_totals-inventory-' . Date("d/m/y") . '.pdf');
    }

    public function pdf_finished_product()
    {
        include('services/totals.php');
        $total = new Totals();

        $finished_products = Lotlog::where('product_category', 'finished_product')
            ->orderBy('oil_type','asc')
            ->get();
         $pdf = PDF::loadView('inventories.pdf.finished_product', compact('finished_products', 'total'));
         return $pdf->download('finished_products-inventory-' . Date("d/m/y") . '.pdf');
    }

    public function pdf_spray()
    {
        include('services/totals.php');
        $total = new Totals();

        $sprays = Lotlog::where('product_category', 'sprays')
            ->orderBy('oil_type','asc')
            ->get();
         $pdf = PDF::loadView('inventories.pdf.sprays', compact('sprays', 'total'));
         return $pdf->download('sprays-inventory-' . Date("d/m/y") . '.pdf');
    }

    public function pdf_ready_to_ship()
    {
        include('services/totals.php');
        $total = new Totals();

        $ready_to_ship = Lotlog::where('product_category', 'ready_to_ship')
            ->orderBy('oil_type','asc')
            ->get();
         $pdf = PDF::loadView('inventories.pdf.ready_to_ship', compact('ready_to_ship', 'total'));
         return $pdf->download('ready_to_ship-inventory-' . Date("d/m/y") . '.pdf');
    }

    public function pdf_labels_in_house()
    {
        include('services/totals.php');
        $total = new Totals();

        $labels_in_houses = Lotlog::where('product_category', 'labels_in_house')
            ->orderBy('oil_type','asc')
            ->get();
         $pdf = PDF::loadView('inventories.pdf.labels_in_house', compact('labels_in_houses', 'total'));
         return $pdf->download('labels_in_house-inventory-' . Date("d/m/y") . '.pdf');
    }

    public function pdf_labels_in_labelroom()
    {
        include('services/totals.php');
        $total = new Totals();

        $labels_in_labelrooms = Lotlog::where('product_category', 'labels_in_labelroom')
            ->orderBy('oil_type','asc')
            ->get();
         $pdf = PDF::loadView('inventories.pdf.labels_in_labelrooms', compact('labels_in_labelrooms', 'total'));
         return $pdf->download('labels_in_labelrooms-inventory-' . Date("d/m/y") . '.pdf');
    }

    public function pdf_customers_labels()
    {
        include('services/totals.php');
        $total = new Totals();

        $customers_labels = Lotlog::where('product_category', 'customers_labels')
            ->orderBy('oil_type','asc')
            ->get();
         $pdf = PDF::loadView('inventories.pdf.customers_labels', compact('customers_labels', 'total'));
         return $pdf->download('customers_labels-inventory-' . Date("d/m/y") . '.pdf');
    }

    public function pdf_bottle()
    {
        include('services/totals.php');
        $total = new Totals();

        $bottles = Lotlog::where('product_category', 'bottles')
            ->orderBy('oil_type','asc')
            ->get();
         $pdf = PDF::loadView('inventories.pdf.bottles', compact('bottles', 'total'));
         return $pdf->download('bottles-inventory-' . Date("d/m/y") . '.pdf');
    }

    public function pdf_etched_bottle()
    {
        include('services/totals.php');
        $total = new Totals();

        $etched_bottles = Lotlog::where('product_category', 'etched_bottles')
            ->orderBy('oil_type','asc')
            ->get();
         $pdf = PDF::loadView('inventories.pdf.etched_bottles', compact('etched_bottles', 'total'));
         return $pdf->download('etched_bottle-inventory-' . Date("d/m/y") . '.pdf');
    }

    public function pdf_fustis()
    {
        include('services/totals.php');
        $total = new Totals();

        $fustis = Lotlog::where('product_category', 'fustis')
            ->orderBy('oil_type','asc')
            ->get();
         $pdf = PDF::loadView('inventories.pdf.fustis', compact('fustis', 'total'));
         return $pdf->download('fustis-inventory-' . Date("d/m/y") . '.pdf');
    }

    public function pdf_cap()
    {
        include('services/totals.php');
        $total = new Totals();

        $caps = Lotlog::where('product_category', 'caps')
            ->orderBy('oil_type','asc')
            ->get();
         $pdf = PDF::loadView('inventories.pdf.caps', compact('caps', 'total'));
         return $pdf->download('caps-inventory-' . Date("d/m/y") . '.pdf');
    }

    public function pdf_cardboard_blank()
    {
        include('services/totals.php');
        $total = new Totals();

        $cardboard_blanks = Lotlog::where('product_category', 'cardboard_blank')
            ->orderBy('oil_type','asc')
            ->get();
         $pdf = PDF::loadView('inventories.pdf.cardboard_blank', compact('cardboard_blanks', 'total'));
         return $pdf->download('cardboard_blank-inventory-' . Date("d/m/y") . '.pdf');
    }

    public function pdf_cardboard_printed()
    {
        include('services/totals.php');
        $total = new Totals();

        $cardboard_printed = Lotlog::where('product_category', 'cardboard_printed')
            ->orderBy('oil_type','asc')
            ->get();
         $pdf = PDF::loadView('inventories.pdf.cardboard_printed', compact('cardboard_printed', 'total'));
         return $pdf->download('cardboard_printed-inventory-' . Date("d/m/y") . '.pdf');
    }

    public function pdf_supply()
    {
        include('services/totals.php');
        $total = new Totals();

        $supplies = Lotlog::where('product_category', 'supplies')
            ->orderBy('oil_type','asc')
            ->get();
         $pdf = PDF::loadView('inventories.pdf.supplies', compact('supplies', 'total'));
         return $pdf->download('supplies-inventory-' . Date("d/m/y") . '.pdf');
    }

    public function pdf_material()
    {
        include('services/totals.php');
        $total = new Totals();

        $materials = Lotlog::where('product_category', 'materials')
            ->orderBy('oil_type','asc')
            ->get();
         $pdf = PDF::loadView('inventories.pdf.materials', compact('materials', 'total'));
         return $pdf->download('materials-inventory-' . Date("d/m/y") . '.pdf');
    }

    public function pdf_follmer()
    {
        include('services/totals.php');
        $total = new Totals();

        $follmers = Lotlog::where('product_category', 'follmer')
            ->orderBy('oil_type','asc')
            ->get();
         $pdf = PDF::loadView('inventories.pdf.follmer', compact('follmers', 'total'));
         return $pdf->download('follmer-inventory-' . Date("d/m/y") . '.pdf');
    }

    public function pdf_flavor()
    {
        include('services/totals.php');
        $total = new Totals();

        $flavors = Lotlog::where('product_category', 'flavors')
            ->orderBy('oil_type','asc')
            ->get();
         $pdf = PDF::loadView('inventories.pdf.flavors', compact('flavors', 'total'));
         return $pdf->download('flavors-inventory-' . Date("d/m/y") . '.pdf');
    }

    public function pdf_flavors_for_sample()
    {
        include('services/totals.php');
        $total = new Totals();

        $flavors_for_samples = Lotlog::where('product_category', 'flavors_for_samples')
            ->orderBy('oil_type','asc')
            ->get();
         $pdf = PDF::loadView('inventories.pdf.flavors_for_samples', compact('flavors_for_samples', 'total'));
         return $pdf->download('flavors_for_samples-inventory-' . Date("d/m/y") . '.pdf');
    }

    public function pdf_rework()
    {
        include('services/totals.php');
        $total = new Totals();

        $reworks = Lotlog::where('product_category', 'rework')
            ->orderBy('oil_type','asc')
            ->get();
         $pdf = PDF::loadView('inventories.pdf.rework', compact('reworks', 'total'));
         return $pdf->download('rework-inventory-' . Date("d/m/y") . '.pdf');
    }

    public function pdf_inventory_totals()
    {

        include('services/totals.php');
        $inventory_totals = new Totals();

         $pdf = PDF::loadView('inventories.pdf.inventory_totals', compact('inventory_totals', 'total'));
         return $pdf->download('inventory-totals-' . Date("d/m/y") . '.pdf');

         //debug
        // return View::make('inventories/pdf/inventory_totals', compact('inventory_totals'));
    }

     public function pdf_totals_for_bank()
    {
        include('services/totals.php');
        $totals_for_bank = new Totals();

         $pdf = PDF::loadView('inventories.pdf.totals_for_bank', compact('totals_for_bank', 'total'));
         return $pdf->download('totals-for-bank-' . Date("d/m/y") . '.pdf');

         //debug
        // return View::make('inventories/pdf/inventory_totals', compact('inventory_totals'));
    }

       public function pdf_customers_oils()
    {
        include('services/totals.php');
        $total = new Totals();

        $customers_oils = Lotlog::where('product_category', 'customers_oils')
            ->orderBy('oil_type','asc')
            ->get();
         $pdf = PDF::loadView('inventories.pdf.customers_oils', compact('customers_oils', 'total'));
         return $pdf->download('customers_oils-inventory-' . Date("d/m/y") . '.pdf');
    }

    //print

    public function drums_print()
    {
        $drums = Lotlog::where('product_category','drums')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories/print/drums',compact('drums'));
    }

    public function totes_print()
    {
        $totes = Lotlog::where('product_category','totes')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories/print/totes',compact('totes'));
    }

    public function organics_print()
    {
        $organics = Lotlog::where('product_category','organic')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories/print/organics',compact('organics'));
    }

    public function tanks_print()
    {
        $tanks = Lotlog::where('product_category','tanks')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories/print/tanks',compact('tanks'));
    }

    public function vinegars_print()
    {
        $vinegars = Lotlog::where('product_category','vinegars')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories/print/vinegars',compact('vinegars'));
    }

    public function railcars_print()
    {
        $railcars = Lotlog::where('product_category','railcars')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories/print/railcars',compact('railcars'));
    }

    public function specialty_print()
    {
        $specialties = Lotlog::where('product_category','specialty')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories/print/specialty',compact('specialties'));
    }

    public function infused_oil_drums_print()
    {
        $infused_oil_drums = Lotlog::where('product_category','infused_oil_drums')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories/print/infused_oil_drums',compact('infused_oil_drums'));
    }

    public function spreads_and_olives_print()
    {
        $spreads_and_olives = Lotlog::where('product_category','spreads_and_olives')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories/print/spreads_and_olives',compact('spreads_and_olives'));
    }

    public function website_olive_oils_print()
    {
        $website_olive_oils = Lotlog::where('product_category','website_olive_oils')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories/print/website_olive_oils',compact('website_olive_oils'));
    }

    public function web_flavor_and_infused_print()
    {
        $web_flavor_and_infused = Lotlog::where('product_category','web_flavor_and_infused')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories/print/web_flavor_and_infused',compact('web_flavor_and_infused'));
    }

    public function website_vinegars_print()
    {
        $website_vinegars = Lotlog::where('product_category','website_vinegars')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories/print/website_vinegars',compact('website_vinegars'));
    }

    public function website_other_print()
    {
        $website_others = Lotlog::where('product_category','website_other')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories/print/website_other',compact('website_others'));
    }

    public function finished_product_print()
    {
        $finished_products = Lotlog::where('product_category','finished_product')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories/print/finished_product',compact('finished_products'));
    }

    public function sprays_print()
    {
        $sprays = Lotlog::where('product_category','sprays')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories/print/sprays',compact('sprays'));
    }

    public function ready_to_ship_print()
    {
        $ready_to_ships = Lotlog::where('product_category','ready_to_ship')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories/print/ready_to_ship',compact('ready_to_ships'));
    }

    public function rework_print()
    {
        $reworks = Lotlog::where('product_category','rework')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories/print/rework',compact('reworks'));
    }

    public function labels_in_house_print()
    {
        $labels_in_house = Lotlog::where('product_category','labels_in_house')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories/print/labels_in_house',compact('labels_in_house'));
    }

    public function labels_in_labelroom_print()
    {
        $labels_in_labelroom = Lotlog::where('product_category','labels_in_labelroom')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories/print/labels_in_labelroom',compact('labels_in_labelroom'));
    }

    public function customers_labels_print()
    {
        $customers_labels = Lotlog::where('product_category','customers_labels')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories/print/customers_labels',compact('customers_labels'));
    }

    public function bottles_print()
    {
        $bottles = Lotlog::where('product_category','bottles')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories/print/bottles',compact('bottles'));
    }

    public function etched_bottles_print()
    {
        $etched_bottles = Lotlog::where('product_category','etched_bottles')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories/print/etched_bottles',compact('etched_bottles'));
    }

    public function fustis_print()
    {
        $fustis = Lotlog::where('product_category','fustis')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories/print/fustis',compact('fustis'));
    }

    public function caps_print()
    {
        $caps = Lotlog::where('product_category','caps')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories/print/caps',compact('caps'));
    }

    public function cardboard_blank_print()
    {
        $cardboard_blank = Lotlog::where('product_category','cardboard_blank')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories/print/cardboard_blank',compact('cardboard_blank'));
    }

    public function cardboard_printed_print()
    {
        $cardboard_printed = Lotlog::where('product_category','cardboard_printed')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories/print/cardboard_printed',compact('cardboard_printed'));
    }

    public function supplies_print()
    {
        $supplies = Lotlog::where('product_category','supplies')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories/print/supplies',compact('supplies'));
    }

    public function materials_print()
    {
        $materials = Lotlog::where('product_category','materials')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories/print/materials',compact('materials'));
    }

    public function follmer_print()
    {
        $follmer = Lotlog::where('product_category','follmer')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories/print/follmer',compact('follmer'));
    }

    public function flavors_print()
    {
        $flavors = Lotlog::where('product_category','flavors')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories/print/flavors',compact('flavors'));
    }

    public function flavors_for_samples_print()
    {
        $flavors_for_samples = Lotlog::where('product_category','flavors_for_samples')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories/print/flavors_for_samples',compact('flavors_for_samples'));
    }

    public function customers_oils_print()
    {
        $customers_oils = Lotlog::where('product_category','customers_oils')
            ->orderBy('oil_type','asc')
            ->get();
        return View::make('inventories/print/customers_oils',compact('customers_oils'));
    }

//end controller
}
