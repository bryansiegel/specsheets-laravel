<?php

class CoasController extends \BaseController {

	/**
	 * Display a listing of coas
	 *
	 * @return Response
	 */
	public function index()
	{
		$coas = Coa::all();

		return View::make('coas.index', compact('coas'));
	}

	/**
	 * Show the form for creating a new coa
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('coas.create');
	}

	/**
	 * Store a newly created coa in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Coa::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Coa::create($data);

		return Redirect::route('coas.index');
	}

	/**
	 * Display the specified coa.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$coa = Coa::findOrFail($id);

		return View::make('coas.show', compact('coa'));
	}

	/**
	 * Show the form for editing the specified coa.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$coa = Coa::find($id);

		return View::make('coas.edit', compact('coa'));
	}

	/**
	 * Update the specified coa in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$coa = Coa::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Coa::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$coa->update($data);

		return Redirect::route('coas.index');
	}

	/**
	 * Remove the specified coa from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Coa::destroy($id);

		return Redirect::route('coas.index');
	}

}
