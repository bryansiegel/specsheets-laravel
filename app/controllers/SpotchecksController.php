<?php

class SpotchecksController extends \BaseController {

	/**
	 * Display a listing of spotchecks
	 *
	 * @return Response
	 */
	public function index()
	{
		$spotchecks = DB::table('spotchecks')
		->orderBy('id', 'desc')
		->get();

		return View::make('spotchecks.index', compact('spotchecks'));
	}

	/**
	 * Show the form for creating a new spotcheck
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('spotchecks.create');
	}

	/**
	 * Store a newly created spotcheck in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Spotcheck::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Spotcheck::create($data);
        Session::flash('message', 'Your Spot Check Form Was Created');
        Session::flash('alert-class', 'alert-success');
		return Redirect::route('spotchecks.index');
	}

	/**
	 * Display the specified spotcheck.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$spotcheck = Spotcheck::findOrFail($id);

		return View::make('spotchecks.show', compact('spotcheck'));
	}

	/**
	 * Show the form for editing the specified spotcheck.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$spotcheck = Spotcheck::find($id);

		return View::make('spotchecks.edit', compact('spotcheck'));
	}

	/**
	 * Update the specified spotcheck in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$spotcheck = Spotcheck::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Spotcheck::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$spotcheck->update($data);
        Session::flash('message', 'Your Spot Check Form Was Updated Successfully');
        Session::flash('alert-class', 'alert-success');
		return Redirect::route('spotchecks.index');
	}

	/**
	 * Remove the specified spotcheck from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Spotcheck::destroy($id);
        Session::flash('message', 'Your Spot Check Form Was Deleted');
        Session::flash('alert-class', 'alert-danger');
		return Redirect::route('spotchecks.index');
	}

	/*
	 *
	 * print version of spotchecks index
	 *
	 */

	public function print_page() {
		$spotchecks = Spotcheck::all();

		return View::make('spotchecks.print', compact('spotchecks'));
	}

}
