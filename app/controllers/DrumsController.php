<?php

class DrumsController extends \BaseController
{



    public function update($id)
    {

     
        $update = Input::get('update');

        if($update == "true") {

        $data = Lotlog::findOrFail($id);

        $data->discontinued = Input::get('discontinued');
        $data->redirect_to_product = Input::get('redirect_to_product');
        $data->drums_cost_per = Input::get('drums_cost_per');
       
        //get the drum id
        // $data->id = Input::get('id');
        $count = Input::get('count');
        $username = Input::get('username');
        $historyid = Input::get('historyid');
        $data->size = Input::get('size');
        $data->oil_type = Input::get('oil_type');
        $data->lot = Input::get('lot');
        $data->expiration_date = Input::get('expiration_date');

        //add history information
        if(!empty($count))
        {
            Lotlog::find($id)->history()->where('id', $historyid)->update([
                'count' => $count,
                'lotlog_id' => $id,
                // 'month' => Carbon::now()->month,
                // 'year' => Carbon::now()->year,
                'date' => Carbon::now(),
                'username' => $username, 
                ]);
        }
        
        $data->update();

    } else {
                $data = Lotlog::findOrFail($id);

        //redirect to main product page
        //inventories/product/drums
        $data->discontinued = Input::get('discontinued');
        
        $data->discontinued = Input::get('discontinued');
        $data->redirect_to_product = Input::get('redirect_to_product');
        $data->drums_cost_per = Input::get('drums_cost_per');
        //get the drum id
        $data->id = Input::get('id');

        $count = Input::get('count');

        $username = Input::get('username');

        //add history information
        if(!empty($count))
        {
            Lotlog::find($id)->history()->insert([
                'count' => $count,
                'lotlog_id' => $id,
                'month' => Carbon::now()->month,
                'year' => Carbon::now()->year,
                'date' => Carbon::now(),
                'username' => $username, 
                ]);
        }



        // $data->qty = Input::get('qty');
        $data->size = Input::get('size');
        $data->oil_type = Input::get('oil_type');
        $data->lot = Input::get('lot');
        $data->expiration_date = Input::get('expiration_date');

        $data->save();
    }

        if (Input::get('redirect_to_product') === "1" || Input::get('discontinued') === "1") {
            return Redirect::to('inventories/product/drums');

        } else {
            return Redirect::back();
        
        }


    }

}


