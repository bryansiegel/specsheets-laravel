<?php

class Totals
{
//total values for Lotlog

    //drums
    public function get_total_drums()
    {
        $month = Carbon::now()->month;
        $year = Carbon::now()->year;

        $get_drums = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month,histories.year, lotlogs.drums_cost_per from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'drums' and histories.month = '$month' and histories.year = '$year'"));

        $drum_value = 0;
        foreach ($get_drums as $item) {
                $drums = $item->count * $item->drums_cost_per;
                    $drum_value += $drums;
        }
        return $drum_value;


    }

    //railcars
    public function get_railcars_per_lb_price()
    {
        $month = Carbon::now()->month;
        $year = Carbon::now()->year;

        $railcars_per_lb_price = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month,histories.year, lotlogs.railcars_per_lb_price from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'railcars' and histories.month = '$month' and histories.year = '$year'"));

        $railcars_value = 0;
        foreach ($railcars_per_lb_price as $item) {
                    $railcars = $item->count * $item->railcars_per_lb_price;
                    $railcars_value += $railcars;
        }
        return $railcars_value;
    }

    //totes
    public function get_total_totes()
    {
        $month = Carbon::now()->month;
        $year = Carbon::now()->year;

        // $get_totes = Lotlog::where('product_category', 'totes')->get();
            $get_totes = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month,histories.year, lotlogs.totes_cost_per from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'totes' and histories.month = '$month' and histories.year = '$year'"));

        $totes_value = 0;
        foreach ($get_totes as $item) {
                    $totes = $item->count * $item->totes_cost_per;
                    $totes_value += $totes;
        }
        return $totes_value;
    }

    //organics
    public function get_total_organics()
    {
        $month = Carbon::now()->month;
        $year = Carbon::now()->year;

        // $get_organics = Lotlog::where('product_category', 'organic')->get();
            $get_organics = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month,histories.year, lotlogs.organic_cost_per from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'organic' and histories.month = '$month' and histories.year = '$year'"));

        $value = 0;
        foreach ($get_organics as $item) {
                    $organics = $item->count * $item->organic_cost_per;
                    $value += $organics;
        }
        return $value;
    }




    //total for all tanks
    public function get_total_tanks()
    {
         $month = Carbon::now()->month;
        $year = Carbon::now()->year;

        // $get_tanks = Lotlog::where('product_category', 'tanks')->get();
            $get_tanks = DB::select(DB::raw("select lotlogs.id, lotlogs.gal_inch, histories.count, lotlogs.product_category, histories.month,histories.year, lotlogs.tanks_cost_per from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'tanks' and histories.month = '$month' and histories.year = '$year'"));

        $value = 0;
        foreach ($get_tanks as $item) {
            switch ($item->gal_inch) {
                case 'GAL':
                        $tank_total = $item->count * $item->tanks_cost_per;
                        $value += $tank_total;
                    break;
                case 'gal':
                        $tank_total = $item->count * $item->tanks_cost_per;
                        $value += $tank_total;
                    break;
                case NULL:
                        $tank_total = $item->count * $item->tanks_cost_per;
                        $value += $tank_total;
                    break;
                default:
                        $extension = $item->count * $item->gal_inch;
                        $tank_total = $extension * $item->tanks_cost_per;
                        $value += $tank_total;
                    break;
            }

        }

        return $value;
    }




    //vinegars
    public function get_total_vinegars()
    {
        $month = Carbon::now()->month;
        $year = Carbon::now()->year;

        // $get_vinegars = Lotlog::where('product_category', 'vinegars')->get();
            $get_vinegars = DB::select(DB::raw("select lotlogs.id, lotlogs.gal_inch, histories.count, lotlogs.product_category, histories.month,histories.year, lotlogs.vinegars_cost_per from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'vinegars' and histories.month = '$month' and histories.year = '$year'"));

        $value = 0;
        foreach ($get_vinegars as $item) {
                    $vinegars = $item->count * $item->vinegars_cost_per;
                    $value += $vinegars;
        }
        return $value;
    }


    //specialties
    public function get_total_specialties()
    {
        $month = Carbon::now()->month;
        $year = Carbon::now()->year;

        // $get_specialties = Lotlog::where('product_category', 'specialty')->get();
            $get_specialties = DB::select(DB::raw("select lotlogs.id, lotlogs.gal_inch, histories.count, lotlogs.product_category, histories.month,histories.year, lotlogs.specialty_cost_per from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'specialty' and histories.month = '$month' and histories.year = '$year'"));

        $value = 0;
        foreach ($get_specialties as $item) {
                    $specialties = $item->count * $item->specialty_cost_per;
                    $value += $specialties;
        }
        return $value;
    }

    //infused_oil_drums
    public function get_total_infused_oil_drums()
    {
        $month = Carbon::now()->month;
        $year = Carbon::now()->year;

        // $get_infused_oil_drums = Lotlog::where('product_category', 'infused_oil_drums')->get();
            $get_infused_oil_drums = DB::select(DB::raw("select lotlogs.id, lotlogs.gal_inch, histories.count, lotlogs.product_category, histories.month,histories.year, lotlogs.infused_oil_drums_cost_per from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'infused_oil_drums' and histories.month = '$month' and histories.year = '$year'"));

        $value = 0;
        foreach ($get_infused_oil_drums as $item) {
                    $infused_oil_drums = $item->count * $item->infused_oil_drums_cost_per;
                    $value += $infused_oil_drums;
        }
        return $value;
    }

    //spreads_and_olives
    public function get_total_spreads_and_olives()
    {
        $month = Carbon::now()->month;
        $year = Carbon::now()->year;

        // $spreads_and_olives = Lotlog::where('product_category', 'spreads_and_olives')->get();
            $spreads_and_olives = DB::select(DB::raw("select lotlogs.id, lotlogs.gal_inch, histories.count, lotlogs.product_category, histories.month,histories.year, lotlogs.spreads_and_olives_cost_per from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'spreads_and_olives' and histories.month = '$month' and histories.year = '$year'"));

        $value = 0;
        foreach ($spreads_and_olives as $item) {
                    $spreads_and_olives = $item->count * $item->spreads_and_olives_cost_per;
                    $value += $spreads_and_olives;
        }
        return $value;
    }

    //website olive oils
    public function get_total_website_olive_oils()
    {
        $month = Carbon::now()->month;
        $year = Carbon::now()->year;

        // $website_olive_oils = Lotlog::where('product_category', 'website_olive_oils')->get();
            $website_olive_oils = DB::select(DB::raw("select lotlogs.id, lotlogs.gal_inch, histories.count, lotlogs.product_category, histories.month,histories.year, lotlogs.website_olive_oils_cost_per from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'website_olive_oils' and histories.month = '$month' and histories.year = '$year'"));

        $value = 0;
        foreach ($website_olive_oils as $item) {
                    $website_olive_oils = $item->count * $item->website_olive_oils_cost_per;
                    $value += $website_olive_oils;
        }
        return $value;
    }

    //web flavored and infused
    public function get_total_web_flavored_and_infused()
    {
        $month = Carbon::now()->month;
        $year = Carbon::now()->year;

        // $web_flavor_and_infused = Lotlog::where('product_category', 'web_flavor_and_infused')->get();
            $web_flavor_and_infused = DB::select(DB::raw("select lotlogs.id, lotlogs.gal_inch, histories.count, lotlogs.product_category, histories.month,histories.year, lotlogs.web_flavor_and_infused_cost_per from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'web_flavor_and_infused' and histories.month = '$month' and histories.year = '$year'"));

        $value = 0;
        foreach ($web_flavor_and_infused as $item) {
                    $web_flavored_and_infused = $item->count * $item->web_flavor_and_infused_cost_per;
                    $value += $web_flavored_and_infused;
        }
        return $value;
    }

    //web flavored and infused
    public function get_total_website_vinegars()
    {
        $month = Carbon::now()->month;
        $year = Carbon::now()->year;

        // $website_vinegars = Lotlog::where('product_category', 'website_vinegars')->get();
            $website_vinegars = DB::select(DB::raw("select lotlogs.id, lotlogs.gal_inch, histories.count, lotlogs.product_category, histories.month,histories.year, lotlogs.website_vinegars_cost from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'website_vinegars' and histories.month = '$month' and histories.year = '$year'"));

        $value = 0;
        foreach ($website_vinegars as $item) {
                    $website_vinegars = $item->count * $item->website_vinegars_cost;
                    $value += $website_vinegars;
        }
        return $value;
    }

    //website other
    public function get_total_website_other()
    {
        $month = Carbon::now()->month;
        $year = Carbon::now()->year;

        // $website_other = Lotlog::where('product_category', 'website_other')->get();
            $website_other = DB::select(DB::raw("select lotlogs.id, lotlogs.gal_inch, histories.count, lotlogs.product_category, histories.month,histories.year, lotlogs.website_other_cost_per from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'website_other' and histories.month = '$month' and histories.year = '$year'"));

        $value = 0;
        foreach ($website_other as $item) {
                    $website_other = $item->count * $item->website_other_cost_per;
                    $value += $website_other;
        }
        return $value;
    }

    //finished product
    public function get_total_finished_product()
    {
        $month = Carbon::now()->month;
        $year = Carbon::now()->year;

        // $finished_product = Lotlog::where('product_category', 'finished_product')->get();
            $finished_product = DB::select(DB::raw("select lotlogs.id, lotlogs.gal_inch, histories.count, lotlogs.product_category, histories.month,histories.year, lotlogs.finished_product_cost_per from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'finished_product' and histories.month = '$month' and histories.year = '$year'"));

        $value = 0;
        foreach ($finished_product as $item) {
                    $finished_product = $item->count * $item->finished_product_cost_per;
                    $value += $finished_product;
        }
        return $value;
    }

    //sprays
    public function get_total_sprays()
    {
        $month = Carbon::now()->month;
        $year = Carbon::now()->year;

        // $sprays = Lotlog::where('product_category', 'sprays')->get();
            $sprays = DB::select(DB::raw("select lotlogs.id, lotlogs.gal_inch, histories.count, lotlogs.product_category, histories.month,histories.year, lotlogs.sprays_cost_per from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'sprays' and histories.month = '$month' and histories.year = '$year'"));

        $value = 0;
        foreach ($sprays as $item) {
                    $sprays = $item->count * $item->sprays_cost_per;
                    $value += $sprays;
        }
        return $value;
    }

    //ready_to_ship
    public function get_total_ready_to_ship()
    {
        $month = Carbon::now()->month;
        $year = Carbon::now()->year;

        // $ready_to_ship = Lotlog::where('product_category', 'ready_to_ship')->get();
            $ready_to_ship = DB::select(DB::raw("select lotlogs.id, lotlogs.gal_inch, histories.count, lotlogs.product_category, histories.month,histories.year, lotlogs.ready_to_ship_cost_per from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'ready_to_ship' and histories.month = '$month' and histories.year = '$year'"));

        $value = 0;
        foreach ($ready_to_ship as $item) {
                    $ready_to_ship = $item->count * $item->ready_to_ship_cost_per;
                    $value += $ready_to_ship;
        }
        return $value;
    }

    //rework
    public function get_total_rework()
    {
        $month = Carbon::now()->month;
        $year = Carbon::now()->year;

        // $rework = Lotlog::where('product_category', 'rework')->get();
            $rework = DB::select(DB::raw("select lotlogs.id, lotlogs.gal_inch, histories.count, lotlogs.product_category, histories.month,histories.year, lotlogs.rework_cost_per from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'rework' and histories.month = '$month' and histories.year = '$year'"));

        $value = 0;
        foreach ($rework as $item) {
                    $rework = $item->count * $item->rework_cost_per;
                    $value += $rework;
        }
        return $value;
    }

    //labels_in_house
    public function get_total_labels_in_house()
    {
        $month = Carbon::now()->month;
        $year = Carbon::now()->year;

        // $labels_in_house = Lotlog::where('product_category', 'labels_in_house')->get();
            $labels_in_house = DB::select(DB::raw("select lotlogs.id, lotlogs.gal_inch, histories.count, lotlogs.product_category, histories.month,histories.year, lotlogs.labels_in_house_cost_per from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'labels_in_house' and histories.month = '$month' and histories.year = '$year'"));

        $value = 0;
        foreach ($labels_in_house as $item) {
                    $labels_in_house = $item->count * $item->labels_in_house_cost_per;
                    $value += $labels_in_house;
        }
        return $value;
    }

    //labels_in_house
    public function get_total_labels_in_labelroom()
    {
        $month = Carbon::now()->month;
        $year = Carbon::now()->year;

        // $labels_in_labelroom = Lotlog::where('product_category', 'labels_in_labelroom')->get();
            $labels_in_labelroom = DB::select(DB::raw("select lotlogs.id, lotlogs.gal_inch, histories.count, lotlogs.product_category, histories.month,histories.year, lotlogs.labels_in_labelroom_cost_per from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'labels_in_labelroom' and histories.month = '$month' and histories.year = '$year'"));

        $value = 0;
        foreach ($labels_in_labelroom as $item) {
                    $labels_in_labelroom = $item->count * $item->labels_in_labelroom_cost_per;
                    $value += $labels_in_labelroom;
        }
        return $value;
    }

    //customers_labels
    public function get_total_customers_labels()
    {
        $month = Carbon::now()->month;
        $year = Carbon::now()->year;

        // $customers_labels = Lotlog::where('product_category', 'customers_labels')->get();
            $customers_labels = DB::select(DB::raw("select lotlogs.id, lotlogs.gal_inch, histories.count, lotlogs.product_category, histories.month,histories.year, lotlogs.customers_labels_cost_per from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'customers_labels' and histories.month = '$month' and histories.year = '$year'"));


        $value = 0;
        foreach ($customers_labels as $item) {
                    $customers_labels = $item->count * $item->customers_labels_cost_per;
                    $value += $customers_labels;
        }
        return $value;
    }

    //bottles
    public function get_total_bottles()
    {
        $month = Carbon::now()->month;
        $year = Carbon::now()->year;

        // $bottles = Lotlog::where('product_category', 'bottles')->get();
            $bottles = DB::select(DB::raw("select lotlogs.id, lotlogs.gal_inch, histories.count, lotlogs.product_category, histories.month,histories.year, lotlogs.bottles_cost_per from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'bottles' and histories.month = '$month' and histories.year = '$year'"));

        $value = 0;
        foreach ($bottles as $item) {
                    $bottles = $item->count * $item->bottles_cost_per;
                    $value += $bottles;
        }
        return $value;
    }

    //etched_bottles
    public function get_total_etched_bottles()
    {
        $month = Carbon::now()->month;
        $year = Carbon::now()->year;

        // $etched_bottles = Lotlog::where('product_category', 'etched_bottles')->get();
            $etched_bottles = DB::select(DB::raw("select lotlogs.id, lotlogs.gal_inch, histories.count, lotlogs.product_category, histories.month,histories.year, lotlogs.etched_bottles_cost_per from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'etched_bottles' and histories.month = '$month' and histories.year = '$year'"));

        $value = 0;
        foreach ($etched_bottles as $item) {
                    $etched_bottles = $item->count * $item->etched_bottles_cost_per;
                    $value += $etched_bottles;
        }
        return $value;
    }

    //fustis
    public function get_total_fustis()
    {
        $month = Carbon::now()->month;
        $year = Carbon::now()->year;

        // $fustis = Lotlog::where('product_category', 'fustis')->get();
            $fustis = DB::select(DB::raw("select lotlogs.id, lotlogs.gal_inch, histories.count, lotlogs.product_category, histories.month,histories.year, lotlogs.fustis_cost_per from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'fustis' and histories.month = '$month' and histories.year = '$year'"));

        $value = 0;
        foreach ($fustis as $item) {
                    $fustis = $item->count * $item->fustis_cost_per;
                    $value += $fustis;
        }
        return $value;
    }

    //caps
    public function get_total_caps()
    {
        $month = Carbon::now()->month;
        $year = Carbon::now()->year;

        // $caps = Lotlog::where('product_category', 'caps')->get();
            $caps = DB::select(DB::raw("select lotlogs.id, lotlogs.gal_inch, histories.count, lotlogs.product_category, histories.month,histories.year, lotlogs.caps_price from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'caps' and histories.month = '$month' and histories.year = '$year'"));

        $value = 0;
        foreach ($caps as $item) {
                    $caps = $item->count * $item->caps_price;
                    $value += $caps;
        }
        return $value;
    }

    //cardboard_blank
    public function get_total_cardboard_blank()
    {
        $month = Carbon::now()->month;
        $year = Carbon::now()->year;

        // $cardboard_blank = Lotlog::where('product_category', 'cardboard_blank')->get();
            $cardboard_blank = DB::select(DB::raw("select lotlogs.id, lotlogs.gal_inch, histories.count, lotlogs.product_category, histories.month,histories.year, lotlogs.cardboard_blank_cost_per from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'cardboard_blank' and histories.month = '$month' and histories.year = '$year'"));

        $value = 0;
        foreach ($cardboard_blank as $item) {
                    $cardboard_blank = $item->count * $item->cardboard_blank_cost_per;
                    $value += $cardboard_blank;
        }
        return $value;
    }

    //cardboard_printed
    public function get_total_cardboard_printed()
    {
        $month = Carbon::now()->month;
        $year = Carbon::now()->year;

        // $cardboard_printed = Lotlog::where('product_category', 'cardboard_printed')->get();
            $cardboard_printed = DB::select(DB::raw("select lotlogs.id, lotlogs.gal_inch, histories.count, lotlogs.product_category, histories.month,histories.year, lotlogs.cardboard_printed_cost_per from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'cardboard_printed' and histories.month = '$month' and histories.year = '$year'"));

        $value = 0;
        foreach ($cardboard_printed as $item) {
                    $cardboard_printed = $item->count * $item->cardboard_printed_cost_per;
                    $value += $cardboard_printed;
        }
        return $value;
    }

    //supplies
    public function get_total_supplies()
    {
        $month = Carbon::now()->month;
        $year = Carbon::now()->year;

        // $supplies = Lotlog::where('product_category', 'supplies')->get();
            $supplies = DB::select(DB::raw("select lotlogs.id, lotlogs.gal_inch, histories.count, lotlogs.product_category, histories.month,histories.year, lotlogs.supplies_cost_per from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'supplies' and histories.month = '$month' and histories.year = '$year'"));

        $value = 0;
        foreach ($supplies as $item) {
                    $supplies = $item->count * $item->supplies_cost_per;
                    $value += $supplies;
        }
        return $value;
    }

    //materials
    public function get_total_materials()
    {
        $month = Carbon::now()->month;
        $year = Carbon::now()->year;

        // $materials = Lotlog::where('product_category', 'materials')->get();
            $materials = DB::select(DB::raw("select lotlogs.id, lotlogs.gal_inch, histories.count, lotlogs.product_category, histories.month,histories.year, lotlogs.materials_cost_per from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'materials' and histories.month = '$month' and histories.year = '$year'"));

        $value = 0;
        foreach ($materials as $item) {
                    $materials = $item->count * $item->materials_cost_per;
                    $value += $materials;
        }
        return $value;
    }

    //follmer
    public function get_total_follmer()
    {
        $month = Carbon::now()->month;
        $year = Carbon::now()->year;

        // $follmer = Lotlog::where('product_category', 'follmer')->get();
            $follmer = DB::select(DB::raw("select lotlogs.id, lotlogs.gal_inch, histories.count, lotlogs.product_category, histories.month,histories.year, lotlogs.follmer_cost_per from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'follmer' and histories.month = '$month' and histories.year = '$year'"));

        $value = 0;
        foreach ($follmer as $item) {
                    $follmer = $item->count * $item->follmer_cost_per;
                    $value += $follmer;
        }
        return $value;
    }

    //flavors
    public function get_total_flavors()
    {
        $month = Carbon::now()->month;
        $year = Carbon::now()->year;

        // $flavors = Lotlog::where('product_category', 'flavors')->get();
            $flavors = DB::select(DB::raw("select lotlogs.id, lotlogs.gal_inch, histories.count, lotlogs.product_category, histories.month,histories.year, lotlogs.flavors_cost_per from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'flavors' and histories.month = '$month' and histories.year = '$year'"));

        $value = 0;
        foreach ($flavors as $item) {
                    $flavors = $item->count * $item->flavors_cost_per;
                    $value += $flavors;
        }
        return $value;
    }

    //flavors_for_samples
    public function get_total_flavors_for_samples()
    {
        $month = Carbon::now()->month;
        $year = Carbon::now()->year;

        // $flavors_for_samples = Lotlog::where('product_category', 'flavors_for_samples')->get();
            $flavors_for_samples = DB::select(DB::raw("select lotlogs.id, lotlogs.gal_inch, histories.count, lotlogs.product_category, histories.month,histories.year, lotlogs.flavors_for_samples_cost_per from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'flavors_for_samples' and histories.month = '$month' and histories.year = '$year'"));

        $value = 0;
        foreach ($flavors_for_samples as $item) {
                    $flavors_for_samples = $item->count * $item->flavors_for_samples_cost_per;
                    $value += $flavors_for_samples;
        }
        return $value;
    }

    //Customers Oils
    public function get_total_customers_oils()
    {
        $month = Carbon::now()->month;
        $year = Carbon::now()->year;

        // $flavors_for_samples = Lotlog::where('product_category', 'flavors_for_samples')->get();
            $customers_oils = DB::select(DB::raw("select lotlogs.id, lotlogs.gal_inch, histories.count, lotlogs.product_category, histories.month,histories.year, lotlogs.customers_oils_cost_per from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'customers_oils' and histories.month = '$month' and histories.year = '$year'"));

        $value = 0;
        foreach ($customers_oils as $item) {
                    $customers_oils = $item->count * $item->customers_oils_cost_per;
                    $value += $customers_oils;
        }
        return $value;
    }

    //flavor totals

    public function get_flavor_totals()
    {
        return
            $this->get_total_flavors_for_samples() +
            $this->get_total_flavors();
    }
    //website totals

    public function get_website_totals()
    {
        return
            $this->get_total_website_other() +
            $this->get_total_web_flavored_and_infused() +
            $this->get_total_website_vinegars() +
            $this->get_total_website_olive_oils();
    }

    //label totals

    public function get_label_totals()
    {
        return
            $this->get_total_labels_in_labelroom() +
            $this->get_total_labels_in_house() +
            $this->get_total_customers_labels();
    }

    //pantry items
    public function get_total_pantry_items()
    {
        return $this->get_total_spreads_and_olives();
    }

    //total
    public function total()
    {
        $total_drums = $this->get_total_drums();
        $total_totes = $this->get_total_totes();
        $total_organic = $this->get_total_organics();
        $total_tanks = $this->get_total_tanks();
        $total_vinegar = $this->get_total_vinegars();
        $total_railcars = $this->get_railcars_per_lb_price();
        $total_specialty = $this->get_total_specialties();
        $total_infused = $this->get_total_infused_oil_drums();
        $total_spreads_and_olives = $this->get_total_spreads_and_olives();
        $total_finished_product = $this->get_total_finished_product();
        $total_sprays = $this->get_total_sprays();
        $total_ready_to_ship = $this->get_total_ready_to_ship();
        $total_rework = $this->get_total_rework();

        // $total_labels = $this->get_total_labels_in_house() +
        //                 $this->get_total_labels_in_labelroom() +
        //                 $this->get_total_customers_labels();

        $total_labels_in_house = $this->get_total_labels_in_house();
        $total_labels_in_labelroom = $this->get_total_labels_in_labelroom();
        $total_customers_labels = $this->get_total_customers_labels();

        $total_bottles = $this->get_total_bottles();
        $total_fustis = $this->get_total_fustis();
        $total_caps = $this->get_total_caps();
        $total_cardboard_blank = $this->get_total_cardboard_blank();
        $total_cardboard_printed = $this->get_total_cardboard_printed();
        $total_supplies = $this->get_total_supplies();
        $total_materials = $this->get_total_materials();
        $total_follmer = $this->get_total_follmer();

        // $total_flavors =
        //                 $this->get_total_flavors() +
        //                 $this->get_total_flavors_for_samples();

        $total_flavors = $this->get_total_flavors();
        $total_flavors_for_samples = $this->get_total_flavors_for_samples();



        // $total_website =
        //                 $this->get_total_web_flavored_and_infused() +
        //                 $this->get_total_website_olive_oils() +
        //                 $this->get_total_website_other() +
        //                 $this->get_total_website_vinegars();

        $total_web_flavor_and_infused = $this->get_total_web_flavored_and_infused();
        $total_website_olive_oils = $this->get_total_website_olive_oils();
        $total_website_other = $this->get_total_website_other();
        $total_website_vinegars = $this->get_total_website_vinegars();

        $total_etched_bottles = $this->get_total_etched_bottles();

        $total =
           $total_drums +
           $total_totes +
           $total_organic +
           $total_tanks +
           $total_vinegar +
           $total_railcars +
           $total_specialty +
           $total_infused +
           $total_spreads_and_olives +
           $total_finished_product +
           $total_sprays +
           $total_ready_to_ship +
           $total_rework +
           // $total_labels +
           $total_bottles +
           $total_fustis +
           $total_caps +
           $total_cardboard_blank +
           $total_cardboard_printed +
           $total_supplies +
           $total_materials +
           $total_follmer +
           $total_labels_in_house +
           $total_labels_in_labelroom +
           $total_customers_labels +
           $total_flavors +
           $total_flavors_for_samples +
           $total_web_flavor_and_infused +
           $total_website_olive_oils +
           $total_website_other +
           $total_website_vinegars +


           // $total_flavors +
           // $total_website +
           $total_etched_bottles;



        return $total;
    }

    //totals for bank
    public function bank_totals()
    {
       $total_drums = $this->get_total_drums();
        $total_totes = $this->get_total_totes();
        $total_organic = $this->get_total_organics();
        $total_tanks = $this->get_total_tanks();
        $total_vinegar = $this->get_total_vinegars();
        $total_railcars = $this->get_railcars_per_lb_price();
        $total_specialty = $this->get_total_specialties();
        $total_infused = $this->get_total_infused_oil_drums();
        $total_spreads_and_olives = $this->get_total_spreads_and_olives();
        $total_finished_product = $this->get_total_finished_product();
        $total_sprays = $this->get_total_sprays();
        $total_ready_to_ship = $this->get_total_ready_to_ship();
        // $total_rework = $this->get_total_rework();

        // $total_labels = $this->get_total_labels_in_house() +
        //                 $this->get_total_labels_in_labelroom() +
        //                 $this->get_total_customers_labels();

        $total_labels_in_house = $this->get_total_labels_in_house();
        $total_labels_in_labelroom = $this->get_total_labels_in_labelroom();
        $total_customers_labels = $this->get_total_customers_labels();

        $total_bottles = $this->get_total_bottles();
        // $total_fustis = $this->get_total_fustis();
        $total_caps = $this->get_total_caps();
        $total_cardboard_blank = $this->get_total_cardboard_blank();
        $total_cardboard_printed = $this->get_total_cardboard_printed();
        $total_supplies = $this->get_total_supplies();
        // $total_materials = $this->get_total_materials();
        $total_follmer = $this->get_total_follmer();

        // $total_flavors =
        //                 $this->get_total_flavors() +
        //                 $this->get_total_flavors_for_samples();

        $total_flavors = $this->get_total_flavors();
        $total_flavors_for_samples = $this->get_total_flavors_for_samples();



        // $total_website =
        //                 $this->get_total_web_flavored_and_infused() +
        //                 $this->get_total_website_olive_oils() +
        //                 $this->get_total_website_other() +
        //                 $this->get_total_website_vinegars();

        // $total_web_flavor_and_infused = $this->get_total_web_flavored_and_infused();
        // $total_website_olive_oils = $this->get_total_website_olive_oils();
        // $total_website_other = $this->get_total_website_other();
        // $total_website_vinegars = $this->get_total_website_vinegars();

        $total_etched_bottles = $this->get_total_etched_bottles();

        $total =
           $total_drums +
           $total_totes +
           $total_organic +
           $total_tanks +
           $total_vinegar +
           $total_railcars +
           $total_specialty +
           $total_infused +
           $total_spreads_and_olives +
           $total_finished_product +
           $total_sprays +
           $total_ready_to_ship +
           // $total_rework +
           // $total_labels +
           $total_bottles +
           // $total_fustis +
           $total_caps +
           $total_cardboard_blank +
           $total_cardboard_printed +
           $total_supplies +
           // $total_materials +
           $total_follmer +
           $total_labels_in_house +
           $total_labels_in_labelroom +
           $total_customers_labels +
           $total_flavors +
           $total_flavors_for_samples +
           // $total_web_flavor_and_infused +
           // $total_website_olive_oils +
           // $total_website_other +
           // $total_website_vinegars +


           // $total_flavors +
           // $total_website +
           $total_etched_bottles;


        return $total;
    }

//end class
}
