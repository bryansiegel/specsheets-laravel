<?php 

class InventoryHistory {


/*
* Counts
* @return histories.count from lotlogs
*/
	public function lastMonthCount()
	{
		$last_month = Carbon::now()->month - "1";
		$this_year = Carbon::now()->year;

		$last_month_query = DB::select(DB::raw(
						"	select lotlogs.id, histories.count, lotlogs.product_category, histories.year, lotlogs.size, lotlogs.oil_type, histories.month
							from lotlogs 
							inner join histories 
							on lotlogs.id = histories.lotlog_id 
							where histories.month = '$last_month'
							and histories.year = '$this_year'
						"));

//get the count from histories.count
		$value = 0;
		foreach ($last_month_query as $item) {
			$item_count = $item->count;
			$value += $item_count;
		}
		return $value;
		}
	

	public function thisMonthCount()
	{
		$this_month = Carbon::now()->month;
		$this_year = Carbon::now()->year;

		$this_month_query = DB::select(DB::raw(
						"select lotlogs.id, histories.count, lotlogs.product_category, histories.year, lotlogs.size, lotlogs.oil_type, histories.month
							from lotlogs 
							inner join histories 
							on lotlogs.id = histories.lotlog_id 
							where histories.month = '$this_month'
							and histories.year = '$this_year'
						"));

//get the count from histories.count
		$value = 0;
		foreach ($this_month_query as $item) {
			$item_count = $item->count;
			$value += $item_count;
		}
		return $value;
	}

/*
* Months
* @return histories.month from Lotlogs
*/

public function thisMonth()
{
	$this_month = Carbon::now()->month;

switch ($this_month) {
        case '1':
            return 'January';
            break;
        case '2':
            return 'February';
            break;
        case '3':
            return 'March';
            break;
        case '4':
            return 'April';
            break;
        case '5':
            return 'May';
            break;
        case '6':
            return 'June';
            break;
        case '7':
            return 'July';
            break;
        case '8':
            return 'August';
            break;
        case '9':
            return 'September';
            break;
        case '10':
            return 'October';
            break;
        case '11':
            return 'November';
            break;
        case '12':
            return 'December';
            break;
        case '0':
            return 'December';
            break;
        case '-1':
            return 'November';
            break;
        case '-2':
            return 'October';
            break;
        case '-3':
            return 'September';
            break;
        case '-4':
            return 'August';
            break;
        case '-5':
            return 'July';
            break;
        case '-6':
            return 'June';
            break;
        case '-7':
            return 'May';
            break;
        case '-8':
            return 'April';
            break;
        case '-9':
            return 'March';
            break;
        case '-10':
            return 'Febuary';
            break;
        case '-11':
            return 'January';
            break;
        default:
            return 'Month Missing';
            break;
    }

	
}

public function lastMonth()
{
	$last_month = Carbon::now()->month - "1";

switch ($last_month) {
        case '1':
            return 'January';
            break;
        case '2':
            return 'February';
            break;
        case '3':
            return 'March';
            break;
        case '4':
            return 'April';
            break;
        case '5':
            return 'May';
            break;
        case '6':
            return 'June';
            break;
        case '7':
            return 'July';
            break;
        case '8':
            return 'August';
            break;
        case '9':
            return 'September';
            break;
        case '10':
            return 'October';
            break;
        case '11':
            return 'November';
            break;
        case '12':
            return 'December';
            break;
        case '0':
            return 'December';
            break;
        case '-1':
            return 'November';
            break;
        case '-2':
            return 'October';
            break;
        case '-3':
            return 'September';
            break;
        case '-4':
            return 'August';
            break;
        case '-5':
            return 'July';
            break;
        case '-6':
            return 'June';
            break;
        case '-7':
            return 'May';
            break;
        case '-8':
            return 'April';
            break;
        case '-9':
            return 'March';
            break;
        case '-10':
            return 'Febuary';
            break;
        case '-11':
            return 'January';
            break;
        default:
            return 'Month Missing';
            break;
    }
	
}

public function threeMonthsAgo()
{
    $last_month = Carbon::now()->month - "3";

switch ($last_month) {
        case '1':
            return 'January';
            break;
        case '2':
            return 'February';
            break;
        case '3':
            return 'March';
            break;
        case '4':
            return 'April';
            break;
        case '5':
            return 'May';
            break;
        case '6':
            return 'June';
            break;
        case '7':
            return 'July';
            break;
        case '8':
            return 'August';
            break;
        case '9':
            return 'September';
            break;
        case '10':
            return 'October';
            break;
        case '11':
            return 'November';
            break;
        case '12':
            return 'December';
            break;
        case '0':
            return 'December';
            break;
        case '-1':
            return 'November';
            break;
        case '-2':
            return 'October';
            break;
        case '-3':
            return 'September';
            break;
        case '-4':
            return 'August';
            break;
        case '-5':
            return 'July';
            break;
        case '-6':
            return 'June';
            break;
        case '-7':
            return 'May';
            break;
        case '-8':
            return 'April';
            break;
        case '-9':
            return 'March';
            break;
        case '-10':
            return 'Febuary';
            break;
        case '-11':
            return 'January';
            break;
        default:
            return 'Month Missing';
            break;
    }
}

public function fourMonthsAgo()
{
    $last_month = Carbon::now()->month - "4";

 switch ($last_month) {
        case '1':
            return 'January';
            break;
        case '2':
            return 'February';
            break;
        case '3':
            return 'March';
            break;
        case '4':
            return 'April';
            break;
        case '5':
            return 'May';
            break;
        case '6':
            return 'June';
            break;
        case '7':
            return 'July';
            break;
        case '8':
            return 'August';
            break;
        case '9':
            return 'September';
            break;
        case '10':
            return 'October';
            break;
        case '11':
            return 'November';
            break;
        case '12':
            return 'December';
            break;
        case '0':
            return 'December';
            break;
        case '-1':
            return 'November';
            break;
        case '-2':
            return 'October';
            break;
        case '-3':
            return 'September';
            break;
        case '-4':
            return 'August';
            break;
        case '-5':
            return 'July';
            break;
        case '-6':
            return 'June';
            break;
        case '-7':
            return 'May';
            break;
        case '-8':
            return 'April';
            break;
        case '-9':
            return 'March';
            break;
        case '-10':
            return 'Febuary';
            break;
        case '-11':
            return 'January';
            break;
        default:
            return 'Month Missing';
            break;
    }
}

public function fiveMonthsAgo()
{
    $last_month = Carbon::now()->month - "5";

 switch ($last_month) {
        case '1':
            return 'January';
            break;
        case '2':
            return 'February';
            break;
        case '3':
            return 'March';
            break;
        case '4':
            return 'April';
            break;
        case '5':
            return 'May';
            break;
        case '6':
            return 'June';
            break;
        case '7':
            return 'July';
            break;
        case '8':
            return 'August';
            break;
        case '9':
            return 'September';
            break;
        case '10':
            return 'October';
            break;
        case '11':
            return 'November';
            break;
        case '12':
            return 'December';
            break;
        case '0':
            return 'December';
            break;
        case '-1':
            return 'November';
            break;
        case '-2':
            return 'October';
            break;
        case '-3':
            return 'September';
            break;
        case '-4':
            return 'August';
            break;
        case '-5':
            return 'July';
            break;
        case '-6':
            return 'June';
            break;
        case '-7':
            return 'May';
            break;
        case '-8':
            return 'April';
            break;
        case '-9':
            return 'March';
            break;
        case '-10':
            return 'Febuary';
            break;
        case '-11':
            return 'January';
            break;
        default:
            return 'Month Missing';
            break;
    }
}

public function sixMonthsAgo()
{
    $last_month = Carbon::now()->month - "6";

 switch ($last_month) {
        case '1':
            return 'January';
            break;
        case '2':
            return 'February';
            break;
        case '3':
            return 'March';
            break;
        case '4':
            return 'April';
            break;
        case '5':
            return 'May';
            break;
        case '6':
            return 'June';
            break;
        case '7':
            return 'July';
            break;
        case '8':
            return 'August';
            break;
        case '9':
            return 'September';
            break;
        case '10':
            return 'October';
            break;
        case '11':
            return 'November';
            break;
        case '12':
            return 'December';
            break;
        case '0':
            return 'December';
            break;
        case '-1':
            return 'November';
            break;
        case '-2':
            return 'October';
            break;
        case '-3':
            return 'September';
            break;
        case '-4':
            return 'August';
            break;
        case '-5':
            return 'July';
            break;
        case '-6':
            return 'June';
            break;
        case '-7':
            return 'May';
            break;
        case '-8':
            return 'April';
            break;
        case '-9':
            return 'March';
            break;
        case '-10':
            return 'Febuary';
            break;
        case '-11':
            return 'January';
            break;
        default:
            return 'Month Missing';
            break;
    }
}

public function sevenMonthsAgo()
{
    $last_month = Carbon::now()->month - "7";

 switch ($last_month) {
        case '1':
            return 'January';
            break;
        case '2':
            return 'February';
            break;
        case '3':
            return 'March';
            break;
        case '4':
            return 'April';
            break;
        case '5':
            return 'May';
            break;
        case '6':
            return 'June';
            break;
        case '7':
            return 'July';
            break;
        case '8':
            return 'August';
            break;
        case '9':
            return 'September';
            break;
        case '10':
            return 'October';
            break;
        case '11':
            return 'November';
            break;
        case '12':
            return 'December';
            break;
        case '0':
            return 'December';
            break;
        case '-1':
            return 'November';
            break;
        case '-2':
            return 'October';
            break;
        case '-3':
            return 'September';
            break;
        case '-4':
            return 'August';
            break;
        case '-5':
            return 'July';
            break;
        case '-6':
            return 'June';
            break;
        case '-7':
            return 'May';
            break;
        case '-8':
            return 'April';
            break;
        case '-9':
            return 'March';
            break;
        case '-10':
            return 'Febuary';
            break;
        case '-11':
            return 'January';
            break;
        default:
            return 'Month Missing';
            break;
    }
}

public function eightMonthsAgo()
{
    $last_month = Carbon::now()->month - "8";

 switch ($last_month) {
        case '1':
            return 'January';
            break;
        case '2':
            return 'February';
            break;
        case '3':
            return 'March';
            break;
        case '4':
            return 'April';
            break;
        case '5':
            return 'May';
            break;
        case '6':
            return 'June';
            break;
        case '7':
            return 'July';
            break;
        case '8':
            return 'August';
            break;
        case '9':
            return 'September';
            break;
        case '10':
            return 'October';
            break;
        case '11':
            return 'November';
            break;
        case '12':
            return 'December';
            break;
        case '0':
            return 'December';
            break;
        case '-1':
            return 'November';
            break;
        case '-2':
            return 'October';
            break;
        case '-3':
            return 'September';
            break;
        case '-4':
            return 'August';
            break;
        case '-5':
            return 'July';
            break;
        case '-6':
            return 'June';
            break;
        case '-7':
            return 'May';
            break;
        case '-8':
            return 'April';
            break;
        case '-9':
            return 'March';
            break;
        case '-10':
            return 'Febuary';
            break;
        case '-11':
            return 'January';
            break;
        default:
            return 'Month Missing';
            break;
    }
}

public function nineMonthsAgo()
{
    $last_month = Carbon::now()->month - "9";

 switch ($last_month) {
        case '1':
            return 'January';
            break;
        case '2':
            return 'February';
            break;
        case '3':
            return 'March';
            break;
        case '4':
            return 'April';
            break;
        case '5':
            return 'May';
            break;
        case '6':
            return 'June';
            break;
        case '7':
            return 'July';
            break;
        case '8':
            return 'August';
            break;
        case '9':
            return 'September';
            break;
        case '10':
            return 'October';
            break;
        case '11':
            return 'November';
            break;
        case '12':
            return 'December';
            break;
        case '0':
            return 'December';
            break;
        case '-1':
            return 'November';
            break;
        case '-2':
            return 'October';
            break;
        case '-3':
            return 'September';
            break;
        case '-4':
            return 'August';
            break;
        case '-5':
            return 'July';
            break;
        case '-6':
            return 'June';
            break;
        case '-7':
            return 'May';
            break;
        case '-8':
            return 'April';
            break;
        case '-9':
            return 'March';
            break;
        case '-10':
            return 'Febuary';
            break;
        case '-11':
            return 'January';
            break;
        default:
            return 'Month Missing';
            break;
    }
}

public function tenMonthsAgo()
{
    $last_month = Carbon::now()->month - "10";

switch ($last_month) {
        case '1':
            return 'January';
            break;
        case '2':
            return 'February';
            break;
        case '3':
            return 'March';
            break;
        case '4':
            return 'April';
            break;
        case '5':
            return 'May';
            break;
        case '6':
            return 'June';
            break;
        case '7':
            return 'July';
            break;
        case '8':
            return 'August';
            break;
        case '9':
            return 'September';
            break;
        case '10':
            return 'October';
            break;
        case '11':
            return 'November';
            break;
        case '12':
            return 'December';
            break;
        case '0':
            return 'December';
            break;
        case '-1':
            return 'November';
            break;
        case '-2':
            return 'October';
            break;
        case '-3':
            return 'September';
            break;
        case '-4':
            return 'August';
            break;
        case '-5':
            return 'July';
            break;
        case '-6':
            return 'June';
            break;
        case '-7':
            return 'May';
            break;
        case '-8':
            return 'April';
            break;
        case '-9':
            return 'March';
            break;
        case '-10':
            return 'Febuary';
            break;
        case '-11':
            return 'January';
            break;
        default:
            return 'Month Missing';
            break;
    }
}

public function elevenMonthsAgo()
{
    $last_month = Carbon::now()->month - "11";

switch ($last_month) {
        case '1':
            return 'January';
            break;
        case '2':
            return 'February';
            break;
        case '3':
            return 'March';
            break;
        case '4':
            return 'April';
            break;
        case '5':
            return 'May';
            break;
        case '6':
            return 'June';
            break;
        case '7':
            return 'July';
            break;
        case '8':
            return 'August';
            break;
        case '9':
            return 'September';
            break;
        case '10':
            return 'October';
            break;
        case '11':
            return 'November';
            break;
        case '12':
            return 'December';
            break;
        case '0':
            return 'December';
            break;
        case '-1':
            return 'November';
            break;
        case '-2':
            return 'October';
            break;
        case '-3':
            return 'September';
            break;
        case '-4':
            return 'August';
            break;
        case '-5':
            return 'July';
            break;
        case '-6':
            return 'June';
            break;
        case '-7':
            return 'May';
            break;
        case '-8':
            return 'April';
            break;
        case '-9':
            return 'March';
            break;
        case '-10':
            return 'Febuary';
            break;
        case '-11':
            return 'January';
            break;
        default:
            return 'Month Missing';
            break;
    }
}

public function twelveMonthsAgo()
{
    $last_month = Carbon::now()->month - "12";

switch ($last_month) {
        case '1':
            return 'January';
            break;
        case '2':
            return 'February';
            break;
        case '3':
            return 'March';
            break;
        case '4':
            return 'April';
            break;
        case '5':
            return 'May';
            break;
        case '6':
            return 'June';
            break;
        case '7':
            return 'July';
            break;
        case '8':
            return 'August';
            break;
        case '9':
            return 'September';
            break;
        case '10':
            return 'October';
            break;
        case '11':
            return 'November';
            break;
        case '12':
            return 'December';
            break;
        case '0':
            return 'December';
            break;
        case '-1':
            return 'November';
            break;
        case '-2':
            return 'October';
            break;
        case '-3':
            return 'September';
            break;
        case '-4':
            return 'August';
            break;
        case '-5':
            return 'July';
            break;
        case '-6':
            return 'June';
            break;
        case '-7':
            return 'May';
            break;
        case '-8':
            return 'April';
            break;
        case '-9':
            return 'March';
            break;
        case '-10':
            return 'Febuary';
            break;
        case '-11':
            return 'January';
            break;
        default:
            return 'Month Missing';
            break;
    }
}

public function thirteenMonthsAgo()
{
    $last_month = Carbon::now()->month - "13";

switch ($last_month) {
        case '1':
            return 'January';
            break;
        case '2':
            return 'February';
            break;
        case '3':
            return 'March';
            break;
        case '4':
            return 'April';
            break;
        case '5':
            return 'May';
            break;
        case '6':
            return 'June';
            break;
        case '7':
            return 'July';
            break;
        case '8':
            return 'August';
            break;
        case '9':
            return 'September';
            break;
        case '10':
            return 'October';
            break;
        case '11':
            return 'November';
            break;
        case '12':
            return 'December';
            break;
        case '0':
            return 'December';
            break;
        case '-1':
            return 'November';
            break;
        case '-2':
            return 'October';
            break;
        case '-3':
            return 'September';
            break;
        case '-4':
            return 'August';
            break;
        case '-5':
            return 'July';
            break;
        case '-6':
            return 'June';
            break;
        case '-7':
            return 'May';
            break;
        case '-8':
            return 'April';
            break;
        case '-9':
            return 'March';
            break;
        case '-10':
            return 'Febuary';
            break;
        case '-11':
            return 'January';
            break;
        default:
            return 'Month Missing';
            break;
    }
}


//last month
//drums

	    public function last_month_drums()
    {

        $last_month = Carbon::now()->month - "1";
        $current_year = Carbon::now()->year;

        //get all drum counts from this month and year
        $drums = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'drums' and histories.month = '$last_month' and histories.year = '$current_year'"));

        //get the count of all the drums from this month and year
        $value = 0;
        foreach ($drums as $drum) {
            $drum_count = $drum->count;
            $value += $drum_count;
        }
        return $value;
    }

//totes
    public function last_month_totes()
    {
        $last_month = Carbon::now()->month - "1";
        $current_year = Carbon::now()->year;

        //get all of the tote counts from this month and this year
        $totes = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'totes' and histories.month = '$last_month' and '$current_year'"));

        //get the count of all totes from this month and year
        $value = 0;
        foreach ($totes as $tote) {
            $tote_count = $tote->count;
            $value += $tote_count;
        }
        return $value;
    }

     //organic
    public function last_month_organics()
    {
        
        $last_month = Carbon::now()->month - "1";
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $organics = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'organic' and histories.month = '$last_month' and '$current_year'"));

        //get the count of all from this month and year



        $value = 0;
        foreach ($organics as $organic) {
            $organic_count = $organic->count;
            $value += $organic_count;
        }
        return $value;
    }


  //tanks
    public function last_month_tanks()
    {
        
        $last_month = Carbon::now()->month - "1";
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $tanks = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'tanks' and histories.month = '$last_month' and '$current_year'"));

        //get the count of all from this month and year

        $value = 0;
        foreach ($tanks as $tank) {
            $tank_count = $tank->count;
            $value += $tank_count;
        }
        return $value;
    }

    //vinegars
    public function last_month_vinegars()
    {
        
        $last_month = Carbon::now()->month - "1";
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $vinegars = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'vinegars' and histories.month = '$last_month' and '$current_year'"));

        //get the count of all from this month and year

        $value = 0;
        foreach ($vinegars as $vinegar) {
            $vinegar_count = $vinegar->count;
            $value += $vinegar_count;
        }
        return $value;
    }

    //website vinegars
    public function last_month_website_vinegars()
    {
        
        $last_month = Carbon::now()->month - "1";
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $website_vinegars = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'website_vinegars' and histories.month = '$last_month' and '$current_year'"));

        //get the count of all from this month and year

        $value = 0;
        foreach ($website_vinegars as $website_vinegar) {
            $website_vinegars_count = $website_vinegar->count;
            $value += $website_vinegars_count;
        }
        return $value;
    }

        //railcars
    public function last_month_railcars()
    {
        
        $last_month = Carbon::now()->month - "1";
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $railcars = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'railcars' and histories.month = '$last_month' and '$current_year'"));

        //get the count of all from this month and year

        $value = 0;
        foreach ($railcars as $railcar) {
            $railcars_count = $railcar->count;
            $value += $railcars_count;
        }
        return $value;
    }

     //specialties
    public function last_month_specialties()
    {
        
        $last_month = Carbon::now()->month - "1";
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $specialties = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'specialty' and histories.month = '$last_month' and '$current_year'"));



        $value = 0;
        foreach ($specialties as $specialty) {
            $specialties_count = $specialty->count;
            $value += $specialties_count;
        }
        return $value;
    }

    //infused_oil_drums
    public function last_month_infused_oil_drums()
    {
        
        $last_month = Carbon::now()->month - "1";
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $infused_oil_drums = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'infused_oil_drums' and histories.month = '$last_month' and '$current_year'"));

        $value = 0;
        foreach ($infused_oil_drums as $infused_oil_drum) {
            $infused_oil_drums_count = $infused_oil_drum->count;
            $value += $infused_oil_drums_count;
        }
        return $value;
    }

        //spreads_and_olives
    public function last_month_spreads_and_olives()
    {
        
        $last_month = Carbon::now()->month - "1";
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $spreads_and_olives = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'spreads_and_olives' and histories.month = '$last_month' and '$current_year'"));



        $value = 0;
        foreach ($spreads_and_olives as $spreads_and_olive) {
            $spreads_and_olives_count = $spreads_and_olive->count;
            $value += $spreads_and_olives_count;
        }
        return $value;
    }

    //web flavored and infused
    public function last_month_web_flavor_and_infused()
    {
        
        $last_month = Carbon::now()->month - "1";
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $web_flavored_and_infuseds = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'web_flavor_and_infused' and histories.month = '$last_month' and '$current_year'"));


        $value = 0;
        foreach ($web_flavored_and_infuseds as $web_flavored_and_infused) {
            $web_flavored_and_infused_count = $web_flavored_and_infused->count;
            $value += $web_flavored_and_infused_count;
        }
        return $value;
    }

    //website_olive_oils
    public function last_month_website_olive_oils()
    {
        
        $last_month = Carbon::now()->month - "1";
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $website_olive_oils = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'website_olive_oils' and histories.month = '$last_month' and '$current_year'"));


        $value = 0;
        foreach ($website_olive_oils as $website_olive_oil) {
            $website_olive_oil_count = $website_olive_oil->count;
            $value += $website_olive_oil_count;
        }
        return $value;
    }

     //website_other
    public function last_month_website_other()
    {
        
        $last_month = Carbon::now()->month - "1";
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $website_others = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'website_other' and histories.month = '$last_month' and '$current_year'"));

        $value = 0;
        foreach ($website_others as $website_other) {
            $website_other_count = $website_other->count;
            $value += $website_other_count;
        }
        return $value;
    }

    //finished_product
    public function last_month_finished_product()
    {
        
        $last_month = Carbon::now()->month - "1";
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $finished_products = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'finished_product' and histories.month = '$last_month' and '$current_year'"));

        $value = 0;
        foreach ($finished_products as $finished_product) {
            $finished_product_count = $finished_product->count;
            $value += $finished_product_count;
        }
        return $value;
    }

    //sprays
    public function last_month_sprays()
    {
        $last_month = Carbon::now()->month - "1";
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $sprays = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'sprays' and histories.month = '$last_month' and '$current_year'"));


        $value = 0;
        foreach ($sprays as $spray) {
            $sprays_count = $spray->count;
            $value += $sprays_count;
        }
        return $value;
    }

    //ready_to_ship
    public function last_month_ready_to_ship()
    {
        $last_month = Carbon::now()->month - "1";
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $ready_to_ships = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'ready_to_ship' and histories.month = '$last_month' and '$current_year'"));


        $value = 0;
        foreach ($ready_to_ships as $ready_to_ship) {
            $ready_to_ships_count = $ready_to_ship->count;
            $value += $ready_to_ships_count;
        }
        return $value;
    }

    //rework
    public function last_month_rework()
    {
        $last_month = Carbon::now()->month - "1";
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $reworks = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'rework' and histories.month = '$last_month' and '$current_year'"));

        $value = 0;
        foreach ($reworks as $rework) {
            $rework_count = $rework->count;
            $value += $rework_count;
        }
        return $value;
    }

    //labels_in_house
    public function last_month_labels_in_house()
    {

        $last_month = Carbon::now()->month - "1";
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $labels_in_houses = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'labels_in_house' and histories.month = '$last_month' and '$current_year'"));

        $value = 0;
        foreach ($labels_in_houses as $labels_in_house) {
            $labels_in_house_count = $labels_in_house->count;
            $value += $labels_in_house_count;
        }
        return $value;
    }

    //labels_in_labelroom
    public function last_month_labels_in_labelroom()
    {

        $last_month = Carbon::now()->month - "1";
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $labels_in_labelrooms = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'labels_in_labelroom' and histories.month = '$last_month' and '$current_year'"));

        $value = 0;
        foreach ($labels_in_labelrooms as $labels_in_labelroom) {
            $labels_in_labelroom_count = $labels_in_labelroom->count;
            $value += $labels_in_labelroom_count;
        }
        return $value;
    }

   //customers_labels
    public function last_month_customers_labels()
    {
        $last_month = Carbon::now()->month - "1";
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $customers_labels = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'customers_labels' and histories.month = '$last_month' and '$current_year'"));

        $value = 0;
        foreach ($customers_labels as $customers_label) {
            $customers_labels_count = $customers_label->count;
            $value += $customers_labels_count;
        }
        return $value;
    }

   //bottles
    public function last_month_bottles()
    {
        $last_month = Carbon::now()->month - "1";
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $bottles = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'bottles' and histories.month = '$last_month' and '$current_year'"));


        $value = 0;
        foreach ($bottles as $bottle) {
            $bottles_count = $bottle->count;
            $value += $bottles_count;
        }
        return $value;
    }

       //etched_bottles
    public function last_month_etched_bottles()
    {
        $last_month = Carbon::now()->month - "1";
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $etched_bottles = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'etched_bottles' and histories.month = '$last_month' and '$current_year'"));


        $value = 0;
        foreach ($etched_bottles as $etched_bottle) {
            $etched_bottles_count = $etched_bottle->count;
            $value += $etched_bottles_count;
        }
        return $value;
    }

       //fustis
    public function last_month_fustis()
    {
        $last_month = Carbon::now()->month - "1";
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $fustis = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'fustis' and histories.month = '$last_month' and '$current_year'"));



        $value = 0;
        foreach ($fustis as $fusti) {
            $fustis_count = $fusti->count;
            $value += $fustis_count;
        }
        return $value;
    }

     //caps
    public function last_month_caps()
    {
        // $caps = Lotlog::where('product_category', 'caps')->get();

        $last_month = Carbon::now()->month - "1";
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $caps = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'caps' and histories.month = '$last_month' and '$current_year'"));


        $value = 0;
        foreach ($caps as $cap) {
            $caps_count = $cap->count;
            $value += $caps_count;
        }
        return $value;
    }

    //cardboard_blank
    public function last_month_cardboard_blank()
    {
        // $cardboard_blanks = Lotlog::where('product_category', 'cardboard_blank')->get();
        $last_month = Carbon::now()->month - "1";
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $cardboard_blanks = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'cardboard_blank' and histories.month = '$last_month' and '$current_year'"));


        $value = 0;
        foreach ($cardboard_blanks as $cardboard_blank) {
            $cardboard_blank_count = $cardboard_blank->count;
            $value += $cardboard_blank_count;
        }
        return $value;
    }

        //cardboard_blank
    public function last_month_cardboard_printed()
    {
        
        $last_month = Carbon::now()->month - "1";
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $cardboard_printed = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'cardboard_printed' and histories.month = '$last_month' and '$current_year'"));


        $value = 0;
        foreach ($cardboard_printed as $cardboard_prints) {
            $cardboard_printed_count = $cardboard_prints->count;
            $value += $cardboard_printed_count;
        }
        return $value;
    }

     //supplies
    public function last_month_supplies()
    {
        // $supplies = Lotlog::where('product_category', 'supplies')->get();

        $last_month = Carbon::now()->month - "1";
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $supplies = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'supplies' and histories.month = '$last_month' and '$current_year'"));


        $value = 0;
        foreach ($supplies as $supply) {
            $supplies_count = $supply->count;
            $value += $supplies_count;
        }
        return $value;
    }

       //materials
    public function last_month_materials()
    {
        // $materials = Lotlog::where('product_category', 'materials')->get();
        
        $last_month = Carbon::now()->month - "1";
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $materials = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'materials' and histories.month = '$last_month' and '$current_year'"));


        $value = 0;
        foreach ($materials as $material) {
            $materials_count = $material->count;
            $value += $materials_count;
        }
        return $value;
    }

        //follmer
    public function last_month_follmer()
    {
        // $follmers = Lotlog::where('product_category', 'follmer')->get();

        $last_month = Carbon::now()->month - "1";
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $follmers = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'follmer' and histories.month = '$last_month' and '$current_year'"));


        $value = 0;
        foreach ($follmers as $follmer) {
            $follmer_count = $follmer->count;
            $value += $follmer_count;
        }
        return $value;
    }

    //flavors
    public function last_month_flavors()
    {
        // $flavors = Lotlog::where('product_category', 'flavors')->get();

        $last_month = Carbon::now()->month - "1";
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $flavors = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'flavors' and histories.month = '$last_month' and '$current_year'"));


        $value = 0;
        foreach ($flavors as $flavor) {
            $flavors_count = $flavor->count;
            $value += $flavors_count;
        }
        return $value;
    }

        //flavors_for_samples
    public function last_month_flavors_for_samples()
    {
        // $flavors_for_samples = Lotlog::where('product_category', 'flavors_for_samples')->get();
        
        $last_month = Carbon::now()->month - "1";
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $flavors_for_samples = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'flavors_for_samples' and histories.month = '$last_month' and '$current_year'"));

        $value = 0;
        foreach ($flavors_for_samples as $flavors_for_sample) {
            $flavors_for_samples_count = $flavors_for_sample->count;
            $value += $flavors_for_samples_count;
        }
        return $value;
    }


//end class
}