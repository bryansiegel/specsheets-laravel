<?php

class RmasController extends \BaseController {

	/**
	 * Display a listing of rmas
	 *
	 * @return Response
	 */
	public function index()
	{
		$rmas = Rma::all();

		return View::make('rmas.index', compact('rmas'));
	}

	/**
	 * Show the form for creating a new rma
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('rmas.create');
	}

	/**
	 * Store a newly created rma in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Rma::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Rma::create($data);

		return Redirect::route('rmas.index');
	}

	/**
	 * Display the specified rma.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$rma = Rma::findOrFail($id);

		return View::make('rmas.show', compact('rma'));
	}

	/**
	 * Show the form for editing the specified rma.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$rma = Rma::find($id);

		return View::make('rmas.edit', compact('rma'));
	}

	/**
	 * Update the specified rma in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rma = Rma::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Rma::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$rma->update($data);

		return Redirect::route('rmas.index');
	}

	/**
	 * Remove the specified rma from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Rma::destroy($id);

		return Redirect::route('rmas.index');
	}

}
