<?php

class Receivinglog2018Controller extends \BaseController {

	/**
	 * Display a listing of receivinglog2018s
	 *
	 * @return Response
	 */
	public function index()
	{
$receivinglogs = DB::table('receivingappointmentlogs')
								->where('date', 'LIKE', '%2018')
								->where('active', '=', '2')
								->orderBy('date', 'asc')
								->get();

		return View::make('receivinglogs2018.index', compact('receivinglogs'));
	}

	/**
	 * Show the form for creating a new receivinglog2018
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created receivinglog2018 in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Receivingappointmentlog::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Receivingappointmentlog::create($data);

		return Redirect::route('receivinglogs2018.index');
	}

	/**
	 * Display the specified receivinglog2018.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$receivinglog = Receivingappointmentlog::findOrFail($id);

		return View::make('receivinglogs2018.show', compact('receivinglog'));
	}

	/**
	 * Show the form for editing the specified receivinglog2018.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$receivinglog = Receivingappointmentlog::find($id);

		return View::make('receivinglogs2018.edit', compact('receivinglog'));
	}

	/**
	 * Update the specified receivinglog2018 in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$$receivinglog = Receivingappointmentlog::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Receivinglog2018::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$$receivinglog->update($data);

		return Redirect::route('receivinglogs2018.index');
	}

	/**
	 * Remove the specified receivinglog2018 from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Receivingappointmentlog::destroy($id);

		return Redirect::route('receivinglogs2018.index');
	}

		/**
	 * Print all of Recievinglog2015 from storage
	 *
	 * @return Response
	 */

	public function print_page() {
		$receivinglogs = DB::table('receivingappointmentlogs')
								->where('date', 'LIKE', '%2018')
								->where('active', '=', '2')
								->get();

		return View::make('receivinglogs2018.print', compact('receivinglogs'));
	}

}
