<?php

class Materiallotlog extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
    protected $fillable = ['lot', 'so', 'original_lot', 'oil_type', 'supplier', 'origin', 'qty', 'size', 'customer', 'date', 'notes1', 'notes2', 'notes3', 'quantity_per_layer', 'quantity_per_pallet'];


}