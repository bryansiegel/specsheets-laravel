<?php

class Lotlog extends \Eloquent
{

    // Add your validation rules here
    public static $rules = [
         // 'lot' => 'required',
         // 'progress' => 'required',
         // 'approved_by' => 'required',

    ];

     public function history()
    {
      return $this->hasMany('History');
    } 

    // Don't forget to fill this array
    protected $fillable = [
            'lot',
            'count',
            'month',
            'year',
            'lotlog_id',
            'active',
            'so',
            'original_lot',
            'oil_type',
            'supplier',
            'approved_by',
            'origin',
            'qty',
            'size',
            'customer',
            'container_number',
            'expiration_date',
            'date',
            'notes1',
            'notes2',
            'notes3',
            'sample_location', 'po',
            'packslip', 'container', 'carrier',
            'product_on_hold',
            'product_contract',
            'kosher_cert_file',
            'year', 'spotchecked', 'paperwork', 'progress', 'progress_notes',
            'im_trucker_inspection',
            'im_date_received',
            'im_labeling',
            'im_bol',
            'im_condition',
            'im_cartons',
            'im_primary_secondary',
            'im_approved_for_use',
            'im_notes',
            'im_date_approved',
            'im_seal',
            'im_not_approved_why',
            'ip_seals',
            'ip_truck_tanker',
            'ip_kosher_shed_a',
            'ip_certificate_of_analysis',
            'ip_weight_certificate',
            'ip_wash_certificate',
            'ip_organic_certificate',
            'ip_caps_and_seals',
            'ip_cartons',
            'ip_drum_condition',
            'ip_labels',
            'ip_product_type',
            'ip_vendor_lot_number',
            'ip_notes',
            'ip_approved_for_use',
            'ip_if_no_explain_why',
            'ip_date_received',
            'ip_approved_by',
            'im_approved_by',
            'product_category',
            'drums_cost_per',
            'railcars_per_lb_price',
            'totes_cost_per',
            'organic_cost_per',
            'tanks_cost_per',
            'vinegars_cost_per',
            'specialty_cost_per',
            'infused_oil_drums_cost_per',
            'spreads_and_olives_cost_per',
            'website_olive_oils_cost_per',
            'web_flavor_and_infused_cost_per',
            'website_vinegars_cost',
            'website_other_cost_per',
            'finished_product_cost_per',
            'sprays_cost_per',
            'ready_to_ship_cost_per',
            'rework_cost_per',
            'labels_in_house_cost_per',
            'labels_in_labelroom',
            'customers_labels_cost_per',
            'bottles_cost_per',
            'etched_bottles_cost_per',
            'fustis_cost_per',
            'caps_price',
            'cardboard_blank_cost_per',
            'cardboard_printed',
            'supplies_cost_per',
            'materials_cost_per',
            'follmer_cost_per',
            'flavors_cost_per',
            'flavors_for_samples',
            'gal_inch',
            'tank_total',
            'extension',
            'lot1',
            'lot2',
            'cases',
            'jars',
            'loads_taken',
            'order_number',
            'in_bulk',
            'in_carton',
            'pcs_plt',
            'placement',
            'per_case',
            'customers_oils_cost_per',
            'sub_product_category',


    ];

    //Relationships
    public function rma()
    {
        return $this->hasOne('Rma');
    }

    public function blankpaperwork()
    {
        return $this->hasOne('Blankpaperwork');
    }
}
