<?php

class Nutritionfact extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = ['title','category', 'active', 'serving_size', 'servings_per_container', 'calories', 'calories_from_fat', 'total_fat_grams', 'total_fat_percent', 'saturated_fat_grams', 'saturated_fat_percentage', 'trans_fat', 'cholesterol_grams', 'cholesterol_percentage', 'sodium_grams', 'sodium_percentage', 'total_carbohydrates_grams', 'total_carbohydrates_percentage', 'dietary_fiber_grams', 'dietary_fiber_percentage', 'sugars_grams', 'protein_grams', 'blurb', 'ingredients', 'vitamin_d_grams', 'vitamin_d_percentage', 'calcium_grams', 'calcium_percentage', 'iron_grams', 'iron_percentage', 'potassium_grams', 'potassium_percentage', 'added_sugars' ];

}