<?php

class Producthold extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = [ 
							'date',
							'description',
							'hold_by',
							'hold_starting_date',
							'classification_of_hold',
							'reason_for_hold',
							'action_required_before_release',
							'notes',
							'status',
							'released_notes',
							'released',
							'hold_release_date',
							'person_releasing',
							];

}