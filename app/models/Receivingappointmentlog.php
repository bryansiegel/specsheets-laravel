<?php

class Receivingappointmentlog extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		 // 'customer_product' => 'required'
		 'date' => 'required'

	];

	// Don't forget to fill this array
	protected $fillable = ['active', 'conf_name','conf_number', 'vendor', 'carrier', 'product', 'size', 'in_out', 'date', 'time', 'qty', 'po', 'packslip', 'lot', 'container', 'product_on_hold', 'product_contract', 'condition_of_truck', 'kosher_cert_file', 'customer_product'];

}