<?php

class Rma extends \Eloquent {
	protected $fillable = ['date','request_received_by','company_address1','company_address2', 'contact_phone','email','state','rma_number','city','issued_by','issued_on','good_until','restocking_fee','return_received_on','return_received_by','credit_issued_by','credit_ammount','credit_issued_on','replacement_sent','credit_return_authorized_yes_by'];


//Relationships

	public function lotlog()
	{
		$this->belongsTo('Lotlog');
	}


}