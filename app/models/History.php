<?php

class History extends \Eloquent {
	protected $fillable = ['lotlog_id','date','count', 'year', 'month'];

	public function inventory()
	{
		return $this->belongsTo('Lotlog');
	}


}