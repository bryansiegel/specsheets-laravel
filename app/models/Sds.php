<?php

class Sds extends \Eloquent {

	public function specsheet()
	{
		return $this->belongsTo('Specsheet');
	}
	
	protected $fillable = [			
			'sds_active',
			'sds_name',
			'sds_special_hazard_for_personal',
			'sds_cas_number',
			'sds_upon_inhalation',
			'sds_upon_skin_contact',
			'sds_extinguishing_media',
			'sds_not_to_be_used_extinguishing_media',
			'sds_special_exposure_hazard',
			'sds_special_protection',
			'sds_precautions_related_to_personnel',
			'sds_procedure_for_cleanup',
			'sds_small_spill',
			'sds_large_spill',
			'sds_handling',
			'sds_storage',
			'sds_threshold_limit_value',
			'sds_inhalation_health_risks',
			'sds_skin_absorption',
			'sds_appearance',
			'sds_form',
			'sds_color',
			'sds_odor',
			'sds_melting_point',
			'sds_flash_point',
			'sds_ignition_temperature',
			'sds_density',
			'sds_water_solubility',
			'sds_general_information',
			'sds_conditions_to_be_avoided',
			'sds_dangerous_decomposition_product',
			'sds_toxicological_testing',
			'sds_practical_experience',
			'sds_general_considerations',
			'sds_information_on_product',
			'sds_behavior_in_ecological_setting',
			'sds_toxic_effects_on_the_ecosystem',
			'sds_additional_ecological_information',
			'sds_disposal_considerations',
			'sds_transportation_information',
			'sds_regulatory_information',
			];
}