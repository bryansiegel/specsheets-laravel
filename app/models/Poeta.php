<?php

class Poeta extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = ['date', 'po', 'vendor', 'product', 'amount', 'open_balance', 'eta', 'notes', 'status'];

}