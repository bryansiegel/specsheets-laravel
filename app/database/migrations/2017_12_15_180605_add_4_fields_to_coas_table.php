<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Add4FieldsToCoasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('coas', function(Blueprint $table)
		{
			$table->string('k268');
			$table->string('blue');
			$table->string('neutral');
			$table->string('difference_ecn42');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('coas', function(Blueprint $table)
		{
			$table->dropColumn('k268');
			$table->dropColumn('blue');
			$table->dropColumn('neutral');
			$table->dropColumn('difference_ecn42');
			
		});
	}

}
