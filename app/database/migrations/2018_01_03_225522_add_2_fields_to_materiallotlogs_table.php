<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Add2FieldsToMateriallotlogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('materiallotlogs', function(Blueprint $table)
		{
			$table->string('quantity_per_layer');
			$table->string('quantity_per_pallet');
			
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('materiallotlogs', function(Blueprint $table)
		{
			$table->dropColumn('quantity_per_layer');
			$table->dropColumn('quantity_per_pallet');
		});
	}

}
