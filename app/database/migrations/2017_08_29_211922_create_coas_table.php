<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCoasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('coas', function(Blueprint $table)
		{
			$table->increments('id');
			//
			$table->string('lot');
			$table->string('description');
			$table->string('manf_date');
			$table->string('expiration_date');
			$table->string('origin');
			$table->string('po_number');
			$table->string('item_number');
			//fatty acid
			$table->string('capronic_acid');
			$table->string('caprylic_acid');
			$table->string('capric_acid');
			$table->string('lauric_acid');
			$table->string('myristic');
			$table->string('myistoleic_acid');
			$table->string('palmitic_acid');
			$table->string('palmitoliec_acid');
			$table->string('heptadacanoic_acid');
			$table->string('heptadecenoic_acid');
			$table->string('stearic_acid');
			$table->string('oleic_acid');
			$table->string('linoleic_acid');
			$table->string('linolenic_acid');
			$table->string('octadecatetraen_acid');
			$table->string('arachidic_acid');
			$table->string('gadoleic_acid');
			$table->string('eicosanedienoic_acid');
			$table->string('eicosaneteetraenoic_acid');
			$table->string('behenic_acid');
			$table->string('erucic_acid');
			$table->string('lignoceric_acid');
			$table->string('nervonic_acid');
			$table->string('minor');
			//
			$table->text('additives');
			$table->text('aom');
			$table->text('ash');
			$table->text('absorbtion_in_ultra_violet');
			$table->text('acid_value');
			$table->text('acidity');
			$table->text('alcohol');
			$table->text('appearance');
			$table->text('aroma');
			$table->text('brix');
			$table->text('copper');
			$table->text('cold_test');
			$table->text('color');
			$table->text('color_gardner');
			$table->text('color_lovibond');
			$table->text('color_red');
			$table->text('color_yellow');
			$table->text('color_visual');
			$table->text('developed_alcohol_degree');
			$table->text('dextros_equivalent');
			$table->text('density_20');
			$table->text('ffa');
			$table->text('free_sulpphurous_anhydride');
			$table->text('fructose');
			$table->text('flavor');
			$table->text('free_acidity');
			$table->text('free_fatty_acids');
			$table->text('free_so2');
			$table->text('grains');
			$table->text('glucose');
			$table->text('heavy_metals_as_lead');
			$table->text('hydroxyl_value');
			$table->text('iv_wijs');
			$table->text('iron');
			$table->text('identification_a');
			$table->text('identification_b');
			$table->text('identification_c');
			$table->text('insoluble_impurities');
			$table->text('lead');
			$table->text('maltose');
			$table->text('m_i');
			$table->text('melting_point');
			$table->text('moisture');
			$table->text('moisture_and_impurities');
			$table->text('moisture_and_sediment_content');
			$table->text('moisture_level');
			$table->text('odor');
			$table->text('osi');
			$table->text('other_carbohydrates');
			$table->text('optical_rotation');
			$table->text('pv');
			$table->text('peroxide_value');
			$table->text('pounds_per_gallon');
			$table->text('rifractometric_at_20');
			$table->text('refractive_index');
			$table->text('smoke_point');
			$table->text('specific_gravity');
			$table->text('specific_weight');
			$table->text('starch');
			$table->text('salt');
			$table->text('saponification_value');
			$table->text('saturated_fatty_acids');
			$table->text('specific_weight_as_baumic_degrees');
			$table->text('so2');
			$table->text('tbhq');
			$table->text('total_acidity_as_acetic_acid');
			$table->text('total_dry_extract');
			$table->text('total_monounsaturated');
			$table->text('total_polyunsaturated');
			$table->text('total_saturated');
			$table->text('total_sulphurous_anhydride');
			$table->text('trans_c_18_1');
			$table->text('trans_c_18_2');
			$table->text('c18_1t');
			$table->text('c18_2t_c18_3t');
			$table->text('taste');
			$table->text('total_so2');
			$table->text('trans_fatty_acids');
			$table->text('turbidity');
			$table->text('unsaponifiable_matter');
			$table->text('vitamin_e');
			$table->text('zinc');
			$table->text('ph');
			$table->text('cholesterol');
			$table->text('brassicasterol');
			$table->text('compesterol');
			$table->text('stigmasterol');
			$table->text('delta7_stigmasternol');
			$table->text('beta_sitosterol_delta5_avenasterol');
			$table->text('total_sterols');
			$table->text('iodine_value');
			$table->text('iodine_value_ri');
			$table->text('refactive_index');
			$table->text('saponification');

			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('coas');
	}

}
