<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePoEtasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('PoEtas', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('date');
			$table->string('po');
			$table->string('vendor');
			$table->string('product');
			$table->string('amount');
			$table->string('open_balance');
			$table->string('eta');
			$table->text('notes');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('PoEtas');
	}

}
