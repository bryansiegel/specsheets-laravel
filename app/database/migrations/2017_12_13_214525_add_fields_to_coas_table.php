<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFieldsToCoasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('coas', function(Blueprint $table)
		{
			$table->string('deltak');
			$table->string('232nm');
			$table->string('k270');
			$table->string('im');
			$table->string('pph');
			$table->string('moi');
			$table->string('ind');
			$table->string('bit225');
			$table->string('ppp');
			$table->string('dag');
			$table->string('defects');
			$table->string('fruitness');
			$table->string('bitterness');
			$table->string('pungency');			
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('coas', function(Blueprint $table)
		{
			$table->dropColumn('deltak');
			$table->dropColumn('232nm');
			$table->dropColumn('k270');
			$table->dropColumn('im');
			$table->dropColumn('pph');
			$table->dropColumn('moi');
			$table->dropColumn('ind');
			$table->dropColumn('bit225');
			$table->dropColumn('ppp');
			$table->dropColumn('dag');
			$table->dropColumn('defects');
			$table->dropColumn('fruitness');
			$table->dropColumn('bitterness');
			$table->dropColumn('pungency');	
		});
	}

}
