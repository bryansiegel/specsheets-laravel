<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Print</title>
    <style>
        body {
            margin: 20px;
        }

        li {
            font-size: small;
            font-weight: normal;
        }

        h2 {
            text-align: center;
            font-size: 15px;
            font-family: arial, sans-serif;
        }

        table {
        }

        td {
            font-size: 12px;
            text-align: center;
            font-family: arial, sans-serif;
        }

        th {
            font-size: 11px;
            font-weight: bold;
            text-align: center;
            font-family: arial, sans-serif;
        }

        @media print {
            thead {
                display: table-header-group;
                margin-bottom: 2px;
            }

            .noPrint {
                display: none;
            }

            .product_print {
                width: 450px;
            }

            .container_print {
                width: 200px;
            }

            .lot_print {
                width: 80px;
            }
        }

        @page {

            margin-top: 1cm;
            margin-left: 0cm;
            margin-right: 0cm;
            margin-bottom: 1cm;
            font-size:9px;
        }

    </style>
</head>

<body>
<div class="container">
    <div class="span12">
        @yield('content')
    </div>
</div>
<footer>
</footer>
</body>
</html>