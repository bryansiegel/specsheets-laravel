<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    {{-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> --}}
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title></title>

    <!-- Bootstrap Core CSS -->

    {{ HTML::style('css/bootstrap.css') }}
    {{ HTML::style('css/sb-admin.css') }}
    {{ HTML::style('css/plugins/morris.css') }}
    {{ HTML::style('font-awesome-4.1.0/css/font-awesome.min.css') }}
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    {{ HTML::style('css/jquery.timepicker.css') }}



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->



</head>

<body>



    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/admin">Cibaria International</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>  <?php
                        $username = Auth::user()->username;
                        echo $username;
                        ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        {{--<li>--}}
                            {{--<a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>--}}
                        {{--</li>--}}

                        <?php if(Auth::user()->group == "100") :?>
                        <li>
                            <a href="{{ url() . "/user" }}"><i class="fa fa-fw fa-gear"></i> Users</a>
                        </li>
                        <?php endif; ?>
                        <li class="divider"></li>
                        <li>
                            <a href="{{ url() . "/logout" }}"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="/admin"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    @if(Auth::user()->group != 10)
                    @if(Auth::user()->group != 60)
                    <li>
                        <a href="/specsheets/"><i class="fa fa-fw fa-bar-chart-o"></i> Specsheets</a>
                    </li>
                    @endif
                    @endif
                    @if(Auth::user()->group != 10)
                    @if(Auth::user()->group != 60)
                    <li>
                        <a href="/nutritionfacts/"><i class="fa fa-fw fa-table"></i> Nutrition Facts</a>
                    </li>
                    @endif
                    @endif
                   
                    @if(Auth::user()->group != 10)
                    <li>
                        <a href="javascript:" data-toggle="collapse" data-target="#log"><i class="fa fa-fw fa-edit"></i> Receiving Logs <i class="fa fa-fw fa-caret-down"></i></a>
                    @endif
                   
                    @if(Auth::user()->group != 10)
                        <ul id="log" class="collapse">
                    @endif

                    @if(Auth::user()->group != 10)
                            <li>
                                <a href="/receivingappointmentlogs/">Appointments</a>
                            </li>
                    @endif

                    @if(Auth::user()->group != 10)
                    @if(Auth::user()->group != 60)
                            <li>
                                <a href="/receivinglogs2016">2016</a>
                            </li>
                    @endif
                    @endif

                    @if(Auth::user()->group != 10)
                    @if(Auth::user()->group != 60)
                            <li>
                                <a href="/recievinglog2015s/">2015</a>
                            </li>
                    @endif
                    @endif

                    @if(Auth::user()->group != 10)
                    @if(Auth::user()->group != 60)
                            <li>
                                <a href="/recievinglog2014s/">2014</a>
                            </li>
                    @endif
                    @endif

                    @if(Auth::user()->group != 10)
                    @if(Auth::user()->group != 60)
                            <li>
                                <a href="/receivinglogs/">2013</a>
                            </li>
                    @endif
                    @endif

                    @if(Auth::user()->group != 10)
                    @if(Auth::user()->group != 60)
                            <li>
                                <a href="/receivinglog2012s/">2012</a>
                            </li>
                    @endif
                    @endif

                    @if(Auth::user()->group != 10)
                        </ul>
                    @endif
                    
                    @if(Auth::user()->group != 10)
                    </li>
                    @endif
                   
                    @if(Auth::user()->group != 10)
                    @if(Auth::user()->group != 60)
                      <li>
                          <a href="javascript:" data-toggle="collapse" data-target="#cs"><i class="fa fa-fw fa-edit"></i> Customer Service <i class="fa fa-fw fa-caret-down"></i></a>
                          <ul id="cs" class="collapse">
                              <li>
                                  <a href="/freightquotes/">Freight Quotes</a>
                              </li>
                              <li>
                                  <a href="/customercomplaints">Customer Complaints</a>
                              </li>
                              <li>
                                <a href="/orders">Orders</a>
                              </li>

                          </ul>
                      </li>
                    @endif
                    @endif

                    @if(Auth::user()->group != 10)
                    @if(Auth::user()->group != 60)
                    <li><a href="/shippings/"><i class="fa fa-fw fa-edit"></i>Shipping Logs</a></li>
                    @endif
                    @endif
                    
                    @if(Auth::user()->group != 60)
                    <li><a href="/inventories/"><i class="fa fa-fw fa-edit"></i>Inventory</a></li>
                    @endif

                    @if(Auth::user()->group != 10)
                    <li>
                        <a href="javascript:" data-toggle="collapse" data-target="#qc"><i class="fa fa-fw fa-edit"></i> Q.C. <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="qc" class="collapse">
                        <li><a href="/rmas/">RMA</a></li>
                            <li>
                                <a href="/blends/">Blends</a>
                            </li>
                            <li>
                                <a href="/lotlogs/">Lot Log</a>
                            </li>
                            <li>
                                <a href="/materiallotlogs/">Material Lot Logs</a>
                            </li>
                            <li>
                                <a href="/spotchecks/">Spot Check Form</a>
                            </li>
                            <li><a href="/productholds/">Product Hold</a></li>
                            <li>
                                {{--<a href="/samples/">Samples</a>--}}
                            </li>
                        </ul>
                    </li>
                    @endif



                    {{--<li>--}}
                        {{--<a href="/salescharts/"><i class="fa fa-fw fa-desktop"></i> Sales Chart</a>--}}
                    {{--</li>--}}
                   {{--<li>--}}
                        {{--<a href="bootstrap-grid.html"><i class="fa fa-fw fa-wrench"></i> Bootstrap Grid</a>--}}
                    {{--</li>--}}

                    {{--<li>--}}
                        {{--<a href="blank-page.html"><i class="fa fa-fw fa-file"></i> Blank Page</a>--}}
                    {{--</li>--}}
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">
        
        <!-- vue.js -->
    
            @yield('content')
     
            {{--content--}}

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->


    <!-- jQuery Version 1.11.0 -->
        @yield('footer')

   <script src="/js/jquery-1.11.0.js"></script>
   <script src="/js/ajax.js"></script>
  


</body>

</html>
