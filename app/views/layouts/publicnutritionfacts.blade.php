<!DOCTYPE html>
<html lang='en'>
<head>
    <title></title>
    <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
    <link media="all" type="text/css" rel="stylesheet" href="http://cibariaspecsheets.com/css/publicview/bootstrap.css" />
    <link media="print" type="text/css" rel="stylesheet" href="http://cibariaspecsheets.com/css/publicview/print.css" />
    <link type="text/css" rel="stylesheet" href="http://cibariaspecsheets.com/css/jquery.timepicker.css?1425526398" />
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
    <script type="text/javascript" src="http://cibariaspecsheets.com/js/html2canvas.js?1425526977"></script>
    <script type="text/javascript" src="http://cibariaspecsheets.com/js/jquery.plugin.html2canvas.js?1425526980"></script>
    <script type="text/javascript" src="http://cibariaspecsheets.com/js/jquery.timepicker.js?1425526981"></script>

    <style>
        body { margin: 40px;-webkit-print-color-adjust:exact; }
        .hide { display: none!important; }
    </style>

</head>
<body>

<div class="container">
    <div class="span12">
        @yield('content')
        <input type="submit" value="Print Nutrition Label" onclick="window.print();" class="btn btn-success"/>
        <input type="submit" value="Download" onclick="capture();" style="margin-top:10px"/>
    </div>


</div>


</body>
</html>