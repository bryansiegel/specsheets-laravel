<!DOCTYPE html>
<html lang='en'>
<head>
    <title></title>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
        <link rel='stylesheet' href='//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css'>
        <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

        <style>
        body {
            margin-top:15px;
        }
        </style>
</head>
<body>

<div class="container-fluid">
    <div class="row">

@yield('content')

    </div>
</div>

</body>
</html>