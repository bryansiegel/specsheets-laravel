<!DOCTYPE html>
<html lang="en">
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Print</title>
    <style>
        body {
            margin: 40px;
        }

        li {
            font-size: small;
            font-weight: normal;
        }

        h2 {
            text-align: center;
            font-size: 15px;
            font-family: arial, sans-serif;
        }

        table {
        }

        td {
            font-size: 11px;
            /*text-align: center;*/
            font-family: arial, sans-serif;
        }

        th {
            font-size: 11px;
            font-weight: bold;
            /*text-align: center;*/
            font-family: arial, sans-serif;
        }

        @media print {
            thead {
                display: table-header-group;
                margin-bottom: 2px;
            }

            .noPrint {
                display: none;
            }

            .product_print {
                width: 450px;
            }

            .container_print {
                width: 200px;
            }

            .lot_print {
                width: 80px;
            }
            @page{
                size:landscape;
            }
        }

        @page {
            margin-top: 1cm;
            margin-left: 1cm;
            margin-right: 1cm;
            margin-bottom: .5cm;
            size:landscape;
        }

    </style>
            <link rel='stylesheet' href='//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css'>

</head>

<body>
<div class="container">
    <div class="span12">
        @yield('content')
    </div>
</div>
<footer>
</footer>
</body>
</html>