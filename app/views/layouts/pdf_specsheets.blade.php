<!DOCTYPE html>
<html lang='en'>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>



    <style>
        /*body { margin: 40px;-webkit-print-color-adjust:exact; }*/
        body {
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            font-size: 10px;
             margin: 18px;-webkit-print-color-adjust:exact; 
        }
        .page{margin-bottom:2000px;}
        .msds{height: 50%;}
        .noshow{display: none;}
        .hide { display: none!important; }
        .container,
        .navbar-fixed-top .container,
        .navbar-fixed-bottom .container {
            width: 940px;
        }

        .span12 {
            width: 100%;
        }


    </style>
</head>
<body>

<div class="container">
    <div class="span12">
        @yield('content')
    </div>
</div>


</body>
</html>