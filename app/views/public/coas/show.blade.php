@extends('layouts.publiccoas')

@section('content')


{{-- top --}}
<div style="text-align:center;">
        <img src="{{ url() . "/images/cibaria-logo.jpg" }}" alt="" width="178" height="157"/><br />
        <span style="font-size:14px;margin-left:20px;"><strong>TEL: (951) 823 - 8490 - FAX: (951) 823 - 8495</strong></span><br />
        <span style="font-size:14px;margin-left:20px;"><strong>705 Columbia Ave Riverside, CA 92507</strong></span><br/>
        <br/>
        <br/>
        <h2 style="font-size:26px;margin-left:20px;"><u>CERTIFICATE OF ANALYSIS</u></h2>
        <h2 style="font-size:26px;margin-left:20px;">{{ strtoupper($coa->description) }}</h2>
    </div>
{{-- end top --}}
<br />
<br />

<div style="margin-left:80px;">
<table width="100%">

		@if(!empty($coa->lot))
		<tr>
		<td><strong>LOT NUMBER:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ $coa->lot }}</strong></td>
		<tr>
		@endif

		@if(!empty($coa->origin))
		<tr>
		<td><strong>ORIGIN:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->origin) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->expiration_date))
		<td><strong>MANF DATE:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->manf_date) }}</strong></td>
		</tr>
		@endif
		
		@if(!empty($coa->expiration_date))
		<td><strong>EXP DATE:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->expiration_date) }}</strong></td>
		</tr>
		@endif

<tr>
	<td height="50px"></td>
</tr>
{{-- results --}}


		@if(!empty($coa->absorbtion_in_ultra_violet))
		<td><strong>Absorbtion in Ultra Violet:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->absorbtion_in_ultra_violet) }}</strong></td>
		</tr>
		@endif
		
		@if(!empty($coa->k268))
		<td><strong>k268:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->k268) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->deltak))
		<td><strong>Delta K:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->deltak) }}</strong></td>
		</tr>
		@endif																						
		@if(!empty($coa->nm232))
		<td><strong>232nm:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->nm232) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->acid_value))
		<td><strong>Acid Value:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->acid_value) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->acidity))
		<td><strong>Acidity:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->acidity) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->additives))
		<td><strong>Additives:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->additives) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->alcohol))
		<td><strong>Alcohol:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->alcohol) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->aom))
		<td><strong>AOM:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->aom) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->appearance))
		<td><strong>Appearance:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->appearance) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->arachidic_acid))
		<td><strong>Arachidic Acid:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->arachidic_acid) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->aroma))
		<td><strong>Aroma:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->aroma) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->ash))
		<td><strong>Ash:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->ash) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->behenic_acid))
		<td><strong>Behenic Acid:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->behenic_acid) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->beta_sitosterol_delta5_avenasterol))
		<td><strong>Beta Sitosterol Delta5 Avenasterol:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->beta_sitosterol_delta5_avenasterol) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->brassicasterol))
		<td><strong>Brassicasterol:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->brassicasterol) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->brix))
		<td><strong>Brix:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->brix) }}</strong></td>
		</tr>
		@endif

		{{-- fatty acids --}}

		@if(!empty($coa->C3_0))
		<td><strong>Propionic acid C3:0:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->C3_0) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->C4_0))
		<td><strong>Butyric acid C4:0:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->C4_0) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->C5_0))
		<td><strong>Valeric acid C5:0:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->C5_0) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->C6_0))
		<td><strong>Caproic acid C6:0:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->C6_0) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->C7_0))
		<td><strong>Enanthic acid C7:0:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->C7_0) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->C8_0))
		<td><strong>Caprylic acid C8:0:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->C8_0) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->C9_0))
		<td><strong>Pelargonic acid C9:0:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->C9_0) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->C10_0))
		<td><strong>Capric acid C10:0:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->C10_0) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->C11_0))
		<td><strong>Undecylic acid C11:0:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->C11_0) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->C12_0))
		<td><strong>Lauric acid C12:0:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->C12_0) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->C13_0))
		<td><strong>Tridecylic acid C13:0:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->C13_0) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->C14_0))
		<td><strong>Myristic acid C14:0:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->C14_0) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->C15_0))
		<td><strong>Pentadecylic acid C15:0:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->C15_0) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->C16_0))
		<td><strong>Palmitic acid C16:0:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->C16_0) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->C16_1))
		<td><strong>Palmitoliec acid C16:1:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->C16_1) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->C17_0))
		<td><strong>Margaric acid C17:0:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->C17_0) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->C17_0))
		<td><strong>Heptadecenoic acid C17:1:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->C17_1) }}</strong></td>
		</tr>
		@endif


		@if(!empty($coa->C18_0))
		<td><strong>Stearic acid C18:0:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->C18_0) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->C18_1))
		<td><strong>Oleic acid C18:1:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->C18_1) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->trans_c_18_1))
		<td><strong>Alpha Linolenic C18:3:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->trans_c_18_1) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->c18_1t))
		<td><strong>C18:1T:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->c18_1t) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->C18_2))
		<td><strong>Linoleic acid C18:2:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->C18_2) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->C18_3))
		<td><strong>Alpha Linolenic C18:3:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->C18_3) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->c18_2t_c18_3t))
		<td><strong>C18:2T + C18:3T:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->c18_2t_c18_3t) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->C19_0))
		<td><strong>Nonadecylic acid C19:0:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->C19_0) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->C20_0))
		<td><strong>Arachidic acid C20:0:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->C20_0) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->C20_1))
		<td><strong>Eicosenic Acid C20:1:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->C20_1) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->C21_0))
		<td><strong>Heneicosylic acid C21:0:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->C21_0) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->C22_0))
		<td><strong>Behenic acid C22:0:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->C22_0) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->C23_0))
		<td><strong>Tricosylic acid C23:0:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->C23_0) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->C24_0))
		<td><strong>Lignoceric acid C24:0:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->C24_0) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->C25_0))
		<td><strong>Pentacosylic acid C25:0:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->C25_0) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->C26_0))
		<td><strong>Cerotic acid C26:0:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->C26_0) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->C27_0))
		<td><strong>Heptacosylic acid C27:0:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->C27_0) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->C28_0))
		<td><strong>Montanic acid C28:0:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->C28_0) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->C29_0))
		<td><strong>Nonacosylic acid C29:0:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->C29_0) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->C30_0))
		<td><strong>Melissic acid C30:0:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->C30_0) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->C31_0))
		<td><strong>Henatriacontylic acid C31:0:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->C31_0) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->C32_0))
		<td><strong>Lacceroic acid C32:0:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->C32_0) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->C33_0))
		<td><strong>Psyllic acid C33:0:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->C33_0) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->C34_0))
		<td><strong>Geddic acid C34:0:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->C34_0) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->C35_0))
		<td><strong>Ceroplastic acid C35:0:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->C35_0) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->C36_0))
		<td><strong>Hexatriacontylic acid C36:0:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->C36_0) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->C37_0))
		<td><strong>Heptatriacontanoic acid C37:0:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->C37_0) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->C38_0))
		<td><strong>Octatriacontanoic acid C38:0:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->C38_0) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->campestanol_campesterol))
		<td><strong>Campestanol+Campesterol:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->campestanol_campesterol) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->capric_acid))
		<td><strong>Capric Acid:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->capric_acid) }}</strong></td>
		</tr>
		@endif											

		@if(!empty($coa->capronic_acid))
		<td><strong>Capronic Acid:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->capronic_acid) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->caprylic_acid))
		<td><strong>Caprylic Acid:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->caprylic_acid) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->cholesterol))
		<td><strong>Cholesterol:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->cholesterol) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->cold_test))
		<td><strong>Cold Test:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->cold_test) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->color))
		<td><strong>Color:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->color) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->color_gardner))
		<td><strong>Color Gardner:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->color_gardner) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->color_lovibond))
		<td><strong>Color Lovibond:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->color_lovibond) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->color_red))
		<td><strong>Color Red:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->color_red) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->blue))
		<td><strong>Color Blue:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->blue) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->neutral))
		<td><strong>Color Neutral:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->neutral) }}</strong></td>
		</tr>
		@endif		


		@if(!empty($coa->color_visual))
		<td><strong>Color Visual:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->color_visual) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->color_yellow))
		<td><strong>Color Yellow:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->color_yellow) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->neutral))
		<td><strong>Color Neutral:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->neutral) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->compesterol))
		<td><strong>Compesterol:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->compesterol) }}</strong></td>
		</tr>
		@endif																												
		@if(!empty($coa->copper))
		<td><strong>Copper:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->copper) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->delta7_stigmasternol))
		<td><strong>Delta7 Stigmasternol:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->delta7_stigmasternol) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->density_20))
		<td><strong>Density 20:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->density_20) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->developed_alcohol_degree))
		<td><strong>Developed Alcohol Degree:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->developed_alcohol_degree) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->dextros_equivalent))
		<td><strong>Dextros Equivalent:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->dextros_equivalent) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->eicosanedienoic_acid))
		<td><strong>Eicosanedienoic Acid:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->eicosanedienoic_acid) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->eicosaneteetraenoic_acid))
		<td><strong>Eicosaneteetraenoic Acid:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->eicosaneteetraenoic_acid) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->erucic_acid))
		<td><strong>Erucic Acid:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->erucic_acid) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->fatty_acid))
		<td><strong>Fatty Acid:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->fatty_acid) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->ffa))
		<td><strong>FFA:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->ffa) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->flavor))
		<td><strong>Flavor:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->flavor) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->free_acidity))
		<td><strong>Free Acidity:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->free_acidity) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->free_fatty_acids))
		<td><strong>Free Fatty Acids:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->free_fatty_acids) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->free_so2))
		<td><strong>Free SO2:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->free_so2) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->free_sulpphurous_anhydride))
		<td><strong>Free Sulpphurous Anhydride:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->free_sulpphurous_anhydride) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->fructose))
		<td><strong>Fructose:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->fructose) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->gadoleic_acid))
		<td><strong>Gadoleic Acid:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->gadoleic_acid) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->glucose))
		<td><strong>Glucose:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->glucose) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->grains))
		<td><strong>Grains:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->grains) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->heavy_metals_as_lead))
		<td><strong>Heavy Metals as Lead:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->heavy_metals_as_lead) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->heptadacanoic_acid))
		<td><strong>Heptadacanoic Acid:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->heptadacanoic_acid) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->heptadecenoic_acid))
		<td><strong>Heptadecenoic Acid:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->heptadecenoic_acid) }}</strong></td>
		</tr>
		@endif																					

		@if(!empty($coa->hydroxyl_value))
		<td><strong>Hydroxyl Value:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->hydroxyl_value) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->identification_a))
		<td><strong>Identification A:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->identification_a) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->identification_b))
		<td><strong>Identification B:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->identification_b) }}</strong></td>
		</tr>
		@endif																					


		@if(!empty($coa->identification_c))
		<td><strong>Identification C:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->identification_c) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->insoluble_impurities))
		<td><strong>Insoluble Impurities:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->insoluble_impurities) }}</strong></td>
		</tr>
		@endif			

		@if(!empty($coa->iodine_value))
		<td><strong>Iodine Value:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->iodine_value) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->iodine_value_ri))
		<td><strong>Iodine Value RI:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->iodine_value_ri) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->iron))
		<td><strong>Iron:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->iron) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->iv_wijs))
		<td><strong>iv_wijs:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->iv_wijs) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->lauric_acid))
		<td><strong>Lauric Acid:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->lauric_acid) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->lead))
		<td><strong>Lead:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->lead) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->lignoceric_acid))
		<td><strong>Lignoceric Acid:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->lignoceric_acid) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->linoleic_acid))
		<td><strong>Linoleic Acid:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->linoleic_acid) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->linolenic_acid))
		<td><strong>Linolenic Acid:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->linolenic_acid) }}</strong></td>
		</tr>
		@endif											

		@if(!empty($coa->m_i))
		<td><strong>m_i:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->m_i) }}</strong></td>
		</tr>
		@endif											

		@if(!empty($coa->maltose))
		<td><strong>Maltose:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->maltose) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->melting_point))
		<td><strong>Melting Point:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->melting_point) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->minor))
		<td><strong>Minor:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->minor) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->moisture))
		<td><strong>Moisture:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->moisture) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->moisture_and_impurities))
		<td><strong>Moisture and Impurities:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->moisture_and_impurities) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->moisture_and_sediment_content))
		<td><strong>Moisture and Sediment Content:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->moisture_and_sediment_content) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->moisture_level))
		<td><strong>Moisture level:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->moisture_level) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->myistoleic_acid))
		<td><strong>Myistoleic Acid:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->myistoleic_acid) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->myristic))
		<td><strong>Myristic:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->myristic) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->nervonic_acid))
		<td><strong>Nervonic Acid:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->nervonic_acid) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->octadecatetraen_acid))
		<td><strong>Octadecatetraen Acid:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->octadecatetraen_acid) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->odor))
		<td><strong>Odor:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->odor) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->oleic_acid))
		<td><strong>Oleic Acid:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->oleic_acid) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->optical_rotation))
		<td><strong>Optical Rotation:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->optical_rotation) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->osi))
		<td><strong>OSI:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->osi) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->other_carbohydrates))
		<td><strong>Other Carbohydrates:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->other_carbohydrates) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->palmitic_acid))
		<td><strong>Palmitic Acid:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->palmitic_acid) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->palmitoliec_acid))
		<td><strong>Palmitoliec Acid:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->palmitoliec_acid) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->peroxide_value))
		<td><strong>Peroxide Value:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->peroxide_value) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->ph))
		<td><strong>Ph:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->ph) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->pounds_per_gallon))
		<td><strong>Pounds Per Gallon:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->pounds_per_gallon) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->pv))
		<td><strong>PV:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->pv) }}</strong></td>
		</tr>
		@endif																																											

		@if(!empty($coa->refactive_index))
		<td><strong>Refactive Index:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->refactive_index) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->refractive_index))
		<td><strong>Refractive Index:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->refractive_index) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->rifractometric_at_20))
		<td><strong>rifractometric_at_20:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->rifractometric_at_20) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->salt))
		<td><strong>Salt:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->salt) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->saponification))
		<td><strong>Saponification:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->saponification) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->saponification_value))
		<td><strong>Saponification Value:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->saponification_value) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->saturated_fatty_acids))
		<td><strong>Saturated Fatty Acids:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->saturated_fatty_acids) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->smoke_point))
		<td><strong>Smoke Point:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->smoke_point) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->so2))
		<td><strong>SO2:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->so2) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->specific_gravity))
		<td><strong>Specific Gravity:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->specific_gravity) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->specific_weight))
		<td><strong>Specific Weight:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->specific_weight) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->specific_weight_as_baumic_degrees))
		<td><strong>Specific Weight as Baumic Degrees:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->specific_weight_as_baumic_degrees) }}</strong></td>
		</tr>
		@endif		

		@if(!empty($coa->starch))
		<td><strong>Starch:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->starch) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->stearic_acid))
		<td><strong>Stearic Acid:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->stearic_acid) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->stigmasterol))
		<td><strong>Stigmasterol:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->stigmasterol) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->taste))
		<td><strong>Taste:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->taste) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->tbhq))
		<td><strong>tbhq:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->tbhq) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->total_acidity_as_acetic_acid))
		<td><strong>Total Acidity as Acetic Acid:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->total_acidity_as_acetic_acid) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->total_dry_extract))
		<td><strong>Total Dry Extract:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->total_dry_extract) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->total_monounsaturated))
		<td><strong>Total Monounsaturated:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->total_monounsaturated) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->total_polyunsaturated))
		<td><strong>Total Polyunsaturated:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->total_polyunsaturated) }}</strong></td>
		</tr>
		@endif																																											
		@if(!empty($coa->total_saturated))
		<td><strong>Total Saturated:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->total_saturated) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->total_so2))
		<td><strong>Total SO2:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->total_so2) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->total_sterols))
		<td><strong>Total Sterols:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->total_sterols) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->total_sulphurous_anhydride))
		<td><strong>Total Sulphurous Anhydride:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->total_sulphurous_anhydride) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->trans_c_18_1))
		<td><strong>trans_c_18_1:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->trans_c_18_1) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->trans_c_18_2))
		<td><strong>trans_c_18_2:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->trans_c_18_2) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->trans_fatty_acids))
		<td><strong>trans_fatty_acids:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->trans_fatty_acids) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->turbidity))
		<td><strong>turbidity:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->turbidity) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->unsaponifiable_matter))
		<td><strong>Unsaponifiable Matter:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->unsaponifiable_matter) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->vitamin_e))
		<td><strong>Vitamin E:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->vitamin_e) }}</strong></td>
		</tr>
		@endif	

		@if(!empty($coa->zinc))
		<td><strong>Zinc:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->zinc) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->k268))
		<td><strong>k268:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->k268) }}</strong></td>
		</tr>
		@endif		

		@if(!empty($coa->k270))
		<td><strong>K270:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->k270) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->im))
		<td><strong>IM:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->im) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->pph))
		<td><strong>PPH:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->pph) }}</strong></td>
		</tr>
		@endif
																										
		@if(!empty($coa->moi))
		<td><strong>MOI:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->moi) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->ind))
		<td><strong>IND:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->ind) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->bit225))
		<td><strong>BIT(225):</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->bit225) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->ppp))
		<td><strong>PPP:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->ppp) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->dag))
		<td><strong>DAG:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->dag) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->defects))
		<td><strong>Defects:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->defects) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->fruitness))
		<td><strong>Fruitness:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->fruitness) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->bitterness))
		<td><strong>Bitterness:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->bitterness) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->pungency))
		<td><strong>Pungency:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->pungency) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->campesterol))
		<td><strong>Campesterol:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->campesterol) }}</strong></td>
		</tr>
		@endif

		@if(!empty($coa->difference_ecn42))
		<td><strong>Triglecerides Difference ECN42:</strong></td>
		<td width="150px"></td>
		<td><strong>{{ strtoupper($coa->difference_ecn42) }}</strong></td>
		</tr>
		@endif




</table>

<hr>
		<p style="font-weight:bold;"><i>This is an electronically generated document and is valid without signature.</i></p>
</div>

  


@stop
