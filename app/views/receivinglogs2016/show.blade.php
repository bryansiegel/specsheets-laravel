@extends('layouts.admin')

@section('content')

    <div id="blends">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">View</span>2016 Receiving Logs - Lot# {{ $receivinglog->lot }}
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/receivinglogs2016/">2016 Receiving Logs</a> >
                        View
                    </li>
                </ol>
            </div>
        </div>


        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <strong>Date:</strong> {{ $receivinglog->date }}
                            </li>
                            <li class="list-group-item">
                                <strong>Container:</strong> {{ $receivinglog->container }}
                            </li>
                            <li class="list-group-item">
                                <strong>Po:</strong> {{ $receivinglog->po }}
                            </li>
                            <li class="list-group-item">
                                <strong>QTY:</strong> {{ $receivinglog->qty }}
                            </li>
                            <li class="list-group-item">
                                <strong>Vendor:</strong> {{ $receivinglog->vendor }}
                            </li>
                            <li class="list-group-item">
                                <strong>Packslip:</strong> {{ $receivinglog->packslip }}
                            </li>

                            <li class="list-group-item">
                                <strong>Product Description:</strong> {{ $receivinglog->product }}
                            </li>
                            <li class="list-group-item">
                                <strong>Lot:</strong> {{ $receivinglog->lot }}
                            </li>

                            <li class="list-group-item">
                                <strong>Carrier:</strong> {{ $receivinglog->carrier }}
                            </li>
                            <li class="list-group-item">
                                <strong>Product On Hold:</strong> {{ $receivinglog->product_on_hold }}
                            </li>
                            <li class="list-group-item">
                                <strong>Product Contract:</strong> {{ $receivinglog->product_contract}}
                            </li>
                            <li class="list-group-item">
                                <strong>Condition Truck:</strong> {{ $receivinglog->condition_of_truck}}
                            </li>
                            <li class="list-group-item">
                                <strong>Kosher Cert File:</strong> {{ $receivinglog->kosher_cert_file}}
                            </li>
                        </ul>

                        <hr/>
                        <div class="alert alert-dismissible alert-success" style="margin-bottom:0px">
                            <strong>Created: </strong><?php echo \Carbon\Carbon::createFromTimestamp(strtotime($receivinglog->created_at))->diffForHumans(); ?>
                        </div>
                        <div class="alert alert-dismissible alert-warning">
                            <strong>Updated: </strong><?php echo \Carbon\Carbon::createFromTimestamp(strtotime($receivinglog->updated_at))->diffForHumans(); ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
@stop