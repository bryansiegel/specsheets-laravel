@extends('layouts.admin')

@section('content')
    <div id="shipping">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Create</span> A New Shipping Log
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/shippings/">Shipping Logs</a> >
                        Create
                    </li>
                </ol>
            </div>
        </div>

        {{--errors--}}
        @if($errors->has())
            <div class="alert alert-dismissible alert-danger">
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
        @endif

        {{--form--}}
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        {{ Form::open(['route' => 'shippings.store', 'class' => 'form-horizontal']) }}
                        <fieldset>

                            <div class="form-group">
                                {{ Form::label('order_number','Order Number') }}
                                {{ Form::text('order_number', null, ['class' => 'form-control', 'placeholder' => 'Order Number'] )  }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('carrier', 'Carrier') }}
                                {{ Form::text('carrier', null, ['class' => 'form-control', 'placeholder' => 'Order Number']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('ship_date', 'Ship Date') }}
                                {{ Form::text('ship_date', null, ['class' => 'form-control', 'id' => 'datepicker', 'placeholder' => 'Ship Date']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::submit('Create', ['class' => 'btn btn-default']) }}
                            </div>
                        </fieldset>
                        {{ Form::close() }}

                    </div><!--end row-->
                </div>
            </div><!--end col lg 8 -->
        </div><!--end container fluid-->



    </div><!--end blends-->
@stop