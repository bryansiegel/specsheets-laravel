@extends('layouts.admin')

@section('content')
    <div id="shipping">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Edit</span> Shipping Log
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/shippings/">Shipping Logs</a> >
                        Edit
                    </li>
                </ol>
            </div>
        </div>

        {{--errors--}}
        @if($errors->has())
            <div class="alert alert-dismissible alert-danger">
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
        @endif

        {{--form--}}
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        {{ Form::model($shipping, array('method' => 'PATCH', 'route' =>
                                                         array('shippings.update', $shipping->id))) }}
                        <fieldset>

                            <div class="form-group">
                                {{ Form::label('active', 'Shipped?') }}
                                {{ Form::select('active', ['' => 'Please Select', 1 => 'Yes', 2 => 'No'], $shipping->active, ['class' => 'form-control']) }}

                            </div>

                            @if(Auth::user()->group == 20 || Auth::user()->group == 100)
                            <div class="form-group">
                                {{ Form::label('status', 'Status') }}
                                {{ Form::select('status', ['Hold' => 'Hold', 'Paid' => 'Paid'], '', ['class' => 'form-control']) }}
                            </div>
                            @endif

                            <div class="form-group">
                                {{ Form::label('order_number', 'Order Number') }}
                                {{ Form::text('order_number',null,['class' => 'form-control', 'rows' => '4', 'placeholder' => 'Order Number']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('carrier', 'Carrier') }}
                                {{ Form::text('carrier',null,['class' => 'form-control', 'rows' => '4', 'placeholder' => 'Carrier']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('ship_date', 'Ship Date') }}
                                {{ Form::text('ship_date',null,['class' => 'form-control', 'rows' => '4', 'placeholder' => 'Ship Date']) }}
                            </div>

                            {{ Form::submit('Update',['class' => 'btn btn-default', 'style' => 'float:right']) }}
                            {{ link_to_route('shippings.index', 'Cancel',null,['class' => 'btn btn-primary', 'style' => 'float:right;']) }}
                        </fieldset>
                        {{ Form::close() }}

                    </div><!--end row-->
                </div>
            </div><!--end col lg 8 -->
        </div><!--end container fluid-->



    </div><!--end blends-->
@stop