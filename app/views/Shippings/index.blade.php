
@extends('layouts.admin')

@section('content')

    <div id="shipping">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Active</span>Shipping Logs
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > Shipping Logs Logs
                    </li>
                </ol>
            </div>
        </div>
        <div class="alert alert-dismissible alert-info">
            <h1>Date: <?php echo Date("m/d/Y");?></h1>
        </div>

        {{--sessions--}}
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif

        {{--sub nav--}}
        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">
                    {{ link_to_route('shippings.create', 'Create A New Shipping Log',null, ['class' => 'btn btn-primary navbar-btn']) }}
                    <a href="/shippings/shipped" class="btn btn-warning navbar-btn">Shipped</a>
                    <a href="/shippings/warehouse" class="btn btn-danger navbar-btn">Warehouse</a>
                    {{--search--}}

                    <form class="navbar-form navbar-right" role="search">
                        <div class="form-group">
                            <input type="text" class=" search form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-primary" data-sort="shipping">Submit</button>
                    </form>
                </nav>
            </div>
        </div>

        <table class="table table-stripped table-responsive table-hover">
            <thead>
            <tr>
                <td><strong>Order#</strong></td>
                <td><strong>Ship date</strong></td>
                <td><strong>Carrier</strong></td>
                <td><strong>Status</strong></td>
                <td></td>
            </tr>
            </thead>
            <tbody class="list">
            @foreach($shippings  as $shipping)

                @if($shipping->active == 2)
                    @if(Date("m/d/Y") === $shipping->ship_date && $shipping->status === 'Paid')
                        <tr class="success">
                    @elseif(Date("m/d/Y") === $shipping->ship_date && $shipping->status === 'Hold')
                        <tr class="danger">
                        @else
                        <tr>
                            @endif
                            <td class="order_number">{{ $shipping->order_number }}</td>
                            <td class="ship_date">{{ $shipping->ship_date }}</td>
                            <td class="carrier">{{ $shipping->carrier }}</td>
                            <td class="status">{{ $shipping->status }}</td>
                            <td>
                                @if(Auth::user()->group == 100 || Auth::user()->group == 50)
                                    <div class="btn-group" role="group" aria-label="">
                                        {{ link_to_route('shippings.edit', 'Edit', [$shipping->id],['class' => 'btn btn-warning']) }}
                                        {{ Form::open(array('method' => 'DELETE', 'style' => 'display:inline','route' => array('shippings.destroy', $shipping->id))) }}
                                        {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                                        {{ Form::close() }}
                                    </div>
                                @endif
                            </td>
                        </tr>
                    @endif
                    @endforeach

            </tbody>
        </table>


        <ul class="pagination"></ul>
    </div>
@stop

{{--end content--}}


{{--footer--}}
@section('footer')
    <script type="text/javascript">
        var options = {
            valueNames: [ 'order_number', 'carrier', 'status', 'ship_date', 'active'],
            // pagination
            page: 10,
            plugins: [
                ListPagination({})
            ]
        };

        var shippingList = new List('shipping', options);
    </script>
@stop
