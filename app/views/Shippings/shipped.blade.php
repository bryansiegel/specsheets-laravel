
@extends('layouts.admin')

@section('content')

    <div id="shippings">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-danger" style="padding-right:15px;">Shipped</span>Shipping Logs
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a
                                href="/shippings/">Shippings Logs</a> > Shipped
                    </li>
                </ol>
            </div>
        </div>

        {{--sub nav--}}
        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">
                    <button type="button" class="btn btn-primary navbar-btn">Add A New Shipping Log</button>
                    <a href="/shippings/" class="btn btn-warning navbar-btn">Not Shipped</a>
                    {{--search--}}

                    <form class="navbar-form navbar-right" role="search">
                        <div class="form-group">
                            <input type="text" class=" search form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-primary" data-sort="shipping">Submit</button>
                    </form>
                </nav>
            </div>
        </div>

        <table class="table table-stripped table-responsive table-hover">
            <thead>
            <tr>
                <td><strong>Order#</strong></td>
                <td><strong>Ship date</strong></td>
                <td><strong>Carrier</strong></td>
                <td><strong>Status</strong></td>
                <td></td>
            </tr>
            </thead>
            <tbody class="list">
            @foreach($shippings  as $shipping)

                @if($shipping->active == 1)
                    @if(Date("m/d/Y") === $shipping->ship_date)
                        <tr class="danger">
                    @else
                        <tr>
                            @endif
                            <td class="order_number">{{ $shipping->order_number }}</td>
                            <td class="ship_date">{{ $shipping->ship_date }}</td>
                            <td class="carrier">{{ $shipping->carrier }}</td>
                            <td class="status">{{ $shipping->status }}</td>
                            <td>
                                @if(Auth::user()->group == 100 || Auth::user()->group == 50)
                                    <div class="btn-group" role="group" aria-label="">
                                        {{ link_to_route('shippings.edit', 'Edit', [$shipping->id],['class' => 'btn btn-warning']) }}
                                        {{ Form::open(array('method' => 'DELETE', 'style' => 'display:inline','route' => array('shippings.destroy', $shipping->id))) }}
                                        {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                                        {{ Form::close() }}
                                    </div>
                                @endif
                            </td>
                        </tr>
                    @endif
                    @endforeach

            </tbody>
        </table>


        <ul class="pagination"></ul>
    </div>
@stop

{{--end content--}}


{{--footer--}}
@section('footer')
    <script type="text/javascript">
        var options = {
            valueNames: [ 'order_number', 'carrier', 'status', 'ship_date', 'active'],
            // pagination
            page: 10,
            plugins: [
                ListPagination({})
            ]
        };

        var shippingsList = new List('shippings', options);
    </script>
@stop
