@extends('layouts.admin')

@section('content')
    <div id="blends">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Create</span> A Product Hold
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/productholds/">Product Holds</a> >
                        Create
                    </li>
                </ol>
            </div>
        </div>

        {{--errors--}}
            @if($errors->has())
            <div class="alert alert-dismissible alert-danger">
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
            @endif

        {{--form--}}
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                <div class="col-lg-8">
                {{ Form::open(['route' => 'productholds.store', 'class' => 'form-horizontal']) }}
                    <fieldset>
                     
                    <div class="form-group">
                        {{ Form::label('hold_starting_date', 'Hold Starting Date') }}
                        {{ Form::text('hold_starting_date', null,['class' => 'form-control', 'placeholder' => 'Hold Starting Date', 'id' => 'datepicker1']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('hold_by', 'Hold By') }}
                        {{ Form::text('hold_by', null,['class' => 'form-control', 'placeholder' => 'Hold By']) }}
                    </div>                    
                    <div class="form-group">
                        {{ Form::label('description', 'Description') }}
                        {{ Form::text('description', null,['class' => 'form-control', 'placeholder' => 'Description']) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('classification_of_hold', 'Classification of Hold') }}
                        {{ Form::select('classification_of_hold',['' => 'Please Enter', 'Specifications Non-Conformance' => 'Specifications Non-Conformance', 'Post Return Inspection' => 'Post Return Inspection', 'Damage Inspection' => 'Damage Inspection', 'Other' => 'Other'], '', ['class' => 'form-control']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('reason_for_hold', 'Reason For Hold') }}
                        {{ Form::textarea('reason_for_hold', null,['class' => 'form-control', 'placeholder' => 'Reason For Hold', 'rows' => '8', 'cols' => '8']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('action_required_before_release', 'Action Required Before Release') }}
                        {{ Form::textarea('action_required_before_release', null,['class' => 'form-control', 'placeholder' => 'Action Required Before Release', 'rows' => '8', 'cols' => '8']) }}
                    </div>
                    <div class="form-group">
                    {{ Form::label('notes', 'Note') }}
                    {{ Form::textarea('notes', null,['cols' => '8', 'rows' => '8', 'class' => 'form-control', 'placeholder' => 'Notes']) }}
                    </div>
                   
                    {{ Form::submit('Create',['class' => 'btn btn-default', 'style' => 'float:right']) }}

                    </fieldset>
                {{ Form::close() }}

            </div><!--end row-->
                </div>
            </div><!--end col lg 8 -->
        </div><!--end container fluid-->



    </div><!--end blends-->
@stop
{{--footer--}}
@section('footer')
   <script>
          $(function() {
            $( "#datepicker" ).datepicker();
            $( "#datepicker1" ).datepicker();
        });
    </script>

    @stop