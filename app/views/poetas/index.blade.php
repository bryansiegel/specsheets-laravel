@extends('layouts.admin')

@section('content')

    <div id="blends">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    PO ETA
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > PO ETA
                    </li>
                </ol>
            </div>
        </div>

        {{--sessions--}}
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif

        {{--sub nav--}}
        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">
                    @if(Auth::user()->group != 20)
                    {{ link_to_route('poetas.create', 'Add A New PO ETA',null, array('class' => 'btn btn-primary navbar-btn')) }}
                    <a href="/poetas/archive" class="btn btn-danger navbar-btn">Archive</a>
                    @endif
                    {{--search--}}

{{--                     <form class="navbar-form navbar-right" role="search">
                        <div class="form-group">
                            <input type="text" class=" search form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-primary" data-sort="poeta">Submit</button>
                    </form> --}}
                </nav>
            </div>
        </div>

        <table id="datatable" class="table table-stripped table-responsive table-hover">
            <thead>
            <tr>
                <td style="display: none">ID</td>
                <td><strong>Date</strong></td>
                <td><strong>PO#</strong></td>
                <td><strong>Vendor</strong></td>
                <td><strong>Product</strong></td>
                <td><strong>Amount</strong></td>
                <td><strong>Open Balance</strong></td>
                <td><strong>ETA</strong></td>
                <td><strong>Notes</strong></td>
                <td></td>
            </tr>
            </thead>
            <tbody class="list">
            @foreach($poetas as $poeta)
            @if($poeta->status != 'Closed PO')

            @if($poeta->status == 'Open PO')
                <tr style="background-color:yellow">
                @elseif($poeta->status == 'Confirmed PO')
                <tr style="background-color:green; color:white;">
                @else
                <tr>
            @endif
                    <td style="display:none;">{{ $poeta->id }}</td>
                    <td class="lot">{{ $poeta->date }}</td>
                    <td class="description">{{ $poeta->po}}</td>
                    <td class="receiving_log">{{ $poeta->vendor }}</td>
                    <td class="location">{{ $poeta->product }}</td>
                    <td class="location">{{ $poeta->amount }}</td>
                    <td class="location">{{ $poeta->open_balance }}</td>
                    <td class="location">{{ $poeta->eta }}</td>
                    <td class="location">{{ $poeta->notes }}</td>
                    <td>
                        <div class="btn-group" role="group" aria-label="">
                            {{-- {{ link_to_route('poetas.show', 'View', [$poeta->id],['class' => 'btn btn-success']) }} --}}

                            @if(Auth::user()->group != 20)
                            {{ link_to_route('poetas.edit', 'Edit', [$poeta->id],['class' => 'btn btn-warning']) }}
                            {{ Form::open(array('method' => 'DELETE','style' => 'display:inline', 'route' => array('poetas.destroy', $poeta->id))) }}
                            {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                            @endif
        
                            {{ Form::close() }}
                        </div>

                </tr>
                @endif
            @endforeach

            </tbody>
        </table>


        <ul class="pagination"></ul>
    </div>
    @stop


{{--footer--}}
@section('footer')
   <script>
          $(function() {
            $( "#datepicker" ).datepicker();
            $( "#datepicker1" ).datepicker();
            $('#timepicker').timepicker();
            $('#datatable').DataTable({
                paging:true,
                "order": [[ 0, "desc" ]],
                // scrollY:400,
                deferRender:true,
                // scroller:true,
                columnDefs: [
                                { targets: 'no-sort', orderable: false }
                            ],

            });
        });
    </script>

    @stop