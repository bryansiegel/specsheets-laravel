@extends('layouts.admin')

@section('content')
    <div id="blends">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Create</span>A PO ETA
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a
                                href="/poetas/">PO ETA</a> >
                        Create
                    </li>
                </ol>
            </div>
        </div>

        {{--errors--}}
        @if($errors->has())
            <div class="alert alert-dismissible alert-danger">
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
        @endif

        {{--form--}}
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        {{ Form::open(['route' => 'poetas.store', 'class' => 'form-horizontal']) }}

                        <fieldset>
                            <div class="form-group">
                                {{ Form::label('status', 'Status?') }}
                                {{ Form::select('status',['' => 'Please Select', 'Open PO' => 'Open PO', 'Closed PO' => 'Closed PO', 'Confirmed PO' => 'Confirmed PO'], '', ['class' => 'form-control']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('date', 'Date') }}
                                {{ Form::text('date', null,['class' => 'form-control',  'placeholder' => 'Date', 'id' => 'datepicker']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('po', 'PO#') }}
                                {{ Form::text('po', null,['class' => 'form-control', 'placeholder' => 'PO#']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('vendor', 'Vendor') }}
                                {{ Form::text('vendor', null,['class' => 'form-control', 'placeholder' => 'Vendor']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('product', 'Product') }}
                                {{ Form::text('product', null,['class' => 'form-control', 'placeholder' => 'Product']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('amount', 'Amount') }}
                                {{ Form::text('amount', null,['class' => 'form-control', 'placeholder' => 'Amount']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('open_balance', 'Open Balance') }}
                                {{ Form::text('open_balance', null,['class' => 'form-control', 'placeholder' => 'Open Balance']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('eta', 'ETA') }}
                                {{ Form::text('eta', null,['class' => 'form-control', 'placeholder' => 'ETA']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('notes', 'Notes') }}
                                {{ Form::textarea('notes', null,['class' => 'form-control', 'placeholder' => 'Notes', 'rows' => '8', 'cols' => '8']) }}
                            </div>

                            {{ Form::submit('Create',['class' => 'btn btn-default', 'style' => 'float:right']) }}

                        </fieldset>
                        {{ Form::close() }}

                    </div><!--end row-->
                </div>
            </div><!--end col lg 8 -->
        </div><!--end container fluid-->



    </div><!--end blends-->
@stop

@section('footer')
   <script>
          $(function() {
            $( "#datepicker" ).datepicker();
        });
    </script>

    @stop