@extends('layouts.master')

@section('content')

<div class='col-lg-4 col-lg-offset-4'>


@if ($errors->has())
<div class="alert alert-danger">
    @foreach ($errors->all() as $error)
    {{ $error }}
    @endforeach
</div>
@endif

<!-- logo -->
<div style="text-align:center">
<img src="images/logo.jpg" width="200px" height="200px"/>
</div>

<!--form-->
<h1><i class='fa fa-lock'></i> Login</h1>

{{ Form::open(['role' => 'form']) }}

<div class='form-group'>
    {{ Form::label('username', 'Username') }}
    {{ Form::text('username', null, array("placeholder" => "Username", "class" => "form-control")) }}
</div>

 <div class='form-group'>
    {{ Form::label('password', 'Password') }}
    {{ Form::password('password', ['placeholder' => 'Password', 'class' => 'form-control']) }}
    </div>

    {{ Form::submit('Login', ['class' => 'btn btn-primary']) }}

{{ Form::close() }}

</div>
@stop