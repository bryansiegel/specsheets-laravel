@extends('layouts.admin')

@section('content')

    <div id="receivinglogs">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    2014 Receiving Logs
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > 2014 Receiving Logs
                    </li>
                </ol>
            </div>
        </div>

        <!--sessions-->
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif

        <!--sub nav-->
        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">
                    {{ link_to_route('recievinglog2014s.create', 'Create A New Receiving Log',null, ['class' => 'btn btn-primary navbar-btn']) }}

                    <a href="/recievinglog2014s/print/" class="btn btn-success navbar-btn">Print</a>
                    <!--search

                    <form class="navbar-form navbar-right" role="search">
                        <div class="form-group">
                            <input type="text" class=" search form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-primary" data-sort="nutritionfact">Submit</button>
                    </form>-->
                </nav>
            </div>
        </div>

        <table id="datatable" class="table table-stripped">
            <thead>
            <tr>
                <td><strong>Date</strong></td>
                <td><strong>Container</strong></td>
                <td><strong>PO</strong></td>
                <td><strong>QTY</strong></td>
                <td><strong>Vendor</strong></td>
                <td><strong>Packslip</strong></td>
                <td><strong>Product Description</strong></td>
                <td><strong>Lot</strong></td>
                <td><strong>Kosher Certification File#</strong></td>
                <td class="no-sort"></td>
            </tr>
            </thead>
            <tbody class="list">
            @foreach($recievinglog2014s as $receivinglog)
                <tr>
                    <td class="date">{{ $receivinglog->date }}</td>
                    <td class="containers">{{ $receivinglog->container }}</td>
                    <td class="po">{{ $receivinglog->po }}</td>
                    <td class="qty">{{ $receivinglog->qty }}</td>
                    <td class="vendor">{{ $receivinglog->vendor }}</td>
                    <td class="packslip">{{ $receivinglog->packslip }}</td>
                    <td class="description">{{ $receivinglog->product_description }}</td>
                    <td class="lot">{{ $receivinglog->lot }}</td>
                    <td class="kosher">{{ $receivinglog->kosher_cert_file }}</td>
                    <td>
                        <div class="btn-group" role="group" aria-label="">
                            {{ link_to_route('recievinglog2014s.show', 'View', [$receivinglog->id],['class' => 'btn btn-success']) }}
                            @if(Auth::user()->group == 100 || Auth::user()->group == 50)
                            {{ link_to_route('recievinglog2014s.edit', 'Edit', [$receivinglog->id],['class' => 'btn btn-warning']) }}
                            {{ Form::open(array('method' => 'DELETE', 'style' => 'display:inline','route' => array('recievinglog2014s.destroy', $receivinglog->id))) }}
                            {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                            {{ Form::close() }}
                                @endif
                        </div>

                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>


        <ul class="pagination"></ul>
    </div>
@stop

<!--end content-->


<!--footer-->
@section('footer')
   <script>
          $(function() {
            $( "#datepicker" ).datepicker();
            $( "#datepicker1" ).datepicker();
            $('#timepicker').timepicker();
            $('#datatable').DataTable({
                paging:true,
                "order": [[ 0, "desc" ]],
                // scrollY:400,
                deferRender:true,
                // scroller:true,
                columnDefs: [
                                { targets: 'no-sort', orderable: false }
                            ],

            });
        });
    </script>

    @stop