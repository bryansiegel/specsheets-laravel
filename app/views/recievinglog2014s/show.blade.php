@extends('layouts.admin')

@section('content')

    <div id="blends">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">View</span>2014 Receiving Logs - Lot# {{ $recievinglog2014->lot }}
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/recievinglog2014s/">2014 Receiving Logs</a> >
                        View
                    </li>
                </ol>
            </div>
        </div>


        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <strong>Date:</strong> {{ $recievinglog2014->date }}
                            </li>
                            <li class="list-group-item">
                                <strong>Po:</strong> {{ $recievinglog2014->po }}
                            </li>
                            <li class="list-group-item">
                                <strong>Vendor:</strong> {{ $recievinglog2014->vendor }}
                            </li>
                            <li class="list-group-item">
                                <strong>Packslip:</strong> {{ $recievinglog2014->packslip }}
                            </li>
                            <li class="list-group-item">
                                <strong>QTY:</strong> {{ $recievinglog2014->qty }}
                            </li>
                            <li class="list-group-item">
                                <strong>Product Description:</strong> {{ $recievinglog2014->product_description }}
                            </li>
                            <li class="list-group-item">
                                <strong>Lot:</strong> {{ $recievinglog2014->lot }}
                            </li>
                            <li class="list-group-item">
                                <strong>Container:</strong> {{ $recievinglog2014->container }}
                            </li>
                            <li class="list-group-item">
                                <strong>Carrier:</strong> {{ $recievinglog2014->carrier }}
                            </li>
                            <li class="list-group-item">
                                <strong>Product On Hold:</strong> {{ $recievinglog2014->product_on_hold }}
                            </li>
                            <li class="list-group-item">
                                <strong>Product Contract:</strong> {{ $recievinglog2014->product_contract}}
                            </li>
                            <li class="list-group-item">
                                <strong>Condition Truck:</strong> {{ $recievinglog2014->condition_truck}}
                            </li>
                            <li class="list-group-item">
                                <strong>Kosher Cert File:</strong> {{ $recievinglog2014->kosher_cert_file}}
                            </li>
                            <li class="list-group-item">
                                <strong>Notes:</strong> {{ $recievinglog2014->notes}}
                            </li>
                        </ul>

                        <hr/>
                        <div class="alert alert-dismissible alert-success" style="margin-bottom:0px">
                            <strong>Created: </strong><?php echo \Carbon\Carbon::createFromTimestamp(strtotime($recievinglog2014->created_at))->diffForHumans(); ?>
                        </div>
                        <div class="alert alert-dismissible alert-warning">
                            <strong>Updated: </strong><?php echo \Carbon\Carbon::createFromTimestamp(strtotime($recievinglog2014->updated_at))->diffForHumans(); ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
@stop