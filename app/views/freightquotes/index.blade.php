@extends('layouts.admin')

@section('content')

    <div id="spotchecks">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Freight Quotes
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > Freight Quotes
                    </li>
                </ol>
            </div>
        </div>

        <!--sessions-->
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
            @endif

        <!--sub nav-->
        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">
                    {{ link_to_route('freightquotes.create', 'Create A New Freight Quote',null, ['class' => 'btn btn-primary navbar-btn']) }}
                    <!-- <a href="/spotchecks/print/" class="btn btn-success navbar-btn">Print</a> -->

                    <!--search
                    <form class="navbar-form navbar-right" role="search">
                        <div class="form-group">
                            <input type="text" class=" search form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-primary" data-sort="nutritionfact">Submit</button>
                    </form>-->
                </nav>
            </div>
        </div>

        <table id="datatable" class="table table-stripped">
            <thead>
            <tr>
                <td><strong>Date</strong></td>
                <td><strong>Vendor</strong></td>
                <td><strong>P.O.</strong></td>
                <td><strong>Pallets</strong></td>
                <td><strong>Freight Company</strong></td>
                <td><strong>Freight Rate</strong></td>
                <!-- <td><strong>Box Label</strong></td> -->
                <!-- <td><strong>Required Weight</strong></td> -->
                <!-- <td><strong>Actual Weight</strong></td> -->
                <!-- <td><strong>By</strong></td> -->
                <!-- <td><strong>Resolution</strong></td> -->
                <td class="no-sort"></td>
            </tr>
            </thead>
            <tbody class="list">
            @foreach($freightquotes as $freightquote)
                <tr>
                    <td>{{ $freightquote->date }}</td>
                    <td>{{ $freightquote->vendor }}</td>
                    <td class="lot">{{ $freightquote->po }}</td>
                    <td>{{ $freightquote->pallets }}</td>
                    <td>{{ $freightquote->freight_company }}</td>
                    <td>{{ $freightquote->freight_rate }}</td>
                    <!-- <td>{{ $freightquote->box_label }}</td> -->
                    <!-- <td>{{ $freightquote->required_weight }}</td> -->
                    <!-- <td>{{ $freightquote->actual_weight }}</td> -->
                    <!-- <td>{{ $freightquote->by }}</td> -->
                    <!-- <td>{{ $freightquote->resolution }}</td> -->
                    <td>
                        <div class="btn-group" role="group" aria-label="">
                            {{ link_to_route('freightquotes.show', 'View', [$freightquote->id],['class' => 'btn btn-success']) }}
                            {{ link_to_route('freightquotes.edit', 'Edit', [$freightquote->id],['class' => 'btn btn-warning']) }}
                            {{ Form::open(array('method' => 'DELETE', 'style' => 'display:inline','route' => array('freightquotes.destroy', $freightquote->id))) }}
                            {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                            {{ Form::close() }}
                        </div>
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>


        <ul class="pagination"></ul>
    </div>
@stop

<!--end content-->


<!--footer-->
@section('footer')
   <script>
          $(function() {
            $( "#datepicker" ).datepicker();
            $( "#datepicker1" ).datepicker();
            $('#timepicker').timepicker();
            $('#datatable').DataTable({
                paging:true,
                "order": [[ 0, "desc" ]],
                // scrollY:400,
                deferRender:true,
                // scroller:true,
                columnDefs: [
                                { targets: 'no-sort', orderable: false }
                            ],

            });
        });
    </script>

    @stop