@extends('layouts.admin')

@section('content')

    <div id="blends">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">View</span>Freight Quote Form - Vendor {{ $freightquote->vendor }}
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/freightquotes/">Freight Quote Form</a> >
                        View
                    </li>
                </ol>
            </div>
        </div>


        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <strong>Date:</strong> {{ $freightquote->date }}
                            </li><li class="list-group-item">
                                <strong>Vendor:</strong> {{ $freightquote->vendor }}
                            </li>
                            <li class="list-group-item">
                                <strong>P.O.:</strong> {{ $freightquote->po }}
                            </li>
                            <li class="list-group-item">
                                <strong>Pallets:</strong> {{ $freightquote->pallets }}
                            </li>
                            <li class="list-group-item">
                                <strong>Freight Company</strong> {{ $freightquote->freight_company }}
                            </li>
                            <li class="list-group-item">
                                <strong>Freight Rate</strong> {{ $freightquote->freight_rate }}
                            </li>
                        </ul>

                        <hr/>
                        <div class="alert alert-dismissible alert-success" style="margin-bottom:0px">
                            <strong>Created: </strong><?php echo \Carbon\Carbon::createFromTimestamp(strtotime($freightquote->created_at))->diffForHumans(); ?>
                        </div>
                        <div class="alert alert-dismissible alert-warning">
                            <strong>Updated: </strong><?php echo \Carbon\Carbon::createFromTimestamp(strtotime($freightquote->updated_at))->diffForHumans(); ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
@stop