



@extends('layouts.admin')

@section('content')

<div id="specsheets">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Website Orders
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > Orders
                </li>
            </ol>
        </div>
    </div>

    {{--sessions--}}
    @if(Session::has('message'))
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
    @endif

    {{--sub nav--}}
    <div class="row">
        <div class="col-lg-12">
            <nav class="navbar navbar-default">
                {{ link_to_route('specsheets.create', 'Create A New Specsheet',null, ['class' => 'btn btn-primary navbar-btn']) }}                {{--search--}}

                <form class="navbar-form navbar-right" role="search">
                    <div class="form-group">
                        <input type="text" class=" search form-control" placeholder="Search">
                    </div>
                    <button type="submit" class="btn btn-primary" data-sort="nutritionfact">Submit</button>
                </form>
            </nav>
        </div>
    </div>

    
        
               {{ $orders->storeSupplyOrders() }}
          
<ul class="pagination"></ul>
</div>
    @stop

{{--end content--}}


{{--footer--}}
@section('footer')
    <script type="text/javascript">
        var options = {
            valueNames: [ 'specsheet', 'isdone'],
            // pagination
            page: 10,
            plugins: [
                ListPagination({})
            ]
        };

        var specsheetList = new List('specsheets', options);
    </script>
    @stop

