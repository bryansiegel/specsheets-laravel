@extends('layouts.master')


@section('content')
<div class='col-lg-4 col-lg-offset-4'>

    @if ($errors->has())
        @foreach ($errors->all() as $error)
            <div class='bg-danger alert'>{{ $error }}</div>
        @endforeach
    @endif

    <h1><i class='fa fa-user'></i>Edit User</h1>

    {{ Form::model($user, ['role' => 'form', 'url' => '/user/' . $user->id, 'method' => 'PUT']) }}

    <div class="form-group">
        {{ Form::label('username') }}
        {{ Form::text('username', null,['placeholder' => 'Username', 'class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('password') }}
        {{ Form::text('password', null, ['placeholder' => 'Password', 'class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('group') }}
        {{ Form::select('group', ['100' => 'Admin', '50' => 'Employee', '10' => 'Guest'], true, ['placeholder' => 'Role', 'class' => 'form-control']) }}
    </div>


    <div class='form-group'>
        {{ Form::submit('Edit', ['class' => 'btn btn-primary']) }}
    </div>

    {{ Form::close() }}

</div>

@stop