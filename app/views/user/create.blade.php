@extends('layouts.admin')


@section('content')


    @if ($errors->has())
        @foreach ($errors->all() as $error)
            <div class='bg-danger alert'>{{ $error }}</div>
        @endforeach
    @endif

    <h1><i class='fa fa-user'></i> Add User</h1>

    {{ Form::open(['route' => 'user.store', 'url' => '/user']) }}

    <div class="form-group">
        {{ Form::label('username') }}
        {{ Form::text('username', null,['placeholder' => 'Username', 'class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('email') }}
        {{ Form::text('email', null, ['placeholder' => 'email', 'class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('password') }}
        {{ Form::text('password', null, ['placeholder' => 'Password', 'class' => 'form-control']) }}
    </div>


    <div class="form-group">
        {{ Form::label('group') }}
        {{ Form::select('group', ['100' => 'Admin', '50' => 'Employee', '10' => 'Guest', '20' => 'Accounting'], true, ['placeholder' => 'Group', 'class' => 'form-control']) }}
    </div>


    <div class='form-group'>
        {{ Form::submit('Add', ['class' => 'btn btn-default']) }}
    </div>

    {{ Form::close() }}



@stop