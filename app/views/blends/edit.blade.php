@extends('layouts.admin')

@section('content')
    <div id="blends">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Edit</span>Blend - Lot# {{ $blend->lot }}
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/blends/">Blends</a> >
                        Edit
                    </li>
                </ol>
            </div>
        </div>

        {{--errors--}}
        @if($errors->has())
            <div class="alert alert-dismissible alert-danger">
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
        @endif

        {{--form--}}
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        {{ Form::model($blend, array('method' => 'PATCH', 'route' =>
                                 array('blends.update', $blend->id))) }}                        <fieldset>
                            <div class="form-group">
                                {{ Form::label('lot', 'Lot') }}
                                {{ Form::text('lot', null,['class' => 'form-control', 'placeholder' => 'Lot']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('sales_order', 'SO#') }}
                                {{ Form::text('sales_order', null,['class' => 'form-control', 'placeholder' => 'SO#']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('oil_used1', 'Oil Used') }}
                                {{ Form::text('oil_used1', null,['class' => 'form-control', 'placeholder' => 'Oil Used']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('oil_used2', 'Oil Used') }}
                                {{ Form::text('oil_used2', null,['class' => 'form-control', 'placeholder' => 'Oil Used']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('oil_used3', 'Oil Used') }}
                                {{ Form::text('oil_used3', null,['class' => 'form-control', 'placeholder' => 'Oil Used']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('blend_type', 'Blend Type') }}
                                {{ Form::text('blend_type', null,['class' => 'form-control', 'placeholder' => 'Blend Type']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('quantity', 'QTY') }}
                                {{ Form::text('quantity', null,['class' => 'form-control', 'placeholder' => 'QTY']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('size', 'Size') }}
                                {{ Form::text('size', null,['class' => 'form-control', 'placeholder' => 'Size']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('customer', 'Customer') }}
                                {{ Form::text('customer', null,['class' => 'form-control', 'placeholder' => 'Customer']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('date', 'Date') }}
                                {{ Form::text('date', null,['class' => 'form-control', 'id' => 'datepicker', 'placeholder' => 'Date']) }}
                            </div>

                            <hr/>
                            <h2>Notes</h2>
                            <div class="form-group">
                                {{ Form::label('note1', 'Note') }}
                                {{ Form::textarea('note1', null,['cols' => '3', 'rows' => '3', 'class' => 'form-control', 'placeholder' => 'Note']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('note2', 'Note') }}
                                {{ Form::textarea('note2', null,['cols' => '3', 'rows' => '3', 'class' => 'form-control', 'placeholder' => 'Note']) }}
                            </div>

                            {{ Form::submit('Update',['class' => 'btn btn-default', 'style' => 'float:right']) }}
                            {{ link_to_route('blends.index', 'Cancel',null,['class' => 'btn btn-primary', 'style' => 'float:right;']) }}

                        </fieldset>
                        {{ Form::close() }}

                    </div><!--end row-->
                </div>
            </div><!--end col lg 8 -->
        </div><!--end container fluid-->



    </div><!--end blends-->
@stop