@extends('layouts.admin')

@section('content')

    <div id="blends">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">View</span>Blend - Lot# {{ $blend->lot }}
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/blends/">Blends</a> >
                        View
                    </li>
                </ol>
            </div>
        </div>


    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="col-lg-8">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <strong>Lot:</strong> {{ $blend->lot }}
                        </li>
                        <li class="list-group-item">
                            <strong>SO#</strong> {{ $blend->sales_order }}
                        </li>
                        <li class="list-group-item">
                            <strong>Oils Used:</strong> {{ $blend->oil_used1 }}
                        </li>
                        <li class="list-group-item">
                            <strong>Oils Used:</strong> {{ $blend->oil_used2 }}
                        </li>
                        <li class="list-group-item">
                            <strong>Oils Used:</strong> {{ $blend->oil_used3 }}
                        </li>
                        <li class="list-group-item">
                            <strong>Blend Type:</strong> {{ $blend->blend_type }}
                        </li>
                        <li class="list-group-item">
                            <strong>Quantity:</strong> {{ $blend->quantity }}
                        </li>
                        <li class="list-group-item">
                            <strong>Size:</strong> {{ $blend->size }}
                        </li>
                        <li class="list-group-item">
                            <strong>Customer:</strong> {{ $blend->customer }}
                        </li>
                        <li class="list-group-item">
                            <strong>Date:</strong> {{ $blend->date }}
                        </li>
                        <li class="list-group-item">
                            <strong>Note</strong> {{ $blend->note1 }}
                        </li>
                        <li class="list-group-item">
                            <strong>Note</strong> {{ $blend->note2 }}
                        </li>
                    </ul>

                    <hr/>
                    <div class="alert alert-dismissible alert-success" style="margin-bottom:0px">
                    <strong>Created: </strong><?php echo \Carbon\Carbon::createFromTimestamp(strtotime($blend->created_at))->diffForHumans(); ?>
                        </div>
                    <div class="alert alert-dismissible alert-warning">
                    <strong>Updated: </strong><?php echo \Carbon\Carbon::createFromTimestamp(strtotime($blend->updated_at))->diffForHumans(); ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
@stop