@extends('layouts.admin')

@section('content')

    <div id="blends">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">View</span>Specsheet
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/specsheets/">Specsheets</a> >
                        View
                    </li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">
                    <a href="{{ url() . "/public/specsheets/" . $specsheet->id }}" class="btn btn-primary navbar-btn">View Public Specsheet</a>
                    <a href="{{ url()  . "/specsheets/"}}" class="btn btn-danger navbar-btn"><< Back</a>
                    <a href="{{ url()  . "/specsheets/pdf/specsheets/". $specsheet->id }}" class="btn btn-primary
                    navbar-btn">Print
                        PDF</a>
                </nav>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        <ul class="list-group">
                            <h2>Information</h2>
                            <li class="list-group-item">
                                <strong>Complete?:</strong> {{ $specsheet->isdone }}
                            </li>

                            {{--is oil or vinegar dropdown--}}
                            <li class="list-group-item">
                                @if($specsheet->is_oil_vinegar == 1 || $specsheet->is_oil_vinegar == "oil")
                                <strong>Oil or Vinegar?</strong> Oil
                                    @elseif($specsheet->is_oil_vinegar == 2 || $specsheet->is_oil_vinegar == "vinegar")
                                    <strong>Oil or Vinegar?</strong> Vinegar
                                    @else
                                    <strong>Oil or Vinegar?</strong>
                                    @endif
                            </li>

                            {{--website--}}
                            <li class="list-group-item">
                                <strong>Website</strong> {{ $specsheet->website }}
                            </li>

                            {{--main--}}
                            <li class="list-group-item">
                                <strong>Specsheet Title</strong> {{ $specsheet->title }}
                            </li>
                            <li class="list-group-item">
                                <strong>Sku</strong> {{ $specsheet->sku }}
                            </li>
                            <li class="list-group-item">
                                <strong>Revised</strong> {{ $specsheet->revised }}
                            </li>
                            <li class="list-group-item">
                                <strong>Product Description</strong> {{ $specsheet->description }}
                            </li>
                            <li class="list-group-item">
                                <strong>Bullet Point 1</strong> {{ $specsheet->bullet_point1 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Bullet Point 2</strong> {{ $specsheet->bullet_point2 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Bullet Point 3</strong> {{ $specsheet->bullet_point3 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Bullet Point 4</strong> {{ $specsheet->bullet_point4 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Bullet Point 5</strong> {{ $specsheet->bullet_point5 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Bullet Point 6</strong> {{ $specsheet->bullet_point6 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Bullet Point 7</strong> {{ $specsheet->bullet_point7 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Bullet Point 8</strong> {{ $specsheet->bullet_point8 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Prop 65 Statement</strong> {{ $specsheet->prop_65_statement }}
                            </li>

                            <li class="list-group-item">
                                <strong>Storage</strong> {{ $specsheet->storage }}
                            </li>
                            <li class="list-group-item">
                                <strong>Shelf Life</strong> {{ $specsheet->shelf_life }}
                            </li>
                            <li class="list-group-item">
                                <strong>Sewer Sludge and Irradiation Statement:</strong> {{ $specsheet->sewer }}
                            </li>
                            <li class="list-group-item">
                                <strong>Applications for the Product</strong> {{ $specsheet->applications }}
                            </li>
                            <li class="list-group-item">
                                <strong>Country of Origin:</strong> {{ $specsheet->country }}
                            </li>
                            <li class="list-group-item">
                                <strong>Nutrition Label</strong>
                                <br />
                                <img src="<?php echo url('/') . '/images/nutrition/'. $specsheet->nutrition ?>" alt="" width="150px" height="300px"/>
                            </li>
                            <li class="list-group-item">
                                <strong>Nutrition Fact 1</strong>{{ $specsheet->nutrition_fact1 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Nutrition Fact 2</strong>{{ $specsheet->nutrition_fact2 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Nutrition Fact 3</strong>{{ $specsheet->nutrition_fact3 }}
                            </li>

                        </ul>



                    </div>
                </div>

                {{--right--}}
                <div class="col-md-6">
                    @if($specsheet->is_oil_vinegar == 1 || $specsheet->is_oil_vinegar == "oil")
                        <h2>Under Allergen</h2>
                        <li class="list-group-item">
                            <strong>Under Allergen</strong>{{ $specsheet->under_allergen }}
                        </li>
                    <h2>Organoleptic Characteristics</h2>
                        <li class="list-group-item">
                            <strong>Appearance/Clarity</strong> {{ $specsheet->organoleptic_appearance }}
                        </li>
                        <li class="list-group-item">
                            <strong>Flavor/Odor</strong>{{ $specsheet->organoleptic_flavor }}
                        </li>
                        <li class="list-group-item">
                            <strong>Color (Lovibond) Red</strong> {{ $specsheet->organoleptic_color_red }}
                        </li>
                        <li class="list-group-item">
                            <strong>Color (Lovibond) Yellow</strong> {{ $specsheet->organoleptic_color_yellow }}
                        </li>
                        <h2>Typical Analysis</h2>
                        <li class="list-group-item">
                            <strong>Free Fatty Acid</strong> {{ $specsheet->typical_fatty_acid }}
                        </li>
                        <li class="list-group-item">
                            <strong>Moisture</strong> {{ $specsheet->typical_moisture2 }}
                        </li>
                        <li class="list-group-item">
                            <strong>Peroxide Value</strong> {{ $specsheet->typical_peroxide }}
                        </li>
                        <li class="list-group-item">
                            <strong>Iodine Value</strong> {{ $specsheet->typical_iodine }}
                        </li>
                        <li class="list-group-item">
                            <strong>Saponification Value</strong> {{ $specsheet->typical_saponification }}
                        </li>
                        <li class="list-group-item">
                            <strong>Anisidine Value</strong> {{ $specsheet->typical_anisidine }}
                        </li>
                        <li class="list-group-item">
                            <strong>Cold Test</strong> {{ $specsheet->typical_cold }}
                        </li>
                        <li class="list-group-item">
                            <strong>Refractive Index</strong> {{ $specsheet->typical_refractive }}
                        </li>
                        <li class="list-group-item">
                            <strong>Specific Gravity</strong> {{ $specsheet->typical_gravity }}
                        </li>
                        <li class="list-group-item">
                            <strong>Oil Stability Index(OSI)</strong> {{ $specsheet->typical_stability }}
                        </li>
                        <li class="list-group-item">
                            <strong>Smoke Point</strong> {{ $specsheet->typical_smoke }}
                        </li>
                        <li class="list-group-item">
                            <strong>Additives</strong> {{ $specsheet->typical_additives }}
                        </li>
                        <h2>Typical Fatty</h2>
                        <li class="list-group-item">
                            <strong>Myristic Acid</strong> {{ $specsheet->typical_myristic }}
                        </li>
                        <li class="list-group-item">
                            <strong>Palmitic Acid</strong> {{ $specsheet->typical_palmitic }}
                        </li>
                        <li class="list-group-item">
                            <strong>Palmitoleic Acid</strong> {{ $specsheet->typical_palmitoleic }}
                        </li>
                        <li class="list-group-item">
                            <strong>Heptadecanoic Acid</strong> {{ $specsheet->typical_heptadecanoic }}
                        </li>
                        <li class="list-group-item">
                            <strong>Heptadecenoic Acid</strong> {{ $specsheet->typical_heptadecenoic }}
                        </li>
                        <li class="list-group-item">
                            <strong>Stearic Acid</strong> {{ $specsheet->typical_stearic }}
                        </li>
                        <li class="list-group-item">
                            <strong>Oleic Acid</strong> {{ $specsheet->typical_oleic }}
                        </li>
                        <li class="list-group-item">
                            <strong>Linoleic Acid</strong> {{ $specsheet->typical_linoleic }}
                        </li>
                        <li class="list-group-item">
                            <strong>Linolenic Acid</strong> {{ $specsheet->typical_linolenic }}
                        </li>
                        <li class="list-group-item">
                            <strong>Arachidic Acid</strong> {{ $specsheet->typical_arachidic }}
                        </li>
                        <li class="list-group-item">
                            <strong>Gadoleic Acid</strong> {{ $specsheet->typical_gadoleic }}
                        </li>
                        <li class="list-group-item">
                            <strong>Behenic Acid</strong> {{ $specsheet->typical_behenic }}
                        </li>
                        <li class="list-group-item">
                            <strong>Erucic Acid</strong> {{ $specsheet->typical_erucic }}
                        </li>
                        <li class="list-group-item">
                            <strong>Lignoceric Acid</strong> {{ $specsheet->typical_lignoceric }}
                        </li>
                        <h2>USDA NDB (National Nutrition Database)</h2>
                        <br/>
                        <table width="100%" border="1" bordercolor="#97b854" cellpadding="5" cellspacing="0">
                            <tr bgcolor="#97b854" style="padding:10px; text-align:center;color:white;">
                                <td>Nutrient</td>
                                <td>Unit</td>
                                <td>Value per 100.0g</td>
                                <td>Tbsp 13.5g</td>
                            </tr>
                            <tr>
                                <td colspan="4" bgcolor="#d6e3bc"> Proximates</td>
                            </tr>
                            <tr>
                                <td>Water</td>
                                <td>g</td>
                                <td> {{  $specsheet->water_value }}</td>
                                <td> {{  $specsheet->water_tbsp }}</td>
                            </tr>
                            <tr>
                                <td>Energy</td>
                                <td>kcal</td>
                                <td>{{ $specsheet->energy_value }}</td>
                                <td>{{ $specsheet->energy_tbsp }}</td>
                            </tr>
                            <tr>
                                <td>Protein</td>
                                <td>g</td>
                                <td> {{ $specsheet->protein_value }}</td>
                                <td>{{ $specsheet->protein_tbsp }}</td>
                            </tr>
                            <tr>
                                <td>Total lipid (fat)</td>
                                <td>g</td>
                                <td>{{ $specsheet->totallipid_value }}</td>
                                <td>{{ $specsheet->totallipid_tbsp }}</td>
                            </tr>
                            <tr>
                                <td>Carbohydrate, by difference</td>
                                <td>g</td>
                                <td>{{ $specsheet->carbohydrate_value }}</td>
                                <td>{{ $specsheet->carbohydrate_tbsp }}</td>
                            </tr>
                            <tr>
                                <td>Fiber, total dietary</td>
                                <td>g</td>
                                <td>{{ $specsheet->fiber_value }}</td>
                                <td>{{ $specsheet->fiber_tbsp }}</td>
                            </tr>
                            <tr>
                                <td>Sugars, total</td>
                                <td>g</td>
                                <td> {{ $specsheet->sugars_value }}</td>
                                <td>{{ $specsheet->sugars_tbsp }}</td>
                            </tr>
                            <tr>
                                <td colspan="4" bgcolor="#d6e3bc">Minerals</td>
                            </tr>
                            <tr>
                                <td>Calcium, Ca</td>
                                <td>mg</td>
                                <td>{{ $specsheet->calcium_value }}</td>
                                <td>{{ $specsheet->calcium_tbsp }}</td>
                            </tr>
                            <tr>
                                <td>Iron, Fe</td>
                                <td>mg</td>
                                <td>{{ $specsheet->iron_value }}</td>
                                <td>{{ $specsheet->iron_tbsp }}</td>
                            </tr>
                            <tr>
                                <td>Magnesium, Mg</td>
                                <td>mg</td>
                                <td>{{ $specsheet->magnesium_value }}</td>
                                <td>{{ $specsheet->magnesium_tbsp }}</td>
                            </tr>
                            <tr>
                                <td>Phosphorus, P</td>
                                <td>mg</td>
                                <td> {{ $specsheet->phosphorus_value }}</td>
                                <td>{{ $specsheet->phosphorus_tbsp }}</td>
                            </tr>
                            <tr>
                                <td>Potassium, K</td>
                                <td>mg</td>
                                <td> {{ $specsheet->potassium_value }}</td>
                                <td>{{ $specsheet->potassium_tbsp }}</td>
                            </tr>
                            <tr>
                                <td>Sodium, Na</td>
                                <td>mg</td>
                                <td>{{ $specsheet->sodium_value }}</td>
                                <td>{{ $specsheet->sodium_tbsp }}</td>
                            </tr>
                            <tr>
                                <td>Zinc, Zn</td>
                                <td>mg</td>
                                <td>{{ $specsheet->zinc_value }}</td>
                                <td>{{ $specsheet->zinc_tbsp }}</td>
                            </tr>
                            <tr>
                                <td colspan="4" bgcolor="#d6e3bc">Vitamins</td>
                            </tr>
                            <tr>
                                <td>Vitamin C, total ascorbic acid</td>
                                <td>mg</td>
                                <td>{{ $specsheet->vitaminc_value }}</td>
                                <td>{{ $specsheet->vitaminc_tbsp }}</td>
                            </tr>
                            <tr>
                                <td>Thiamin</td>
                                <td>mg</td>
                                <td>{{ $specsheet->thiamin_value }}</td>
                                <td>{{ $specsheet->thiamin_tbsp }}</td>
                            </tr>
                            <tr>
                                <td>Riboflavin</td>
                                <td>mg</td>
                                <td>{{ $specsheet->riboflavin_value }}</td>
                                <td>{{ $specsheet->riboflavin_tbsp }}</td>
                            </tr>
                            <tr>
                                <td>Niacin</td>
                                <td>mg</td>
                                <td>{{ $specsheet->niacin_value }}</td>
                                <td>{{ $specsheet->niacin_tbsp }}</td>
                            </tr>
                            <tr>
                                <td>Vitamin B-6</td>
                                <td>mg</td>
                                <td>{{ $specsheet->vitaminb6_value }}</td>
                                <td>{{ $specsheet->vitaminb6_tbsp }}</td>
                            </tr>
                            <tr>
                                <td>Folate, DFE</td>
                                <td>µg</td>
                                <td>{{ $specsheet->folate_value }}</td>
                                <td>{{ $specsheet->folate_tbsp }}</td>
                            </tr>
                            <tr>
                                <td>Vitamin B-12</td>
                                <td>µg</td>
                                <td>{{ $specsheet->vitaminb12_value }}</td>
                                <td>{{ $specsheet->vitaminb12_tbsp }}</td>
                            </tr>
                            <tr>
                                <td>Vitamin A, RAE</td>
                                <td>µg</td>
                                <td>{{ $specsheet->vitaminar_value }}</td>
                                <td>{{ $specsheet->vitaminar_tbsp }}</td>
                            </tr>
                            <tr>
                                <td>Vitamin A, IU</td>
                                <td>IU</td>
                                <td>{{ $specsheet->vitaminai_value }}</td>
                                <td>{{ $specsheet->vitaminai_tbsp }}</td>
                            </tr>
                            <tr>
                                <td>Vitamin E (alpha-tocopherol)</td>
                                <td>mg</td>
                                <td>{{ $specsheet->vitamine_value }}</td>
                                <td>{{ $specsheet->vitamine_tbsp }}</td>
                            </tr>
                            <tr>
                                <td>Vitamin D (D2+D3)</td>
                                <td>µg</td>
                                <td>{{ $specsheet->vitamind2_value }}</td>
                                <td>{{ $specsheet->vitamind2_tbsp }}</td>
                            </tr>
                            <tr>
                                <td>Vitamin D</td>
                                <td>IU</td>
                                <td>{{ $specsheet->vitamind_value }}</td>
                                <td>{{ $specsheet->vitamind_tbsp }}</td>
                            </tr>
                            <tr>
                                <td>Vitamin K (phylloquinone)</td>
                                <td>µg</td>
                                <td>{{ $specsheet->vitamink_value }}</td>
                                <td>{{ $specsheet->vitamink_tbsp }}</td>
                            </tr>
                            <tr>
                                <td colspan="4" bgcolor="#d6e3bc">Lipids</td>
                            </tr>
                            <tr>
                                <td>Fatty acids, total saturated</td>
                                <td>g</td>
                                <td>{{ $specsheet->fattysaturated_value }}</td>
                                <td>{{ $specsheet->fattysaturated_tbsp }}</td>
                            </tr>
                            <tr>
                                <td>Fatty acids, total monounsaturated</td>
                                <td>g</td>
                                <td>{{ $specsheet->fattymonoun_value }}</td>
                                <td>{{ $specsheet->fattymonoun_tbsp }}</td>
                            </tr>
                            <tr>
                                <td>Fatty acids, polyunsaturated</td>
                                <td>g</td>
                                <td>{{ $specsheet->fattypoly_value }}</td>
                                <td>{{ $specsheet->fattypoly_tbsp }}</td>
                            </tr>
                            <tr>
                                <td>Cholesterol</td>
                                <td>mg</td>
                                <td>{{ $specsheet->cholesterol_value }}</td>
                                <td>{{ $specsheet->cholesterol_tbsp }}</td>
                            </tr>
                            <tr>
                                <td colspan="4" bgcolor="#d6e3bc">Other</td>
                            </tr>
                            <tr>
                                <td>Caffeine</td>
                                <td>mg</td>
                                <td>{{ $specsheet->caffeine_value }}</td>
                                <td>{{ $specsheet->caffeine_tbsp }}</td>
                            </tr>
                        </table>
                        <h2>Registrations and Other Product Information</h2>
                        <li class="list-group-item">
                            <strong>CAS:</strong> {{ $specsheet->certifications_cas }}
                        </li>
                        <li class="list-group-item">
                            <strong>EINCS:</strong> {{ $specsheet->certifications_einecs }}
                        </li>
                        <li class="list-group-item">
                            <strong>INCL:</strong> {{ $specsheet->certifications_incl }}
                        </li>
                        <li class="list-group-item">
                            <strong>Notes:</strong> {{ $specsheet->certifications_notes }}
                        </li>
                        {{--end oils--}}
                        @elseif($specsheet->is_oil_vinegar === 2 || $specsheet->is_oil_vinegar == "vinegar")
                        <h2>Organoleptic Characteristics</h2>
                        <li class="list-group-item">
                            <strong>Color:</strong> {{ $specsheet->organoleptic_color }}
                        </li>
                        <li class="list-group-item">
                            <strong>Bouquet/Odor:</strong> {{ $specsheet->organoleptic_odor }}
                        </li>
                        <li class="list-group-item">
                            <strong>Taste/Flavor:</strong> {{ $specsheet->organoleptic_flavor }}
                        </li>
                        <li class="list-group-item">
                            <strong>Appearance/Texture:</strong> {{ $specsheet->organoleptic_appearance }}
                        </li>
                        <h2>Chemical & Physical Characteristics:</h2>
                        <li class="list-group-item">
                            <strong>Total Acidity:</strong> {{ $specsheet->vinegar_acidity }}
                        </li>
                        <li class="list-group-item">
                            <strong>PH:</strong> {{ $specsheet->vinegar_ph }}
                        </li>
                        <li class="list-group-item">
                            <strong>Specific Gravity:</strong> {{ $specsheet->vinegar_gravity }}
                        </li>
                        <li class="list-group-item">
                            <strong>Foreign Matters:</strong> {{ $specsheet->vinegar_matters }}
                        </li>
                        <li class="list-group-item">
                            <strong>Heavy Metals:</strong> {{ $specsheet->vinegar_metals }}
                        </li>
                        <li class="list-group-item">
                            <strong>Total Dry Extract:</strong> {{ $specsheet->vinegar_dry }}
                        </li>
                        <li class="list-group-item">
                            <strong>Total Sulphurous Anhydride:</strong> {{ $specsheet->vinegar_sulphurous }}
                        </li>
                        <li class="list-group-item">
                            <strong>Ash:</strong> {{ $specsheet->ashes }}
                        </li>
                        <li class="list-group-item">
                            <strong>Grain:</strong> {{ $specsheet->chemical_grain }}
                        </li>
                        <li class="list-group-item">
                            <strong>Residual Alcohol:</strong> {{ $specsheet->chemical_alcohol }}
                        </li>
                        <li class="list-group-item">
                            <strong>Sugar Free Dry Extract:</strong> {{ $specsheet->chemical_sugar_free_extract }}
                        </li>
                        <li class="list-group-item">
                            <strong>Density:</strong> {{ $specsheet->vinegar_density }}
                        </li>
                        <li class="list-group-item">
                            <strong>Brix:</strong> {{ $specsheet->chemical_brix }}
                        </li>
                        <li class="list-group-item">
                            <strong>Developed Alcohol Degree:</strong> {{ $specsheet->chemical_alcohol_degree }}
                        </li>
                        <li class="list-group-item">
                            <strong>Reduced Dry Extract:</strong> {{ $specsheet->chemical_reduced_dry_extract }}
                        </li>
                        <li class="list-group-item">
                            <strong>Reducing Sugars:</strong> {{ $specsheet->reducing_sugars }}
                        </li>
                        <li class="list-group-item">
                            <strong>Rifractometric at 20°C:</strong> {{ $specsheet->chemical_rif }}
                        </li>
                        <h2>Nutrition Facts</h2>
                        <br/>
                        <table width="100%" border="1" bordercolor="#97b854" cellpadding="5" cellspacing="0">
                            <tr bgcolor="#97b854" style="padding:10px; text-align:center;color:white;">
                                <td>Nutrient</td>
                                <td>Unit</td>
                                <td>Value per 100g</td>
                                <td>Value per 100ml</td>
                            </tr>
                            <tr>
                                <td>Total Fat</td>
                                <td>g</td>
                                <td> {{ $specsheet->vinegar_nutrition_fat_100g }}</td>
                                <td> {{ $specsheet->vinegar_nutrition_fat_100ml }}</td>
                            </tr>
                            <tr>
                                <td>Sodium</td>
                                <td>mg</td>
                                <td>{{ $specsheet->vinegar_nutrition_sodium_100g }}</td>
                                <td>{{ $specsheet->vinegar_nutrition_sodium_100ml }}</td>
                            </tr>
                            <tr>
                                <td>Total Carbohydrates</td>
                                <td>g</td>
                                <td> {{ $specsheet->vinegar_nutrition_carb_100g }}</td>
                                <td>{{ $specsheet->vinegar_nutrition_carb_100ml }}</td>
                            </tr>
                            <tr>
                                <td>Sugars</td>
                                <td>g</td>
                                <td>{{ $specsheet->vinegar_nutrition_sugars_100g }}</td>
                                <td>{{ $specsheet->vinegar_nutrition_sugars_100ml }}</td>
                            </tr>
                            <tr>
                                <td>Protein</td>
                                <td>g</td>
                                <td>{{ $specsheet->vinegar_nutrition_protein_100g }}</td>
                                <td>{{ $specsheet->vinegar_nutrition_protein_100ml }}</td>
                            </tr>
                            <tr>
                                <td>Kcal</td>
                                <td>kcal</td>
                                <td>{{ $specsheet->vinegar_nutrition_kcal_100g }}</td>
                                <td>{{ $specsheet->vinegar_nutrition_kcal_100ml }}</td>
                            </tr>
                            <tr>
                                <td>Calories</td>
                                <td>cal</td>
                                <td> {{ $specsheet->vinegar_nutrition_calories_100g }}</td>
                                <td>{{ $specsheet->vinegar_nutrition_calories_100ml }}</td>
                            </tr>
                            <tr>
                                <td>Calories From Fat</td>
                                <td>cal</td>
                                <td>{{ $specsheet->vinegar_nutrition_calories_fat_100g }}</td>
                                <td>{{ $specsheet->vinegar_nutrition_calories_fat_100ml }}</td>
                            </tr>
                            <tr>
                                <td>Vitamin A</td>
                                <td>%</td>
                                <td>{{ $specsheet->vinegar_nutrition_vitamin_a_100g }}</td>
                                <td>{{ $specsheet->vinegar_nutrition_vitamin_a_100ml }}</td>
                            </tr>
                            <tr>
                                <td>Vitamin C</td>
                                <td>%</td>
                                <td>{{ $specsheet->vinegar_nutrition_vitamin_c_100g }}</td>
                                <td>{{ $specsheet->vinegar_nutrition_vitamin_c_100ml }}</td>
                            </tr>
                            <tr>
                                <td>Calcium</td>
                                <td>%</td>
                                <td> {{ $specsheet->vinegar_nutrition_calcium_100g }}</td>
                                <td>{{ $specsheet->vinegar_nutrition_calcium_100ml }}</td>
                            </tr>
                            <tr>
                                <td>Iron</td>
                                <td>%</td>
                                <td> {{ $specsheet->vinegar_nutrition_iron_100g }}</td>
                                <td>{{ $specsheet->vinegar_nutrition_iron_100ml }}</td>
                            </tr>
                            <tr>
                                <td>Cholesterol</td>
                                <td>mg</td>
                                <td>{{ $specsheet->vinegar_nutrition_cholesterol_100g }}</td>
                                <td>{{ $specsheet->vinegar_nutrition_cholesterol_100ml }}</td>
                            </tr>
                            <tr>
                                <td>Fiber</td>
                                <td>g</td>
                                <td>{{ $specsheet->vinegar_nutrition_fiber_100g }}</td>
                                <td>{{ $specsheet->vinegar_nutrition_fiber_100ml }}</td>
                            </tr>
                            <tr>
                                <td>Moisture</td>
                                <td>g</td>
                                <td>{{ $specsheet->vinegar_nutrition_moisture_100g }}</td>
                                <td>{{ $specsheet->vinegar_nutrition_moisture_100ml }}</td>
                            </tr>
                        </table>
                        <h2>Other Product Information:</h2>
                        <li class="list-group-item">
                            <strong>Other Product Info 1:</strong> {{ $specsheet->other_product_info1 }}
                        </li>
                        <li class="list-group-item">
                            <strong>Other Product Info 2:</strong> {{ $specsheet->other_product_info2 }}
                        </li>
                        <li class="list-group-item">
                            <strong>Other Product Info 3:</strong> {{ $specsheet->other_product_info3 }}
                        </li>
                        <li class="list-group-item">
                            <strong>Other Product Info 4:</strong> {{ $specsheet->other_product_info4 }}
                        </li>
                        <li class="list-group-item">
                            <strong>Other Product Info 5:</strong> {{ $specsheet->other_product_info5 }}
                        </li>
                        <li class="list-group-item">
                            <strong>Other Product Info 6:</strong> {{ $specsheet->other_product_info6 }}
                        </li>
                        {{--end vinegar--}}

                    @endif
                </div><!--end right-->



                </div>

            <hr/>

            @if($specsheet->sds_active == '1')

                <h1>SDS</h1>
                 <ul class="list-group">
            <li class="list-group-item"><strong>Active  </strong>@if($specsheet->sds_active == '1') Active @else Not Active @endif</li>
            <li class="list-group-item"><strong>Name </strong>{{ $specsheet->sds_name }}</li>
            <li class="list-group-item"><strong>Special Hazard for personal </strong>{{ $specsheet->sds_special_hazard_for_personal }}</li>
            <li class="list-group-item"><strong>CAS Number </strong>{{ $specsheet->sds_cas_number }}</li>
            <li class="list-group-item"><strong>Upon in halation </strong>{{ $specsheet->sds_upon_inhalation }}</li>
            <li class="list-group-item"><strong> Upon Skin contact </strong>{{ $specsheet->sds_upon_skin_contact }}</li>
            <li class="list-group-item"><strong> Extinguishing Media </strong>{{ $specsheet->sds_extinguishing_media }}</li>
            <li class="list-group-item"><strong> Not to be used extinguishing media </strong>{{ $specsheet->sds_not_to_be_used_extinguishing_media }}</li>
            <li class="list-group-item"><strong> Exposure Hazard </strong>{{ $specsheet->sds_special_exposure_hazard }}</li>
            <li class="list-group-item"><strong> Special protection </strong>{{ $specsheet->sds_special_protection }}</li>
            <li class="list-group-item"><strong> precautions related to personnel </strong>{{ $specsheet->sds_precautions_related_to_personnel }}</li>
            <li class="list-group-item"><strong> procedure for cleanup </strong>{{ $specsheet->sds_procedure_for_cleanup }}</li>
            <li class="list-group-item"><strong> small spill </strong>{{ $specsheet->sds_small_spill }}</li>
            <li class="list-group-item"><strong> large spill </strong>{{ $specsheet->sds_large_spill }}</li>
            <li class="list-group-item"><strong> handling </strong>{{ $specsheet->sds_handling }}</li>
            <li class="list-group-item"><strong> storage </strong>{{ $specsheet->sds_storage }}</li>
            <li class="list-group-item"><strong> threshold limit value </strong>{{ $specsheet->sds_threshold_limit_value }}</li>
            <li class="list-group-item"><strong> inhalation health risks </strong>{{ $specsheet->sds_inhalation_health_risks }}</li>
            <li class="list-group-item"><strong> skin absorption </strong>{{ $specsheet->sds_skin_absorption }}</li>
            <li class="list-group-item"><strong> appearance </strong>{{ $specsheet->sds_appearance }}</li>
            <li class="list-group-item"><strong> form </strong>{{ $specsheet->sds_form }}</li>
            <li class="list-group-item"><strong> color </strong>{{ $specsheet->sds_color }}</li>
            <li class="list-group-item"><strong> odor </strong>{{ $specsheet->sds_odor }}</li>
            <li class="list-group-item"><strong> melting point </strong>{{ $specsheet->sds_melting_point }}</li>
            <li class="list-group-item"><strong> flash point </strong>{{ $specsheet->sds_flash_point }}</li>
            <li class="list-group-item"><strong> ignition temperature </strong>{{ $specsheet->sds_ignition_temperature }}</li>
            <li class="list-group-item"><strong> density </strong>{{ $specsheet->sds_density }}</li>
            <li class="list-group-item"><strong> water solubility </strong>{{ $specsheet->sds_water_solubility }}</li>
            <li class="list-group-item"><strong> general information </strong>{{ $specsheet->sds_general_information }}</li>
            <li class="list-group-item"><strong> conditions to be avoided </strong>{{ $specsheet->sds_conditions_to_be_avoided }}</li>
            <li class="list-group-item"><strong> dangerous decomotioin product </strong>{{ $specsheet->sds_dangerous_decomposition_product }}</li>
            <li class="list-group-item"><strong> toxicoligical testing </strong>{{ $specsheet->sds_toxicological_testing }}</li>
            <li class="list-group-item"><strong> practical experience </strong>{{ $specsheet->sds_practical_experience }}</li>
            <li class="list-group-item"><strong> general considerations </strong>{{ $specsheet->sds_general_considerations }}</li>
            <li class="list-group-item"><strong> information on product </strong>{{ $specsheet->sds_information_on_product }}</li>
            <li class="list-group-item"><strong> behavior in ecological setting </strong>{{ $specsheet->sds_behavior_in_ecological_setting }}</li>
            <li class="list-group-item"><strong> toxic effects on the ecosystem </strong>{{ $specsheet->sds_toxic_effects_on_the_ecosystem }}</li>
            <li class="list-group-item"><strong> additional ecological information </strong>{{ $specsheet->sds_additional_ecological_information }}</li>
            <li class="list-group-item"><strong> disposal considerations </strong>{{ $specsheet->sds_disposal_considerations }}</li>
            <li class="list-group-item"><strong> transportation information </strong>{{ $specsheet->sds_transportation_information }}</li>
            <li class="list-group-item"><strong> regulatory information </strong>{{ $specsheet->sds_regulatory_information }}</li>
                 </ul>
            @endif

            <div class="alert alert-dismissible alert-success" style="margin-bottom:0px">
                <strong>Created: </strong><?php echo \Carbon\Carbon::createFromTimestamp(strtotime($specsheet->created_at))->diffForHumans(); ?>
            </div>
            <div class="alert alert-dismissible alert-warning">
                <strong>Updated: </strong><?php echo \Carbon\Carbon::createFromTimestamp(strtotime($specsheet->updated_at))->diffForHumans(); ?>
            </div>
        </div>
@stop