<h2>Other Product Information:</h2>
<div class="form-group">
    {{ Form::label('other_product_info1','Other Product Info 1:') }}
    {{ Form::textarea('other_product_info1', null, ['class' => 'form-control','rows' => 8, 'placeholder' => 'Other Product Info 1']) }}
</div>
<div class="form-group">
    {{ Form::label('other_product_info2', 'Other Product Info 2:' ) }}
    {{ Form::textarea('other_product_info2', null, ['class' => 'form-control','rows' => 8, 'placeholder' => 'Other Product Info 2']) }}
</div>
<div class="form-group">
    {{ Form::label('other_product_info3','Other Product Info 3:') }}
    {{ Form::textarea('other_product_info3', null, ['class' => 'form-control','rows' => 8, 'placeholder' => 'other_product_info3']) }}
</div>
<div class="form-group">
    {{ Form::label('other_product_info4','Other Product Info 4:') }}
    {{ Form::textarea('other_product_info4', null, ['class' => 'form-control', 'rows' => 8, 'placeholder' => 'Other Product Info 4']) }}
</div>
<div class="form-group">
    {{ Form::label('other_product_info5','Other Product Info 5:') }}
    {{ Form::textarea('other_product_info5', null, ['class' => 'form-control', 'rows' => 8, 'placeholder' => 'Other Product Info 5']) }}
</div>
<div class="form-group">
    {{ Form::label('other_product_info6','Other Product Info 6:') }}
    {{ Form::textarea('other_product_info6', null, ['class' => 'form-control', 'rows' => 8, 'placeholder' => 'Other Product Info 6']) }}
</div>