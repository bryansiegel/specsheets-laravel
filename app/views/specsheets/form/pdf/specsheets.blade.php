@extends('layouts.pdf_specsheets')

@section('content')



    <div style="width:900px;">
<table width="78%">
    <tr>
        <td width="190px"><img src="http://cibariaspecsheets.com/images/cibaria-logo.jpg" alt="" width="178" height="157"/></td>
        <td width="170px">
        <strong>705 Columbia Ave<br/>
                Riverside, CA 92507<br/>
                (P) (951) 823-8490<br/>
                (F) (951) 823-8495<br/>
                (E) <a href="mailto:info@cibaria-intl.com">info@cibaria-intl.com</a>
            </strong>
            </span>
        </td>
        <td>
            <span style="font-size:24px;font-weight:bold;"><u>Specification Sheet</u></span><br />
            Label Ingredients Statement:<br />
            <span style="font-size:28px;font-weight:bold;"><?php echo $specsheet->title;?></span>

        </td>
    </tr>
    <tr>
    <td colspan="3" height="4px" bgcolor="#97b854">
    </td>
    </tr>
</table>
        <br/>

        <div style="width:78%">
            <u><strong>Product Description:</strong></u> <?php echo $specsheet->description;?>
        </div>

<table width="78%">
<tr>
    <td><ul>
                    <?php if ($specsheet->bullet_point1) {
                        echo "<li>" . $specsheet->bullet_point1 . "</li>";
                    }
                    ?>
                    <?php if ($specsheet->bullet_point2) {
                        echo "<li>" . $specsheet->bullet_point2 . "</li>";
                    }
                    ?>
                    <?php if ($specsheet->bullet_point3) {
                        echo "<li>" . $specsheet->bullet_point3 . "</li>";
                    }
                    ?>
                    <?php if ($specsheet->bullet_point4) {
                        echo "<li>" . $specsheet->bullet_point4 . "</li>";
                    }
                    ?>
                </ul></td>
    <td>
        <ul>
                    <?php if ($specsheet->bullet_point5) {
                        echo "<li>" . $specsheet->bullet_point5 . "</li>";
                    }
                    ?>
                    <?php if ($specsheet->bullet_point6) {
                        echo "<li>" . $specsheet->bullet_point6 . "</li>";
                    }
                    ?>
                    <?php if ($specsheet->bullet_point7) {
                        echo "<li>" . $specsheet->bullet_point7 . "</li>";
                    }
                    ?>
                    <?php if ($specsheet->bullet_point8) {
                        echo "<li>" . $specsheet->bullet_point8 . "</li>";
                    }
                    ?>

                </ul>

    </td>

</tr>
<tr>
    <td><strong><u>Prop 65 Statement:</u></strong> {{ $specsheet->prop_65_statement }}</td>
</tr>
</table>
        <!-- end bullet points -->

        <!-- middle section -->
        <div style="clear:both"></div>
        <br/>

        <table width="78%" cellpadding="10">
            <tr>
                <td valign="top">
                    <div style="border: 1px solid #97b854; width:400px; pxpadding:5px; height:350px;"><!-- allergen disclosure -->
                <h3 style="text-align:center;"><u>Allergen Disclosure</u></h3>

                <p style="padding:10px;">Statement: <?php echo $specsheet->title;?>  does not contain any of the following allergens, sensitive ingredients, or restricted ingredients:</p>
                <!-- bullet list -->

                <table>
                <?php if (isset($specsheet) && $specsheet->is_oil_vinegar == 1 || $specsheet->is_oil_vinegar == "oil") {?>
                <tr>
                <td>Peanut/peanut products<br/>
                        Egg/egg products<br/>
                        Shellfish/shellfish products<br/>
                        <?php if (!$specsheet->title === "Macadamia Nut Oil, Virgin") {?>
                        Nuts/nut products<br/>
                        <?php }
                        ?>
                        Other legumes<br/>
                        Gluten<br/>
                        Animal fat/oil<br/>
                        Hydrolyzed vegetable protein<br/>
                        Sulfites<br/>
                        BMA<br/>
                        FD&C colors<br/>
                        Food Starch<br/></td>
                <td>Milk/milk products<br/>
                        Fish/fish products<br/>
                        Mollusks<br/>
                        Soy/soybeans/soy products<br/>
                        Wheat/wheat products<br/>
                        Lecithin<br/>
                        Hydrolyzed animal protein<br/>
                        Yeast Extract<br/>
                        Monosodium Glutamate<br/>
                        BHT<br/>
                        Natural Colors<br/>
                        Maltodextrin<br/></td>
                </tr>
 <?php } else {?>
 <tr>
     <td>
           Peanut/peanut products<br/>
                        Egg/egg products<br/>
                        Shellfish/shellfish products<br/>
                        Nuts/nut products<br/>
                        Other legumes<br/>
                        Gluten<br/>
                        Animal fat/oil<br/>
                        Hydrolyzed vegetable protein<br/>
                        BMA<br/>
                        FD&C colors<br/>
                        Food Starch<br/>
     </td>
     <td>
          Milk/milk products<br/>
                        Fish/fish products<br/>
                        Mollusks<br/>
                        Soy/soybeans/soy products<br/>
                        Wheat/wheat products<br/>
                        Lecithin<br/>
                        Hydrolyzed animal protein<br/>
                        Yeast Extract<br/>
                        Monosodium Glutamate<br/>
                        BHT<br/>
                        Natural Colors<br/>
                        Maltodextrin<br/>
     </td>
 </tr>

<?php } ?>

                </table>

                
                <?php if (isset($specsheet) && $specsheet->is_oil_vinegar == 1 || $specsheet->is_oil_vinegar == "oil") {?>
                <br />
                <div style="margin-top:15px;">
                    <p style="width: 350px;word-wrap: break-word;margin-left:5px;"><?php echo $specsheet->under_allergen;?></p>
                </div>
                <?php }
                ?>


            </div>

                </td>
                <td width="5%">
                </td>
                <td valign="top" width="60%"><img src="{{"/home/cibsheet/public_html/images/nutrition/" . $specsheet->nutrition }}" alt="" width="200" height="400"/>
                <?php if ($specsheet->is_oil_vinegar == 1 || $specsheet->is_oil_vinegar == "oil") {?>
                <p style="margin-left:65px;"><?php echo $specsheet->nutrition_fact1;?></p>

                <p style="margin-left:65px;"><?php echo $specsheet->nutrition_fact2;?></p>

                <p style="margin-left:65px;"><?php echo $specsheet->nutrition_fact3;?></p>
                <?php } else {echo "";}
                ?></td>
            </tr>


        </table>

        <div style="clear:both;"></div>
        <div><!-- nutrition facts -->

        </div>

    </div><!-- end middle container -->
    <div style="clear:both;"></div>
    <br/>
    <div style="width:78%"">
    <p><strong><u>Storage</u></strong>: <?php echo $specsheet->storage;?></p>
    <p><strong><u>Shelf Life</u></strong>: <?php echo $specsheet->shelf_life;?></p>
    <p><strong><u>Sewer Sludge and Irradiation Statement</u></strong>: <?php echo $specsheet->sewer;?></p>
    <p><strong><u>Applications For Product</u></strong>: <?php echo $specsheet->applications;?></p>
    <p><strong><u>Country of Origin</u></strong>: <?php echo $specsheet->country;?></p>
    </div>
    <br/>
    <br/>
    <!--2nd page-->
    <div>
        <div class="page"></div>



            <?php if ($specsheet->is_oil_vinegar == 1 || $specsheet->is_oil_vinegar == "oil") {?>
            <h1 style="margin-left:150px;">USDA NDB (National Nutrition Database)</h1>
            <table  border="1" bordercolor="#97b854" cellpadding="5" cellspacing="0" width="70%">
                <tr border="1" bordercolor="#97b854" bgcolor="#97b854" style="padding:10px; text-align:center;color:white;">
                    <td>Nutrient</td>
                    <td>Unit</td>
                    <td>Value per 100.0g</td>
                    <td>Tbsp 13.5g</td>
                </tr>
                <tr>
                    <td colspan="4" bgcolor="#d6e3bc"> Proximates</td>
                </tr>
                <tr>
                    <td>Water</td>
                    <td>g</td>
                    <td><?php echo $specsheet->water_value;?></td>
                    <td><?php echo $specsheet->water_tbsp;?></td>
                </tr>
                <tr>
                    <td>Energy</td>
                    <td>kcal</td>
                    <td><?php echo $specsheet->energy_value;?></td>
                    <td><?php echo $specsheet->energy_tbsp;?></td>
                </tr>
                <tr>
                    <td>Protein</td>
                    <td>g</td>
                    <td><?php echo $specsheet->protein_value;?></td>
                    <td><?php echo $specsheet->protein_tbsp;?></td>
                </tr>
                <tr>
                    <td>Total lipid (fat)</td>
                    <td>g</td>
                    <td><?php echo $specsheet->totallipid_value;?></td>
                    <td><?php echo $specsheet->totallipid_tbsp;?></td>
                </tr>
                <tr>
                    <td>Carbohydrate, by difference</td>
                    <td>g</td>
                    <td><?php echo $specsheet->carbohydrate_value;?></td>
                    <td><?php echo $specsheet->carbohydrate_tbsp;?></td>
                </tr>
                <tr>
                    <td>Fiber, total dietary</td>
                    <td>g</td>
                    <td><?php echo $specsheet->fiber_value;?></td>
                    <td><?php echo $specsheet->fiber_tbsp;?></td>
                </tr>
                <tr>
                    <td>Sugars, total</td>
                    <td>g</td>
                    <td><?php echo $specsheet->sugars_value;?></td>
                    <td><?php echo $specsheet->sugars_tbsp;?></td>
                </tr>
                <tr>
                    <td colspan="4" bgcolor="#d6e3bc">Minerals</td>
                </tr>
                <tr>
                    <td>Calcium, Ca</td>
                    <td>mg</td>
                    <td><?php echo $specsheet->calcium_value;?></td>
                    <td><?php echo $specsheet->calcium_tbsp;?></td>
                </tr>
                <tr>
                    <td>Iron, Fe</td>
                    <td>mg</td>
                    <td><?php echo $specsheet->iron_value;?></td>
                    <td><?php echo $specsheet->iron_tbsp;?></td>
                </tr>
                <tr>
                    <td>Magnesium, Mg</td>
                    <td>mg</td>
                    <td><?php echo $specsheet->magnesium_value;?></td>
                    <td><?php echo $specsheet->magnesium_tbsp;?></td>
                </tr>
                <tr>
                    <td>Phosphorus, P</td>
                    <td>mg</td>
                    <td><?php echo $specsheet->phosphorus_value;?></td>
                    <td><?php echo $specsheet->phosphorus_tbsp;?></td>
                </tr>
                <tr>
                    <td>Potassium, K</td>
                    <td>mg</td>
                    <td><?php echo $specsheet->potassium_value;?></td>
                    <td><?php echo $specsheet->potassium_tbsp;?></td>
                </tr>
                <tr>
                    <td>Sodium, Na</td>
                    <td>mg</td>
                    <td><?php echo $specsheet->sodium_value;?></td>
                    <td><?php echo $specsheet->sodium_tbsp;?></td>
                </tr>
                <tr>
                    <td>Zinc, Zn</td>
                    <td>mg</td>
                    <td><?php echo $specsheet->zinc_value;?></td>
                    <td><?php echo $specsheet->zinc_tbsp;?></td>
                </tr>
                <tr>
                    <td colspan="4" bgcolor="#d6e3bc">Vitamins</td>
                </tr>
                <tr>
                    <td>Vitamin C, total ascorbic acid</td>
                    <td>mg</td>
                    <td><?php echo $specsheet->vitaminc_value;?></td>
                    <td><?php echo $specsheet->vitaminc_tbsp;?></td>
                </tr>
                <tr>
                    <td>Thiamin</td>
                    <td>mg</td>
                    <td><?php echo $specsheet->thiamin_value;?></td>
                    <td><?php echo $specsheet->thiamin_tbsp;?></td>
                </tr>
                <tr>
                    <td>Riboflavin</td>
                    <td>mg</td>
                    <td><?php echo $specsheet->riboflavin_value;?></td>
                    <td><?php echo $specsheet->riboflavin_tbsp;?></td>
                </tr>
                <tr>
                    <td>Niacin</td>
                    <td>mg</td>
                    <td><?php echo $specsheet->niacin_value;?></td>
                    <td><?php echo $specsheet->niacin_tbsp;?></td>
                </tr>
                <tr>
                    <td>Vitamin B-6</td>
                    <td>mg</td>
                    <td><?php echo $specsheet->vitaminb6_value;?></td>
                    <td><?php echo $specsheet->vitaminb6_tbsp;?></td>
                </tr>
                <tr>
                    <td>Folate, DFE</td>
                    <td>�g</td>
                    <td><?php echo $specsheet->folate_value;?></td>
                    <td><?php echo $specsheet->folate_tbsp;?></td>
                </tr>
                <tr>
                    <td>Vitamin B-12</td>
                    <td>�g</td>
                    <td><?php echo $specsheet->vitaminb12_value;?></td>
                    <td><?php echo $specsheet->vitaminb12_tbsp;?></td>
                </tr>
                <tr>
                    <td>Vitamin A, RAE</td>
                    <td>�g</td>
                    <td><?php echo $specsheet->vitaminar_value;?></td>
                    <td><?php echo $specsheet->vitaminar_tbsp;?></td>
                </tr>
                <tr>
                    <td>Vitamin A, IU</td>
                    <td>IU</td>
                    <td><?php echo $specsheet->vitaminai_value;?></td>
                    <td><?php echo $specsheet->vitaminai_tbsp;?></td>
                </tr>
                <tr>
                    <td>Vitamin E (alpha-tocopherol)</td>
                    <td>mg</td>
                    <td><?php echo $specsheet->vitamine_value;?></td>
                    <td><?php echo $specsheet->vitamine_tbsp;?></td>
                </tr>
                <tr>
                    <td>Vitamin D (D2+D3)</td>
                    <td>�g</td>
                    <td><?php echo $specsheet->vitamind2_value;?></td>
                    <td><?php echo $specsheet->vitamind2_tbsp;?></td>
                </tr>
                <tr>
                    <td>Vitamin D</td>
                    <td>IU</td>
                    <td><?php echo $specsheet->vitamind_value;?></td>
                    <td><?php echo $specsheet->vitamind_tbsp;?></td>
                </tr>
                <tr>
                    <td>Vitamin K (phylloquinone)</td>
                    <td>�g</td>
                    <td><?php echo $specsheet->vitamink_value;?></td>
                    <td><?php echo $specsheet->vitamink_tbsp;?></td>
                </tr>
                <tr>
                    <td colspan="4" bgcolor="#d6e3bc">Lipids</td>
                </tr>
                <tr>
                    <td>Fatty acids, total saturated</td>
                    <td>g</td>
                    <td><?php echo $specsheet->fattysaturated_value;?></td>
                    <td><?php echo $specsheet->fattysaturated_tbsp;?></td>
                </tr>
                <tr>
                    <td>Fatty acids, total monounsaturated</td>
                    <td>g</td>
                    <td><?php echo $specsheet->fattymonoun_value;?></td>
                    <td><?php echo $specsheet->fattymonoun_tbsp;?></td>
                </tr>
                <tr>
                    <td>Fatty acids, polyunsaturated</td>
                    <td>g</td>
                    <td><?php echo $specsheet->fattypoly_value;?></td>
                    <td><?php echo $specsheet->fattypoly_tbsp;?></td>
                </tr>
                <tr>
                    <td>Cholesterol</td>
                    <td>mg</td>
                    <td><?php echo $specsheet->cholesterol_value;?></td>
                    <td><?php echo $specsheet->cholesterol_tbsp;?></td>
                </tr>
                <tr>
                    <td colspan="4" bgcolor="#d6e3bc">Other</td>
                </tr>
                <tr>
                    <td>Caffeine</td>
                    <td>mg</td>
                    <td><?php echo $specsheet->caffeine_value;?></td>
                    <td><?php echo $specsheet->caffeine_tbsp;?></td>
                </tr>
            </table>

       
        <!--end usda-->
        <?php } else {?>
                <!--vinegar nutrition facts-->
                <br />
                <br />
                <br />
                <br />
        <h1 style="margin-left:150px;">Nutrition Facts</h3>
        <table  border="1" bordercolor="#97b854" cellpadding="5" cellspacing="0" width="70%">
            <tr bgcolor="#97b854" style="padding:10px; text-align:center;color:white;">
                <td>Nutrient</td>
                <td>Unit</td>
                <td>Value per 100g</td>
                <td>Value per 100ml</td>
            </tr>
            <tr>
                <td>Total Fat</td>
                <td>g</td>
                <td><?php echo $specsheet->vinegar_nutrition_fat_100g;?></td>
                <td><?php echo $specsheet->vinegar_nutrition_fat_100ml;?></td>
            </tr>
            <tr>
                <td>Sodium</td>
                <td>mg</td>
                <td><?php echo $specsheet->vinegar_nutrition_sodium_100g;?></td>
                <td><?php echo $specsheet->vinegar_nutrition_sodium_100ml;?></td>
            </tr>
            <tr>
                <td>Total Carbohydrates</td>
                <td>g</td>
                <td><?php echo $specsheet->vinegar_nutrition_carb_100g;?></td>
                <td><?php echo $specsheet->vinegar_nutrition_carb_100ml;?></td>
            </tr>
            <tr>
                <td>Sugars</td>
                <td>g</td>
                <td><?php echo $specsheet->vinegar_nutrition_sugars_100g;?></td>
                <td><?php echo $specsheet->vinegar_nutrition_sugars_100ml;?></td>
            </tr>
            <tr>
                <td>Protein</td>
                <td>g</td>
                <td><?php echo $specsheet->vinegar_nutrition_protein_100g;?></td>
                <td><?php echo $specsheet->vinegar_nutrition_protein_100ml;?></td>
            </tr>
            <tr>
                <td>Kcal</td>
                <td>kcal</td>
                <td><?php echo $specsheet->vinegar_nutrition_kcal_100g;?></td>
                <td><?php echo $specsheet->vinegar_nutrition_kcal_100ml;?></td>
            </tr>
            <tr>
                <td>Calories</td>
                <td>cal</td>
                <td><?php echo $specsheet->vinegar_nutrition_calories_100g;?></td>
                <td><?php echo $specsheet->vinegar_nutrition_calories_100ml;?></td>
            </tr>
            <tr>
                <td>Calories From Fat</td>
                <td>cal</td>
                <td><?php echo $specsheet->vinegar_nutrition_calories_fat_100g;?></td>
                <td><?php echo $specsheet->vinegar_nutrition_calories_fat_100ml;?></td>
            </tr>
            <tr>
                <td>Vitamin A</td>
                <td>%</td>
                <td><?php echo $specsheet->vinegar_nutrition_vitamin_a_100g;?></td>
                <td><?php echo $specsheet->vinegar_nutrition_vitamin_a_100ml;?></td>
            </tr>
            <tr>
                <td>Vitamin C</td>
                <td>%</td>
                <td><?php echo $specsheet->vinegar_nutrition_vitamin_c_100g;?></td>
                <td><?php echo $specsheet->vinegar_nutrition_vitamin_c_100ml;?></td>
            </tr>
            <tr>
                <td>Calcium</td>
                <td>%</td>
                <td><?php echo $specsheet->vinegar_nutrition_calcium_100g;?></td>
                <td><?php echo $specsheet->vinegar_nutrition_calcium_100ml;?></td>
            </tr>
            <tr>
                <td>Iron</td>
                <td>%</td>
                <td><?php echo $specsheet->vinegar_nutrition_iron_100g;?></td>
                <td><?php echo $specsheet->vinegar_nutrition_iron_100ml;?></td>
            </tr>
            <tr>
                <td>Cholesterol</td>
                <td>mg</td>
                <td><?php echo $specsheet->vinegar_nutrition_cholesterol_100g;?></td>
                <td><?php echo $specsheet->vinegar_nutrition_cholesterol_100ml;?></td>
            </tr>
            <tr>
                <td>Fiber</td>
                <td>g</td>
                <td><?php echo $specsheet->vinegar_nutrition_fiber_100g;?></td>
                <td><?php echo $specsheet->vinegar_nutrition_fiber_100ml;?></td>
            </tr>
            <tr>
                <td>Moisture</td>
                <td>g</td>
                <td><?php echo $specsheet->vinegar_nutrition_moisture_100g;?></td>
                <td><?php echo $specsheet->vinegar_nutrition_moisture_100ml;?></td>
            </tr>
        </table>

 
    <!--end vinegar nutrition facts-->
    <?php }
    ?>
<br />
<br />

    <?php if ($specsheet->is_oil_vinegar == 1 || $specsheet->is_oil_vinegar == "oil") {?>
            <!--    organoleptic characteristics-->
    <div style="border: 1px solid #97b854; width:65%; padding:15px;">
        <table border="0" width="50%">
            <tr>
                <td colspan="2"><h3><u>Organoleptic Characteristics:</u></h3></td>
            </tr>
            <tr>
                <td>Appearance/Clarity</td>
                <td><?php echo $specsheet->organoleptic_appearance;?></td>
            </tr>
            <tr>
                <td>Flavor/Odor</td>
                <td><?php echo $specsheet->organoleptic_flavor;?></td>
            </tr>
            <tr>
                <td>Color (Lovibond) Red</td>
                <td><?php echo $specsheet->organoleptic_color_red;?></td>
            </tr>
            <tr>
                <td>Color (Lovibond) Yellow</td>
                <td><?php echo $specsheet->organoleptic_color_yellow;?></td>
            </tr>

        </table>
    </div>
    <!--    end organoleptic characteristics-->


    <!--    typical analysis range-->
    <div style="border: 1px solid #97b854; width:65%;padding:15px;">
        <table  border="0" width="50%">
            <tr>
                <td colspan="2"><h3><u>Typical Analysis Ranges:</u></h3></td>
            </tr>
            <tr>
                <td>Free Fatty Acid (% m/m expressed in oleic acid)</td>
                <td><?php echo $specsheet->typical_fatty_acid;?></td>
            </tr>
            <tr>
                <td>Moisture</td>
                <td><?php echo $specsheet->typical_moisture2;?></td>
            </tr>
            <tr>
                <td>Peroxide Value</td>
                <td><?php echo $specsheet->typical_peroxide;?></td>
            </tr>
            <tr>
                <td>Iodine Value</td>
                <td><?php echo $specsheet->typical_iodine;?></td>
            </tr>
            <tr>
                <td>Saponification Value</td>
                <td><?php echo $specsheet->typical_saponification;?></td>
            </tr>
            <tr>
                <td>p-Anisidine Value</td>
                <td><?php echo $specsheet->typical_anisidine;?></td>
            </tr>
            <tr>
                <td>Cold Test</td>
                <td><?php echo $specsheet->typical_cold;?></td>
            </tr>
            <tr>
                <td>Refractive Index</td>
                <td><?php echo $specsheet->typical_refractive;?></td>
            </tr>
            <tr>
                <td>Specific Gravity</td>
                <td><?php echo $specsheet->typical_gravity;?></td>
            </tr>
            <tr>
                <td>Oil Stability Index(OSI)</td>
                <td><?php echo $specsheet->typical_stability;?></td>
            </tr>
            <tr>
                <td>Smoke Point</td>
                <td><?php echo $specsheet->typical_smoke;?></td>
            </tr>
            <tr>
                <td>Additives</td>
                <td><?php echo $specsheet->typical_additives;?></td>
            </tr>

        </table>
    </div>
    <!--    end typical analysis range-->

    <!--typical fatty acid-->
    <div style="border: 1px solid #97b854; width:65%;padding:15px;">

        <table  border="0" width="50%">
            <tr>
                <td colspan="2"><h3><u>Typical Fatty Acid Ranges:</u></h3></td>
            </tr>
            <tr>
                <td colspan="2">
                    <table border="0">
                        <tr>
                            <td width="125">C 14:0</td>
                            <td width="263">Myristic acid</td>
                            <td width="298"><?php echo $specsheet->typical_myristic;?></td>
                        </tr>
                        <tr>
                            <td>C 16:0</td>
                            <td>Palmitic Acid</td>
                            <td><?php echo $specsheet->typical_palmitic;?></td>
                        </tr>
                        <tr>
                            <td>C 16:1</td>
                            <td>Palmitoleic Acid</td>
                            <td><?php echo $specsheet->typical_palmitoleic;?></td>
                        </tr>
                        <tr>
                            <td>C 17:0</td>
                            <td>Heptadecanoic Acid</td>
                            <td><?php echo $specsheet->typical_heptadecanoic;?></td>
                        </tr>
                        <tr>
                            <td>C 17:1</td>
                            <td>Heptadecenoic acid</td>
                            <td><?php echo $specsheet->typical_heptadecenoic;?></td>
                        </tr>
                        <tr>
                            <td>C 18:0</td>
                            <td>Stearic acid</td>
                            <td><?php echo $specsheet->typical_stearic;?></td>
                        </tr>
                        <tr>
                            <td>C 18:1</td>
                            <td>Oleic acid</td>
                            <td><?php echo $specsheet->typical_oleic;?></td>
                        </tr>
                        <tr>
                            <td>C 18:2</td>
                            <td>Linoleic acid</td>
                            <td><?php echo $specsheet->typical_linoleic;?></td>
                        </tr>
                        <tr>
                            <td>C 18:3</td>
                            <td>Linolenic acid</td>
                            <td><?php echo $specsheet->typical_linolenic;?></td>
                        </tr>
                        <tr>
                            <td>C 20:0</td>
                            <td>Arachidic acid</td>
                            <td><?php echo $specsheet->typical_arachidic;?></td>
                        </tr>
                        <tr>
                            <td>C 20:1</td>
                            <td>Gadoleic acid (eicosenoic)</td>
                            <td><?php echo $specsheet->typical_gadoleic;?></td>
                        </tr>
                        <tr>
                            <td>C 22:0</td>
                            <td>Behenic acid</td>
                            <td><?php echo $specsheet->typical_behenic;?></td>
                        </tr>
                        <tr>
                            <td>C 22:1</td>
                            <td>Erucic Acid</td>
                            <td><?php echo $specsheet->typical_erucic;?></td>
                        </tr>
                        <tr>
                            <td>C 24:0</td>
                            <td>Lignoceric Acid</td>
                            <td><?php echo $specsheet->typical_lignoceric;?></td>
                        </tr>

                    </table>
                </td>
            </tr>
        </table>
    </div>

    <div style="border: 1px solid #97b854; width:65%;padding:15px;">
        <h3><u>Registrations and Other Product Information:</u></h3>
        <br/>
        <h4 style="text-align: left; padding-left:70px;">CAS <?php echo $specsheet->certifications_cas;?></h4>
        <h4 style="text-align: left; padding-left:70px;">EINCS <?php echo $specsheet->certifications_einecs;?></h4>
        <h4 style="text-align: left; padding-left:70px;">INCL: <?php echo $specsheet->certifications_incl;?></h4>
    </div>
<br />
<br />
<br />
<br />
<br />
<br />
    <div style="border: 1px solid #97b854; width:65%;padding:15px;">
        <h4 style="text-align: left; padding-left:70px;">Notes: <?php echo $specsheet->certifications_notes;?></h4>
    </div>

    </div>
    <br />
    <br />
    <div style="margin-top:15px;">
        <p style="margin-left:280px;">
            <img src="http://cibariaspecsheets.com/images/leaf.png" alt=""/>
        </p>

    </div>
    <!--end-->
    <?php } elseif ($specsheet->is_oil_vinegar == 2 || $specsheet->is_oil_vinegar == "vinegar") {?>

            <!--vinegar stuff-->
    <!--    organoleptic characteristics-->
    <div style="border: 1px solid #97b854; width:65%;padding:15px;">
        <table border="0" width="50%">
            <tr>
                <td colspan="2"><h3><u>Organoleptic Characteristics:</u></h3></td>
            </tr>
            <tr>
                <td>Color</td>
                <td><?php echo $specsheet->organoleptic_color;?></td>
            </tr>
            <tr>
                <td>Bouquet/Odor</td>
                <td><?php echo $specsheet->organoleptic_odor;?></td>
            </tr>
            <tr>
                <td>Taste/Flavor</td>
                <td><?php echo $specsheet->organoleptic_flavor;?></td>
            </tr>
            <tr>
                <td>Appearance/Texture</td>
                <td><?php echo $specsheet->organoleptic_appearance;?></td>
            </tr>

        </table>
    </div>
    <!--    end organoleptic characteristics-->


    <!--    typical analysis range-->
    <div style="border: 1px solid #97b854; width:65%;padding:15px;">
        <table  border="0" width="50%">
            <tr>
                <td colspan="2"><h3><u>Chemical & Physical Characteristics:</u></h3></td>
            </tr>
            <tr>
                <td>Total Acidity</td>
                <td><?php echo $specsheet->vinegar_acidity;?></td>
            </tr>
            <tr>
                <td>PH</td>
                <td><?php echo $specsheet->vinegar_ph;?></td>
            </tr>
            <tr>
                <td>Specific Gravity</td>
                <td><?php echo $specsheet->vinegar_gravity;?></td>
            </tr>
            <tr>
                <td>Foreign Matters</td>
                <td><?php echo $specsheet->vinegar_matters;?></td>
            </tr>
            <tr>
                <td>Heavy Metals</td>
                <td><?php echo $specsheet->vinegar_metals;?></td>
            </tr>
            <tr>
                <td>Total Dry Extract</td>
                <td><?php echo $specsheet->vinegar_dry;?></td>
            </tr>
            <tr>
                <td>Total Sulphurous Anhydride</td>
                <td><?php echo $specsheet->vinegar_sulphurous;?></td>
            </tr>
            <tr>
                <td>Ash</td>
                <td><?php echo $specsheet->ashes;?></td>
            </tr>
            <tr>
                <td>Grain</td>
                <td><?php echo $specsheet->chemical_grain;?></td>
            </tr>
            <tr>
                <td>Residual Alcohol</td>
                <td><?php echo $specsheet->chemical_alcohol;?></td>
            </tr>
            <tr>
                <td>Sugar Free Dry Extract</td>
                <td><?php echo $specsheet->chemical_sugar_free_extract;?></td>
            </tr>
            <tr>
                <td>Density</td>
                <td><?php echo $specsheet->vinegar_density;?></td>
            </tr>
            <tr>
                <td>Brix</td>
                <td><?php echo $specsheet->chemical_brix;?></td>
            </tr>
            <tr>
                <td>Developed Alcohol Degree</td>
                <td><?php echo $specsheet->chemical_alcohol_degree;?></td>
            </tr>
            <tr>
                <td>Reduced Dry Extract</td>
                <td><?php echo $specsheet->chemical_reduced_dry_extract;?></td>
            </tr>
            <tr>
                <td>Reducing Sugars</td>
                <td><?php echo $specsheet->reducing_sugars;?></td>
            </tr>
            <tr>
                <td>Rifractometric at 20�C</td>
                <td><?php echo $specsheet->chemical_rif;?></td>
            </tr>

        </table>
    </div>
    <!--    end typical analysis range-->

    <!--typical fatty acid-->
    <div style="border: 1px solid #97b854; width:65%;padding:15px;">

        <table  border="0" width="50%">
            <tr>
                <td colspan="2"><h3><u>Other Product Information:</u></h3></td>
            </tr>
            <tr>
                <td colspan="2">
                    <table border="0">
                        <tr>
                            <td><?php echo $specsheet->other_product_info1;?></td>
                        </tr>
                        <tr>
                            <td><?php echo $specsheet->other_product_info2;?></td>
                        </tr>
                        <tr>
                            <td><?php echo $specsheet->other_product_info3;?></td>
                        </tr>
                        <tr>
                            <td><?php echo $specsheet->other_product_info4;?></td>
                        </tr>
                        <tr>
                            <td><?php echo $specsheet->other_product_info5;?></td>
                        </tr>
                        <tr>
                            <td><?php echo $specsheet->other_product_info6;?></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div></div>

</div>


        

    

    </div>

    <!--    end vinegars-->
    <br />
    <br />
    <br />
    <br />
    <div style="margin-top:15px; width:65%">
        <p style="margin-left:280px;"><img src= "http://cibariaspecsheets.com/images/leaf.png" alt=""/></p>
    </div>

    <!--    end leaf-->

    <!--end-->
    <?php }
    ?>

    <br/>
    <br/>
    <br/>

    <div style="clear:both;"></div>

    <div style="text-align:center;margin-top:25px; width:65%">
        <p>This specification was developed with the utmost care based on up-to-date information available, but should be scrutinized by the recipient. It does not release him or her from checking the quality of goods delivered with proper diligence.
            <br />
        <hr />
        <strong>Revised: <?php echo $specsheet->revised;?></strong>
        </p>
        <hr />

    </div>
    <!-- end second page -->
    <!--msds-->

    <div class="page"></div>
@stop