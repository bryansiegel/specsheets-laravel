<div style="padding-top:20px;">
    {{-- title --}}
    <h1 style="text-align:center;">SAFETY DATA SHEET</h1>
    {{-- TODO:ADD TITLE --}}
    <br />
<div>
<div style="border: 1px solid black; padding:5px;"><h3> 1.0 Identification of the substance and of the Company:</h3></div>

<table cellpadding="5px" width="940px">
    <tr>
        <td valign="top" width="450px"><h3><strong>Identification of the Substance:</strong></h3></td>
        <td width="40px" valign="top"></td>
        <td><strong>
        {{ Form::text('sds_name', null,['class' => 'form-control', 'placeholder' => 'Identification of the Substance:']) }}
        </strong></td>
    </tr>
    <tr>
    <td width="450px"><strong>
        Cibaria International<br />
        705 Columbia Avenue<br />
        Riverside, CA 92507<br />
        Phone: 951-823-8490
    </strong></td>
    </tr>
</table>

{{-- end 1.0 --}}
{{-- 2.0 --}}
<div style="border: 1px solid black; padding:5px;margin-top:5px;"><h3>2.0 Composition/Information on Ingredients:</h3></div>
<table cellpadding="5px" width="940px">
    <tr>
        <td valign="top" width="450px"><h3><strong>Chemical description (mixture):</strong></h3></td>
        <td width="40px" valign="top"></td>
        <td width="450px"><strong>
        
        </strong></td>
    </tr>
    <tr>
        <td width="450px"><h3><strong>CAS Number:</strong></h3></td>
        <td width="40px" valign="top"></td>
        <td width="450px"><strong>
        {{ Form::text('sds_cas_number', null,['class' => 'form-control', 'placeholder' => 'CAS Number:']) }}
        </strong></td>
    </tr>
</table>
{{-- 3.0 --}}
<div style="border: 1px solid black; padding:5px;margin-top:5px;"><h3>3.0 Hazards Identification:</h3></div>
<table cellpadding="5px" width="940px">
    <tr>
        <td valign="top" width="450px"><h3><strong>Special hazard for personnel and environment:</strong></h3></td>
        <td width="40px"></td>
    </tr>
    <tr>
        <td width="450px"><strong>
        {{ Form::textarea('sds_special_hazard_for_personal', 'This material is not hazardous under Labor Department Regulations. Is generally recognized as Safe (GRAS) under Food Drug and Cosmetic Act.   ',['class' => 'form-control', 'placeholder' => 'Special hazard for personnel and environment:', 'cols' => '8', 'rows' => '8']) }}
        </strong></td>
    </tr>
</table>
{{-- 4.0 --}}
<div style="border: 1px solid black; padding:5px;margin-top:5px;"><h3>4.0 First Aid Measures:</h3></div>
<table cellpadding="5px" width="100%">
    <tr>
        <td valign="top" colspan="3" width="450px"><h3><strong>General measures:</strong></h3></td>
        <td width="40px" valign="top"></td>
    </tr>
    <tr>
        <td valign="top" width="450px"><h3 style="padding-left:5px;"><strong>Upon Inhalation:</strong></h3></td>
        <td width="40px" valign="top"></td>
        <td valign="top" width="450px"><strong>
        {{ Form::text('sds_upon_inhalation', 'May be removed from skin by washing with soap and warm water',['class' => 'form-control', 'placeholder' => 'Upon Inhalation:']) }}
        </strong></td>
    </tr>
    <td valign="top"><h3 style="padding-left:5px;"><strong>Upon Skin contact:</strong></h3></td>
        <td width="100px" valign="top"></td>
    <td valign="top"><strong>
       {{ Form::text('sds_upon_skin_contact', 'Expose individuals to fresh air source',['class' => 'form-control', 'placeholder' => 'Upon Skin contact:']) }}
    </strong></td>
</table>
{{-- 5.0 --}}
<div style="border: 1px solid black; padding:5px;margin-top:5px;"><h3>5.0 Firefighting Measures:</h3></div>
<table cellpadding="5px" width="940px">
    <tr>
        <td valign="top" width="450px" colspan="3">
            <h3><strong>General Information:</strong></h3>
        </td>

    </tr>
    <tr>
    <td valign="top" width="450px"><h3><strong>Extinguishing Media:</strong></h3></td>
    <td width="40px" valign="top"></td>
    <td valign="top" width="450px"><strong>
     {{ Form::text('sds_extinguishing_media', null,['class' => 'form-control', 'placeholder' => 'Extinguishing Media:']) }}
    </strong></td>
    </tr>
    <tr>
        <td valign="top" width="450px">
            <h3><strong>Flash Point:</strong></h3>
        </td>
    <td width="40px" valign="top"></td>
        <td valign="top" width="450px"><strong>
        {{ Form::text('sds_flash_point', null,['class' => 'form-control', 'placeholder' => 'Flash Point:']) }}
        </strong></td>
    </tr>
    <tr>
    <td valign="top" width="450px"><h3><strong>Not to be used extinguishing media:</strong></h3></td>
    <td width="40px" valign="top"></td>
    <td valign="top" width="450px"><strong>
    {{ Form::text('sds_not_to_be_used_extinguishing_media', 'No information available',['class' => 'form-control', 'placeholder' => 'Not to be used extinguishing media:']) }}
    </strong></td>
    </tr>
    <tr>
        <td valign="top" width="900px" colspan="3"><h3><strong>Special exposure hazard of substance or production, its combustion products and gasses thereof:</strong></h3></td>
    </tr>
    <tr>
        
        <td valign="top" width="450px" colspan="3"><strong>
        {{ Form::text('sds_special_exposure_hazard', 'Keep product away from heat source',['class' => 'form-control', 'placeholder' => 'Special exposure hazard of substance or production, its combustion products and gasses thereof:']) }}
        </strong></td>
    </tr>
    <tr>
        <td valign="top" width="450px"><h3><strong>Special protection for firefighting purposes:</strong></h3></td>
        <td width="40px" valign="top"></td>
        <td valign="top" width="450px"><strong>
        {{ Form::textarea('sds_special_protection', 'As in any fire, wear self-contained breathing apparatus pressure-demand, MSHA/NIOSH (approved or equivalent) and full protective gear.',['class' => 'form-control', 'placeholder' => 'Special protection for firefighting purposes:', 'cols' => '8', 'rows' => '8']) }}
        </strong></td>

    </tr>
</table>

{{-- 6.0 --}}
<div style="border: 1px solid black; padding:5px;margin-top:5px;"><h3>6.0 Accidental release measures:</h3></div>
<table cellpadding="5px" width="940px">
    <tr>  
        <td width="450px" valign="top"><h3><strong>Precautions related to personnel:</strong></h3></td>
        <td width="40px" valign="top"></td>
        <td valign="top" width="450px"><strong>
        {{ Form::text('sds_precautions_related_to_personnel', 'Ensure adequate ventilation.',['class' => 'form-control', 'placeholder' => 'Precautions related to personnel:']) }}
        </strong></td>
    </tr>
    <tr>
        <td width = "450px" valign="top"><h3><strong>Procedure for clean-up/removal:</strong></h3></td>
        <td width="40px" valign="top"></td>
        <td valign="top" width = "450px"><strong>
        {{ Form::textarea('sds_procedure_for_cleanup', 'Soak up with inert absorbent material. Keep combustibles (wood, paper, oil, rags and / or other materials away from product) Store wiping rags in metal cans with tight fitting lids. Avoid the use of water in extinguishing oil fires.',['class' => 'form-control', 'placeholder' => 'Procedure for clean-up/removal:', 'rows' => '8', 'cols' => '8']) }}
        </strong></td>
</tr>
<tr>
    <td colspan="3"><h3><strong>Steps to be taken in case material is released or spilled:</strong></h3></td>
</tr>
    <tr>
        <td width="450px" valign="top"><h3><strong>Small Spill:</strong></h3></td>
        <td width="40px" valign="top"></td>
        <td valign="top" width = "450px"><strong>
        {{ Form::textarea('sds_small_spill', 'Add solid absorbent, shovel into disposable container and dry area thoroughly with wiping material',['class' => 'form-control', 'placeholder' => 'Small Spill:', 'rows' => '8', 'cols' => '8']) }}
        </strong></td>
    </tr>
    <tr>
        <td width="450px" valign="top"><h3><strong>Large spill:</strong></h3></td>
        <td width="40px" valign="top"></td>
        <td valign="top" width = "450px"><strong>
        {{ Form::textarea('sds_large_spill', 'Squeegee or pump into holding container, and repeat steps for small spill.',['class' => 'form-control','placeholder' => 'Large spill:', 'cols' => '8', 'rows' => '8']) }}
        </strong></td>
</tr>
</table>
<div class="page"></div>

{{-- 7.0 --}}
<div style="border: 1px solid black; padding:5px;margin-top:5px;"><h3>7.0 Handling and storage:</h3></div>
<table cellpadding="5px" width="940px">
    <tr>
        <td width="450px" valign="top"><h3><strong>Handling:</strong></h3></td>
        <td width="40px" valign="top"></td>
        <td valign="top" width="450px"><strong>
        {{ Form::text('sds_handling', 'Store away from flame and fire, and excessive heat',['class' => 'form-control','placeholder' => 'Handling:']) }}
        </strong></td>
    </tr>
    <tr>
        <td valign="top" width="450px"><h3><strong>Storage:</strong></h3></td>
        <td width="40px" valign="top"></td>
        <td valign="top" width="450px"><strong>
        {{ Form::text('sds_storage', 'Store at room temperature and only in containers that are approved by certified firms.',['class' => 'form-control','placeholder' => 'Storage:']) }}
        </strong></td>
</tr>
</table>
{{-- 8.0 --}}
<div style="border: 1px solid black; padding:5px;margin-top:5px;"><h3>8.0 Exposure controls / personal protection:</h3></div>
<table cellpadding="5px" width="940px">
    <tr>
        <td valign="top" width="450px"><h3><strong>Threshold Limit Value:</strong></h3></td>
        <td width="40px" valign="top"></td>
        <td valign="top" width="450px"><strong>
        {{ Form::text('sds_threshold_limit_value', null,['class' => 'form-control','placeholder' => 'Threshold Limit Value:']) }}
        </strong></td>
    </tr>
    <tr>
    <td valign="top" width="450px"><h3><strong>Inhalation Health Risks and Symptoms of Exposure:</strong></h3></td>
    <td width="40px" valign="top"></td>
    <td valign="top" width="450px"><strong>
    {{ Form::text('sds_inhalation_health_risks', null,['class' => 'form-control', 'placeholder' => 'Inhalation Health Risks and Symptoms of Exposure:']) }}
    </strong></td>
    </tr>
    <tr>
        <td valign="top" width="450px"><h3><strong>Skin Absorption Health Risk and Symptom of Exposure:</strong></h3></td>
        <td width="40px" valign="top"></td>
        <td valign="top" width="450px"><strong>
        {{ Form::text('sds_skin_absorption', null,['class' => 'form-control', 'placeholder' => 'Skin Absorption Health Risk and Symptom of Exposure:']) }}
        </strong></td>
    </tr>
</table>


{{-- 9.0 --}}
<div style="border: 1px solid black; padding:5px;margin-top:5px;"><h3>9.0 Physical and chemical properties:</h3></div>
<table cellpadding="5px" width="940px">
    <tr>
        <td width="450px" valign="top"><h3><strong>Appearance:</strong></h3></td>
        <td width="40px" valign="top"></td>
        <td valign="top" width="450px"><strong>
        {{ Form::text('sds_appearance', null,['class' => 'form-control', 'placeholder' => 'Appearance:']) }}
        </strong></td>
    </tr>
    <tr>
        <td valign="top" width="450px"><h3><strong>Form:</strong></h3></td>
        <td width="40px" valign="top"></td>
        <td valign="top" width="450px"><strong>
        {{ Form::text('sds_form', null,['class' => 'form-control', 'placeholder' => 'Form:']) }}
        </strong></td>
    </tr>
    <tr>
        <td valign="top" width="450px"><h3><strong>Color:</strong></h3></td>
        <td width="40px" valign="top"></td>
        <td valign="top" width="450px"><strong>
        {{ Form::text('sds_color', null,['class' => 'form-control', 'placeholder' => 'Color:']) }}
        </strong></td>
    </tr>
    <tr>
        <td valign="top" width="450px"><h3><strong>Odor:</strong></h3></td>
        <td width="40px" valign="top"></td>
        <td valign="top" width="450px"><strong>
        {{ Form::text('sds_odor', null,['class' => 'form-control', 'placeholder' => 'Odor:']) }}
        </strong></td>
    </tr>
    <tr>
        <td valign="top" width="450px"><h3><strong>Data relevant to safety:</strong></h3></td>
        <td width="40px" valign="top"></td>
        <td valign="top" width="450px"><strong></strong></td>
    </tr>
    <tr>
        <td valign="top" width="450px"><h3><strong>Melting point:</strong></h3></td>
        <td width="40px" valign="top"></td>
        <td valign="top" width="450px"><strong>
        {{ Form::text('sds_melting_point', null,['class' => 'form-control', 'placeholder' => 'Melting point:']) }}
        </strong></td>
    </tr>
    <tr>
        <td valign="top" width="450px"><h3><strong>Flash point:</strong></h3></td>
        <td width="40px" valign="top"></td>
        <td valign="top" width="450px"><strong>
        {{ Form::text('sds_flash_point', null,['class' => 'form-control', 'placeholder' => 'Flash point:']) }}
        </strong></td>
    </tr>
    <tr>
        <td valign="top" width="450px"><h3><strong>Ignition temperature:</strong></h3></td>
        <td width="40px" valign="top"></td>
        <td valign="top" width="450px"><strong>
        {{ Form::text('sds_ignition_temperature', null,['class' => 'form-control', 'placeholder' => 'Ignition temperature:']) }}
        </strong></td>
    </tr>
    <tr>
        <td valign="top" width="450px"><h3><strong>Density:</strong></h3></td>
        <td width="40px" valign="top"></td>
        <td valign="top" width="450px"><strong>
        {{ Form::text('sds_density', null,['class' => 'form-control', 'placeholder' => 'Density:']) }}
        </strong></td>
    </tr>
    <tr>
        <td valign="top" width="450px"><h3><strong>Water solubility:</strong></h3></td>
        <td width="40px" valign="top"></td>
        <td valign="top" width="450px"><strong>
        {{ Form::text('sds_water_solubility', null,['class' => 'form-control', 'placeholder' => 'Water solubility:']) }}
        </strong></td>
    </tr>
</table>
{{-- 10. --}}
<div style="border: 1px solid black; padding:5px;margin-top:5px;"><h3>10.0 Stability and reactivity:</h3></div>
<table cellpadding="5px" width="940px">
    <tr>
        <td valign="top" width="450px"><h3><strong>General information:</strong></h3></td>
        <td width="40px" valign="top"></td>
        <td valign="top" width="450px"><strong>
        {{ Form::text('sds_general_information', 'Spontaneous combustion can occur. Stable under normal conditions.',['class' => 'form-control', 'placeholder' => 'General information:']) }}
        </strong></td>
    </tr>
    <tr>
        <td valign="top" width="450px"><h3><strong>Conditions to be avoided:</strong></h3></td>
        <td width="40px" valign="top"></td>
        <td valign="top" width="450px"><strong>
        {{ Form::text('sds_conditions_to_be_avoided', 'High surface area exposure to oxygen can result in polymerization and release of heat',['class' => 'form-control', 'placeholder' => 'Conditions to be avoided:']) }}
        </strong></td>
    </tr>
    <tr>
        <td valign="top" width="450px"><h3><strong>Dangerous decomposition product:</strong></h3></td>
        <td width="40px" valign="top"></td>
        <td valign="top" width="450px"><strong>
        {{ Form::text('sds_dangerous_decomposition_product', 'Carbon oxidizing agents',['class' => 'form-control', 'placeholder' => 'Dangerous decomposition product:']) }}
        </strong></td>
    </tr>
</table>
{{-- 11 --}}
<div style="border: 1px solid black; padding:5px;margin-top:5px;"><h3>11.0 Toxicological information:</h3></div>
<table cellpadding="5px" width="940px">
    <tr>
        <td valign="top" width="450px"><h3><strong>Toxicological testing:</strong></h3></td>
        <td width="40px" valign="top"></td>
        <td valign="top" width="450px"><strong>
        {{ Form::text('sds_toxicological_testing', 'No acute toxicity information is available for this product',['class' => 'form-control', 'placeholder' => 'Toxicological testing:']) }}
        </strong></td>
    </tr>
    <tr>
        <td valign="top" width="450px"><h3><strong> Practical experience:</strong></h3></td>
        <td width="40px" valign="top"></td>
        <td valign="top" width="450px"><strong>
        {{ Form::text('sds_practical_experience', 'N/A',['class' => 'form-control', 'placeholder' => 'Practical experience:']) }}
        </strong></td>
    </tr>
    <tr>
        <td valign="top" width="450px"><h3><strong>General considerations:</strong></h3></td>
        <td width="40px" valign="top"></td>
        <td valign="top" width="450px"><strong>
        {{ Form::text('sds_general_considerations', 'N/A',['class' => 'form-control', 'placeholder' => 'General considerations:']) }}
        </strong></td>
    </tr>
</table>
{{-- 12 --}}
<div style="border: 1px solid black; padding:5px;margin-top:5px;"><h3>12.0 Ecological Information:</h3></div>
<table cellpadding="5px" width="940px">
    <tr>
        <td width="450px" valign="top"><h3><strong>Information on product:</strong></h3></td>
        <td width="40px" valign="top"></td>
        <td valign="top" width="450px"><strong>
        {{ Form::text('sds_information_on_product', null,['class' => 'form-control', 'placeholder' => 'Information on product:']) }}
        </strong></td>
    </tr>
    <tr>
        <td valign="top" width="450px"><h3><strong>Behavior in ecological setting:</strong></h3></td>
        <td width="40px" valign="top"></td>
        <td valign="top" width="450px"><strong>
        {{ Form::text('sds_behavior_in_ecological_setting', null,['class' => 'form-control', 'placeholder' => 'Behavior in ecological setting:']) }}
        </strong></td>
    </tr>
    <tr>
        <td valign="top" width="450px"><h3><strong>Toxic effects on the ecosystem:</strong></h3></td>
        <td width="40px" valign="top"></td>
        <td valign="top" width="450px"><strong>
        {{ Form::text('sds_toxic_effects_on_the_ecosystem', null,['class' => 'form-control', 'placeholder' => 'Toxic effects on the ecosystem:']) }}
        </strong></td>
    </tr>
    <tr>
        <td valign="top" width="450px"><h3><strong>Additional ecological information:</strong></h3></td>
        <td width="40px" valign="top"></td>
        <td valign="top" width="450px"><strong>
        {{ Form::text('sds_additional_ecological_information', null,['class' => 'form-control', 'placeholder' => 'Additional ecological information:']) }}
        </strong></td>
    </tr>
</table>
{{-- 13.0 --}}
<div style="border: 1px solid black; padding:5px;margin-top:5px;"><h3>13.0 Disposal considerations:</h3></div>
<table cellpadding="5px" width="940px">
    <tr>
        <td valign="top"><strong>
        {{ Form::textarea('sds_disposal_considerations', 'Dispose in accordance with local, state and federal regulations.',['class' => 'form-control', 'placeholder' => 'Disposal considerations:', 'rows' => '8', 'cols' => '8']) }}
        </strong></td>
    </tr>
</table>
{{-- 14.0 --}}
<div style="border: 1px solid black; padding:5px;margin-top:5px;"><h3>14.0 Transportation information:</h3></div>
<table cellpadding="5px" width="940px">
    <tr>
        <td valign="top"><strong>
        {{ Form::text('sds_transportation_information', null,['class' => 'form-control', 'placeholder' => 'Transportation information:' ]) }}
        </strong></td>
    </tr>
</table>
{{-- 15.0 --}}
<div style="border: 1px solid black; padding:5px;margin-top:5px;"><h3>15.0 Regulatory information:</h3></div>
<table cellpadding="5px" width="940px">
    <tr>
        <td valign="top"><strong>
        {{ Form::text('sds_regulatory_information', null,['class' => 'form-control', 'placeholder' => 'Regulatory information:']) }}
        </strong></td>
    </tr>
</table>
{{-- 16.0 --}}
<div style="border: 1px solid black; padding:5px;margin-top:5px;"><h3>16.0 Additional information:</h3></div>
<table cellpadding="5px" width="940px">
    <tr>
        <td valign="top"><strong>This SDS sheet revised to meet the OSH 16-section format</strong></td>
    </tr>
    <tr>
        <td valign="top"><strong>N/A. = not applicable</strong></td>
    </tr>
    <tr>
        <td valign="top"><strong>The above information only describes the safety requirements of the product(s) and is given in good faith and based on our current knowledge of the product(s).  The above information is not claiming characteristics of the product(s) in terms of legal claims of performance / guarantee.</strong></td>
    </tr>
    <tr>
        <td valign="top"><strong>Delivery specifications are listed in separate product documentation sheets.</strong></td>
    </tr>
</table>