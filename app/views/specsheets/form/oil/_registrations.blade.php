<h2>Registrations and Other Product Information</h2>
<div class="form-group">
    {{ Form::label('certifications_cas', 'CAS:') }}
        {{ Form::text('certifications_cas', null,array('class' => 'form-control', 'placeholder' => 'CAS')) }}
    </div>
<div class="form-group">
    {{ Form::label('certifications_einecs', 'EINCS:') }}
        {{ Form::text('certifications_einecs', null,array('class' => 'form-control', 'placeholder' => 'EINCS')) }}
    </div>
<div class="form-group">
    {{ Form::label('certifications_incl','INCL:') }}
        {{ Form::text('certifications_incl', null,array('class' => 'form-control', 'placeholder' => 'INCL')) }}
    </div>
<div class="form-group">
    {{ Form::label('Notes:', 'certifications_notes') }}
        {{ Form::textarea('certifications_notes', null, array('class' => 'form-control', 'rows' => 8, 'placeholder' => 'Notes')) }}
</div>