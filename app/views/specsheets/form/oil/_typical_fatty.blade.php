<h2>Typical Fatty</h2>
<div class="form-group">
    {{ Form::label('typical_myristic', 'Myristic Acid') }}
    {{ Form::text('typical_myristic',null,['class' => 'form-control', 'placeholder' => 'Myristic Acid']) }}
</div>
<div class="form-group">
    {{ Form::label('typical_palmitic', 'Palmitic Acid') }}
    {{ Form::text('typical_palmitic',null,['class' => 'form-control', 'placeholder' => 'Palmitic Acid']) }}
</div>
<div class="form-group">
    {{ Form::label('typical_palmitoleic', 'Palmitoleic Acid') }}
    {{ Form::text('typical_palmitoleic',null,['class' => 'form-control', 'placeholder' => 'Palmitoleic Acid']) }}
</div>
<div class="form-group">
    {{ Form::label('typical_heptadecanoic', 'Heptadecanoic Acid') }}
    {{ Form::text('typical_heptadecanoic',null,['class' => 'form-control', 'placeholder' => 'Heptadecanoic Acid']) }}
</div>
<div class="form-group">
    {{ Form::label('typical_heptadecenoic', 'Heptadecenoic Acid') }}
    {{ Form::text('typical_heptadecenoic',null,['class' => 'form-control', 'placeholder' => 'Heptadecenoic Acid']) }}
</div>
<div class="form-group">
    {{ Form::label('typical_stearic', 'Stearic Acid') }}
    {{ Form::text('typical_stearic',null,['class' => 'form-control', 'placeholder' => 'Heptadecenoic Acid']) }}
</div>
<div class="form-group">
    {{ Form::label('typical_oleic', 'Oleic Acid') }}
    {{ Form::text('typical_oleic',null,['class' => 'form-control', 'placeholder' => 'Oleic Acid']) }}
</div>
<div class="form-group">
    {{ Form::label('typical_linoleic', 'Linoleic Acid') }}
    {{ Form::text('typical_linoleic',null,['class' => 'form-control', 'placeholder' => 'Linoleic Acid']) }}
</div>
<div class="form-group">
    {{ Form::label('typical_linolenic', 'Linolenic Acid') }}
    {{ Form::text('typical_linolenic',null,['class' => 'form-control', 'placeholder' => 'Linolenic Acid']) }}
</div>
<div class="form-group">
    {{ Form::label('typical_arachidic', 'Arachidic Acid') }}
    {{ Form::text('typical_arachidic',null,['class' => 'form-control', 'placeholder' => 'Arachidic Acid']) }}
</div>
<div class="form-group">
    {{ Form::label('typical_gadoleic', 'Gadoleic Acid') }}
    {{ Form::text('typical_gadoleic',null,['class' => 'form-control', 'placeholder' => 'Gadoleic Acid']) }}
</div>
<div class="form-group">
    {{ Form::label('typical_behenic', 'Behenic Acid') }}
    {{ Form::text('typical_behenic',null,['class' => 'form-control', 'placeholder' => 'Behenic Acid']) }}
</div>
<div class="form-group">
    {{ Form::label('typical_erucic', 'Erucic Acid') }}
    {{ Form::text('typical_erucic',null,['class' => 'form-control', 'placeholder' => 'Erucic Acid']) }}
</div>
<div class="form-group">
    {{ Form::label('typical_lignoceric', 'Lignoceric Acid') }}
    {{ Form::text('typical_lignoceric',null,['class' => 'form-control', 'placeholder' => 'Lignoceric Acid']) }}
</div>