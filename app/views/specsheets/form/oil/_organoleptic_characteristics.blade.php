<h2>Organoleptic Characteristics</h2>
<div class="form-group">
    {{ Form::label('organoleptic_appearance', 'Appearance/Clarity') }}
    {{ Form::text('organoleptic_appearance',null,['class' => 'form-control', 'placeholder' => 'Appearance/Clarity']) }}
</div>
<div class="form-group">
    {{ Form::label('organoleptic_flavor', 'Flavor/Odor') }}
    {{ Form::text('organoleptic_flavor',null,['class' => 'form-control', 'placeholder' => 'Flavor/Odor']) }}
</div>
<div class="form-group">
    {{ Form::label('organoleptic_color_red', 'Color (Lovibond) Red') }}
    {{ Form::text('organoleptic_color_red',null,['class' => 'form-control', 'placeholder' => 'Color (Lovibond) Red']) }}
</div>
<div class="form-group">
    {{ Form::label('organoleptic_color_yellow', 'Color (Lovibond) Yellow') }}
    {{ Form::text('organoleptic_color_yellow',null,['class' => 'form-control', 'placeholder' => 'Color (Lovibond) Yellow']) }}
</div>

