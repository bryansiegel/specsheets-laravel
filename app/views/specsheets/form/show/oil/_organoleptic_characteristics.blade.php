<h2>Organoleptic Characteristics</h2>
<li class="list-group-item">
    <strong>Appearance/Clarity: </strong> {{ $specsheet->organoleptic_appearance }}
</li>
<li class="list-group-item">
    <strong>Flavor/Odor: </strong> {{ $specsheet->typical_acidity }}
</li>
<li class="list-group-item">
    <strong>Color (Lovibond) Yellow: </strong> {{ $specsheet->organoleptic_color_yellow }}
</li>


