<h2>USDA NDB (National Nutrition Database)</h2>
<br/>
<table width="390" border="1" bordercolor="#97b854" cellpadding="5" cellspacing="0">
    <tr bgcolor="#97b854" style="padding:10px; text-align:center;color:white;">
        <td>Nutrient</td>
        <td>Unit</td>
        <td>Value per 100.0g</td>
        <td>Tbsp 13.5g</td>
    </tr>
    <tr>
        <td colspan="4" bgcolor="#d6e3bc"> Proximates</td>
    </tr>
    <tr>
        <td>Water</td>
        <td>g</td>
        <td>Water Value {{ $specsheet->water_value }}</td>
        <td>Water tbsp  {{ $specsheet->water_tbsp }}</td>
    </tr>
    <tr>
        <td>Energy</td>
        <td>kcal</td>
        <td>energy Value {{ $specsheet->energy_value }}</td>
        <td>energy tbsp {{ $specsheet->energy_tbsp }}</td>
    </tr>
    <tr>
        <td>Protein</td>
        <td>g</td>
        <td> protein Value {{ $specsheet->protein_value }}</td>
        <td>protein tbsp {{ $specsheet->protein_tbsp }}</td>
    </tr>
    <tr>
        <td>Total lipid (fat)</td>
        <td>g</td>
        <td>totallipid Value {{ $specsheet->totallipid_value }}</td>
        <td>totallipid tbsp {{ $specsheet->totallipid_tbsp }}</td>
    </tr>
    <tr>
        <td>Carbohydrate, by difference</td>
        <td>g</td>
        <td>carbohydrate Value {{ $specsheet->carbohydrate_value }}</td>
        <td>carbohydrate tbsp {{ $specsheet->carbohydrate_tbsp }}</td>
    </tr>
    <tr>
        <td>Fiber, total dietary</td>
        <td>g</td>
        <td>fiber Value {{ $specsheet->fiber_value }}</td>
        <td>fiber tbsp {{ $specsheet->fiber_tbsp }}</td>
    </tr>
    <tr>
        <td>Sugars, total</td>
        <td>g</td>
        <td>sugars Value {{ $specsheet->sugars_value }}</td>
        <td>sugars tbsp {{ $specsheet->sugars_tbsp }}</td>
    </tr>
    <tr>
        <td colspan="4" bgcolor="#d6e3bc">Minerals</td>
    </tr>
    <tr>
        <td>Calcium, Ca</td>
        <td>mg</td>
        <td>calcium Value {{ $specsheet->calcium_value }}</td>
        <td>calcium tbsp {{ $specsheet->calcium_tbsp }}</td>
    </tr>
    <tr>
        <td>Iron, Fe</td>
        <td>mg</td>
        <td>iron Value {{ $specsheet->iron_value }}</td>
        <td>iron tbsp {{ $specsheet->iron_tbsp }}</td>
    </tr>
    <tr>
        <td>Magnesium, Mg</td>
        <td>mg</td>
        <td>magnesium Value{{ $specsheet->magnesium_value }}</td>
        <td>magnesium tbsp {{ $specsheet->magnesium_tbsp }}</td>
    </tr>
    <tr>
        <td>Phosphorus, P</td>
        <td>mg</td>
        <td>phosphorus Value {{ $specsheet->phosphorus_value }}</td>
        <td>phosphorus tbsp {{ $specsheet->phosphorus_tbsp }}</td>
    </tr>
    <tr>
        <td>Potassium, K</td>
        <td>mg</td>
        <td>potassium Value {{ $specsheet->potassium_value }}</td>
        <td>potassium tbsp {{ $specsheet->potassium_tbsp }}</td>
    </tr>
    <tr>
        <td>Sodium, Na</td>
        <td>mg</td>
        <td>sodium Value {{ $specsheet->sodium_value }}</td>
        <td>sodium tbsp {{ $specsheet->sodium_tbsp }}</td>
    </tr>
    <tr>
        <td>Zinc, Zn</td>
        <td>mg</td>
        <td>zinc Value {{ $specsheet->zinc_value }}</td>
        <td>zinc tbsp {{ $specsheet->zinc_tbsp }}</td>
    </tr>
    <tr>
        <td colspan="4" bgcolor="#d6e3bc">Vitamins</td>
    </tr>
    <tr>
        <td>Vitamin C, total ascorbic acid</td>
        <td>mg</td>
        <td>vitaminc Value {{ $specsheet->vitaminc_value }}</td>
        <td>vitaminc tbsp {{ $specsheet->vitaminc_tbsp }}</td>
    </tr>
    <tr>
        <td>Thiamin</td>
        <td>mg</td>
        <td>thiamin Value {{ $specsheet->thiamin_value }}</td>
        <td>thiamin tbsp {{ $specsheet->thiamin_tbsp }}</td>
    </tr>
    <tr>
        <td>Riboflavin</td>
        <td>mg</td>
        <td>riboflavin Value {{ $specsheet->riboflavin_value }}</td>
        <td>riboflavin tbsp {{ $specsheet->riboflavin_tbsp }}</td>
    </tr>
    <tr>
        <td>Niacin</td>
        <td>mg</td>
        <td>niacin Value {{ $specsheet->niacin_value }}</td>
        <td>niacin tbsp {{ $specsheet->niacin_tbsp }}</td>
    </tr>
    <tr>
        <td>Vitamin B-6</td>
        <td>mg</td>
        <td>vitaminb6 Value {{ $specsheet->vitaminb6_value }}</td>
        <td>vitaminb6 tbsp {{ $specsheet->vitaminb6_tbsp }}</td>
    </tr>
    <tr>
        <td>Folate, DFE</td>
        <td>µg</td>
        <td>folate Value {{ $specsheet->folate_value }}</td>
        <td>folate tbsp {{ $specsheet->folate_tbsp }}</td>
    </tr>
    <tr>
        <td>Vitamin B-12</td>
        <td>µg</td>
        <td>vitaminb12 Value {{ $specsheet->vitaminb12_value }}</td>
        <td>vitaminb12 tbsp {{ $specsheet->vitaminb12_tbsp }}</td>
    </tr>
    <tr>
        <td>Vitamin A, RAE</td>
        <td>µg</td>
        <td>vitaminar Value {{ $specsheet->vitaminar_value }}</td>
        <td>vitaminar tbsp {{ $specsheet->vitaminar_tbsp }}</td>
    </tr>
    <tr>
        <td>Vitamin A, IU</td>
        <td>IU</td>
        <td> vitaminai Value {{ $specsheet->vitaminai_value }}</td>
        <td>vitaminai tbsp {{ $specsheet->vitaminai_tbsp }}</td>
    </tr>
    <tr>
        <td>Vitamin E (alpha-tocopherol)</td>
        <td>mg</td>
        <td>vitamine Value {{ $specsheet->vitamine_value }}</td>
        <td>vitamine tbsp {{ $specsheet->vitamine_tbsp }}</td>
    </tr>
    <tr>
        <td>Vitamin D (D2+D3)</td>
        <td>µg</td>
        <td>vitamind2 Value {{ $specsheet->vitamind2_value }}</td>
        <td>vitamind2 tbsp {{ $specsheet->vitamind2_tbsp }}</td>
    </tr>
    <tr>
        <td>Vitamin D</td>
        <td>IU</td>
        <td>vitamind_value Value {{ $specsheet->vitamind_value }}</td>
        <td>vitamind_value tbsp {{ $specsheet->vitamind_tbsp }}</td>
    </tr>
    <tr>
        <td>Vitamin K (phylloquinone)</td>
        <td>µg</td>
        <td>vitamink Value {{ $specsheet->vitamink_value }}</td>
        <td>vitamink tbsp {{ $specsheet->vitamink_tbsp }}</td>
    </tr>
    <tr>
        <td colspan="4" bgcolor="#d6e3bc">Lipids</td>
    </tr>
    <tr>
        <td>Fatty acids, total saturated</td>
        <td>g</td>
        <td>fattysaturated Value {{ $specsheet->fattysaturated_value }}</td>
        <td>fattysaturated tbsp {{ $specsheet->fattysaturated_tbsp }}</td>
    </tr>
    <tr>
        <td>Fatty acids, total monounsaturated</td>
        <td>g</td>
        <td>fattymonoun Value {{ $specsheet->fattymonoun_value }}</td>
        <td>fattymonoun tbsp {{ $specsheet->fattymonoun_tbsp }}</td>
    </tr>
    <tr>
        <td>Fatty acids, polyunsaturated</td>
        <td>g</td>
        <td>fattypoly Value {{ $specsheet->fattypoly_value }}</td>
        <td>fattypoly tbsp {{ $specsheet->fattypoly_tbsp }}</td>
    </tr>
    <tr>
        <td>Cholesterol</td>
        <td>mg</td>
        <td>cholesterol Value {{ $specsheet->cholesterol_value }}</td>
        <td>cholesterol tbsp {{ $specsheet->cholesterol_tbsp }}</td>
    </tr>
    <tr>
        <td colspan="4" bgcolor="#d6e3bc">Other</td>
    </tr>
    <tr>
        <td>Caffeine</td>
        <td>mg</td>
        <td>caffeine Value {{ $specsheet->caffeine_value }}</td>
        <td>caffeine tbsp {{ $specsheet->caffeine_tbsp }}</td>
    </tr>
</table>