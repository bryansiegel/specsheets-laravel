<h2>Registrations and Other Product Information</h2>
<li class="list-group-item">
    <strong>CAS:</strong> {{ $specsheet->certifications_cas }}
</li>
<li class="list-group-item">
    <strong>EINCS:</strong> {{ $specsheet->certifications_einecs }}
</li>
<li class="list-group-item">
    <strong>Notes:</strong> {{ $specsheet->certifications_notes }}
</li>