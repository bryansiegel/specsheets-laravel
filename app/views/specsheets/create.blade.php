@extends('layouts.admin')

@section('content')

    <div id="specsheets">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Create</span>A Specsheet
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/specsheets/">Specsheets</a> >
                        Create
                    </li>
                </ol>
            </div>
        </div>

        {{--errors--}}
        @if($errors->has())
            <div class="alert alert-dismissible alert-danger">
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
        @endif

        {{--form--}}
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-10">
                    <div class="col-lg-20">
                        {{ Form::open(['image/save', 'route' => 'specsheets.store','files' => true, 'class' => 'form-horizontal']) }}


                        <script>
                            $(document).ready(function () {
                                $('.defaultnutrition').hide();

                                $('#form_category').change(function () {
                                    $(this).find("option").each(function () {
                                        $("#" + $(this).val()).hide();
                                        console.log($(this));
                                    });
                                    $("#" + $(this).val()).show();
                                    // hide input fields when they are hidden
                                    $("div").filter(":hidden").children("input[type='text']").attr("disabled", "disabled");
                                    $("div").filter(":hidden").children("textarea[class='form-control']").attr("disabled", "disabled");

                                });
                            });
                        </script>

                        <fieldset>
                            {{--is done--}}
                            <div class="form-group">
                                {{ Form::label('isdone', 'Complete?') }}
                                {{ Form::select('isdone',['' => 'Yes/No', 'Yes' => 'Yes', 'No' => 'No'], '', ['class' => 'form-control']) }}
                            </div>



                            {{--is oil or vinegar dropdown--}}
                            <div class="form-group">
                                {{ Form::label('is_oil_vinegar', 'Oil or Vinegar?') }}
                                {{ Form::select('is_oil_vinegar', ['' => 'Choose', '1' => 'Oil', '2' => 'Vinegar'], '', ['class' => 'form-control', 'id' => 'form_category']) }}
                            </div>

                            {{--website--}}
                            <div class="form-group">
                                {{ Form::label('website', 'Website') }}
                                {{ Form::select('website', ['' => 'Choose', 'store supply' => 'Store Supply', 'soap supply' => 'Soap Supply', 'both' => 'Both'], '', ['class' => 'form-control']) }}
                            </div>
                            <hr/>
                            {{--main--}}
                            <div class="form-group">
                                {{ Form::label('title', 'Specsheet Title') }}
                                {{ Form::text('title', null,['class' => 'form-control', 'placeholder' => 'Title']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('sku', 'Sku') }}
                                {{ Form::text('sku', null,['class' => 'form-control', 'placeholder' => 'Sku']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('revised', 'Revised') }}
                                {{ Form::text('revised', null,['class' => 'form-control', 'placeholder' => 'Revised']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('description', 'Product Description') }}
                                {{ Form::text('description', null,['class' => 'form-control', 'placeholder' => 'Product Description']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('bullet_point1', 'Bullet Point 1') }}
                                {{ Form::text('bullet_point1', null,['class' => 'form-control', 'placeholder' => 'Bullet Point 1']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('bullet_point2', 'Bullet Point 2') }}
                                {{ Form::text('bullet_point2', null,['class' => 'form-control', 'placeholder' => 'Bullet Point 2']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('bullet_point3', 'Bullet Point 3') }}
                                {{ Form::text('bullet_point3', null,['class' => 'form-control', 'placeholder' => 'Bullet Point 3']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('bullet_point4', 'Bullet Point 4') }}
                                {{ Form::text('bullet_point4', null,['class' => 'form-control', 'placeholder' => 'Bullet Point 4']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('bullet_point5', 'Bullet Point 5') }}
                                {{ Form::text('bullet_point5', null,['class' => 'form-control', 'placeholder' => 'Bullet Point 5']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('bullet_point6', 'Bullet Point 6') }}
                                {{ Form::text('bullet_point6', null,['class' => 'form-control', 'placeholder' => 'Bullet Point 6']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('bullet_point7', 'Bullet Point 7') }}
                                {{ Form::text('bullet_point7', null,['class' => 'form-control', 'placeholder' => 'Bullet Point 7']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('bullet_point8', 'Bullet Point 8') }}
                                {{ Form::text('bullet_point8', null,['class' => 'form-control', 'placeholder' => 'Bullet Point 8']) }}
                            </div>
                        <div class="form-group">
                            {{ Form::label('prop_65_statement', 'Prop 65 Statement') }}
                            {{ Form::text('prop_65_statement', null,['class' => 'form-control', 'placeholder' => 'Prop 65 Statement']) }}
                        </div>

                            <div class="form-group">
                                {{ Form::label('storage', 'Storage') }}
                                {{ Form::text('storage', null,['class' => 'form-control', 'placeholder' => 'Storage']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('shelf_life', 'Shelf Life') }}
                                {{ Form::text('shelf_life', null,['class' => 'form-control', 'placeholder' => 'Shelf Life']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('sewer', 'Sewer Sludge and Irradiation Statement:') }}
                                {{ Form::text('sewer', null,['class' => 'form-control', 'placeholder' => 'Sewer Sludge and Irradiation Statement']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('applications', 'Applications for the Product') }}
                                {{ Form::text('applications', null,['class' => 'form-control', 'placeholder' => 'Applications for the Product']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('country', 'Country of Origin:') }}
                                {{ Form::text('country', null,['class' => 'form-control', 'placeholder' => 'Country']) }}
                            </div>
                            {{-- end main --}}
                            <hr/>
                            {{-- nutrition --}}
                            @include('specsheets.form._nutrition_fact')
                            <hr/>
                            <div id="1" class="defaultnutrition">
                                @include('specsheets.form.oil._under_allergen')
                                <hr/>
                                @include('specsheets.form.oil._organoleptic_characteristics')
                                <hr/>
                                @include('specsheets.form.oil._typical_analysis')
                                <hr/>
                                @include('specsheets.form.oil._typical_fatty')
                                <hr/>
                                @include('specsheets.form.oil._usda')
                                <hr/>
                                @include('specsheets.form.oil._registrations')
                                <hr/>
                            </div>
                            <div id="2" class="defaultnutrition">
{{--                                @include('specsheets.form.vinegar._nutrition_label_vinegar')--}}
                                @include('specsheets.form.vinegar._vinegar_organoleptic_characteristics')
                                <hr/>
                                @include('specsheets.form.vinegar._vinegar_chemical_physical_characteristics')
                                <hr/>
                                @include('specsheets.form.vinegar._vinegar_nutrition_facts')
                                <hr/>
                                @include('specsheets.form.vinegar._other_product_info')
                                <hr/>
                            </div>

                        </fieldset>
                        <hr/>
                        <div class="form-group">
                        {{ Form::submit('Create', array('class' => 'btn btn-default')) }}
                        </div>
                        {{ Form::close() }}

                    </div><!--end row-->
                </div>
            </div><!--end col lg 8 -->
        </div><!--end container fluid-->



    </div><!--end specsheets-->
    @stop

    @section('footer')
   <script>
          $(function() {
            $( "#datepicker" ).datepicker();
            $( "#datepicker1" ).datepicker();
            $('#timepicker').timepicker();
        });
     </script>

    @stop