@extends('layouts.admin')

@section('content')
     <div id="specsheets">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Edit</span>A Specsheet
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/specsheets/">Specsheets</a> >
                    Edit
                </li>
            </ol>
        </div>
    </div>

    {{--errors--}}
    @if($errors->has())
        <div class="alert alert-dismissible alert-danger">
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        </div>
    @endif

    {{--form--}}
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="col-lg-8">
                    {{ Form::model($specsheet, array('method' => 'PATCH','files' => true,'image/save' => 'image/save', 'route' => array('specsheets.update', $specsheet->id))) }}<fieldset>
                        <h2>Information</h2>
                        {{--is done--}}
                        <div class="form-group">
                            {{ Form::label('isdone', 'Complete?') }}
                            {{ Form::select('isdone',[$specsheet->isdone, 'Yes' => 'Yes', 'No' => 'No'], $specsheet->isdone, ['class' => 'form-control']) }}
                        </div>

                           {{--sds active--}}
                        <div class="form-group">
                            {{ Form::label('sds_active', 'SDS Active?') }}
                            {{ Form::select('sds_active', ['' => 'Choose', '1' => 'Active', '2' => 'Not Active'],null,['class' => 'form-control']) }}
                        </div>

                        {{--is oil or vinegar dropdown--}}
                        <div class="form-group">
                            {{ Form::label('is_oil_vinegar', 'Oil or Vinegar?') }}
                            {{ Form::select('is_oil_vinegar', ['1' => 'Oil', '2' => 'Vinegar'],null,['class' => 'form-control']) }}
                        </div>

                        {{--website--}}
                        <div class="form-group">
                            {{ Form::label('website', 'Website') }}
                            {{ Form::select('website', ['none' => 'None','store supply' => 'Store Supply', 'soap supply' => 'Soap Supply', 'both' => 'Both'], $specsheet->website, ['class' => 'form-control']) }}
                        </div>

                        {{--main--}}
                        <div class="form-group">
                            {{ Form::label('title', 'Specsheet Title') }}
                            {{ Form::text('title', null,['class' => 'form-control', 'placeholder' => 'Title']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('sku', 'Sku') }}
                            {{ Form::text('sku', null,['class' => 'form-control', 'placeholder' => 'Sku']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('revised', 'Revised') }}
                            {{ Form::text('revised', null,['class' => 'form-control', 'placeholder' => 'Revised']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('description', 'Product Description') }}
                            {{ Form::text('description', null,['class' => 'form-control', 'placeholder' => 'Product Description']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('bullet_point1', 'Bullet Point 1') }}
                            {{ Form::text('bullet_point1', null,['class' => 'form-control', 'placeholder' => 'Bullet Point 1']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('bullet_point2', 'Bullet Point 2') }}
                            {{ Form::text('bullet_point2', null,['class' => 'form-control', 'placeholder' => 'Bullet Point 2']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('bullet_point3', 'Bullet Point 3') }}
                            {{ Form::text('bullet_point3', null,['class' => 'form-control', 'placeholder' => 'Bullet Point 3']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('bullet_point4', 'Bullet Point 4') }}
                            {{ Form::text('bullet_point4', null,['class' => 'form-control', 'placeholder' => 'Bullet Point 4']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('bullet_point5', 'Bullet Point 5') }}
                            {{ Form::text('bullet_point5', null,['class' => 'form-control', 'placeholder' => 'Bullet Point 5']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('bullet_point6', 'Bullet Point 6') }}
                            {{ Form::text('bullet_point6', null,['class' => 'form-control', 'placeholder' => 'Bullet Point 6']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('bullet_point7', 'Bullet Point 7') }}
                            {{ Form::text('bullet_point7', null,['class' => 'form-control', 'placeholder' => 'Bullet Point 7']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('bullet_point8', 'Bullet Point 8') }}
                            {{ Form::text('bullet_point8', null,['class' => 'form-control', 'placeholder' => 'Bullet Point 8']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('prop_65_statement', 'Prop 65 Statement') }}
                            {{ Form::textarea('prop_65_statement', null,['class' => 'form-control', 'placeholder' => 'Prop 65 Statement', 'rows' => '6', 'cols' => '8']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('storage', 'Storage') }}
                            {{ Form::text('storage', null,['class' => 'form-control', 'placeholder' => 'Storage']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('shelf_life', 'Shelf Life') }}
                            {{ Form::text('shelf_life', null,['class' => 'form-control', 'placeholder' => 'Shelf Life']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('sewer', 'Sewer Sludge and Irradiation Statement:') }}
                            {{ Form::text('sewer', null,['class' => 'form-control', 'placeholder' => 'Sewer Sludge and Irradiation Statement']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('applications', 'Applications for the Product') }}
                            {{ Form::text('applications', null,['class' => 'form-control', 'placeholder' => 'Applications for the Product']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('country', 'Country of Origin:') }}
                            {{ Form::text('country', null,['class' => 'form-control', 'placeholder' => 'Country']) }}
                        </div>
                    </fieldset>
                </div><!--end row-->
            </div>
            {{--right--}}
            <div class="col-md-6">
                @if($specsheet->is_oil_vinegar == 1 || $specsheet->is_oil_vinegar == "oil")
                    @include('specsheets.form._nutrition_fact')
                    @include('specsheets.form.oil._under_allergen')
                    @include('specsheets.form.oil._organoleptic_characteristics')
                    @include('specsheets.form.oil._typical_analysis')
                    @include('specsheets.form.oil._typical_fatty')
                    @include('specsheets.form.oil._usda')
                    @include('specsheets.form.oil._registrations')
                @elseif($specsheet->is_oil_vinegar == 2 || $specsheet->is_oil_vinegar == "vinegar")
                        @include('specsheets.form._nutrition_fact')
                        @include('specsheets.form.vinegar._vinegar_organoleptic_characteristics')
                        @include('specsheets.form.vinegar._vinegar_chemical_physical_characteristics')
                        @include('specsheets.form.vinegar._vinegar_nutrition_facts')
                        @include('specsheets.form.vinegar._other_product_info')
                    @endif
            </div>
            {{--end right--}}
        </div><!--end col lg 8 -->
        <hr/>
            {{-- sds --}}
        @include('specsheets.form._sds')
        <div class="form-group">
            {{ Form::submit('Save', array('class' => 'btn btn-default')) }}
        </div>
        {{ Form::close() }}
    </div><!--end container fluid-->



</div><!--end specsheets-->
@stop