@extends('layouts.admin')

@section('content')
    <div id="receivingappointmentlogs">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Edit</span> A Receiving Appointment Log
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/receivingappointmentlogs/">Receiving Appointment Logs</a> >
                        Create
                    </li>
                </ol>
            </div>
        </div>

        {{--errors--}}
        @if($errors->has())
            <div class="alert alert-dismissible alert-danger">
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
        @endif

        {{--form--}}
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        {{ Form::model($receivingappointmentlog, array('method' => 'PATCH', 'route' =>
                                                         array('receivingappointmentlogs.update', $receivingappointmentlog->id))) }}
                        <fieldset>
                            <div class="form-group">
                                {{ Form::label('active', 'Received?') }}
                                {{ Form::select('active', ['1' => 'Not Received', '2' => 'Received'],null,['class' => 'form-control']) }}
                            </div>
                                                      <div class="form-group">
                                {{ Form::label('customer_product', 'Customer Product?') }}
                                {{ Form::select('customer_product', ['' => 'Choose', '1' => 'Yes', '2' => 'No'],null,['class' => 'form-control','id' => 'select']) }}
                            </div>
           <div class="form-group">
                                {{ Form::label('conf_name', 'Conf Name') }}
                                {{ Form::select('conf_name', ['KG' => 'Kathy Griset', 'CS' => 'Customer Service', 'QC' => 'Quality Control'],null,['class' => 'form-control','id' => 'select']) }}
                            </div>                          <div class="form-group">
                                {{ Form::label('po', 'PO#') }}
                                {{ Form::text('po', null, ['class' => 'form-control', 'placeholder' => 'PO#'])}}
                            </div>
                            <div class="form-group">
                                {{ Form::label('packslip', 'Packslip') }}
                                {{ Form::text('packslip', null, ['class' => 'form-control', 'placeholder' => 'Packslip']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('lot', 'Lot') }}
                                {{ Form::text('lot', null, ['class' => 'form-control', 'placeholder' => 'Lot']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('container', 'Container') }}
                                {{ Form::text('container', null, ['class' => 'form-control', 'placeholder' => 'Container']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('product_on_hold', 'Product On Hold') }}
                                {{ Form::text('product_on_hold', null, ['class' => 'form-control', 'placeholder' => 'Product On Hold']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('product_contract', 'Product Contract') }}
                                {{ Form::text('product_contract', null, ['class' => 'form-control', 'placeholder' => 'Product Contract']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('condition_of_truck', 'Condition Of Truck') }}
                                {{ Form::text('condition_of_truck', null, ['class' => 'form-control', 'placeholder' => 'Condition Of Truck']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('qty', 'QTY') }}
                                {{ Form::text('qty', null,['class' => 'form-control', 'placeholder' => 'QTY']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('vendor', 'Vendor') }}
                                {{ Form::text('vendor', null,['class' => 'form-control', 'placeholder' => 'Vendor']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('carrier', 'Carrier') }}
                                {{ Form::text('carrier', null,['class' => 'form-control', 'placeholder' => 'Carrier']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('product', 'Product') }}
                                {{ Form::text('product', null,['class' => 'form-control', 'placeholder' => 'Product']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('size', 'Size') }}
                                {{ Form::select('size', [ 'Tanker' => 'Tanker', 'Totes' => 'Totes', 'Pallets' => 'Pallets', 'Flexy' => 'Flexy', 'Drums' => 'Drums', 'Boxes' => 'Boxes'],$receivingappointmentlog->size,['class' => 'form-control','id' => 'select']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('in_out', 'In/Out') }}
                                {{ Form::select('in_out', ['In' => 'In', 'Out' => 'Out'],'Please Select',['class' => 'form-control','id' => 'select']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('date', 'Date') }}
                                {{ Form::text('date', null,['class' => 'form-control','id' => 'datepicker', 'placeholder' => 'Date']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('time', 'Time') }}
                                {{ Form::text('time', null,['class' => 'form-control','id' => 'timepicker', 'placeholder' => 'Time']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('kosher_cert_file', 'Kosher Cert File') }}
                                {{ Form::text('kosher_cert_file', null,['class' => 'form-control','id' => 'timepicker', 'placeholder' => 'Kosher Cert File']) }}
                            </div>
                            {{ Form::submit('Update',['class' => 'btn btn-default', 'style' => 'float:right']) }}
                            {{ link_to_route('receivingappointmentlogs.index', 'Cancel',null,['class' => 'btn btn-primary', 'style' => 'float:right;']) }}
                        </fieldset>
                        {{ Form::close() }}

                    </div><!--end row-->
                </div>
            </div><!--end col lg 8 -->
        </div><!--end container fluid-->



    </div><!--end blends-->
@stop