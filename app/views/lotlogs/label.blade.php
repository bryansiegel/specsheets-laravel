@extends('layouts.master')


@section('content')
@if($label->qty === 1)
<div style="width:400px; text-align: center;">
<span style="font-size:32px;font-weight:bold;">{{ $label->oil_type }}</span><br />
<!--<span style="font-size:24px;">Drum#: {{ $label->qty }} of 2</span><br />-->
<span style="font-size:24px;">Supplier: {{ $label->oil_type }}</span><br />
<span style="font-size:32px;font-weight:bold;">Lot#: {{ $label->lot }}</span><br />
<span style="font-size:24px;">Recv date: {{ $label->date }}</span><br />
</div>
@else
	<?php 
	$quantity = range(1,$label->qty);
	?>

		
<div class="row">
		{{-- @foreach($quantity as $qty) --}}

		
	<div class="col-md-4">
			<div style="width:400px; text-align: center;">
			<form method="post" action="/lotlogs/label/print" class="form-horizontal">
			{{ Form::hidden('oil_type', $label->oil_type) }}
			{{ Form::hidden('qty', $label->qty) }}
			{{-- {{ Form::hidden('qty_of', $qty) }} --}}
			{{ Form::hidden('supplier', $label->supplier) }}
			{{ Form::hidden('lot', $label->lot) }}
			{{ Form::hidden('date', $label->date) }}

			<span style="font-size:32px;font-weight:bold;">{{ $label->oil_type }}</span><br />
			<span style="font-size:24px;">Supplier: {{ $label->supplier }}</span><br />
			<span style="font-size:32px;font-weight:bold;">Lot#: {{ $label->lot }}</span><br />
			<span style="font-size:24px;">Recv date: {{ $label->date }}</span><br />
			{{ Form::submit('View To Print',['class' => 'btn btn-primary'])}}
			{{ Form::close() }}
			<hr />
			</div>
	</div>
			
			

		
		
		{{-- @endforeach --}}
		</div><!--end row-->
@endif

@stop