<div style="background-color:#ddddcc; padding-left:15px; padding-right:15px; padding-bottom:20px;">
<h2>Incoming Materials QC Form</h2>


<div class="form-group">
	{{ Form::label('im_date_received', 'Date Received') }}
	{{ Form::text('im_date_received',$lotlog->im_date_received,['class' => 'form-control', 'placeholder' => 'Date Received', 'id' => 'datepicker1']) }}
</div>
<h2>Item Received</h2>
<div class="form-group">
	{{ Form::label('im_trucker_inspection', 'Truck/Tanker Inspection') }}
	{{ Form::text('im_trucker_inspection', $lotlog->im_trucker_inspection,['class' => 'form-control', 'placeholder' => 'Truck/Tanker Inspection']) }}
</div>

<div class="form-group">
	{{ Form::label('im_seal', 'Seal') }}
	{{ Form::text('im_seal', $lotlog->im_seal,['class' => 'form-control', 'placeholder' => 'Seal']) }}
</div>
<h2>Documentation</h2>
<div class="form-group">
	{{ Form::label('im_bol', 'B.O.L.') }}
	{{ Form::file('im_bol') }}
</div>
<h3>Product Condition</h3>
<div class="form-group">
	{{ Form::label('im_labeling', 'Labeling') }}
	{{ Form::text('im_labeling', $lotlog->im_labeling,['class' => 'form-control', 'placeholder' => 'Labeling']) }}
</div>
<div class="form-group">
	{{ Form::label('im_cartons', 'Cartons') }}
	{{ Form::text('im_cartons', $lotlog->im_cartons,['class' => 'form-control', 'placeholder' => 'Cartons']) }}
</div>
<div class="form-group">
	{{ Form::label('im_condition', 'Condition') }}
	{{ Form::text('im_condition', $lotlog->im_condition,['class' => 'form-control', 'placeholder' => 'Condition']) }}
</div>
<div class="form-group">
	{{ Form::label('im_primary_secondary', 'Primary/Secondary') }}
	{{ Form::text('im_primary_secondary', $lotlog->im_primary_secondary,['class' => 'form-control', 'placeholder' => 'Primary/Secondary']) }}
</div>
<h2>QC Findings</h2>
<div class="form-group">
	{{ Form::label('im_notes', 'Notes') }}
	{{ Form::textarea('im_notes', $lotlog->im_notes,['class' => 'form-control', 'placeholder' => 'Notes', 'rows' => '8', 'cols' => '4']) }}
</div>
<div class="form-group">
	{{ Form::label('im_approved_for_use', 'Approved For Use') }}
	{{ Form::text('im_approved_for_use', $lotlog->im_approved_for_use,['class' => 'form-control', 'placeholder' => 'Approved For Use']) }}
</div>
<div class="form-group">
	{{ Form::label('im_not_approved_why', 'If no explain why') }}
	{{ Form::text('im_not_approved_why', $lotlog->im_not_approved_why,['class' => 'form-control', 'placeholder' => 'If no explain why']) }}
</div>

<div class="form-group">
	{{ Form::label('approved_by', 'Assigned To/Approved By') }}
	{{ Form::text('approved_by', $lotlog->im_approved_by,['class' => 'form-control', 'placeholder' => 'Assigned To/Approved By']) }}
</div>



</div><!--end incoming materials-->