<h2>Incoming Product QC Form</h2>

<div style="background-color:#ddddcc; padding-left:15px; padding-right:15px; padding-bottom:20px;">

<div class="form-group">
	{{ Form::label('ip_date_received', 'Date Received') }}
	{{ Form::text('ip_date_received', $lotlog->ip_date_received,['class' => 'form-control', 'placeholder' => 'Date Received', 'id' => 'datepicker1']) }}
</div>

<h3>Items Received</h3>
<div class="form-group">
	{{ Form::label('ip_truck_tanker', 'Truck/Tanker Inspection') }}
	{{ Form::text('ip_truck_tanker', $lotlog->ip_truck_tanker,['class' => 'form-control', 'placeholder' => 'Truck/Tanker Inspection']) }}
</div>
<div class="form-group">
	{{ Form::label('ip_seals', 'Seals') }}
	{{ Form::text('ip_seals', $lotlog->ip_seals,['class' => 'form-control', 'placeholder' => 'Seals']) }}
</div>
<h3>Documentation</h3>
	<div class="form-group">
		{{ Form::label('ip_kosher_shed_a', 'Kosher Shed A') }}
		<div class="radio">
			<label>{{ Form::radio('ip_kosher_shed_a', 'Yes',['class' => 'form-control']) }}OK</label>
		</div>
		<div class="radio">
			<label>{{ Form::radio('ip_kosher_shed_a', 'No',['class' => 'form-control']) }}NOT OK</label>
		</div>
	</div>
<div class="form-group">
	{{ Form::label('ip_certificate_of_analysis', 'Certificate of Analysis') }}
	{{ Form::file('ip_certificate_of_analysis') }}
</div>
<div class="form-group">
	{{ Form::label('ip_weight_certificate', 'Weight Certificate') }}
	{{ Form::file('ip_weight_certificate') }}
</div>
<div class="form-group">
	{{ Form::label('ip_wash_certificate', 'Wash Certificate') }}
	{{ Form::file('ip_wash_certificate') }}
</div>
<div class="form-group">
	{{ Form::label('ip_organic_certificate', 'Organic Certificate') }}
	{{ Form::file('ip_organic_certificate') }}
</div>
<h3>Product Condition</h3>
<div class="form-group">
	{{ Form::label('ip_caps_and_seals', 'Caps and Seals') }}
	{{ Form::text('ip_caps_and_seals', $lotlog->ip_caps_and_seals,['class' => 'form-control', 'placeholder' => 'Caps and Seals']) }}
</div>
<div class="form-group">
	{{ Form::label('ip_cartons', 'Cartons') }}
	{{ Form::text('ip_cartons', $lotlog->ip_cartons,['class' => 'form-control', 'placeholder' => 'Caps and Seals']) }}
</div>
<div class="form-group">
	{{ Form::label('ip_drum_condition', 'Drum Condition') }}
	{{ Form::text('ip_drum_condition', $lotlog->ip_drum_condition,['class' => 'form-control', 'placeholder' => 'Drum Condition']) }}
</div>
<div class="form-group">
	{{ Form::label('ip_labels', 'Labels') }}
	{{ Form::text('ip_labels', $lotlog->ip_labels,['class' => 'form-control', 'placeholder' => 'Labels']) }}
</div>
<h3>Product Information</h3>
<div class="form-group">
	{{ Form::label('ip_product_type', 'Product Type') }}
	{{ Form::text('ip_product_type', $lotlog->ip_product_type,['class' => 'form-control', 'placeholder' => 'Product Type']) }}
</div>

<div class="form-group">
	{{ Form::label('ip_vendor_lot_number', 'Vendor Lot Number') }}
	{{ Form::text('ip_vendor_lot_number', $lotlog->ip_vendor_lot_number,['class' => 'form-control', 'placeholder' => 'Vendor Lot Number']) }}
</div>
<h3>QC Findings</h3>
<div class="form-group">
	{{ Form::label('ip_notes', 'Notes') }}
	{{ Form::textarea('ip_notes', $lotlog->ip_notes,['class' => 'form-control', 'placeholder' => 'Notes', 'rows' => '8','cols' => '4']) }}
</div>

<div class="form-group">
	{{ Form::label('ip_approved_for_use', 'Approved For Use') }}
	{{ Form::text('ip_approved_for_use', $lotlog->ip_approved_for_use,['class' => 'form-control', 'placeholder' => 'Approved For Use']) }}
</div>

<div class="form-group">
	{{ Form::label('ip_if_no_explain_why', 'If no explain why') }}
	{{ Form::textarea('ip_if_no_explain_why', $lotlog->ip_if_no_explain_why,['class' => 'form-control', 'placeholder' => 'If no explain why', 'rows' => '8','cols' => '4']) }}
</div>
<div class="form-group">
	{{ Form::label('approved_by', 'Assigned To/Approved By') }}
	{{ Form::text('approved_by', $lotlog->ip_approved_by,['class' => 'form-control', 'placeholder' => 'Assigned To/Approved By']) }}
</div>



</div><!--end-->