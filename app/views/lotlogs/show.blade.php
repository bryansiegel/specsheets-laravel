@extends('layouts.admin')

@section('content')

    <div id="blends">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">View</span>Lot Log - Lot# {{ $lotlog->lot }}
                           @if(Auth::user()->group == 100 || Auth::user()->group == 50) 
                            <a href="/lotlogs/label/{{ $lotlog->id}}" class="btn btn-primary">View Label</a>
                            @endif
                </h1>

                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/lotlogs/">Lot Logs</a> >
                        View <span style="float:right">
                            

                        </span>
                    </li>
                </ol>
            </div>
        </div>


        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <strong>Progress:</strong> {{ $lotlog->progress }}
                            </li>
                            <li class="list-group-item">
                                <strong>Progress Notes:</strong> {{ $lotlog->progress_notes }}
                            </li>
                            <li class="list-group-item">
                                <strong>Lot:</strong> {{ $lotlog->lot }}
                            </li>
                            <li class="list-group-item">
                              <strong>Expiration Date:</strong> {{ $lotlog->expiration_date }}
                            </li>
                            <li class="list-group-item">
                              <strong>Container Number:</strong> {{ $lotlog->container_number }}
                            </li>
                            <li class="list-group-item">
                                <strong>SO#</strong> {{ $lotlog->so }}
                            </li>
                            <li class="list-group-item">
                                <strong>Original Lot</strong> {{ $lotlog->original_lot }}
                            </li>
                            <li class="list-group-item">
                                <strong>Product Description</strong> {{ $lotlog->oil_type }}
                            </li>
                            <li class="list-group-item">
                                <strong>Supplier</strong> {{ $lotlog->supplier }}
                            </li>
                            <li class="list-group-item">
                                <strong>Origin</strong> {{ $lotlog->origin }}
                            </li>
                            <li class="list-group-item">
                                <strong>Quantity:</strong> {{ $lotlog->qty }}
                            </li>
                            <li class="list-group-item">
                                <strong>Size:</strong> {{ $lotlog->size }}
                            </li>
                            <li class="list-group-item">
                                <strong>Customer:</strong> {{ $lotlog->customer }}
                            </li>
                            <li class="list-group-item">
                                <strong>Date:</strong> {{ $lotlog->date }}
                            </li>
                            <li class="list-group-item">
                                <strong>Sample Location:</strong> {{ $lotlog->sample_location }}
                            </li>
                            <li class="list-group-item">
                                <strong>Note</strong> {{ $lotlog->notes1 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Note</strong> {{ $lotlog->notes2 }}
                            </li>    <li class="list-group-item">
                                <strong>Note</strong> {{ $lotlog->notes3 }}
                            </li>
                        </ul>

                        <hr/>
                        <div class="alert alert-dismissible alert-success" style="margin-bottom:0px">
                            <strong>Created: </strong><?php echo \Carbon\Carbon::createFromTimestamp(strtotime($lotlog->created_at))->diffForHumans(); ?>
                        </div>
                        <div class="alert alert-dismissible alert-warning">
                            <strong>Updated: </strong><?php echo \Carbon\Carbon::createFromTimestamp(strtotime($lotlog->updated_at))->diffForHumans(); ?>
                        </div>

                    </div>
                </div>
              
            
            </div>
        </div>
@stop