@extends('layouts.admin')

@section('content')

    <div id="lotlogs">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Lot Logs <?php 
                        $date = Date('Y');
                        $last_year = $date - 1;
                        echo $last_year;
                    ?>
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > Lot Logs > Archive
                    </li>
                </ol>
            </div>
        </div>

        <!--sessions-->
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif

        <!--sub nav-->
        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">
                   
                    {{ link_to('lotlogs/', 'Lotlogs' . ' ' . Date('Y'), ['class' => 'btn btn-success navbar-btn']) }}
                    {{ link_to('lotlogs/archive', 'Archive', ['class' => 'btn btn-danger navbar-btn']) }}


                    <!--search-->

                    <!-- <form class="navbar-form navbar-right" role="search">
                        <div class="form-group">
                            <input type="text" class=" search form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-primary" data-sort="nutritionfact">Submit</button>
                    </form> -->
                </nav>
            </div>
        </div>

        <table id="datatable" class="table table-stripped">
            <thead>
            <tr>
                <td><strong>Lot</strong></td>
                <td><strong>SO#</strong></td>
                <td><strong>Original Lot</strong></td>
                <td><strong>Product Description</strong></td>
                <td><strong>Supplier</strong></td>
                <td><strong>Origin</strong></td>
                <td><strong>QTY</strong></td>
                <td><strong>Size</strong></td>
                <td><strong>Customer</strong></td>
                <td><strong>Reason On Hold</strong></td>
                <td><strong>Date</strong></td>

                <td class="no-sort"></td>
            </tr>
            </thead>
            <tbody class="list">
            @foreach($lotlogs as $lotlog)
            
            <!-- if not inventory category only -->

            @if($lotlog->progress == 'in_progress')
            <tr style="background-color:#faff8c">
            @else
                <tr>
                @endif

                    <td class="lot">{{ $lotlog->lot }}</td>
                    <td class="so">{{ $lotlog->so }}</td>
                    <td class="originallot">{{ $lotlog->original_lot }}</td>
                    <td class="oiltype">{{ $lotlog->oil_type }}</td>
                    <td class="supplier">{{ $lotlog->supplier }}</td>
                    <td class="origin">{{ $lotlog->origin }}</td>
                    <td class="qty">{{ $lotlog->qty }}</td>
                    <td class="size">{{ $lotlog->size }}</td>
                    <td class="customer">{{ $lotlog->customer }}</td>
                    <td>
                        <?php
                            if(!empty($lotlog->im_not_approved_why))
                                {
                                    echo $lotlog->im_not_approved_why;
                                }
                            elseif(!empty($lotlog->ip_not_approved_why))
                                {
                                    echo $lotlog->ip_not_approved_why;
                                }
                            else
                                {
                                    echo "";
                                }


                        ?>

                    </td>
                    <td class="date">{{ $lotlog->date }}</td>

                    <td>
                         <div class="btn-group" role="group" aria-label="">
                        {{ link_to_route('lotlogs.show', 'View', [$lotlog->id],['class' => 'btn btn-success']) }}
                            @if(Auth::user()->group == 100 || Auth::user()->group == 50) 
                        {{ link_to_route('lotlogs.edit', 'Edit', [$lotlog->id],['class' => 'btn btn-warning']) }}

                                @endif 
                            </div>

                </tr>
            @endforeach

            </tbody>
        </table>

    </div>
@stop

<!--end content-->


@section('footer')
   <script>
          $(function() {
            $( "#datepicker" ).datepicker();
            $( "#datepicker1" ).datepicker();
            $('#timepicker').timepicker();
            $('#datatable').DataTable({
                paging:true,
                "order": [[ 0, "desc" ]],
                scrollY:400,
                deferRender:true,
                scroller:true,
                columnDefs: [
                                { targets: 'no-sort', orderable: false }
                            ],

            });
        });
    </script>

    @stop