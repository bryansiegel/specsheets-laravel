@extends('layouts.admin')

@section('content')
    <div id="blends">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Create</span>A COA
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a
                                href="/coas/">COA</a> >
                        Create
                    </li>
                </ol>
            </div>
        </div>

        {{--errors--}}
        @if($errors->has())
            <div class="alert alert-dismissible alert-danger">
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
        @endif

        {{--form--}}
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        {{ Form::open(['route' => 'coas.store', 'class' => 'form-horizontal']) }}

                        <fieldset>
                            <div class="form-group">
                                {{ Form::label('lot', 'Lot') }}
                                {{ Form::text('lot', null,['class' => 'form-control',  'placeholder' => 'Lot']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('description', 'Description') }}
                                {{ Form::text('description', null,['class' => 'form-control', 'placeholder' => 'Description']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('manf_date', 'Manf date') }}
                                {{ Form::text('manf_date', null,['class' => 'form-control', 'placeholder' => 'Manf date', 'id' => 'datepicker1']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('expiration_date', 'Expiration Date') }}
                                {{ Form::text('expiration_date', null,['class' => 'form-control', 'placeholder' => 'Expiration Date', 'id' => 'datepicker']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('origin', 'Origin') }}
                                {{ Form::text('origin', null,['class' => 'form-control', 'placeholder' => 'Origin']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('po_number', 'PO Number') }}
                                {{ Form::text('po_number', null,['class' => 'form-control', 'placeholder' => 'PO Number']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('item_number', 'Item Number') }}
                                {{ Form::text('item_number', null,['class' => 'form-control', 'placeholder' => 'Item Number']) }}
                            </div>
                            <hr>
                            <div class="form-group">
                                {{ Form::label('nm232', '232nm') }}
                                {{ Form::text('nm232', null,['class' => 'form-control', 'placeholder' => '232nm']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('absorbtion_in_ultra_violet', 'Absorbtion in Ultra Violet') }}
                                {{ Form::text('absorbtion_in_ultra_violet', null,['class' => 'form-control', 'placeholder' => 'Absorbtion in Ultra Violet']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('acid_value', 'Acid Value') }}
                                {{ Form::text('acid_value', null,['class' => 'form-control', 'placeholder' => 'Acid Value']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('acidity', 'Acidity') }}
                                {{ Form::text('acidity', null,['class' => 'form-control', 'placeholder' => 'Acidity']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('additives', 'Additives') }}
                                {{ Form::text('additives', null,['class' => 'form-control', 'placeholder' => 'Additives']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('alcohol', 'Alcohol') }}
                                {{ Form::text('alcohol', null,['class' => 'form-control', 'placeholder' => 'Alcohol']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('aom', 'AOM') }}
                                {{ Form::text('aom', null,['class' => 'form-control', 'placeholder' => 'AOM']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('appearance', 'Appearance') }}
                                {{ Form::text('appearance', null,['class' => 'form-control', 'placeholder' => 'Appearance']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('arachidic_acid', 'Arachidic acid') }}
                                {{ Form::text('arachidic_acid', null,['class' => 'form-control', 'placeholder' => 'Arachidic acid']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('aroma', 'Aroma') }}
                                {{ Form::text('aroma', null,['class' => 'form-control', 'placeholder' => 'Aroma']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('ash', 'Ash') }}
                                {{ Form::text('ash', null,['class' => 'form-control', 'placeholder' => 'Ash']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('behenic_acid', 'Behenic acid') }}
                                {{ Form::text('behenic_acid', null,['class' => 'form-control', 'placeholder' => 'Behenic acid']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('beta_sitosterol_delta5_avenasterol', 'delta5-23stigmastadienol + clerosterol +            
                                        sitostanol + delta5-24-stigmastadienol') }}
                                {{ Form::text('beta_sitosterol_delta5_avenasterol', null,['class' => 'form-control', 'placeholder' => 'delta5-23stigmastadienol + clerosterol + sitostanol + delta5-24-stigmastadienol']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('brassicasterol', 'Brassicasterol') }}
                                {{ Form::text('brassicasterol', null,['class' => 'form-control', 'placeholder' => 'Brassicasterol']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('brix', 'Brix') }}
                                {{ Form::text('brix', null,['class' => 'form-control', 'placeholder' => 'Brix']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('taste', 'Taste') }}
                                {{ Form::text('taste', null,['class' => 'form-control', 'placeholder' => 'Taste']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('capric_acid', 'Capric acid') }}
                                {{ Form::text('capric_acid', null,['class' => 'form-control', 'placeholder' => 'Capric acid']) }}
                            </div>                            
                            <div class="form-group">
                                {{ Form::label('campesterol', 'Campesterol') }}
                                {{ Form::text('campesterol', null,['class' => 'form-control', 'placeholder' => 'Campesterol']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('capronic_acid', 'Capronic acid') }}
                                {{ Form::text('capronic_acid', null,['class' => 'form-control', 'placeholder' => 'Capronic acid']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('caprylic_acid', 'Caprylic acid') }}
                                {{ Form::text('caprylic_acid', null,['class' => 'form-control', 'placeholder' => 'Caprylic acid']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('cholesterol', 'Cholesterol') }}
                                {{ Form::text('cholesterol', null,['class' => 'form-control', 'placeholder' => 'Cholesterol']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('cold_test', 'Cold test') }}
                                {{ Form::text('cold_test', null,['class' => 'form-control', 'placeholder' => 'Cold test']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('color', 'Color') }}
                                {{ Form::text('color', null,['class' => 'form-control', 'placeholder' => 'Color']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('color_gardner', 'Color gardner') }}
                                {{ Form::text('color_gardner', null,['class' => 'form-control', 'placeholder' => 'Color gardner']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('color_lovibond', 'Color lovibond') }}
                                {{ Form::text('color_lovibond', null,['class' => 'form-control', 'placeholder' => 'Color lovibond']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('blue', 'Color blue') }}
                                {{ Form::text('blue', null,['class' => 'form-control', 'placeholder' => 'Color blue']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('neutral', 'Color neutral') }}
                                {{ Form::text('neutral', null,['class' => 'form-control', 'placeholder' => 'Color neutral']) }}
                            </div>


                            <div class="form-group">
                                {{ Form::label('color_red', 'Color red') }}
                                {{ Form::text('color_red', null,['class' => 'form-control', 'placeholder' => 'Color red']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('color_visual', 'Color visual') }}
                                {{ Form::text('color_visual', null,['class' => 'form-control', 'placeholder' => 'Color visual']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('color_yellow', 'Color yellow') }}
                                {{ Form::text('color_yellow', null,['class' => 'form-control', 'placeholder' => 'Color yellow']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('compesterol', 'Compesterol') }}
                                {{ Form::text('compesterol', null,['class' => 'form-control', 'placeholder' => 'Compesterol']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('copper', 'Copper') }}
                                {{ Form::text('copper', null,['class' => 'form-control', 'placeholder' => 'Copper']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('delta7_stigmasternol', 'Delta7 stigmasternol') }}
                                {{ Form::text('delta7_stigmasternol', null,['class' => 'form-control', 'placeholder' => 'Delta7 stigmasternol']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('deltak', 'Delta k') }}
                                {{ Form::text('deltak', null,['class' => 'form-control', 'placeholder' => 'Delta k']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('density_20', 'Density 20') }}
                                {{ Form::text('density_20', null,['class' => 'form-control', 'placeholder' => 'Density 20']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('developed_alcohol_degree', 'Developed alcohol degree') }}
                                {{ Form::text('developed_alcohol_degree', null,['class' => 'form-control', 'placeholder' => 'Developed alcohol degree']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('dextros_equivalent', 'Dextros equivalent') }}
                                {{ Form::text('dextros_equivalent', null,['class' => 'form-control', 'placeholder' => 'Dextros equivalent']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('eicosanedienoic_acid', 'Eicosanedienoic acid') }}
                                {{ Form::text('eicosanedienoic_acid', null,['class' => 'form-control', 'placeholder' => 'Eicosanedienoic acid']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('eicosaneteetraenoic_acid', 'Eicosaneteetraenoic acid') }}
                                {{ Form::text('eicosaneteetraenoic_acid', null,['class' => 'form-control', 'placeholder' => 'Eicosaneteetraenoic acid']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('erucic_acid', 'Erucic acid') }}
                                {{ Form::text('erucic_acid', null,['class' => 'form-control', 'placeholder' => 'Erucic acid']) }}
                            </div>
                            <h2>Fatty Acids</h2>

                            <div class="form-group">
                                {{ Form::label('fatty_acids', 'Fatty acid') }}
                                {{ Form::text('fatty_acids', null,['class' => 'form-control', 'placeholder' => 'Fatty acid']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('free_fatty_acids', 'Free Fatty Acids') }}
                                {{ Form::text('free_fatty_acids', null,['class' => 'form-control', 'placeholder' => 'Free Fatty Acids']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('trans_fatty_acids', 'Trans fatty acids') }}
                                {{ Form::text('trans_fatty_acids', null,['class' => 'form-control', 'placeholder' => 'Trans fatty acids']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('saturated_fatty_acids', 'Saturated fatty acids') }}
                                {{ Form::text('saturated_fatty_acids', null,['class' => 'form-control', 'placeholder' => 'Saturated fatty acids']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('C3_0', 'Propionic acid C3:0') }}
                                {{ Form::text('C3_0', null,['class' => 'form-control', 'placeholder' => 'Propionic acid C3:0']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('C4_0', 'Butyric acid C4:0') }}
                                {{ Form::text('C4_0', null,['class' => 'form-control', 'placeholder' => 'Butyric acid C4:0']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('C5_0', 'Valeric acid C5:0') }}
                                {{ Form::text('C5_0', null,['class' => 'form-control', 'placeholder' => 'Valeric acid C5:0']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('C6_0', 'Caproic acid C6:0') }}
                                {{ Form::text('C6_0', null,['class' => 'form-control', 'placeholder' => 'Caproic acid C6:0']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('C7_0', 'Enanthic acid C7:0') }}
                                {{ Form::text('C7_0', null,['class' => 'form-control', 'placeholder' => 'Enanthic acid C7:0']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('C8_0', 'Caprylic acid C8:0') }}
                                {{ Form::text('C8_0', null,['class' => 'form-control', 'placeholder' => 'Caprylic acid C8:0']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('C9_0', 'Pelargonic acid C9:0') }}
                                {{ Form::text('C9_0', null,['class' => 'form-control', 'placeholder' => 'Pelargonic acid C9:0']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('C10_0', 'Capric acid C10:0') }}
                                {{ Form::text('C10_0', null,['class' => 'form-control', 'placeholder' => 'Capric acid C10:0']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('C11_0', 'Undecylic acid C11:0') }}
                                {{ Form::text('C11_0', null,['class' => 'form-control', 'placeholder' => 'Undecylic acid C11:0']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('C12_0', 'Lauric acid C12:0') }}
                                {{ Form::text('C12_0', null,['class' => 'form-control', 'placeholder' => 'Lauric acid C12:0']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('C13_0', 'Tridecylic acid C13:0') }}
                                {{ Form::text('C13_0', null,['class' => 'form-control', 'placeholder' => 'Tridecylic acid C13:0']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('C14_0', 'Myristic acid C14:0') }}
                                {{ Form::text('C14_0', null,['class' => 'form-control', 'placeholder' => 'Myristic acid C14:0']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('C15_0', 'Pentadecylic acid C15:0') }}
                                {{ Form::text('C15_0', null,['class' => 'form-control', 'placeholder' => 'Pentadecylic acid C15:0']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('C16_0', 'Palmitic acid C16:0') }}
                                {{ Form::text('C16_0', null,['class' => 'form-control', 'placeholder' => 'Palmitic acid C16:0']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('C16_1', 'Palmitoliec acid C16:1') }}
                                {{ Form::text('C16_1', null,['class' => 'form-control', 'placeholder' => 'Palmitoliec acid C16:1']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('C17_0', 'Margaric acid C17:0') }}
                                {{ Form::text('C17_0', null,['class' => 'form-control', 'placeholder' => 'Margaric acid C17:0']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('C17_1', 'Heptadecenoic acid C17:1') }}
                                {{ Form::text('C17_1', null,['class' => 'form-control', 'placeholder' => 'Heptadecenoic acid C17:1']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('C18_0', 'Stearic acid C18:0') }}
                                {{ Form::text('C18_0', null,['class' => 'form-control', 'placeholder' => 'Stearic acid C18:0']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('C18_1', 'Oleic acid C18:1') }}
                                {{ Form::text('C18_1', null,['class' => 'form-control', 'placeholder' => 'Oleic acid C18:1']) }}
                            </div>                            
                            <div class="form-group">
                                {{ Form::label('C18_3', 'Alpha Linolenic C18:3') }}
                                {{ Form::text('C18_3', null,['class' => 'form-control', 'placeholder' => 'Alpha Linolenic C18:3']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('C18_2', 'Linoleic acid C18:2') }}
                                {{ Form::text('C18_2', null,['class' => 'form-control', 'placeholder' => 'Linoleic acid C18:2']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('c18_1t', 'C18:1T') }}
                                {{ Form::text('c18_1t', null,['class' => 'form-control', 'placeholder' => 'C18:1T']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('c18_2t_c18_3t', 'C18:2T + C18:3T') }}
                                {{ Form::text('c18_2t_c18_3t', null,['class' => 'form-control', 'placeholder' => 'C18:2T + C18:3T']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('trans_c_18_1', 'Trans C 18:1') }}
                                {{ Form::text('trans_c_18_1', null,['class' => 'form-control', 'placeholder' => 'Trans C 18:1']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('trans_c_18_2', 'Trans C 18:2') }}
                                {{ Form::text('trans_c_18_2', null,['class' => 'form-control', 'placeholder' => 'Trans C 18:2']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('C19_0', 'Nonadecylic acid C19:0') }}
                                {{ Form::text('C19_0', null,['class' => 'form-control', 'placeholder' => 'Nonadecylic acid C19:0']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('C20_0', 'Arachidic acid C20:0') }}
                                {{ Form::text('C20_0', null,['class' => 'form-control', 'placeholder' => 'Arachidic acid C20:0']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('C20_1', 'Eicosenic Acid C20:1') }}
                                {{ Form::text('C20_1', null,['class' => 'form-control', 'placeholder' => 'Eicosenic Acid C20:1']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('C21_0', 'Heneicosylic acid C21:0') }}
                                {{ Form::text('C21_0', null,['class' => 'form-control', 'placeholder' => 'Heneicosylic acid C21:0']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('C22_0', 'Behenic acid C22:0') }}
                                {{ Form::text('C22_0', null,['class' => 'form-control', 'placeholder' => 'Behenic acid C21:0']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('C23_0', 'Tricosylic acid C23:0') }}
                                {{ Form::text('C23_0', null,['class' => 'form-control', 'placeholder' => 'Tricosylic acid C23:0']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('C24_0', 'Lignoceric acid C24:0') }}
                                {{ Form::text('C24_0', null,['class' => 'form-control', 'placeholder' => 'Lignoceric acid C24:0']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('C25_0', 'Pentacosylic acid C25:0') }}
                                {{ Form::text('C25_0', null,['class' => 'form-control', 'placeholder' => 'Pentacosylic acid C25:0']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('C26_0', 'Cerotic acid C26:0') }}
                                {{ Form::text('C26_0', null,['class' => 'form-control', 'placeholder' => 'Cerotic acid C26:0']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('C27_0', 'Heptacosylic acid C27:0') }}
                                {{ Form::text('C27_0', null,['class' => 'form-control', 'placeholder' => 'Heptacosylic acid C27:0']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('C28_0', 'Montanic acid C28:0') }}
                                {{ Form::text('C28_0', null,['class' => 'form-control', 'placeholder' => 'Montanic acid C28:0']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('C29_0', 'Nonacosylic acid C29:0') }}
                                {{ Form::text('C29_0', null,['class' => 'form-control', 'placeholder' => 'Nonacosylic acid C29:0']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('C30_0', 'Melissic acid C30:0') }}
                                {{ Form::text('C30_0', null,['class' => 'form-control', 'placeholder' => 'Melissic acid C30:0']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('C31_0', 'Henatriacontylic acid C31:0') }}
                                {{ Form::text('C31_0', null,['class' => 'form-control', 'placeholder' => 'Henatriacontylic acid C31:0']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('C32_0', 'Lacceroic acid C32:0') }}
                                {{ Form::text('C32_0', null,['class' => 'form-control', 'placeholder' => 'Lacceroic acid C32:0']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('C33_0', 'Psyllic acid C33:0') }}
                                {{ Form::text('C33_0', null,['class' => 'form-control', 'placeholder' => 'Psyllic acid C33:0']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('C34_0', 'Geddic acid C34:0') }}
                                {{ Form::text('C34_0', null,['class' => 'form-control', 'placeholder' => 'Geddic acid C34:0']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('C35_0', 'Ceroplastic acid C35:0') }}
                                {{ Form::text('C35_0', null,['class' => 'form-control', 'placeholder' => 'Ceroplastic acid C35:0']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('C36_0', 'Hexatriacontylic acid C36:0') }}
                                {{ Form::text('C36_0', null,['class' => 'form-control', 'placeholder' => 'Hexatriacontylic acid C36:0']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('C37_0', 'Heptatriacontanoic acid C37:0') }}
                                {{ Form::text('C37_0', null,['class' => 'form-control', 'placeholder' => 'Heptatriacontanoic acid C37:0']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('C38_0', 'Octatriacontanoic acid C38:0') }}
                                {{ Form::text('C38_0', null,['class' => 'form-control', 'placeholder' => 'Octatriacontanoic acid C38:0']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('campestanol_campesterol', 'Campestanol+Campesterol') }}
                                {{ Form::text('campestanol_campesterol', null,['class' => 'form-control', 'placeholder' => 'Campestanol+Campesterol']) }}
                            </div>

<hr>

                       </div><!--end row-->
                    </div>

                    {{-- right --}}
            <div class="col-md-6">
                            <div class="form-group">
                                {{ Form::label('bitterness', 'Bitterness') }}
                                {{ Form::text('bitterness', null,['class' => 'form-control', 'placeholder' => 'Bitterness']) }}
                            </div>                
                            <div class="form-group">
                                {{ Form::label('bit225', 'BIT(225)') }}
                                {{ Form::text('bit225', null,['class' => 'form-control', 'placeholder' => 'BIT(225)']) }}
                            </div>
                             <div class="form-group">
                                {{ Form::label('dag', 'DAG') }}
                                {{ Form::text('dag', null,['class' => 'form-control', 'placeholder' => 'DAG']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('defects', 'Defects') }}
                                {{ Form::text('defects', null,['class' => 'form-control', 'placeholder' => 'Defects']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('ffa', 'FFA') }}
                                {{ Form::text('ffa', null,['class' => 'form-control', 'placeholder' => 'FFA']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('flavor', 'Flavor') }}
                                {{ Form::text('flavor', null,['class' => 'form-control', 'placeholder' => 'Flavor']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('free_acidity', 'Free Acidity') }}
                                {{ Form::text('free_acidity', null,['class' => 'form-control', 'placeholder' => 'Free Acidity']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('free_so2', 'Free SO2') }}
                                {{ Form::text('free_so2', null,['class' => 'form-control', 'placeholder' => 'Free SO2']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('free_sulpphurous_anhydride', 'Free sulpphurous anhydride') }}
                                {{ Form::text('free_sulpphurous_anhydride', null,['class' => 'form-control', 'placeholder' => 'Free sulpphurous anhydride']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('fructose', 'Fructose') }}
                                {{ Form::text('fructose', null,['class' => 'form-control', 'placeholder' => 'Fructose']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('fruitness', 'Fruitness') }}
                                {{ Form::text('fruitness', null,['class' => 'form-control', 'placeholder' => 'Fruitness']) }}
                            </div>                            
                            <div class="form-group">
                                {{ Form::label('gadoleic_acid', 'Gadoleic acid') }}
                                {{ Form::text('gadoleic_acid', null,['class' => 'form-control', 'placeholder' => 'Gadoleic acid']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('glucose', 'Glucose') }}
                                {{ Form::text('glucose', null,['class' => 'form-control', 'placeholder' => 'Glucose']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('grains', 'Grains') }}
                                {{ Form::text('grains', null,['class' => 'form-control', 'placeholder' => 'Grains']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('heavy_metals_as_lead', 'Heavy metals as lead') }}
                                {{ Form::text('heavy_metals_as_lead', null,['class' => 'form-control', 'placeholder' => 'Heavy metals as lead']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('heptadacanoic_acid', 'Heptadacanoic acid') }}
                                {{ Form::text('heptadacanoic_acid', null,['class' => 'form-control', 'placeholder' => 'Heptadacanoic acid']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('heptadecenoic_acid', 'Heptadecenoic acid') }}
                                {{ Form::text('heptadecenoic_acid', null,['class' => 'form-control', 'placeholder' => 'Heptadecenoic acid']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('hydroxyl_value', 'Hydroxyl value') }}
                                {{ Form::text('hydroxyl_value', null,['class' => 'form-control', 'placeholder' => 'Hydroxyl value']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('identification_a', 'Identification A') }}
                                {{ Form::text('identification_a', null,['class' => 'form-control', 'placeholder' => 'Identification A']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('identification_b', 'Identification B') }}
                                {{ Form::text('identification_b', null,['class' => 'form-control', 'placeholder' => 'Identification B']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('identification_c', 'Identification C') }}
                                {{ Form::text('identification_c', null,['class' => 'form-control', 'placeholder' => 'Identification C']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('insoluble_impurities', 'Insoluble impurities') }}
                                {{ Form::text('insoluble_impurities', null,['class' => 'form-control', 'placeholder' => 'Insoluble impurities']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('iodine_value', 'Iodine value') }}
                                {{ Form::text('iodine_value', null,['class' => 'form-control', 'placeholder' => 'Iodine value']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('iodine_value_ri', 'Iodine value RI') }}
                                {{ Form::text('iodine_value_ri', null,['class' => 'form-control', 'placeholder' => 'Iodine value RI']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('iron', 'Iron') }}
                                {{ Form::text('iron', null,['class' => 'form-control', 'placeholder' => 'Iron']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('im', 'IM') }}
                                {{ Form::text('im', null,['class' => 'form-control', 'placeholder' => 'IM']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('iv_wijs', 'IV (WIJS)') }}
                                {{ Form::text('iv_wijs', null,['class' => 'form-control', 'placeholder' => 'IV (WIJS)']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('ind', 'IND') }}
                                {{ Form::text('ind', null,['class' => 'form-control', 'placeholder' => 'IND']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('k268', 'k268') }}
                                {{ Form::text('k268', null,['class' => 'form-control', 'placeholder' => 'k268']) }}
                            </div>                            
                            <div class="form-group">
                                {{ Form::label('k270', 'K270') }}
                                {{ Form::text('k270', null,['class' => 'form-control', 'placeholder' => 'K270']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('lauric_acid', 'Lauric acid') }}
                                {{ Form::text('lauric_acid', null,['class' => 'form-control', 'placeholder' => 'Lauric acid']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('lead', 'Lead') }}
                                {{ Form::text('lead', null,['class' => 'form-control', 'placeholder' => 'Lead']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('lignoceric_acid', 'Lignoceric acid') }}
                                {{ Form::text('lignoceric_acid', null,['class' => 'form-control', 'placeholder' => 'Lignoceric acid']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('linoleic_acid', 'Linoleic acid') }}
                                {{ Form::text('linoleic_acid', null,['class' => 'form-control', 'placeholder' => 'Linoleic acid']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('linolenic_acid', 'Linolenic acid') }}
                                {{ Form::text('linolenic_acid', null,['class' => 'form-control', 'placeholder' => 'Linolenic acid']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('m_i', 'm_i') }}
                                {{ Form::text('m_i', null,['class' => 'form-control', 'placeholder' => 'm_i']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('maltose', 'Maltose') }}
                                {{ Form::text('maltose', null,['class' => 'form-control', 'placeholder' => 'Maltose']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('melting_point', 'Melting point') }}
                                {{ Form::text('melting_point', null,['class' => 'form-control', 'placeholder' => 'Melting point']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('minor', 'Minor') }}
                                {{ Form::text('minor', null,['class' => 'form-control', 'placeholder' => 'Minor']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('moisture', 'Moisture') }}
                                {{ Form::text('moisture', null,['class' => 'form-control', 'placeholder' => 'Moisture']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('moi', 'MOI') }}
                                {{ Form::text('moi', null,['class' => 'form-control', 'placeholder' => 'MOI']) }}
                            </div>                            
                            <div class="form-group">
                                {{ Form::label('moisture_and_impurities', 'Moisture and impurities') }}
                                {{ Form::text('moisture_and_impurities', null,['class' => 'form-control', 'placeholder' => 'Moisture and impurities']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('moisture_and_sediment_content', 'Moisture and sediment content') }}
                                {{ Form::text('moisture_and_sediment_content', null,['class' => 'form-control', 'placeholder' => 'Moisture and sediment content']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('moisture_level', 'Moisture level') }}
                                {{ Form::text('moisture_level', null,['class' => 'form-control', 'placeholder' => 'Moisture level']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('myistoleic_acid', 'Myistoleic acid') }}
                                {{ Form::text('myistoleic_acid', null,['class' => 'form-control', 'placeholder' => 'Myistoleic acid']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('myristic', 'Myristic') }}
                                {{ Form::text('myristic', null,['class' => 'form-control', 'placeholder' => 'Myristic']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('nervonic_acid', 'Nervonic acid') }}
                                {{ Form::text('nervonic_acid', null,['class' => 'form-control', 'placeholder' => 'Nervonic acid']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('octadecatetraen_acid', 'Octadecatetraen acid') }}
                                {{ Form::text('octadecatetraen_acid', null,['class' => 'form-control', 'placeholder' => 'Octadecatetraen acid']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('odor', 'Odor') }}
                                {{ Form::text('odor', null,['class' => 'form-control', 'placeholder' => 'Odor']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('oleic_acid', 'Oleic acid') }}
                                {{ Form::text('oleic_acid', null,['class' => 'form-control', 'placeholder' => 'Oleic acid']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('optical_rotation', 'Optical rotation') }}
                                {{ Form::text('optical_rotation', null,['class' => 'form-control', 'placeholder' => 'Optical rotation']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('osi', 'OSI') }}
                                {{ Form::text('osi', null,['class' => 'form-control', 'placeholder' => 'OSI']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('other_carbohydrates', 'Other Carbohydrates') }}
                                {{ Form::text('other_carbohydrates', null,['class' => 'form-control', 'placeholder' => 'Other Carbohydrates']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('ppp', 'PPP') }}
                                {{ Form::text('ppp', null,['class' => 'form-control', 'placeholder' => 'PPP']) }}
                            </div>                            
                            <div class="form-group">
                                {{ Form::label('pph', 'PPH') }}
                                {{ Form::text('pph', null,['class' => 'form-control', 'placeholder' => 'PPH']) }}
                            </div>                            
                            <div class="form-group">
                                {{ Form::label('palmitic_acid', 'Palmitic acid') }}
                                {{ Form::text('palmitic_acid', null,['class' => 'form-control', 'placeholder' => 'Palmitic acid']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('palmitoliec_acid', 'Palmitoliec acid') }}
                                {{ Form::text('palmitoliec_acid', null,['class' => 'form-control', 'placeholder' => 'Palmitoliec acid']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('peroxide_value', 'Peroxide value') }}
                                {{ Form::text('peroxide_value', null,['class' => 'form-control', 'placeholder' => 'Peroxide value']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('ph', 'PH') }}
                                {{ Form::text('ph', null,['class' => 'form-control', 'placeholder' => 'PH']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('pounds_per_gallon', 'Pounds per gallon') }}
                                {{ Form::text('pounds_per_gallon', null,['class' => 'form-control', 'placeholder' => 'Pounds per gallon']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('pungency', 'Pungency') }}
                                {{ Form::text('pungency', null,['class' => 'form-control', 'placeholder' => 'Pungency']) }}
                            </div>                            
                            <div class="form-group">
                                {{ Form::label('pv', 'PV') }}
                                {{ Form::text('pv', null,['class' => 'form-control', 'placeholder' => 'PV']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('refactive_index', 'Refactive index') }}
                                {{ Form::text('refactive_index', null,['class' => 'form-control', 'placeholder' => 'Refactive index']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('refractive_index', 'Refractive index') }}
                                {{ Form::text('refractive_index', null,['class' => 'form-control', 'placeholder' => 'Refractive index']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('rifractometric_at_20', 'Rifractometric at 20') }}
                                {{ Form::text('rifractometric_at_20', null,['class' => 'form-control', 'placeholder' => 'Rifractometric at 20']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('salt', 'Salt') }}
                                {{ Form::text('salt', null,['class' => 'form-control', 'placeholder' => 'Salt']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('saponification', 'Saponification') }}
                                {{ Form::text('saponification', null,['class' => 'form-control', 'placeholder' => 'Saponification']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('saponification_value', 'Saponification value') }}
                                {{ Form::text('saponification_value', null,['class' => 'form-control', 'placeholder' => 'Saponification value']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('smoke_point', 'Smoke point') }}
                                {{ Form::text('smoke_point', null,['class' => 'form-control', 'placeholder' => 'Smoke point']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('so2', 'SO2') }}
                                {{ Form::text('so2', null,['class' => 'form-control', 'placeholder' => 'SO2']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('specific_gravity', 'Specific gravity') }}
                                {{ Form::text('specific_gravity', null,['class' => 'form-control', 'placeholder' => 'Specific gravity']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('specific_weight', 'Specific weight') }}
                                {{ Form::text('specific_weight', null,['class' => 'form-control', 'placeholder' => 'Specific weight']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('specific_weight_as_baumic_degrees', 'Specific weight as baumic degrees') }}
                                {{ Form::text('specific_weight_as_baumic_degrees', null,['class' => 'form-control', 'placeholder' => 'Specific weight as baumic degrees']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('starch', 'Starch') }}
                                {{ Form::text('starch', null,['class' => 'form-control', 'placeholder' => 'Starch']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('stearic_acid', 'Stearic acid') }}
                                {{ Form::text('stearic_acid', null,['class' => 'form-control', 'placeholder' => 'Stearic acid']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('stigmasterol', 'Stigmasterol') }}
                                {{ Form::text('stigmasterol', null,['class' => 'form-control', 'placeholder' => 'Stigmasterol']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('taste', 'Taste') }}
                                {{ Form::text('taste', null,['class' => 'form-control', 'placeholder' => 'Taste']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('tbhq', 'TBHQ') }}
                                {{ Form::text('tbhq', null,['class' => 'form-control', 'placeholder' => 'TBHQ']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('total_acidity_as_acetic_acid', 'Total acidity as acetic acid') }}
                                {{ Form::text('total_acidity_as_acetic_acid', null,['class' => 'form-control', 'placeholder' => 'Total acidity as acetic acid']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('total_dry_extract', 'Total dry extract') }}
                                {{ Form::text('total_dry_extract', null,['class' => 'form-control', 'placeholder' => 'Total dry extract']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('total_monounsaturated', 'Total monounsaturated') }}
                                {{ Form::text('total_monounsaturated', null,['class' => 'form-control', 'placeholder' => 'Total monounsaturated']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('total_polyunsaturated', 'Total polyunsaturated') }}
                                {{ Form::text('total_polyunsaturated', null,['class' => 'form-control', 'placeholder' => 'Total polyunsaturated']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('total_saturated', 'Total saturated') }}
                                {{ Form::text('total_saturated', null,['class' => 'form-control', 'placeholder' => 'Total saturated']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('total_so2', 'Total SO2') }}
                                {{ Form::text('total_so2', null,['class' => 'form-control', 'placeholder' => 'Total SO2']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('total_sterols', 'Total Sterols') }}
                                {{ Form::text('total_sterols', null,['class' => 'form-control', 'placeholder' => 'Total Sterols']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('total_sulphurous_anhydride', 'Total sulphurous anhydride') }}
                                {{ Form::text('total_sulphurous_anhydride', null,['class' => 'form-control', 'placeholder' => 'Total sulphurous anhydride']) }}
                            </div>


                            <div class="form-group">
                                {{ Form::label('turbidity', 'Turbidity') }}
                                {{ Form::text('turbidity', null,['class' => 'form-control', 'placeholder' => 'Turbidity']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('unsaponifiable_matter', 'Unsaponifiable matter') }}
                                {{ Form::text('unsaponifiable_matter', null,['class' => 'form-control', 'placeholder' => 'Unsaponifiable matter']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('vitamin_e', 'Vitamin e') }}
                                {{ Form::text('vitamin_e', null,['class' => 'form-control', 'placeholder' => 'Vitamin e']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('zinc', 'Zinc') }}
                                {{ Form::text('zinc', null,['class' => 'form-control', 'placeholder' => 'Zinc']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('difference_ecn42', 'Triglecerides Difference ECN42') }}
                                {{ Form::text('difference_ecn42', null,['class' => 'form-control', 'placeholder' => 'Triglecerides Difference ECN42']) }}
                            </div>                            
                            {{ Form::submit('Create',['class' => 'btn btn-default', 'style' => 'float:right']) }}

                        </fieldset>
                        {{ Form::close() }}

                    </div><!--end row-->
                </div>
            </div><!--end col lg 8 -->
        </div><!--end container fluid-->



    </div><!--end blends-->
@stop

@section('footer')
   <script>
          $(function() {
            $( "#datepicker" ).datepicker();
            $( "#datepicker1" ).datepicker();
        });
    </script>

    @stop