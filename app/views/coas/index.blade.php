@extends('layouts.admin')

@section('content')

<div id="coas">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                COA
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > COA
                </li>
            </ol>
        </div>
    </div>

    {{--sessions--}}
    @if(Session::has('message'))
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
    @endif

    {{--sub nav--}}
    <div class="row">
        <div class="col-lg-12">
            <nav class="navbar navbar-default">
            @if(Auth::user()->group != 20)
                {{ link_to_route('coas.create', 'Create A New COA',null, ['class' => 'btn btn-primary navbar-btn']) }}
            @endif                
                {{--search

                <form class="navbar-form navbar-right" role="search">
                    <div class="form-group">
                        <input type="text" class=" search form-control" placeholder="Search">
                    </div>
                    <button type="submit" class="btn btn-primary" data-sort="nutritionfact">Submit</button>
                </form>--}}
            </nav>
        </div>
    </div>

        <table id="datatable" class="table table-stripped">
        <thead>
        <tr>
           <td><strong>Lot</strong></td>
           <td><strong>Description</strong></td>
           <td><strong>Manf Date</strong></td>
           <td><strong>Expiration Date</strong></td>
           <td><strong>Origin</strong></td>
           <td><strong>PO Number</strong></td>
           <td><strong>Item Number</strong></td>
           <td class="no-sort"></td>
        </tr>
        </thead>
        <tbody class="list">
        @foreach($coas as $coa)
            <tr>
             <td class="isdone">{{ $coa->lot }}</td>
             <td>{{ $coa->description }}</td>
             <td class="coa">{{ $coa->manf_date }}</td>
             <td>{{ $coa->expiration_date }}</td>
             <td>{{ $coa->origin }}</td>
             <td>{{ $coa->po_number }}</td>
             <td>{{ $coa->item_number }}</td>
             <td>
                 <div class="btn-group" role="group" aria-label="">
                    <a href="/public/coas/{{ $coa->id }}" class="btn btn-technology">Public COA</a>
                     {{ link_to_route('coas.show', 'View', [$coa->id],['class' => 'btn btn-success']) }}
                     @if(Auth::user()->group == 100)
                     {{ link_to_route('coas.edit', 'Edit', [$coa->id],['class' => 'btn btn-warning']) }}

                     {{ Form::open(array('method' => 'DELETE', 'style' => 'display:inline','route' => array('coas.destroy', $coa->id))) }}
                     {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                     {{ Form::close() }}
                     @endif
                 </div>
             </td>
            </tr>
        @endforeach

        </tbody>
    </table>


<ul class="pagination"></ul>
</div>
    @stop

{{--end content--}}


{{--footer--}}
@section('footer')
   <script>
          $(function() {
            $( "#datepicker" ).datepicker();
            $( "#datepicker1" ).datepicker();
            $('#timepicker').timepicker();
            $('#datatable').DataTable({
                paging:true,
                "order": [[ 0, "desc" ]],
                // scrollY:400,
                deferRender:true,
                // scroller:true,
                columnDefs: [
                                { targets: 'no-sort', orderable: false }
                            ],

            });
        });
    </script>

    @stop

