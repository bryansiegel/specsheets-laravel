@extends('layouts.admin')

@section('content')

    <div id="blends">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">View</span>2012 Receiving Logs - Lot# {{ $receivinglog2012->lot }}
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/receivinglog2012s/">2012 Receiving Logs</a> >
                        View
                    </li>
                </ol>
            </div>
        </div>


        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <strong>Date:</strong> {{ $receivinglog2012->date }}
                            </li>
                            <li class="list-group-item">
                                <strong>Po:</strong> {{ $receivinglog2012->po }}
                            </li>
                            <li class="list-group-item">
                                <strong>Vendor:</strong> {{ $receivinglog2012->vendor }}
                            </li>
                            <li class="list-group-item">
                                <strong>Packslip:</strong> {{ $receivinglog2012->packslip }}
                            </li>
                            <li class="list-group-item">
                                <strong>QTY:</strong> {{ $receivinglog2012->qty }}
                            </li>
                            <li class="list-group-item">
                                <strong>Product Description:</strong> {{ $receivinglog2012->product_description }}
                            </li>
                            <li class="list-group-item">
                                <strong>Lot:</strong> {{ $receivinglog2012->lot }}
                            </li>
                            <li class="list-group-item">
                                <strong>Container:</strong> {{ $receivinglog2012->container }}
                            </li>
                            <li class="list-group-item">
                                <strong>Carrier:</strong> {{ $receivinglog2012->carrier }}
                            </li>
                            <li class="list-group-item">
                                <strong>Product On Hold:</strong> {{ $receivinglog2012->product_on_hold }}
                            </li>
                            <li class="list-group-item">
                                <strong>Product Contract:</strong> {{ $receivinglog2012->product_contract}}
                            </li>
                            <li class="list-group-item">
                                <strong>Condition Truck:</strong> {{ $receivinglog2012->condition_truck}}
                            </li>
                            <li class="list-group-item">
                                <strong>Kosher Cert File:</strong> {{ $receivinglog2012->kosher_cert_file}}
                            </li>
                            <li class="list-group-item">
                                <strong>Notes:</strong> {{ $receivinglog2012->notes}}
                            </li>
                        </ul>

                        <hr/>
                        <div class="alert alert-dismissible alert-success" style="margin-bottom:0px">
                            <strong>Created: </strong><?php echo \Carbon\Carbon::createFromTimestamp(strtotime($receivinglog2012->created_at))->diffForHumans(); ?>
                        </div>
                        <div class="alert alert-dismissible alert-warning">
                            <strong>Updated: </strong><?php echo \Carbon\Carbon::createFromTimestamp(strtotime($receivinglog2012->updated_at))->diffForHumans(); ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
@stop