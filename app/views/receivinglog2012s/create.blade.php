@extends('layouts.admin')

@section('content')
    <div id="blends">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Create</span>2012 Receiving Log
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/receivinglog2012s/">2012 Receiving Logs</a> >
                        Create
                    </li>
                </ol>
            </div>
        </div>

        {{--errors--}}
        @if($errors->has())
            <div class="alert alert-dismissible alert-danger">
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
        @endif

        {{--form--}}
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        {{ Form::open(['route' => 'receivinglog2012s.store', 'class' => 'form-horizontal']) }}

                        <fieldset>
                            <div class="form-group">
                                {{ Form::label('date', 'Date') }}
                                {{ Form::text('date', null,['class' => 'form-control', 'id'=> 'datepicker', 'placeholder' => 'Date']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('po', 'Po') }}
                                {{ Form::text('po', null,['class' => 'form-control', 'placeholder' => 'Po']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('vendor', 'Vendor') }}
                                {{ Form::text('vendor', null,['class' => 'form-control', 'placeholder' => 'Vendor']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('packslip', 'Packslip') }}
                                {{ Form::text('packslip', null,['class' => 'form-control', 'placeholder' => 'Packslip']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('qty', 'QTY') }}
                                {{ Form::text('qty', null,['class' => 'form-control', 'placeholder' => 'QTY']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('product_description', 'Product Description') }}
                                {{ Form::textarea('product_description', null,['cols' => '3', 'rows' => '3', 'class' => 'form-control', 'placeholder' => 'Product Description']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('lot', 'Lot') }}
                                {{ Form::text('lot', null,['class' => 'form-control', 'placeholder' => 'Lot']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('container', 'Container') }}
                                {{ Form::text('container', null,['class' => 'form-control', 'placeholder' => 'Container']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('carrier', 'Carrier') }}
                                {{ Form::text('carrier', null,['class' => 'form-control', 'placeholder' => 'Carrier']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('product_on_hold', 'Product On Hold') }}
                                {{ Form::text('product_on_hold', null,['class' => 'form-control',  'placeholder' => 'Product On Hold']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('product_contract', 'Product Contract') }}
                                {{ Form::text('product_contract', null,['class' => 'form-control',  'placeholder' => 'Product Contract']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('condition_truck', 'Condition Truck') }}
                                {{ Form::text('condition_truck', null,['class' => 'form-control',  'placeholder' => 'Condition Truck']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('kosher_cert_file', 'Kosher Cert File') }}
                                {{ Form::text('kosher_cert_file', null,['class' => 'form-control',  'placeholder' => 'Kosher Cert File']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('notes', 'Notes') }}
                                {{ Form::textarea('notes', null,['cols' => '3', 'rows' => '3', 'class' => 'form-control', 'placeholder' => 'Notes']) }}
                            </div>

                            {{ Form::submit('Create',['class' => 'btn btn-default', 'style' => 'float:right']) }}

                        </fieldset>
                        {{ Form::close() }}

                    </div><!--end row-->
                </div>
            </div><!--end col lg 8 -->
        </div><!--end container fluid-->



    </div><!--end blends-->
@stop