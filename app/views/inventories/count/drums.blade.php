@extends('layouts.admin')

@section('content')

    <div id="lotlogs">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Drums
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/inventories">Inventory</a> > <a href="/inventories/product/drums">Drums</a>
                    </li>
                </ol>
            </div>
        </div>

        {{--sessions--}}
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif

        {{--sub nav--}}
         <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">
                     {{ link_to('inventories/print/drums', 'Print', ['class' => 'btn btn-primary navbar-btn']) }}
                </nav>
            </div>
        </div>

        <!-- end view -->

        <table class="table table-stripped table-responsive table-hover">
            <thead>
            <tr>
                <td><strong>Size</strong></td>
                <td><strong>Description</strong></td>
                <td><strong>QTY</strong></td>
                <td><strong>Lot#</strong></td>
                <td><strong>Area</strong></td>
                <td></td>
            </tr>
            </thead>
            <tbody class="list">

            @foreach($drums as $drum)

                @if($drum->discontinued != 1)


                <tr>

                    <td class="lot">{{ $drum->size }}</td>
                    <td class="so">{{ $drum->oil_type }}</td>
                    {{ Form::model($drum, array('method' => 'PATCH','class' => 'ajax', 'route' => array('drumupdates.update', $drum->id))) }}
                    @forelse($drum->history as $historys)
                    @if($historys == $drum->history->last() && $historys->month == Carbon::now()->month && $historys->year == Carbon::now()->year)
                    {{ Form::hidden('historyid', $historys->id) }}
                    {{ Form::hidden('update', "true") }}

                  @elseif($historys == $drum->history->first() && $historys->month != Carbon::now()->month && $historys->year != Carbon::now()->year)
                    {{ Form::hidden('update', "false") }}
                    {{ Form::hidden('id', $drum->id) }}
                  @else
                    {{ Form::hidden('update', "false") }}
                    {{ Form::hidden('id', $drum->id) }}
                  @endif
                @empty
                  {{ Form::hidden('update', "false") }}
                  {{ Form::hidden('id', $drum->id) }}
                    @endforelse

                    {{ Form::hidden('expiration_date') }}
                    {{ Form::hidden('username',Auth::user()->username) }}
                    {{ Form::hidden('drums_cost_per')}}
                    {{ Form::hidden('size') }}
                    {{ Form::hidden('oil_type') }}
                    {{ Form::hidden('lot') }}
                    {{ Form::hidden('area') }}
                    <td>
                       <input class="form-control" name="count" type="number" step="any" value="<?php
foreach ($drum->history as $history):

    if ($history->year == Carbon::now()->year && $history->month == Carbon::now()->month) {
        echo $history->count;
    }
endforeach;
?>">
                    </td>

                    <td class="originallot">{{ $drum->lot }}</td>
                    <td>{{ $drum->area }}</td>



                    {{-- @foreach($drum->history as $history)

                      @if($history == $drum->history->last() && $history->month == Carbon::now()->month && $history->year == Carbon::now()->year)
                        <td>{{ Form::submit('Update', ['class' => 'btn btn-update']) }}</td>
                      @elseif($history->month != Carbon::now()->month && $history->year != Carbon::now()->year && $history->month == Carbon::now()->month && $history->year == Carbon::now()->year)
                        <td>{{ Form::submit('Add', ['class' => 'btn btn-danger']) }}</td>
                      @else
                        <td>{{ $history->month }}</td>
                      @endif
                    @endforeach --}}

                    <td>{{ Form::submit('Insert', ['class' => 'btn btn-danger']) }}</td>




                {{-- @forelse($drum->history as $history)
                    @if($history->month == Carbon::now()->month && $history->year == Carbon::now()->year )
                    <td>{{ Form::submit('Update', ['class' => 'btn btn-update']) }}</td>
                    @endif
                @empty
                    <td>{{ Form::submit('Add', ['class' => 'btn btn-danger']) }}</td>
                @endforelse --}}

                </tr>
                {{ Form::close() }}

                @endif

            @endforeach
            <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><strong>
            </strong></td>
            </tr>
            </tbody>
        </table>
        <ul class="pagination"></ul>
    </div>
@stop
{{--end content--}}


{{--footer--}}
@section('footer')
   
@stop
