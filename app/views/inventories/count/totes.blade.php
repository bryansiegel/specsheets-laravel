@extends('layouts.admin')

@section('content')

    <div id="lotlogs">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Totes
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/inventories">Inventory</a> > <a href="/inventories/product/totes">totes</a>
                    </li>
                </ol>
            </div>
        </div>

        {{--sessions--}}
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif

        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">
                    {{ link_to('inventories/print/totes', 'Print', ['class' => 'btn btn-primary navbar-btn']) }}
                </nav>
            </div>
        </div>

        <table class="table table-stripped table-responsive table-hover">
            <thead>
            <tr>
                <td><strong>Size</strong></td>
                <td><strong>Description</strong></td>
                <td><strong>QTY</strong></td>
                <td><strong>Lot#</strong></td>
                <td>
                  <strong>Area</strong>
                </td>
                <td></td>
            </tr>
            </thead>
            <tbody class="list">

            @foreach($totes as $tote)
                @if($tote->discontinued != 1)

                <tr>
                    <td class="lot">{{ $tote->size }}</td>
                    <td class="so">{{ $tote->oil_type }}</td>
                    {{ Form::model($tote, array('method' => 'PATCH', 'route' => array('toteupdates.update', $tote->id))) }}
                    @forelse($tote->history as $historys)
                    @if($historys == $tote->history->last() && $historys->month == Carbon::now()->month && $historys->year == Carbon::now()->year)
                    {{ Form::hidden('historyid', $historys->id) }}
                    {{ Form::hidden('update', "true") }}
                    @elseif($historys == $tote->history->first() && $historys->month != Carbon::now()->month && $historys->year != Carbon::now()->year)
                    {{ Form::hidden('update', "false") }}
                    {{ Form::hidden('id', $tote->id) }}
                    @else
                    {{ Form::hidden('update', "false") }}
                    {{ Form::hidden('id', $tote->id) }}
                    @endif
                    @empty
                    {{ Form::hidden('update', "false") }}
                    {{ Form::hidden('id', $tote->id) }}


                    @endforelse

                    {{ Form::hidden('username',Auth::user()->username) }}

                    {{ Form::hidden('expiration_date') }}

                    {{ Form::hidden('lot') }}
                    {{ Form::hidden('area') }}
                    {{ Form::hidden('size') }}
                    {{ Form::hidden('oil_type') }}
                    {{ Form::hidden('totes_cost_per') }}

                    <td>
                        <input class="form-control" name="count" type="number" step="any" value="<?php
                            foreach($tote->history as $history):

                               if($history->year == Carbon::now()->year && $history->month == Carbon::now()->month)
                               {
                                echo $history->count;
                               }
                                endforeach;
                        ?>">

                    </td>


                    <td class="originallot">{{ $tote->lot }}</td>
                    <td>
                      {{ $tote->area }}
                    </td>
              {{-- @forelse($tote->history as $history)
                    @if($history->month == Carbon::now()->month && $history->year == Carbon::now()->year)
                    <td>{{ Form::submit('Update', ['class' => 'btn btn-update']) }}</td>
                    @endif
                @empty
                    <td>{{ Form::submit('Add', ['class' => 'btn btn-danger']) }}</td>
                @endforelse --}}
                <td>{{ Form::submit('Insert', ['class' => 'btn btn-danger']) }}</td>



                </tr>
                {{ Form::close() }}
                @endif
            @endforeach
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td><strong>
                </strong></td>
            </tr>
            </tbody>
        </table>
        <ul class="pagination"></ul>
    </div>
@stop
{{--end content--}}


{{--footer--}}
@section('footer')
    <script type="text/javascript">
        var options = {
            valueNames: [ 'lot', 'so', 'originallot', 'oiltype', 'supplier', 'origin', 'qty', 'size', 'customer', 'date'],
            // pagination
            // page: 10,
            plugins: [
                ListPagination({})
            ]
        };

        var lotlogList = new List('lotlogs', options);
    </script>
@stop
