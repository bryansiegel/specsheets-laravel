@extends('layouts.pdf')

@section('content')


<div>
    <div style="text-align:center">
<img src="http://cibariaspecsheets.com/images/logo.jpg" width="200px" height="200px">
<br />
        <strong>705 Columbia Ave<br/>
            Riverside, CA 92507<br/>
            (P) (951) 823-8490<br/>
            (F) (951) 823-8495<br/>
            (E) <a href="mailto:info@cibaria-intl.com">info@cibaria-intl.com</a>
        </strong>
    </div>
    <br />
    <h1 style="text-align:center;">Supply Inventory</h1>
    <br />
    </div>


<table width="100%"  border="1" cellpadding="0" cellspacing="0">
            <thead>
            <tr style="border:1px solid black">
                <td><strong>Size</strong></td>
                <td><strong>Description</strong></td>
                <td><strong>QTY</strong></td>
                <td><strong>Lot#</strong></td>
                <td><strong>Area</strong></td>
                <td><strong>Cost Per</strong></td>
                <td><strong>Total Val</strong></td>
                <td><strong>EXP</strong></td>
                <td><strong>Total</strong></td>
            </tr>
            </thead>
            <tbody class="list">
            @foreach($supplies as $supply)
                @if($supply->discontinued != 1)
                <tr>
                    <td class="lot">{{ $supply->size }}</td>
                    <td class="so">{{ $supply->oil_type }}</td>
                   <td>
                    <?php
                    foreach($supply->history as $history){
                        if($history->month == Carbon::now()->month && $history->year == Carbon::now()->year){
                            echo  number_format((float)$history->count,3);
                        }else {
                            echo " ";
                        }
                    }

                    ?>
                    </td>
                    <td class="originallot">{{ $supply->lot }}</td>
                    <td>{{ $supply->area }}</td>
                    <td class="oiltype">{{ $supply->supplies_cost_per }}</td>

                    <td class="supplier">
                        <?php
                        //calculate total value
                        foreach($supply->history as $history) {
                    if($history->month == Carbon::now()->month && $history->year == Carbon::now()->year) {
                        $total_value = $history->count * $supply->supplies_cost_per;
                        echo number_format((float)$total_value,  3);
                    } else {
                        $total_value = "0";
                    }
                }
                        ?>



                    </td>
                    <td border="1">{{ $supply->expiration_date }}</td>
                    <td></td>
                </tr>
                @endif
            @endforeach
            <tr>
              <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><strong>{{ number_format($total->get_total_supplies(), 3) }}</strong></td>

            </tr>

            </tbody>
        </table>

@stop
