@extends('layouts.admin')

@section('content')

<table class="table  table-responsive table-hover">
                            <thead>
                            <tr>
                                <td><strong>Product</strong></td>
                                <td><strong>Product Totals</strong></td>
                                <td><strong>Product Counts</strong></td>

                                <td></td>
                            </tr>
                            </thead>
                            <tbody class="list">

                            <tr>
                                <td><strong>Drums</strong></td>
                                <td class="lot">{{ number_format($total->get_total_drums(), 3) }}</td>
                            </tr>
                            <tr>
                                <td><strong>Totes</strong></td>
                                <td>{{ number_format($total->get_total_totes(), 3) }}</td>
                            </tr>
                            <tr>
                                <td><strong>Organic</strong></td>
                                <td>{{ number_format($total->get_total_organics(), 3) }}</td>
                            </tr>
                            <tr>
                                <td><strong>Vinegar</strong></td>
                                <td>{{ number_format($total->get_total_vinegars(), 3) }}</td>
                            </tr>
                            <tr>
                                <td><strong>Tanks</strong></td>
                                <td>{{ number_format($total->get_total_tanks(), 3) }}</td>
                            </tr>
                            <tr>
                                <td><strong>Railcars</strong></td>
                                <td>{{ number_format($total->get_railcars_per_lb_price(), 3) }}</td>
                            </tr>
                            <tr>
                                <td><strong>Specialty</strong></td>
                                <td>{{ number_format($total->get_total_specialties(), 3) }}</td>
                            </tr>
                            <tr>
                                <td><strong>Infused</strong></td>
                                <td>{{ number_format($total->get_total_infused_oil_drums(), 3) }}</td>
                            </tr>
                            <tr>
                                <td><strong>Website</strong></td>
                                <td>{{ number_format($total->get_website_totals(), 3) }}</td>
                            </tr>
                            <tr>
                                <td><strong>Labels</strong></td>
                                <td>{{ number_format($total->get_label_totals(), 3) }}</td>
                            </tr>
                            <tr>
                                <td><strong>Bottles</strong></td>
                                <td>{{ number_format($total->get_total_bottles(), 3) }}</td>
                            </tr>
                            <tr>
                                <td><strong>Caps</strong></td>
                                <td>{{ number_format($total->get_total_caps(), 3) }}</td>
                            </tr>
                            <tr>
                                <td><strong>Cardboard Blank</strong></td>
                                <td>{{ number_format($total->get_total_cardboard_blank(), 3) }}</td>
                            </tr>
                            <tr>
                                <td><strong>Cardboard Printed</strong></td>
                                <td>{{ number_format($total->get_total_cardboard_printed(), 3) }}</td>
                            </tr>
                            <tr>
                                <td><strong>Ready To Ship</strong></td>
                                <td>{{ number_format($total->get_total_ready_to_ship(), 3) }}</td>
                            </tr>
                            <tr>
                                <td><strong>Finished Product</strong></td>
                                <td>{{ number_format($total->get_total_finished_product(), 3) }}</td>
                            </tr>
                            <tr>
                                <td><strong>Spray</strong></td>
                                <td>{{ number_format($total->get_total_sprays(), 3) }}</td>
                            </tr>
                            <tr>
                                <td><strong>Rework</strong></td>
                                <td>{{ number_format($total->get_total_rework(), 3) }}</td>
                            </tr>
                            <tr>
                                <td><strong>Etched Bottles</strong></td>
                                <td>{{ number_format($total->get_total_etched_bottles(), 3) }}</td>
                            </tr>
                            <tr>
                                <td><strong>Flavors</strong></td>
                                <td>{{ number_format($total->get_flavor_totals(), 3) }}</td>
                            </tr>
                            <tr>
                                <td><strong>Materials</strong></td>
                                <td>{{ number_format($total->get_total_materials(), 3) }}</td>
                            </tr>
                            <tr>
                                <td><strong>Follmer</strong></td>
                                <td>{{ number_format($total->get_total_follmer(), 3) }}</td>
                            </tr>
                            <tr>
                                <td><strong>Pantry Items</strong></td>
                                <td>{{ number_format($total->get_total_spreads_and_olives(), 3) }}</td>
                            </tr>
                            <tr>
                                <td><strong>Fustis</strong></td>
                                <td>{{ number_format($total->get_total_fustis(), 3) }}</td>
                            </tr>
                            <hr />

                            </tbody>
                        </table>
    @stop