@extends('layouts.admin')

@section('content')
    <div id="receivingappointmentlogs">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Edit</span> railcar
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/inventories/product/railcars/">railcars</a> >
                        Edit
                    </li>
                </ol>
            </div>
        </div>

        {{--errors--}}
        @if($errors->has())
            <div class="alert alert-dismissible alert-danger">
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
        @endif

        {{--form--}}
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        {{ Form::model($railcar, array('method' => 'PATCH', 'route' =>
                                                         array('railcarupdates.update', $railcar->id))) }}
                          {{ Form::hidden('update', 'true') }}                               
                          {{ Form::hidden('redirect_to_product', 1)}}    
                          @foreach($railcar->history as $history)
                          @if($history->month == Carbon::now()->month && $history->year == Carbon::now()->year)
                            {{ Form::hidden('count', $history->count) }}
                            {{ Form::hidden('drums_cost_per', $railcar->railcars_per_lb_price) }}
                          @endif
                          @endforeach                         
                        <fieldset>
                            <div class="form-group">
                                {{ Form::label('size', 'Car Received') }}
                                {{ Form::text('size',null,['class' => 'form-control', 'placeholder' => 'Car Received']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('oil_type', 'Inventory Date') }}
                                {{ Form::text('oil_type',null, ['class' => 'form-control', 'placeholder' => 'Inventory Date']) }}
                            </div>


                            <div class="form-group">
                                {{ Form::label('loads_taken', 'Loads Taken') }}
                                {{ Form::text('loads_taken', null, ['class' => 'form-control', 'placeholder' => 'Loads Taken']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('lot', 'Lot#') }}
                                {{ Form::text('lot', null, ['class' => 'form-control', 'placeholder' => 'Lot#']) }}
                            </div>

                            {{ Form::submit('Update',['class' => 'btn btn-default', 'style' => 'float:right']) }}
                            	<a href="/inventories/product/railcars" class="btn btn-primary" style="float:right">Cancel</a>
                            {{-- {{ link_to_route('inventories.product.railcars', 'Cancel',null,['class' => 'btn btn-primary', 'style' => 'float:right;']) }} --}}
                        </fieldset>
                        {{ Form::close() }}

                    </div><!--end row-->
                </div>
            </div><!--end col lg 8 -->
        </div><!--end container fluid-->



    </div><!--end blends-->
@stop