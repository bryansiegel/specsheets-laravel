@extends('layouts.admin')

@section('content')
    <div id="receivingappointmentlogs">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Edit</span> tote
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/inventories/product/totes/">Totes</a> >
                        Edit
                    </li>
                </ol>
            </div>
        </div>

        {{--errors--}}
        @if($errors->has())
            <div class="alert alert-dismissible alert-danger">
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
        @endif

        {{--form--}}
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        {{ Form::model($tote, array('method' => 'PATCH', 'route' =>
                                                         array('toteupdates.update', $tote->id))) }}
                          {{ Form::hidden('update', 'true') }}                               
                          {{ Form::hidden('redirect_to_product', 1)}}                 @foreach($tote->history as $history)
                          @if($history->month == Carbon::now()->month && $history->year == Carbon::now()->year)
                            {{ Form::hidden('count', $history->count) }}
                            {{ Form::hidden('drums_cost_per', $tote->totes_cost_per) }}
                          @endif
                          @endforeach                        
                        <fieldset>
                        <div class="form-group">
                            {{ Form::label('sub_product_category', 'Sub Category') }}
                            {{ Form::select('sub_product_category', [
                                '' => 'Choose',
                                'canola_rbd' => 'Canola, RBD',
                                'canola_non_gmo' => 'Canola, NON-GMO',
                                'soy_rbd' => 'Soy, RBD',
                                'soy_non_gmo' => 'Soy, NON-GMO',
                                'soy_winterized' => 'Soy, Winterized',
                                'per_90_can_10_evoo' => '90% CAN/10% EVOO',
                                'per_75_can_25_evoo' => '75% CAN/25% EVOO',
                                'per_90_soy_10_evoo' => '90% SOY/10% EVOO',
                                'evoo_italian_coratina' => 'EVOO, Italian, Coratina',
                                'evoo_italian_umbrian' => 'EVOO, Italian, Umbrian',
                                'evoo_spanish_arbequina' => 'EVOO, Spanish Arbequina',
                                'evoo_spanish_hojiblanca' => 'EVOO, Spanish, Hojiblanca',
                                'evoo_chilean_frantoio' => 'EVOO, Chilean, Frantoio',
                                'pomace_olive_oil' => 'Pomace Olive Oil',
                                'pure_olive_oil' => 'Pure Olive Oil',
                                 ],$tote->sub_product_category, ['class' => 'form-control', 'id' => 'sub_product_category']) }}
    
                            </div>
                            <div class="form-group">
                                {{ Form::label('size', 'Size') }}
                                {{ Form::text('size',null,['class' => 'form-control', 'placeholder' => 'Size']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('oil_type', 'Description') }}
                                {{ Form::text('oil_type',null, ['class' => 'form-control', 'placeholder' => 'Description']) }}
                            </div>


                            <div class="form-group">
                                {{ Form::label('lot', 'Lot#') }}
                                {{ Form::text('lot', null, ['class' => 'form-control', 'placeholder' => 'Lot#']) }}
                            </div>


                            {{ Form::submit('Update',['class' => 'btn btn-default', 'style' => 'float:right']) }}
                            	<a href="/inventories/product/totes" class="btn btn-primary" style="float:right">Cancel</a>
                            {{-- {{ link_to_route('inventories.product.totes', 'Cancel',null,['class' => 'btn btn-primary', 'style' => 'float:right;']) }} --}}
                        </fieldset>
                        {{ Form::close() }}

                    </div><!--end row-->
                </div>
            </div><!--end col lg 8 -->
        </div><!--end container fluid-->



    </div><!--end blends-->
@stop