@extends('layouts.admin')

@section('content')
    <div id="receivingappointmentlogs">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Edit</span> tank
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/inventories/product/tanks/">tanks</a> >
                        Edit
                    </li>
                </ol>
            </div>
        </div>

        {{--errors--}}
        @if($errors->has())
            <div class="alert alert-dismissible alert-danger">
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
        @endif

        {{--form--}}
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        {{ Form::model($tank, array('method' => 'PATCH', 'route' =>
                                                         array('tankupdates.update', $tank->id))) }}
                          {{ Form::hidden('update', 'true') }}                               
                          {{ Form::hidden('id', $tank->id )}}                              
                        <fieldset>
                            <div class="form-group">
                                {{ Form::label('gal_inch', 'GAL/Inch') }}
                                {{ Form::text('gal_inch',null,['class' => 'form-control', 'placeholder' => 'GAL/Inch']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('tank_total', 'Total') }}
                                {{ Form::text('tank_total',null,['class' => 'form-control', 'placeholder' => 'GAL/Inch']) }}
                            </div>


                            <div class="form-group">
                                {{ Form::label('size', 'Size') }}
                                {{ Form::text('size',null,['class' => 'form-control', 'placeholder' => 'Size']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('oil_type', 'Description') }}
                                {{ Form::text('oil_type',null, ['class' => 'form-control', 'placeholder' => 'Description']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('lot1', 'Lot 1') }}
                                {{ Form::text('lot1',null, ['class' => 'form-control', 'placeholder' => 'Lot 1']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('lot2', 'Lot 2') }}
                                {{ Form::text('lot2',null, ['class' => 'form-control', 'placeholder' => 'Lot 2']) }}
                            </div>


                            {{ Form::submit('Update',['class' => 'btn btn-default', 'style' => 'float:right']) }}
                            	<a href="/inventories/product/tanks" class="btn btn-primary" style="float:right">Cancel</a>
                            {{-- {{ link_to_route('inventories.product.tanks', 'Cancel',null,['class' => 'btn btn-primary', 'style' => 'float:right;']) }} --}}
                        </fieldset>
                        {{ Form::close() }}

                    </div><!--end row-->
                </div>
            </div><!--end col lg 8 -->
        </div><!--end container fluid-->



    </div><!--end blends-->
@stop