@extends('layouts.print')


@section('content')
    <div>
        <a href="javascript:window.print()">
            {{ HTML::image('images/print_icon.jpg', 'Print',['width' => '100', 'height' => '100', 'class' => 'noPrint', 'style' => 'border:none']) }}</a>
    </div>
    <h2>Ready To Ship Count</h2>
    <small>Date:<?php echo Date('m/d/y'); ?></small><span style="float:right;">Count By:______________________</span>
    <table border="1" cellpadding="0" cellspacing="0" width="100%">
        <thead>
        <tr>
            <td><strong>Size</strong></td>
            <td><strong>Description</strong></td>
            <td><strong>QTY</strong></td>
            <td><strong>Lot#</strong></td>
        </tr>
        </thead>
        <tbody>

        @foreach($ready_to_ships as $ready_to_ship)
            @if($ready_to_ship->discontinued != 1)
                <tr>
                    <td>{{ $ready_to_ship->size }}</td>
                    <td>{{ $ready_to_ship->oil_type }}</td>
                    <td></td>
                    <td>{{ $ready_to_ship->lot }}</td>
                </tr>
            @endif
        @endforeach

        </tbody>
    </table>
    <h2>Not In Inventory</h2>
    <table border="1" cellpadding="0" cellspacing="0" width="100%">
        <thead>
        <tr>
            <td><strong>Size</strong></td>
            <td><strong>Description</strong></td>
            <td><strong>QTY</strong></td>
            <td><strong>Lot#</strong></td>
        </tr>
        </thead>
        <tbody>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
 <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                 <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                 <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                 <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                 <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                 <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                 <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                 <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                 <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                 <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                 <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                 <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                 <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                 <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
        </tbody>
    </table>
@stop