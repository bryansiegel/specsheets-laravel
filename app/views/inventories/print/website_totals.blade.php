@extends('layouts.print')


@section('content')
    <div>
        <a href="javascript:window.print()">
            {{ HTML::image('images/print_icon.jpg', 'Print',['width' => '100', 'height' => '100', 'class' => 'noPrint', 'style' => 'border:none']) }}</a>
    </div>
    <table cellspacing=0 cellpadding=0>
        <thead>
        <tr><th colspan=14><h2>CIBARIA INTERNATIONAL, INC<br />Website Totals</h2></th></tr>
        <tr>
            <th style="border-left: 3px solid black; border-top: 3px solid black;border-right: 1px solid black; border-bottom:3px solid black;">Olive Oils</th>
            <th style="border-top: 3px solid black; border-right:1px solid black; border-bottom: 3px solid black;">Infused</th>
            <th style="border-top: 3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;">Vinegars</th>
            <th style="border-top:3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;">Other</th>
            <th style="border-top: 3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;">Total</th>
        </tr>
        </thead>
        <tbody style="border-bottom: 3px solid black;">
            <tr>
                <td style="border-left: 3px solid black; border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $total->get_total_website_olive_oils(); ?></td>
                <td style="border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $total->get_total_web_flavored_and_infused(); ?></td>
                <td style="border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $total->get_total_website_vinegars(); ?></td>
                <td style="border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $total->get_total_website_other(); ?></td>
                <td style="border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $total->get_website_totals(); ?></td>
            </tr>
        </tbody>
    </table>

@stop