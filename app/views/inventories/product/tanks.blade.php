@extends('layouts.admin')

@section('content')

    <div id="lotlogs">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Tanks
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/inventories">Inventory</a> >Tanks
                    </li>
                </ol>
            </div>
        </div>

        {{--sessions--}}
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif

        {{--sub nav--}}
        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">

                    {{ link_to('inventories/count/tanks', 'New Tank Count', ['class' => 'btn btn-primary navbar-btn']) }}
                    {{ link_to('inventories/create', 'Create A Product', ['class' => 'btn btn-danger navbar-btn']) }}
                    @if(Auth::user()->group == 100)
                    {{ link_to('inventories/cost/tanks', 'Edit Cost Per', ['class' => 'btn btn-success navbar-btn'])}}

                    {{ link_to('inventories/discontinued/tanks', 'Discontinued', ['class' => 'btn btn-danger navbar-btn'])}}
                    {{ link_to('inventories/pdf/tanks', 'Print PDF', ['class' => 'btn btn-primary navbar-btn']) }}
                    @endif

                    {{--search--}}

                    <form class="navbar-form navbar-right" role="search">
                        <div class="form-group">
                            <input type="text" class=" search form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-primary" data-sort="nutritionfact">Submit</button>
                    </form>
                </nav>
            </div>
        </div>

<!--         <pre>

            <?php echo number_format($total->get_total_tanks(),3); ?>
        </pre> -->

        <table class="table table-stripped table-responsive table-hover">
            <thead>
            <tr>
                <td><strong>GAL/Inch</strong></td>
                <td><strong>Total</strong></td>
                <td><strong>Size</strong></td>
                <td><strong>Description</strong></td>
                <td><strong>Lot 1</strong></td>
                <td><strong>Lot 2</strong></td>
                <td><strong>QTY</strong></td>
                <td><strong>Extension</strong></td>
                <td><strong>Cost</strong></td>
                <td><strong>Total</strong></td>
                <td><strong>EXP</strong></td>
                <td></td>
                <td></td>
            </tr>
            </thead>
            <tbody class="list">
            @foreach($tanks as $tank)
                @if($tank->discontinued != 1)

                <tr>
                    <td>{{ $tank->gal_inch }}</td>
                    <td>{{ $tank->tank_total }}</td>
                    <td class="lot">{{ $tank->size }}</td>
                    <td class="so">{{ $tank->oil_type }}</td>
                    <td>{{ $tank->lot1 }}</td>
                    <td>{{ $tank->lot2 }}</td>
                    <td>
                    @foreach($tank->history as $history)
                    @if($history->month == Carbon::now()->month && $history->year == Carbon::now()->year)
                    {{ $history->count }}</td>
                    @endif
                    @endforeach
                    </td>



                    <td>

                    <?php
                    foreach($tank->history as $history) {
                        if($history->month == Carbon::now()->month && $history->year == Carbon::now()->year) {

                   if($tank->gal_inch == 'GAL'){
                    foreach ($tank->history as $history) {
                        echo $history->count;
                    }
                   } elseif($tank->gal_inch == NULL){
                    foreach ($tank->history as $history) {
                    echo $history->count;
                }
                   } else {
                    foreach ($tank->history as $history) {
                        $extension = $tank->gal_inch * $history->count;
                        echo $extension;
                    }
                   }

}
               }

                    ?>


                        </td>


                    <td class="oiltype">{{ number_format((float)$tank->tanks_cost_per, 3) }}</td>
                    <td class="supplier">
                        <?php
                    foreach($tank->history as $history) {
                        if($history->month == Carbon::now()->month && $history->year == Carbon::now()->year) {

                       if($tank->gal_inch == 'GAL'){
                    foreach ($tank->history as $history) {
                        $extension = $history->count;
                        $total_tanks = $extension * $tank->tanks_cost_per;
                        echo number_format($total_tanks,3);
                    }
                   } elseif($tank->gal_inch == NULL){
                    foreach ($tank->history as $history) {
                    $extension = $history->count;
                    $total_tanks = $extension * $tank->tanks_cost_per;
                    echo number_format($total_tanks,3);
                }
                   } else {
                    foreach ($tank->history as $history) {
                        $extension = $tank->gal_inch * $history->count;
                        $total_tanks = $extension * $tank->tanks_cost_per;
                        echo number_format($total_tanks,3);
                    }
                   }


}}
                                ?>

                    </td>
                    <td class="origin">{{ $tank->expiration_date }}</td>

                    <td>
                        <div class="btn-group" role="group" aria-label="">
                        <a href="/inventories/edit/tanks/<?php echo $tank->id ?>" class="btn btn-warning">Edit</a>
                   @if(Auth::user()->group == 100 || Auth::user()->group == 50)
                       {{ Form::model($tank, array('method' => 'PATCH', 'route' => array('tankupdates.update', $tank->id))) }}
                                {{ Form::hidden('discontinued',1) }}
                                {{ Form::hidden('id', $tank->id) }}
                                {{ Form::hidden('count', "0") }}
                                {{ Form::hidden('gal_inch') }}
                                {{ Form::hidden('lot1') }}
                                {{ Form::hidden('tank_total') }}
                                {{ Form::hidden('lot2') }}
                                {{ Form::hidden('size') }}
                                {{ Form::hidden('oil_type') }}
                                {{ Form::hidden('qty', "0") }}
                                {{ Form::hidden('lot') }}
                                {{ Form::hidden('tanks_cost_per', "0") }}
                                {{ Form::hidden('extension', "0") }}
                                {{ Form::hidden('expiration_date') }}

                                {{ Form::submit('Discontinue', ['class' => 'btn btn-danger']) }}
                        {{ Form::close() }}
                        @endif
                        </div>


                </tr>
                @endif
            @endforeach
            <tr>
                <td><h3>Total</h3></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td><strong>
                        <?php
                            // var_dump($total);
                       echo number_format($total->get_total_tanks(),3);
                        // echo number_format($total->calculate_tanks_total(), 3);
                        // echo $total->calculate_tanks_total();
                        ?>

                    </strong></td>
            </tr>

            </tbody>
        </table>


        <ul class="pagination"></ul>
    </div>
@stop

{{--end content--}}


{{--footer--}}
@section('footer')
    <script type="text/javascript">
        var options = {
            valueNames: [ 'lot', 'so', 'originallot', 'oiltype', 'supplier', 'origin', 'qty', 'size', 'customer', 'date'],
            // pagination
            // page: 10,
            plugins: [
                ListPagination({})
            ]
        };

        var lotlogList = new List('lotlogs', options);
    </script>
@stop
