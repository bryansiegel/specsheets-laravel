                <nav class="navbar navbar-default">

                @if($category == 'totes')
                    {{ link_to('inventories/count/totes', 'New Tote Count', ['class' => 'btn btn-primary navbar-btn']) }}
                    {{ link_to('inventories/create', 'Create A Product', ['class' => 'btn btn-danger navbar-btn']) }}
                    @if(Auth::user()->group == 100)
                    {{ link_to('inventories/cost/totes', 'Edit Cost Per', ['class' => 'btn btn-success navbar-btn'])}}
                    {{ link_to('inventories/discontinued/totes', 'Discontinued', ['class' => 'btn btn-danger navbar-btn'])}}
                    {{ link_to('inventories/pdf/totes', 'Print PDF', ['class' => 'btn btn-primary navbar-btn']) }}
                    @endif
                @elseif($category == 'specialty')
                     {{ link_to('inventories/count/specialties', 'New specialty Count', ['class' => 'btn btn-primary navbar-btn']) }}
                    {{ link_to('inventories/create', 'Create A Product', ['class' => 'btn btn-danger navbar-btn']) }}
                    @if(Auth::user()->group == 100)
                    {{ link_to('inventories/cost/specialties', 'Edit Cost Per', ['class' => 'btn btn-success navbar-btn'])}}
                    {{ link_to('inventories/discontinued/specialty', 'Discontinued', ['class' => 'btn btn-danger navbar-btn'])}}
                    {{ link_to('inventories/pdf/specialty', 'Print PDF', ['class' => 'btn btn-primary navbar-btn']) }}
                    @endif
                @elseif($category == 'vinegars')
                    {{ link_to('inventories/count/vinegars', 'New Vinegar Count', ['class' => 'btn btn-primary navbar-btn']) }}
                    {{ link_to('inventories/create', 'Create A Product', ['class' => 'btn btn-danger navbar-btn']) }}
                    @if(Auth::user()->group == 100)
                    {{ link_to('inventories/cost/vinegars', 'Edit Cost Per', ['class' => 'btn btn-success navbar-btn'])}}
                    {{ link_to('inventories/discontinued/vinegars', 'Discontinued', ['class' => 'btn btn-danger navbar-btn'])}}
                    {{ link_to('inventories/pdf/vinegars', 'Print PDF', ['class' => 'btn btn-primary navbar-btn']) }}
                    @endif
                @endif

                    {{--search--}}

                    <form class="navbar-form navbar-right" role="search">
                    {{-- sub cateogories --}}
                    <div class="form-group">

                    @if($category == 'totes')
                        @if(Request::url() != url() . '/inventories/product/totes')
                    {{ link_to('/inventories/product/totes', '<< Back To Totes', ['class' => 'btn btn-danger navbar-btn']) }}
                        @endif
                    <select id="selectbox" name="" onchange="javascript:location.href = this.value;" class="form-control">
                            <option value="" selected>Sub Category</option>
                            <option value="/inventories/product/totes/canola_oil" style="font-weight:bold">Canola Oil</option>
                            <option value="/inventories/product/totes/canola_oil/canola_rbd">&nbsp;&nbsp;&nbsp;&nbsp;Canola, RBD</option>
                            <option value="/inventories/product/totes/canola_oil/canola_non_gmo">&nbsp;&nbsp;&nbsp;&nbsp;Canola, NON-GMO</option>
                            <option value="/inventories/product/totes/soybean_oil" style="font-weight:bold;">Soybean Oil</option>
                            <option value="/inventories/product/totes/soybean_oil/soy_rbd">&nbsp;&nbsp;&nbsp;&nbsp;Soy, RBD</option>
                            <option value="/inventories/product/totes/soybean_oil/soy_non_gmo">&nbsp;&nbsp;&nbsp;&nbsp;Soy, NON-GMO</option>
                            <option value="/inventories/product/totes/soybean_oil/soy_winterized">&nbsp;&nbsp;&nbsp;&nbsp;Soy, Winterized</option>
                            <option value="/inventories/product/totes/blended_oils" style="font-weight:bold;">Blended Oils</option>
                            <option value="/inventories/product/totes/blended_oils/per_90_can_10_evoo">&nbsp;&nbsp;&nbsp;&nbsp;90% CAN/10% EVOO</option>
                            <option value="/inventories/product/totes/blended_oils/per_75_can_25_evoo">&nbsp;&nbsp;&nbsp;&nbsp;75% CAN/25% EVOO</option>
                            <option value="/inventories/product/totes/blended_oils/per_90_soy_10_evoo">&nbsp;&nbsp;&nbsp;&nbsp;90% SOY/10% EVOO</option>
                            <option value="/inventories/product/totes/olive_oil" style="font-weight:bold;">Olive Oil</option>
                            <option value="/inventories/product/totes/olive_oil/extra_virgin_olive_oil" style="font-weight:bold;">&nbsp;&nbsp;Extra Virgin Olive Oil</option>
                            <option value="/inventories/product/totes/olive_oil/extra_virgin_olive_oil/italian" style="font-weight:bold;">&nbsp;&nbsp;&nbsp;Italian</option>
                            <option value="/inventories/product/totes/olive_oil/extra_virgin_olive_oil/italian/evoo_italian_coratina">&nbsp;&nbsp;&nbsp;&nbsp;EVOO, Italian, Coratina</option>
                            <option value="/inventories/product/totes/olive_oil/extra_virgin_olive_oil/italian/evoo_italian_umbrian">&nbsp;&nbsp;&nbsp;&nbsp;EVOO, Italian, Umbrian</option>
                            <option value="/inventories/product/totes/olive_oil/extra_virgin_olive_oil/spanish" style="font-weight:bold;">&nbsp;&nbsp;&nbsp;Spanish</option>
                            <option value="/inventories/product/totes/olive_oil/extra_virgin_olive_oil/spanish/evoo_spanish_arbequina">&nbsp;&nbsp;&nbsp;&nbsp;EVOO, Spanish, Arbequina</option>
                            <option value="/inventories/product/totes/olive_oil/extra_virgin_olive_oil/spanish/evoo_spanish_hojiblanca">&nbsp;&nbsp;&nbsp;&nbsp;EVOO, Spanish, Hojiblanca</option>
                            <option value="/inventories/product/totes/olive_oil/extra_virgin_olive_oil/chilean" style="font-weight:bold;">&nbsp;&nbsp;&nbsp;Chilean</option>
                            <option value="/inventories/product/totes/olive_oil/extra_virgin_olive_oil/chilean/evoo_chilean_frantoio">&nbsp;&nbsp;&nbsp;&nbsp;EVOO, Chilean, Frantoio</option>
                            <option value="/inventories/product/totes/olive_oil/pomace_olive_oil" style="font-weight:bold;">&nbsp;&nbsp;Pomace Olive Oil</option>
                            <option value="/inventories/product/totes/olive_oil/pure_olive_oil" style="font-weight:bold;">&nbsp;&nbsp;Pure Olive Oil</option>
                      </select> 
                       @elseif($category == 'specialty')
                        @if(Request::url() != url() . '/inventories/product/specialty')
                            {{ link_to('/inventories/product/specialty', '<< Back To Specialty', ['class' => 'btn btn-danger navbar-btn']) }}
                        @endif
                            <select id="selectbox" name="" onchange="javascript:location.href = this.value;" class="form-control">
                                <option value="" selected>Sub Category</option>
                                <option value="/inventories/product/specialties/truffle_oil" style="font-weight:bold">Truffle Oil</option>
                                <option value="/inventories/product/specialties/truffle_oil_black">&nbsp;&nbsp;&nbsp;&nbsp;Truffle Oil, Black</option>
                                <option value="/inventories/product/specialties/truffle_oil_white">&nbsp;&nbsp;&nbsp;&nbsp;Truffle Oil, White</option>
                                <option value="/inventories/product/specialties/cocoa_butter" style="font-weight:bold">Cocoa Butter</option>
                                <option value="/inventories/product/specialties/cocoa_butter_deodorized">&nbsp;&nbsp;&nbsp;&nbsp;Cocoa Butter, Deodorized</option>
                                <option value="/inventories/product/specialties/cocoa_butter_natural">&nbsp;&nbsp;&nbsp;&nbsp;Cocoa Butter, Natural</option>
                                <option value="/inventories/product/specialties/sesame_oil" style="font-weight:bold">Sesame Oil</option>
                                <option value="/inventories/product/specialties/sesame_clear">&nbsp;&nbsp;&nbsp;&nbsp;Sesame, Clear</option>
                                <option value="/inventories/product/specialties/sesame_toasted">&nbsp;&nbsp;&nbsp;&nbsp;Sesame, Toasted</option>

                            </select>
                            @elseif($category == 'vinegars')
                        @if(Request::url() != url() . '/inventories/product/vinegars')
                            {{ link_to('/inventories/product/vinegars', '<< Back To Vinegars', ['class' => 'btn btn-danger navbar-btn']) }}
                        @endif
                            <select id="selectbox" name="" onchange="javascript:location.href = this.value;" class="form-control">
                                <option value="" selected>Sub Category</option>
                                <option value="/inventories/product/vinegars/balsamic_vinegar" style="font-weight:bold;">Balsamic Vinegar</option>
                                <option value="/inventories/product/vinegars/4_star_dark">&nbsp;&nbsp;&nbsp;&nbsp;4 Star Dark</option>
                                <option value="/inventories/product/vinegars/4_star_white">&nbsp;&nbsp;&nbsp;&nbsp;4 Star White</option>
                                <option value="/inventories/product/vinegars/8_star_dark">&nbsp;&nbsp;&nbsp;&nbsp;8 Star Dark</option>
                                <option value="/inventories/product/vinegars/25_star" style="font-weight:bold;">25 Star</option>
                                <option value="/inventories/product/vinegars/25_star_dark">&nbsp;&nbsp;&nbsp;&nbsp;25 Star Dark</option>
                                <option value="/inventories/product/vinegars/25_star_white">&nbsp;&nbsp;&nbsp;&nbsp;25 Star White</option>
                                <option value="/inventories/product/vinegars/honey_vinegar_serrano">Honey Vinegar, Serrano</option>
                                <option value="/inventories/product/vinegars/lambrusco">Lambrusco</option>
                                <option value="/inventories/product/vinegars/wine_vinegar" style="font-weight:bold;">Wine Vinegar</option>
                                <option value="/inventories/product/vinegars/wine_vinegar_white">&nbsp;&nbsp;&nbsp;&nbsp;Wine Vinegar, White</option>
                                <option value="/inventories/product/vinegars/wine_vinegar_red">&nbsp;&nbsp;&nbsp;&nbsp;Wine Vinegar, Red</option>
                                <option value="/inventories/product/vinegars/rice_vinegar">Rice Vinegar</option>
                            </select>
                       @endif
                        
                    </div>
                        <div class="form-group">
                            <input type="text" class=" search form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-primary" data-sort="nutritionfact">Submit</button>
                    </form>
                </nav>