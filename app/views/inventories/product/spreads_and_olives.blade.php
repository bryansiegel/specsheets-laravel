@extends('layouts.admin')

@section('content')

    <div id="lotlogs">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Spreads and Olives
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/inventories">Inventory</a> >Spreads and Olives
                    </li>
                </ol>
            </div>
        </div>

        {{--sessions--}}
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif

        {{--sub nav--}}
        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">

                    {{ link_to('inventories/count/spreads_and_olives', 'New Spreads and Olive Count', ['class' => 'btn btn-primary navbar-btn']) }}
                    {{ link_to('inventories/create', 'Create A Product', ['class' => 'btn btn-danger navbar-btn']) }}
                    @if(Auth::user()->group == 100)
                    {{ link_to('inventories/cost/spreads_and_olives', 'Edit Cost Per', ['class' => 'btn btn-success navbar-btn'])}}
                    {{ link_to('inventories/discontinued/spreads_and_olives', 'Discontinued', ['class' => 'btn btn-danger navbar-btn'])}}
                    {{ link_to('inventories/pdf/spreads_and_olives', 'Print PDF', ['class' => 'btn btn-primary navbar-btn']) }}
                    @endif

                    {{--search--}}

                    <form class="navbar-form navbar-right" role="search">
                        <div class="form-group">
                            <input type="text" class=" search form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-primary" data-sort="nutritionfact">Submit</button>
                    </form>
                </nav>
            </div>
        </div>

        <table class="table table-stripped table-responsive table-hover">
            <thead>
            <tr>
                <td><strong>Size</strong></td>
                <td><strong>Description</strong></td>
                <td><strong>QTY</strong></td>
                <td><strong>Lot#</strong></td>
                <td><strong>Area</strong></td>
                <td><strong>Cases</strong></td>
                <td><strong>Jars</strong></td>
                <td><strong>Cost Per</strong></td>
                <td><strong>Total Val</strong></td>
                <td><strong>EXP</strong></td>
                <td></td>
            </tr>
            </thead>
            <tbody class="list">
            @foreach($spreads_and_olives as $spreads_and_olive)
                @if($spreads_and_olive->discontinued != 1)

                <tr>
                    <td class="lot">{{ $spreads_and_olive->size }}</td>
                    <td class="so">{{ $spreads_and_olive->oil_type }}</td>
                    <td>
                    @foreach($spreads_and_olive->history as $history)
                    @if($history->month == Carbon::now()->month && $history->year == Carbon::now()->year)
                    {{ $history->count }}</td>
                    @endif
                    @endforeach
                    </td>
                    <td class="originallot">{{ $spreads_and_olive->lot }}</td>
                    <td>{{ $spreads_and_olive->area }}</td>
                    <td>{{ $spreads_and_olive->cases }}</td>
                    <td>{{ $spreads_and_olive->jars }}</td>
                    <td class="oiltype">{{ number_format((float)$spreads_and_olive->spreads_and_olives_cost_per, 3) }}</td>
                    <td class="supplier">
                        <?php
                        //calculate total value
                        foreach ($spreads_and_olive->history as $history) {
                            if($history == $spreads_and_olive->history->last() && $history->month == Carbon::now()->month && $history->year == Carbon::now()->year ) {
                        $total_value = $history->count * $spreads_and_olive->spreads_and_olives_cost_per;
                        echo number_format($total_value, 3);
                    }

                        }
                        ?>
                    </td>
                    <td class="origin">{{ $spreads_and_olive->expiration_date }}</td>
                    <td>
                        <div class="btn-group" role="group" aria-label="">
                                <a href="/inventories/edit/spreads_and_olives/<?php echo $spreads_and_olive->id ?>" class="btn btn-warning">Edit</a>
                        @if(Auth::user()->group == 100 || Auth::user()->group == 50)
                        {{ Form::model($spreads_and_olive, array('method' => 'PATCH', 'route' => array('spreads_and_olivesupdates.update', $spreads_and_olive->id))) }}
                                {{ Form::hidden('discontinued',1) }}
                                {{ Form::hidden('count', "0") }}
                                {{ Form::hidden('id', $spreads_and_olive->id) }}
                                {{ Form::hidden('size') }}
                                {{ Form::hidden('oil_type') }}
                                {{ Form::hidden('cases') }}
                                {{ Form::hidden('jars') }}
                                {{ Form::hidden('qty', "0") }}
                                {{ Form::hidden('lot') }}
                                {{ Form::hidden('area') }}
                                {{ Form::hidden('spreads_and_olives_cost_per', "0") }}
                                {{ Form::hidden('expiration_date') }}

                                {{ Form::submit('Discontinue', ['class' => 'btn btn-danger']) }}
                        {{ Form::close() }}
                        @endif
                        </div>


                </tr>
                @endif
            @endforeach
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td><strong>{{ number_format($total->get_total_spreads_and_olives(), 3) }}</strong></td>
            </tr>
            </tbody>
        </table>
        <ul class="pagination"></ul>
    </div>
@stop
{{--end content--}}

{{--footer--}}
@section('footer')
    <script type="text/javascript">
        var options = {
            valueNames: [ 'lot', 'so', 'originallot', 'oiltype', 'supplier', 'origin', 'qty', 'size', 'customer', 'date'],
            // pagination
            // page: 10,
            plugins: [
                ListPagination({})
            ]
        };

        var lotlogList = new List('lotlogs', options);
    </script>
@stop
