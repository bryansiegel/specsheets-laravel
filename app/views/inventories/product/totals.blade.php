@extends('layouts.admin')

@section('content')
    <div id="blends">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Totals
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/inventories/">Inventory</a> >
                        Totals
                    </li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">

                    <a href="/inventories/pdf/inventory_totals/" class="btn btn-success navbar-btn">Print Inventory Totals</a>
                    <a href="/inventories/pdf/totals_for_bank" class="btn btn-info navbar-btn">Print Totals For Bank</a>

                </nav>
            </div>
        </div>


        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        <table class="table  table-responsive table-hover">
            <thead>
            <tr>
                <td><strong>Inventory Totals</strong></td>
                
                <td></td>
            </tr>
            </thead>
            <tbody class="list">
           
                 <tr>
                    <td><strong>Drums</strong></td>
                    <td class="lot">${{ number_format($total->get_total_drums(), 3) }}</td>
                </tr>
                <tr>
                    <td><strong>Totes</strong></td>
                    <td>${{ number_format($total->get_total_totes(), 3) }}</td>
                </tr>
                <tr>
                    <td><strong>Organic</strong></td>
                    <td>${{ number_format($total->get_total_organics(), 3) }}</td>
                </tr>
                <tr>
                    <td><strong>Vinegar</strong></td>
                    <td>${{ number_format($total->get_total_vinegars(), 3) }}</td>
                </tr>
                <tr>
                    <td><strong>Tanks</strong></td>
                    <td>${{ number_format($total->get_total_tanks(), 3) }}</td>
                </tr>
                <tr>
                    <td><strong>Railcars</strong></td>
                    <td>${{ number_format($total->get_railcars_per_lb_price(), 3) }}</td>
                </tr>
                <tr>
                    <td><strong>Specialty</strong></td>
                    <td>${{ number_format($total->get_total_specialties(), 3) }}</td>
                </tr>
                <tr>
                    <td><strong>Infused</strong></td>
                    <td>${{ number_format($total->get_total_infused_oil_drums(), 3) }}</td>
                </tr>
                <tr>
                    <td><strong>Website</strong></td>
                    <td>${{ number_format($total->get_website_totals(), 3) }}</td>
                </tr>
                <tr>
                    <td><strong>Labels</strong></td>
                    <td>${{ number_format($total->get_label_totals(), 3) }}</td>
                </tr>
                <tr>
                    <td><strong>Bottles</strong></td>
                    <td>${{ number_format($total->get_total_bottles(), 3) }}</td>
                </tr>
                <tr>
                    <td><strong>Caps</strong></td>
                    <td>${{ number_format($total->get_total_caps(), 3) }}</td>
                </tr>
                <tr>
                    <td><strong>Cardboard Blank</strong></td>
                    <td>${{ number_format($total->get_total_cardboard_blank(), 3) }}</td>
                </tr>
                <tr>
                    <td><strong>Cardboard Printed</strong></td>
                    <td>${{ number_format($total->get_total_cardboard_printed(), 3) }}</td>
                </tr>
                <tr>
                    <td><strong>Ready To Ship</strong></td>
                    <td>${{ number_format($total->get_total_ready_to_ship(), 3) }}</td>
                </tr>
                <tr>
                    <td><strong>Finished Product</strong></td>
                    <td>${{ number_format($total->get_total_finished_product(), 3) }}</td>
                </tr>
                <tr>
                    <td><strong>Spray</strong></td>
                    <td>${{ number_format($total->get_total_sprays(), 3) }}</td>
                </tr>
                <tr>
                    <td><strong>Supplies</strong></td>
                    <td>${{ number_format($total->get_total_supplies(), 3) }}</td>
                </tr>
                <tr>
                    <td><strong>Rework</strong></td>
                    <td>${{ number_format($total->get_total_rework(), 3) }}</td>
                </tr>
                <tr>
                    <td><strong>Etched Bottles</strong></td>
                    <td>${{ number_format($total->get_total_etched_bottles(), 3) }}</td>
                </tr>
                <tr>
                    <td><strong>Flavors</strong></td>
                    <td>${{ number_format($total->get_flavor_totals(), 3) }}</td>
                </tr>
                <tr>
                    <td><strong>Materials</strong></td>
                    <td>${{ number_format($total->get_total_materials(), 3) }}</td>
                </tr>
                <tr>
                    <td><strong>Follmer</strong></td>
                    <td>${{ number_format($total->get_total_follmer(), 3) }}</td>
                </tr>
                <tr>
                    <td><strong>Pantry Items</strong></td>
                    <td>${{ number_format($total->get_total_pantry_items(), 3) }}</td>
                </tr>
                <tr>
                    <td><strong>Fustis</strong></td>
                    <td>${{ number_format($total->get_total_fustis(), 3) }}</td>
                </tr>
                <hr />
                <tr>
                    <td><span style="font-size:32px;">Totals</span></td>
                    <td><span style="font-size:32px;">${{ number_format($total->total(), 3); }}</span></td>
                </tr>
           

            </tbody>
        </table>
                      

                    </div>
                </div>
                {{-- second column --}}
                <div class="col-md-6">
             <table class="table  table-responsive table-hover">
            <thead>
            <tr>
                <td><strong>Totals For Bank</strong></td>
                
                <td></td>
            </tr>
            </thead>
            <tbody class="list">
           
                <tr>
               		<td><strong>Drums</strong></td>
                    <td class="lot">${{ number_format($total->get_total_drums(), 3) }}</td>
                </tr>
                <tr>
                	<td><strong>Totes</strong></td>
                	<td>${{ number_format($total->get_total_totes(), 3) }}</td>
                </tr>
                <tr>
                	<td><strong>Organic</strong></td>
                	<td>${{ number_format($total->get_total_organics(), 3) }}</td>
                </tr>
                <tr>
                	<td><strong>Vinegar</strong></td>
                	<td>${{ number_format($total->get_total_vinegars(), 3) }}</td>
                </tr>
                <tr>
                	<td><strong>Tanks</strong></td>
                	<td>${{ number_format($total->get_total_tanks(), 3) }}</td>
                </tr>
                <tr>
                	<td><strong>Railcars</strong></td>
                	<td>${{ number_format($total->get_railcars_per_lb_price(), 3) }}</td>
                </tr>
                <tr>
                	<td><strong>Specialty</strong></td>
                	<td>${{ number_format($total->get_total_specialties(), 3) }}</td>
                </tr>
                <tr>
                	<td><strong>Infused</strong></td>
                	<td>${{ number_format($total->get_total_infused_oil_drums(), 3) }}</td>
                </tr>
                <tr>
                	<td><strong>Website</strong></td>
                	<td>$0</td>
                </tr>
                <tr>
                	<td><strong>Labels</strong></td>
                	<td>${{ number_format($total->get_label_totals(), 3) }}</td>
                </tr>
                <tr>
                	<td><strong>Bottles</strong></td>
                	<td>${{ number_format($total->get_total_bottles(), 3) }}</td>
                </tr>
                <tr>
                	<td><strong>Caps</strong></td>
                	<td>${{ number_format($total->get_total_caps(), 3) }}</td>
                </tr>
                <tr>
                	<td><strong>Cardboard Blank</strong></td>
                	<td>${{ number_format($total->get_total_cardboard_blank(), 3) }}</td>
                </tr>
                <tr>
                	<td><strong>Cardboard Printed</strong></td>
                	<td>${{ number_format($total->get_total_cardboard_printed(), 3) }}</td>
                </tr>
                <tr>
                	<td><strong>Ready To Ship</strong></td>
                	<td>${{ number_format($total->get_total_ready_to_ship(), 3) }}</td>
                </tr>
                <tr>
                	<td><strong>Finished Product</strong></td>
                	<td>${{ number_format($total->get_total_finished_product(), 3) }}</td>
                </tr>
                <tr>
                    <td><strong>Spray</strong></td>
                    <td>${{ number_format($total->get_total_sprays(), 3) }}</td>
                </tr>
                <tr>
                    <td><strong>Supplies</strong></td>
                    <td>${{ number_format($total->get_total_supplies(), 3) }}</td>
                </tr>
                <tr>
                	<td><strong>Rework</strong></td>
                	<td>$0</td>
                </tr>
                <tr>
                	<td><strong>Etched Bottles</strong></td>
                	<td>${{ number_format($total->get_total_etched_bottles(), 3) }}</td>
                </tr>
                <tr>
                	<td><strong>Flavors</strong></td>
                	<td>${{ number_format($total->get_flavor_totals(), 3)  }}</td>
                </tr>
                <tr>
                	<td><strong>Materials</strong></td>
                	<td>$0</td>
                </tr>
                <tr>
                	<td><strong>Follmer</strong></td>
                	<td>${{ number_format($total->get_total_follmer(), 3) }}</td>
                </tr>
           		<tr>
           			<td><strong>Pantry Items</strong></td>
           			<td>${{ number_format($total->get_total_pantry_items(), 3) }}</td>
           		</tr>
           		<tr>
           			<td><strong>Fustis</strong></td>
           			<td>$0</td>
           		</tr>
           		<hr />
           		<tr>
           			<td><span style="font-size:32px;">Totals</span></td>
           			<td><span style="font-size:32px;">${{ number_format($total->bank_totals(), 3)  }}</span></td>
           		</tr>

            </tbody>
        </table>
                

                
               

            
                </div><!--end col-md-6-->
            
            </div>
        </div>
@stop