@extends('layouts.admin')

@section('content')

    <div id="lotlogs">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Bottles
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/inventories">Inventory</a> >Bottles
                    </li>
                </ol>
            </div>
        </div>

        {{--sessions--}}
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif

        {{--sub nav--}}
        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">

                    {{ link_to('inventories/count/bottles', 'New Bottle Count', ['class' => 'btn btn-primary navbar-btn']) }}
                    {{ link_to('inventories/create', 'Create A Product', ['class' => 'btn btn-danger navbar-btn']) }}
                     @if(Auth::user()->group == 100)
                    {{ link_to('inventories/cost/bottles', 'Edit Cost Per', ['class' => 'btn btn-success navbar-btn'])}}

                    {{ link_to('inventories/discontinued/bottles', 'Discontinued', ['class' => 'btn btn-danger navbar-btn'])}}
                    {{ link_to('inventories/pdf/bottles', 'Print PDF', ['class' => 'btn btn-primary navbar-btn']) }}
                     @endif
                    {{--search--}}
                    <form class="navbar-form navbar-right" role="search">
                        <div class="form-group">
                            <input type="text" class=" search form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-primary" data-sort="nutritionfact">Submit</button>
                    </form>
                </nav>
            </div>
        </div>

        <table class="table table-stripped table-responsive table-hover">
            <thead>
            <tr>
                <td><strong>Size</strong></td>
                <td><strong>Description</strong></td>
                <td><strong>QTY</strong></td>
                <td><strong>Lot#</strong></td>
                <td><strong>Area</strong></td>
                <td><strong>Supplier</strong></td>
                <td><strong>Manufacturer Part #</strong></td>
                <td><strong>In Bulk</strong></td>
                <td><strong>In Carton</strong></td>
                <td><strong>PCS/PLT</strong></td>
                <td><strong>Cost Per</strong></td>
                <td><strong>Total Val</strong></td>
                <td></td>
            </tr>
            </thead>
            <tbody class="list">
            @foreach($bottles as $bottle)
                @if($bottle->discontinued != 1)

                <tr>
                    <td class="lot">{{ $bottle->size }}</td>
                    <td class="so">{{ $bottle->oil_type }}</td>

                    <td>
                    @foreach($bottle->history as $history)
                    @if($history->month == Carbon::now()->month && $history->year == Carbon::now()->year)
                    {{ $history->count }}
                    @endif
                    @endforeach
                    </td>

                    <td class="originallot">{{ $bottle->lot }}</td>
                    <td>{{ $bottle->area }}</td>
                    <td>{{ $bottle->supplier }}</td>
                    <td>{{ $bottle->manufacturer_part_number }}</td>
                    <td>{{ $bottle->in_bulk }}</td>
                    <td>{{ $bottle->in_carton }}</td>
                    <td>{{ $bottle->pcs_plt }}</td>
                    <td class="oiltype">{{ number_format((float)$bottle->bottles_cost_per, 3) }}</td>
                    <td class="supplier">
                        <?php
                        //calculate total value
                        foreach ($bottle->history as $history) {
                            if($history == $bottle->history->last() && $history->month == Carbon::now()->month && $history->year == Carbon::now()->year) {
                            $total_value = $history->count * $bottle->bottles_cost_per;
                            echo number_format((float)$total_value, 3);
                            }
                        }
                        ?>


                    </td>
                    <td>
                        <div class="btn-group" role="group" aria-label="">

                                <a href="/inventories/edit/bottles/<?php echo $bottle->id ?>" class="btn btn-warning">Edit</a>
 @if(Auth::user()->group == 100 || Auth::user()->group == 50)
                         {{ Form::model($bottle, array('method' => 'PATCH', 'route' => array('bottlesupdates.update', $bottle->id))) }}
                                {{ Form::hidden('id', $bottle->id) }}
                                {{ Form::hidden('discontinued',1) }}
                                {{ Form::hidden('size') }}
                                {{ Form::hidden('oil_type') }}
                                {{ Form::hidden('in_bulk') }}
                                {{ Form::hidden('in_carton') }}
                                {{ Form::hidden('pcs_plt') }}
                                {{ Form::hidden('count', "0") }}
                                {{ Form::hidden('lot') }}
                                {{ Form::hidden('area') }}
                                {{ Form::hidden('supplier') }}
                                {{ Form::hidden('manufacturer_part_number') }}
                                {{ Form::hidden('bottles_cost_per', "0") }}

                                {{ Form::submit('Discontinue', ['class' => 'btn btn-danger']) }}
                        {{ Form::close() }}
                        @endif
                        </div>
                </tr>
                @endif
            @endforeach
            <tr>
                <td><h3>Total</h3></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>

                <td><strong>{{ number_format((float)$total->get_total_bottles(), 3) }}</strong></td>
            </tr>

            </tbody>
        </table>


        <ul class="pagination"></ul>
    </div>
@stop

{{--end content--}}


{{--footer--}}
@section('footer')
    <script type="text/javascript">
        var options = {
            valueNames: [ 'lot', 'so', 'originallot', 'oiltype', 'supplier', 'origin', 'qty', 'size', 'customer', 'date'],
            // pagination
            // page: 10,
            plugins: [
                ListPagination({})
            ]
        };

        var lotlogList = new List('lotlogs', options);
    </script>
@stop
