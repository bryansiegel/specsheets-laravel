@extends('layouts.admin')

@section('content')

    <div id="lotlogs">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Caps
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/inventories">Inventory</a> >Caps
                    </li>
                </ol>
            </div>
        </div>

        {{--sessions--}}
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif

        {{--sub nav--}}
        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">

                    {{ link_to('inventories/count/caps', 'New Cap Count', ['class' => 'btn btn-primary navbar-btn']) }}
                    {{ link_to('inventories/create', 'Create A Product', ['class' => 'btn btn-danger navbar-btn']) }}
                    @if(Auth::user()->group == 100)
                    {{ link_to('inventories/cost/caps', 'Edit Cost Per', ['class' => 'btn btn-success navbar-btn'])}}
                    {{ link_to('inventories/discontinued/caps', 'Discontinued', ['class' => 'btn btn-danger navbar-btn'])}}
                    {{ link_to('inventories/pdf/caps', 'Print PDF', ['class' => 'btn btn-primary navbar-btn']) }}
                    @endif


                    {{--search--}}

                    <form class="navbar-form navbar-right" role="search">
                        <div class="form-group">
                            <input type="text" class=" search form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-primary" data-sort="nutritionfact">Submit</button>
                    </form>
                </nav>
            </div>
        </div>

        <table class="table table-stripped table-responsive table-hover">
            <thead>
            <tr>
                {{--replace size--}}
                <td><strong>Bottle(s) Used In</strong></td>
                <td><strong>Description</strong></td>
                <td>Per Case</td>
                <td>Supplier</td>
                <td><strong>Manufacturer Part #</strong></td>
                {{--replace qty--}}
                <td><strong>Count</strong></td>
                <td><strong>Lot#</strong></td>
                <td><strong>Area</strong></td>
                {{--replace cost per--}}
                <td><strong>Price</strong></td>
                {{--replace total value--}}
                <td><strong>Value</strong></td>

            </tr>
            </thead>
            <tbody class="list">
            @foreach($caps as $cap)
                @if($cap->discontinued != 1)

                <tr>
                    <td class="lot">{{ $cap->size }}</td>
                    <td class="so">{{ $cap->oil_type }}</td>
                    <td>{{ $cap->per_case }}</td>
                    <td class="supplier">{{ $cap->supplier }}</td>
                    <td>{{ $cap->manufacturer_part_number }}</td>
                    <td>
                    @foreach($cap->history as $history)
                    @if($history->month === Carbon::now()->month && $history->year === Carbon::now()->year)
                    {{ $history->count }}</td>
                    @endif
                    @endforeach
                    </td>
                    <td class="originallot">{{ $cap->lot }}</td>
                    <td>{{ $cap->area }}</td>
                    <td class="oiltype">{{ number_format((float)$cap->caps_price, 3) }}</td>
                    <td class="count">
                        <?php
                        //calculate total value
                        foreach($cap->history as $history) {
                            if($history == $cap->history->last() && $history->month == Carbon::now()->month && $history->year == Carbon::now()->year) {
                                $total_value = $history->count * $cap->caps_price;
                                echo number_format($total_value, 3);
                            }

                        }
                        ?>

                    </td>

                    <td>
                        <div class="btn-group" role="group" aria-label="">
                                <a href="/inventories/edit/caps/<?php echo $cap->id ?>" class="btn btn-warning">Edit</a>
                        @if(Auth::user()->group == 100 || Auth::user()->group == 50)
                        {{ Form::model($cap, array('method' => 'PATCH', 'route' => array('capsupdates.update', $cap->id))) }}
                                {{ Form::hidden('discontinued',1) }}
                                {{ Form::hidden('id', $cap->id) }}
                                {{ Form::hidden('size') }}
                                {{ Form::hidden('oil_type') }}
                                {{ Form::hidden('supplier') }}
                                {{ Form::hidden('manufacturer_part_number') }}
                                {{ Form::hidden('per_case') }}
                                {{ Form::hidden('count', "0") }}
                                {{ Form::hidden('lot') }}
                                {{ Form::hidden('area') }}
                                {{ Form::hidden('caps_price', "0") }}
                                {{ Form::hidden('expiration_date') }}
                                {{ Form::submit('Discontinue', ['class' => 'btn btn-danger']) }}
                        {{ Form::close() }}
                        @endif
                        </div>


                </tr>
                @endif
            @endforeach
            <tr>
                <td><h3>Total</h3></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td><strong>{{ number_format($total->get_total_caps(), 3) }}</strong></td>
            </tr>

            </tbody>
        </table>


        <ul class="pagination"></ul>
    </div>
@stop

{{--end content--}}


{{--footer--}}
@section('footer')
    <script type="text/javascript">
        var options = {
            valueNames: [ 'lot', 'so','count', 'originallot', 'oiltype', 'supplier', 'origin', 'qty', 'size', 'customer', 'date'],
            // pagination
            // page: 10,
            plugins: [
                ListPagination({})
            ]
        };

        var lotlogList = new List('lotlogs', options);
    </script>
@stop
