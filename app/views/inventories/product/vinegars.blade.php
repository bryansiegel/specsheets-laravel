@extends('layouts.admin')

@section('content')

    <div id="lotlogs">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Vinegars
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/inventories">Inventory</a> >Vinegars
                    </li>
                </ol>
            </div>
        </div>

        {{--sessions--}}
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif

        {{--sub nav--}}
        <div class="row">
            <div class="col-lg-12">
                @include('inventories/product/sub_category/partials/nav')
            </div>
        </div>

        <table class="table table-stripped table-responsive table-hover">
            <thead>
            <tr>
              {{-- <td>ID</td> --}}
              {{-- <td>Month</td> --}}
              {{-- <td>Year</td> --}}
                <td><strong>Size</strong></td>
                <td><strong>Description</strong></td>
                <td><strong>QTY</strong></td>
                <td><strong>Lot#</strong></td>
                <td>
                  <strong>Area</strong>
                </td>
                <td><strong>Cost Per</strong></td>
                <td><strong>Total Val</strong></td>
                <td><strong>EXP</strong></td>
                <td></td>
            </tr>
            </thead>
            <tbody class="list">
            @foreach($vinegars as $vinegar)
                @if($vinegar->discontinued != 1)

                <tr>
                  {{-- <td>{{ $vinegar->id }}</td> --}}
                  @foreach($vinegar->history as $history)
                    {{-- <td>{{ $history->month }}  </td> --}}
                    {{-- <td>{{ $history->year }}</td> --}}
                  @endforeach

                    <td class="lot">{{ $vinegar->size }}</td>
                    <td class="so">{{ $vinegar->oil_type }}</td>
                    <td>
                    @foreach($vinegar->history as $history)
                    @if($history->month == Carbon::now()->month && $history->year == Carbon::now()->year)
                    {{ $history->count }}
                    </td>
                    @endif
                    @endforeach
                    </td>
                    <td class="originallot">{{ $vinegar->lot }}</td>
                    <td>
                      {{ $vinegar->area }}
                    </td>
                    <td class="oiltype">{{ number_format((float)$vinegar->vinegars_cost_per, 3) }}</td>
                    <td class="supplier">
                        <?php
                        //calculate total value
                        foreach ($vinegar->history as $history) {
                            if($history == $vinegar->history->last() && $history->month == Carbon::now()->month && $history->year == Carbon::now()->year) {
                        $total_value = $history->count * $vinegar->vinegars_cost_per;
                        echo number_format((float)$total_value, 3);
                    }

                        }
                        ?>

                    </td>
                    <td class="origin">{{ $vinegar->expiration_date }}</td>
                    <td>
                        <div class="btn-group" role="group" aria-label="">
                        <a href="/inventories/edit/vinegars/<?php echo $vinegar->id ?>" class="btn btn-warning">Edit</a>
                        @if(Auth::user()->group == 100 || Auth::user()->group == 50)
                       {{ Form::model($vinegar, array('method' => 'PATCH', 'route' => array('vinegarupdates.update', $vinegar->id))) }}
                                {{ Form::hidden('discontinued',1) }}
                                {{ Form::hidden('id', $vinegar->id) }}
                                {{ Form::hidden('count', "0") }}
                                {{ Form::hidden('size') }}
                                {{ Form::hidden('oil_type') }}
                                {{ Form::hidden('qty', "0") }}
                                {{ Form::hidden('lot') }}
                                {{ Form::hidden('area') }}
                                {{ Form::hidden('vinegars_cost_per', "0") }}
                                {{ Form::hidden('expiration_date') }}
                                {{ Form::submit('Discontinue', ['class' => 'btn btn-danger']) }}
                        {{ Form::close() }}
                        @endif
                        </div>
                </tr>
                @endif
            @endforeach
            <tr>
                <td><h3>Total</h3></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td><strong>{{ number_format((float)$total->get_total_vinegars(), 3) }}</strong></td>
            </tr>
            </tbody>
        </table>
        <ul class="pagination"></ul>
    </div>
@stop
{{--end content--}}


{{--footer--}}
@section('footer')
    <script type="text/javascript">
        var options = {
            valueNames: [ 'lot', 'so', 'originallot', 'oiltype', 'supplier', 'origin', 'qty', 'size', 'customer', 'date'],
            // pagination
            // page: 10,
            plugins: [
                ListPagination({})
            ]
        };

        var lotlogList = new List('lotlogs', options);
    </script>
@stop
