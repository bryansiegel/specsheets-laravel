@extends('layouts.print')


@section('content')
<div>
    <a href="javascript:window.print()">
        {{ HTML::image('images/print_icon.jpg', 'Print',['width' => '100', 'height' => '100', 'class' => 'noPrint', 'style' => 'border:none']) }}</a>
</div>
<table cellspacing=0 cellpadding=0>
    <thead>
    <tr><th colspan=14><h2>CIBARIA INTERNATIONAL, INC<br />RECEIVING LOG</h2></th></tr>
    <tr>
        <th style="border-left: 3px solid black; border-top: 3px solid black;border-right: 1px solid black; border-bottom:3px solid black;">DATE</th>
        <th style="border-top: 3px solid black; border-right:1px solid black; border-bottom: 3px solid black;">P.O.#</th>
        <th style="border-top: 3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;">VENDOR</th>
        <th style="border-top:3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;">PACKSLIP/WT TICKET#</th>
        <th style="border-top: 3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;">QTY</th>
        <th style="border-top: 3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;" class="product_print">PRODUCT DESCRIPTION/PRODUCT SIZE</th>
        <th style="border-top: 3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;width:10px;" class="lot_print">LOT#</th>
        <th style="border-top: 3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;width:80px;" class="container_print">CONTAINER#</th>
        <th style="border-top: 3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;">CARRIER</th>
        <th style="border-top: 3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;">PRODUCT ON HOLD<br />YES/NO</th>
        <th style="border-top: 3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;">PRODUCT CONTACT?<br />YES/NO</th>
        <th style="border-top: 3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;">CONDITION OF TRUCK<br />ACCEPTABLE/NOT ACCEPTABLE</th>
        <th style="border-top: 3px solid black; border-bottom: 3px solid black; border-right: 3px solid black;">KOSHER<br />CERTIFICATION FILE#</th>
    </tr>
    </thead>
    <tbody style="border-bottom: 3px solid black;">
    @foreach($receivinglogs as $receivinglog)
    <tr>
        <td style="border-left: 3px solid black; border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $receivinglog->date; ?></td>
        <td style="border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $receivinglog->po; ?></td>
        <td style="border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $receivinglog->vendor; ?></td>
        <td style="border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $receivinglog->packslip; ?></td>
        <td style="border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $receivinglog->qty; ?></td>
        <td style="border-bottom: 1px solid black; border-right: 1px solid black;" class="product_print"><?php echo $receivinglog->product_description; ?></td>
        <td style="border-bottom: 1px solid black; border-right: 1px solid black;" class="lot_print"><?php echo $receivinglog->lot; ?></td>
        <td style="border-bottom: 1px solid black; border-right: 1px solid black;" class="container_print"><?php echo $receivinglog->container; ?></td>
        <td style="border-bottom: 1px solid black; border-right: 1px solid black; width:300px;"><?php echo $receivinglog->carrier; ?></td>
        <td style="border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $receivinglog->product_on_hold; ?></td>
        <td style="border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $receivinglog->product_contact; ?></td>
        <td style="border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $receivinglog->condition_truck; ?></td>
        <td style="border-bottom: 1px solid black; border-right: 3px solid black;"><?php echo $receivinglog->kosher_cert_file; ?></td>
    </tr>
    @endforeach
    </tbody>
</table>

    @stop