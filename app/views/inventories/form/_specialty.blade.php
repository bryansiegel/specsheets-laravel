<div class="form-group">
	{{ Form::label('sub_product_category', 'Sub Category') }}
	{{ Form::select('sub_product_category', [
		'' => 'Choose',
		'truffle_oil_black' => 'Truffle Oil, Black',
		'truffle_oil_white' => 'Truffle Oil, White',
		'cocoa_butter_deodorized' => 'Cocoa Butter, Deodorized',
		'cocoa_butter_natural' => 'Cocoa Butter, Natural',
		'sesame_clear' => 'Sesame, Clear',
		'sesame_toasted' => 'Sesame, Toasted',
		 ],'', ['class' => 'form-control', 'id' => 'sub_product_category']) }}
	
</div>

<div class="form-group">
    {{ Form::label('size', 'Size') }}
    {{ Form::text('size', null,['class' => 'form-control', 'placeholder' => 'Size']) }}
</div>

<div class="form-group">
    {{ Form::label('oil_type', 'Description') }}
    {{ Form::textarea('oil_type', null,['class' => 'form-control', 'placeholder' => 'Description', 'rows' => '8', 'cols' => '4']) }}
</div>
<div class="form-group">
    {{ Form::label('lot', 'Lot Number') }}
    {{ Form::text('lot', null,['class' => 'form-control', 'placeholder' => 'Lot Number']) }}
</div>
<div class="form-group">
    {{ Form::label('expiration_date', 'Expiration Date') }}
    {{ Form::text('expiration_date',null,['class' => 'form-control', 'placeholder' => 'Expiration Date', 'id' => 'datepicker']) }}
</div>
