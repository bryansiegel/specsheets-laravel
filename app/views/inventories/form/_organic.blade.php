<div class="form-group">
	{{ Form::label('size', 'Size') }}
	{{ Form::text('size', null,['class' => 'form-control', 'placeholder' => 'Size']) }}
</div>

<div class="form-group">
	{{ Form::label('oil_type', 'Description') }}
	{{ Form::textarea('oil_type', null,['class' => 'form-control', 'placeholder' => 'Description', 'rows' => '8', 'cols' => '4']) }}
</div>
<div class="form-group">
	{{ Form::label('lot', 'Lot Number') }}
	{{ Form::text('lot', null,['class' => 'form-control', 'placeholder' => 'Lot Number']) }}
</div>
<div class="form-group">
	{{ Form::label('expiration_date', 'Expiration Date') }}
	{{ Form::text('expiration_date',null,['class' => 'form-control', 'placeholder' => 'Expiration Date', 'id' => 'datepicker']) }}
</div>
