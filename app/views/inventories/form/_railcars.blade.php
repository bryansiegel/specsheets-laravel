<div class="form-group">
    {{ Form::label('size', 'Car Received') }}
    {{ Form::text('size', null,['class' => 'form-control', 'placeholder' => 'Car Received']) }}
</div>

<div class="form-group">
    {{ Form::label('oil_type', 'Inventory Date') }}
    {{ Form::textarea('oil_type', null,['class' => 'form-control', 'placeholder' => 'Inventory Date', 'rows' => '8', 'cols' => '4']) }}
</div>
<div class="form-group">
    {{ Form::label('lot', 'Lot Number') }}
    {{ Form::text('lot', null,['class' => 'form-control', 'placeholder' => 'Lot Number']) }}
</div>

<div class="form-group">
    {{ Form::label('loads_taken', 'Loads Taken') }}
    {{ Form::text('loads_taken', null,['class' => 'form-control', 'placeholder' => 'Lot Number']) }}
</div>