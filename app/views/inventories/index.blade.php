@extends('layouts.admin')

@section('content')

    <div id="inventory">
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Inventory <small>Statistics Overview</small>
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> Dashboard
                    </li>
                </ol>
            </div>
        </div>

        <div class="row">


        


        </div>
      {{--sub nav--}}
    <div class="row">
        <div class="col-lg-12">
            <nav class="navbar navbar-default">

            
                {{ link_to_route('inventories.create', 'Create A Product',null, ['class' => 'btn btn-primary navbar-btn']) }}  
                       
            @if(Auth::user()->group == 100)
                {{ link_to('inventories/website_totals', 'Website Totals', ['class' => 'btn btn-success navbar-btn']) }}
                {{ link_to('inventories/totals', 'Totals', ['class' => 'btn btn-danger navbar-btn']) }}
                {{ link_to('inventories/product/product_totals', 'Product Count Totals', ['class' => 'btn btn-info navbar-btn']) }}
                {{ link_to('history', 'Inventory History', ['class' => 'btn btn-warning navbar-btn']) }}

                <form class="navbar-form navbar-right" role="search">
                    <div class="form-group">
                        <input type="text" class=" search form-control" placeholder="Search">
                    </div>
                    <button type="submit" class="btn btn-primary" data-sort="nutritionfact">Submit</button>
                </form>
                 @endif  
            </nav>
        </div>
    </div>


        <div class="row">
            <div class="list">
                <div class="product">
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                    <h3 class="product">Drums</h3>
                    <hr />
                        <div class="row">
                            <div class="col-xs-3">
                            <div class="huge">${{ number_format($total->get_total_drums(), 3) }}</div>
                                <div>Drum Value</div>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ number_format($count->count_drums(), 3) }}</div>
                                <div>Total Drums</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/drums">
                        <div class="panel-footer">
                            <span class="pull-left">View All Drums</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
                </div>
                </div>

            <div class="list">
                <div class="product">
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-green">
                    <div class="panel-heading">
                        <h3 class="product">Totes</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="huge">${{ number_format($total->get_total_totes(),3) }}</div>
                                <div>Total Totes</div>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ number_format($count->count_totes(), 3) }}</div>
                                <div>Total Totes</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/totes">
                        <div class="panel-footer">
                            <span class="pull-left">View All Totes</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
</div>
            </div>

            <div class="list">
                <div class="product">
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        <h3 class="product">Organic</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="huge">${{ number_format($total->get_total_organics(),3) }}</div>
                                <div>Organic Value</div>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ number_format($count->count_organics(), 3) }}</div>
                                <div>Total Organic</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/organic">
                        <div class="panel-footer">
                            <span class="pull-left">View All Organics</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
</div>
                </div>
            <div class="list">
                <div class="product">
            <div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <h3 class="product">Tanks</h3>
                            <hr />
                            <div class="row">
                                <div class="col-xs-3">
                                    <div class="huge">${{ number_format($total->get_total_tanks(), 3) }}</div>
                                    <div>Tank Value</div>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">{{ number_format($count->count_tanks(), 3) }}</div>
                                    <div>Total Tanks</div>
                                </div>
                            </div>
                        </div>
                        <a href="/inventories/product/tanks">
                            <div class="panel-footer">
                                <span class="pull-left">View All Tanks</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                </div>
                </div>
                </div><!--end product-->




        <!--row-->
<div class="list">
<div class="row">

            <div class="col-lg-3 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="product">Vinegars</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="huge">${{ number_format($total->get_total_vinegars(), 3) }}</div>
                                <div>Vinegar Value</div>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ number_format($count->count_vinegars(), 3) }}</div>
                                <div>Total Vinegar</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/vinegars">
                        <div class="panel-footer">
                            <span class="pull-left">View All Vinegars</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>


            <div class="col-lg-3 col-md-6">
                <div class="panel panel-green">
                    <div class="panel-heading">
                        <h3 class="product">Railcars</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="huge">${{ number_format($total->get_railcars_per_lb_price(),3) }}</div>
                                <div>Railcars Value</div>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ number_format($count->count_railcars(), 3) }}</div>
                                <div>Total Rail Cars</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/railcars">
                        <div class="panel-footer">
                            <span class="pull-left">View All Railcars</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>


            <div class="col-lg-3 col-md-6">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        <h3 class="product">Specialty</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="huge">${{ number_format($total->get_total_specialties(),3) }}</div>
                                <div>Specialty Value</div>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ number_format($count->count_specialties(), 3) }}</div>
                                <div>Total Specialty</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/specialty">
                        <div class="panel-footer">
                            <span class="pull-left">View All Specialties</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>


            <div class="col-lg-3 col-md-6">
                <div class="panel panel-red">
                    <div class="panel-heading">
                        <h3 class="product">Infused Oil Drums</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="huge">${{ number_format($total->get_total_infused_oil_drums(),3) }}</div>
                                <div>Infused Oil Drum Value</div>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ number_format($count->count_infused_oil_drums(), 3) }}</div>
                                <div>Total Infused Oil Drums</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/infused_oil_drums">
                        <div class="panel-footer">
                            <span class="pull-left">View All Infused Oil Drums</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>

        </div>
        </div>
</div>
<!--end row-->

 <!--row-->
        <div class="list">
<div class="row">

            <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="product">Spreads and Olives</h3>
                    <hr />
                    <div class="row">
                        <div class="col-xs-3">
                            <div class="huge">${{ number_format($total->get_total_spreads_and_olives(), 3) }}</div>
                            <div>Spreads and Olives Value</div>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{ number_format($count->count_spreads_and_olives(), 3) }}</div>
                            <div>Total Spreads and Olives</div>
                        </div>
                    </div>
                </div>
                <a href="/inventories/product/spreads_and_olives">
                    <div class="panel-footer">
                        <span class="pull-left">View All Spreads and Olives</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>


            <div class="col-lg-3 col-md-6">
                <div class="panel panel-green">
                    <div class="panel-heading">
                        <h3 class="product">Website Olive Oil</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="huge">${{ number_format($total->get_total_website_olive_oils(), 3) }}</div>
                                <div>Website Olive Oils Value</div>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ number_format($count->count_website_olive_oils(), 3) }}</div>
                                <div>Total Website Olive Oils</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/website_olive_oils">
                        <div class="panel-footer">
                            <span class="pull-left">View All Website Olive Oils</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>


            <div class="col-lg-3 col-md-6">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        <h3 class="product">Web Flavor and Infused</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="huge">${{ number_format($total->get_total_web_flavored_and_infused(), 3) }}</div>
                                <div>Web Flavor and Infused Value</div>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ number_format($count->count_web_flavor_and_infused(), 3) }}</div>
                                <div>Total Web Flavor and Infused</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/web_flavor_and_infused">
                        <div class="panel-footer">
                            <span class="pull-left">View All Web Flavor and Infused</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>


            <div class="col-lg-3 col-md-6">
        <div class="panel panel-red">
            <div class="panel-heading">
                <h3 class="product">Website Vinegars</h3>
                <hr />
                <div class="row">
                    <div class="col-xs-3">
                        <div class="huge">${{ number_format($total->get_total_website_vinegars(), 3) }}</div>
                        <div>Website Vinegars Value</div>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{ number_format($count->count_website_vinegars(), 3) }}</div>
                        <div>Total Website Vinegars</div>
                    </div>
                </div>
            </div>
            <a href="/inventories/product/website_vinegars">
                <div class="panel-footer">
                    <span class="pull-left">View All Website Vinegars</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>

        </div>
        </div>
</div>
<!--end row-->

 <!--row-->
        <div class="list">
<div class="row">

            <div class="col-lg-3 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="product">Website Other</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="huge">${{ number_format($total->get_total_website_other(), 3) }}</div>
                                <div>Website Other Value</div>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ number_format($count->count_website_other(), 3) }}</div>
                                <div>Total Website Other</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/website_other">
                        <div class="panel-footer">
                            <span class="pull-left">View All Website Other</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>


            <div class="col-lg-3 col-md-6">
                <div class="panel panel-green">
                    <div class="panel-heading">
                        <h3 class="product">Website Totals</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="huge">${{ number_format($total->get_website_totals(), 3) }}</div>
                                <div>Website Totals Value</div>
                            </div>

                            <div class="col-xs-9 text-right">
                                <div class="huge"></div>
                                <div>Total Website Totals</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/website_totals">
                        <div class="panel-footer">
                            <span class="pull-left">View All Website Totals</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>


            <div class="col-lg-3 col-md-6">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        <h3 class="product">Finished Product</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="huge">${{ number_format($total->get_total_finished_product(), 3) }}</div>
                                <div>Finished Product Value</div>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ number_format($count->count_finished_product(), 3) }}</div>
                                <div>Total Finished Product</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/finished_product">
                        <div class="panel-footer">
                            <span class="pull-left">View All Finished Product</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>


            <div class="col-lg-3 col-md-6">
                <div class="panel panel-red">
                    <div class="panel-heading">
                        <h3 class="product">Sprays</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="huge">${{ number_format($total->get_total_sprays(), 3) }}</div>
                                <div>Spray Value</div>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ number_format($count->count_sprays(), 3) }}</div>
                                <div>Total Sprays</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/sprays">
                        <div class="panel-footer">
                            <span class="pull-left">View All Sprays</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        </div>
</div>
<!--end row-->

 <!--row-->
        <div class="list">
<div class="row">

            <div class="col-lg-3 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="product">Ready To Ship</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="huge">${{ number_format($total->get_total_ready_to_ship(), 3) }}</div>
                                <div>Ready To Ship Value</div>
                            </div>
                            <div class="col-xs-9 text-right">


                                <div class="huge">{{ number_format($count->count_ready_to_ship(), 3) }}</div>
                                <div>Total Ready To Ship</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/ready_to_ship">
                        <div class="panel-footer">
                            <span class="pull-left">View All Ready To Ship</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>


            <div class="col-lg-3 col-md-6">
                <div class="panel panel-green">
                    <div class="panel-heading">
                        <h3 class="product">Rework</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="huge">${{ number_format($total->get_total_rework(), 3) }}</div>
                                <div>Rework Value</div>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ number_format($count->count_rework(), 3) }}</div>
                                <div>Total Rework</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/rework">
                        <div class="panel-footer">
                            <span class="pull-left">View All Rework</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>


            <div class="col-lg-3 col-md-6">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        <h3 class="product">Labels In House</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="huge">${{ number_format($total->get_total_labels_in_house(), 3) }}</div>
                                <div>Labels In House Value</div>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ number_format($count->count_labels_in_house(), 3) }}</div>
                                <div>Total Labels in House</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/labels_in_house">
                        <div class="panel-footer">
                            <span class="pull-left">View All Labels In House</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>


            <div class="col-lg-3 col-md-6">
                <div class="panel panel-red">
                    <div class="panel-heading">
                        <h3 class="product">Labels in Labelroom</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="huge">${{ number_format($total->get_total_labels_in_labelroom(), 3) }}</div>
                                <div>Label In Labelroom Value</div>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ number_format($count->count_labels_in_labelroom(), 3) }}</div>
                                <div>Total Labels in Label Room</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/labels_in_labelroom">
                        <div class="panel-footer">
                            <span class="pull-left">View All Labels in Labelroom</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>

        </div>
<!--end row-->

 <!--row-->
            <div class="list">
<div class="row">

            <div class="col-lg-3 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="product">Customer Labels</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="huge">${{ number_format($total->get_total_customers_labels(), 3) }}</div>
                                <div>Customers Labels Value</div>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ number_format($count->count_customers_labels(), 3) }}</div>
                                <div>Total Customers Labels</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/customers_labels">
                        <div class="panel-footer">
                            <span class="pull-left">View All Customer Labels</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>


            <div class="col-lg-3 col-md-6">
                <div class="panel panel-green">
                    <div class="panel-heading">
                        <h3 class="product">Bottles</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="huge">${{ number_format($total->get_total_bottles(), 3) }}</div>
                                <div>Bottles Value</div>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ number_format($count->count_bottles(), 3) }}</div>
                                <div>Total Bottles</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/bottles">
                        <div class="panel-footer">
                            <span class="pull-left">View All Bottles</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>


            <div class="col-lg-3 col-md-6">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        <h3 class="product">Etched Bottles</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="huge">${{ number_format($total->get_total_etched_bottles(), 3) }}</div>
                                <div>Etched Bottle Value</div>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ number_format($count->count_etched_bottles(), 3) }}</div>
                                <div>Total Etched Bottles</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/etched_bottles">
                        <div class="panel-footer">
                            <span class="pull-left">View All Etched Bottles</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>


            <div class="col-lg-3 col-md-6">
                <div class="panel panel-red">
                    <div class="panel-heading">
                        <h3 class="product">Fustis</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="huge">${{ number_format($total->get_total_fustis(), 3) }}</div>
                                <div>Fustis Value</div>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ number_format($count->count_fustis(), 3) }}</div>
                                <div>Total Fustis</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/fustis">
                        <div class="panel-footer">
                            <span class="pull-left">View All Fustis</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>

        </div>
                </div>
<!--end row-->

 <!--row-->
            <div class="list">
<div class="row">

            <div class="col-lg-3 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="product">Caps</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="huge">${{ number_format($total->get_total_caps(), 3) }}</div>
                                <div>Caps Value</div>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ number_format($count->count_caps(), 3) }}</div>
                                <div>Total Caps</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/caps">
                        <div class="panel-footer">
                            <span class="pull-left">View All Caps</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>


            <div class="col-lg-3 col-md-6">
                <div class="panel panel-green">
                    <div class="panel-heading">
                        <h3 class="product">Cardboard Blank</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="huge">${{ number_format($total->get_total_cardboard_blank(), 3) }}</div>
                                <div>Cardboard Blank Value</div>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ number_format($count->count_cardboard_blank(), 3) }}</div>
                                <div>Total Cardboard Blank</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/cardboard_blank">
                        <div class="panel-footer">
                            <span class="pull-left">View All Cardboard Blank</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>

    <div class="col-lg-3 col-md-6">
        <div class="panel panel-green">
            <div class="panel-heading">
                <h3 class="product">Cardboard Printed</h3>
                <hr />
                <div class="row">
                    <div class="col-xs-3">
                        <div class="huge">${{ number_format($total->get_total_cardboard_printed(), 3) }}</div>
                        <div>Cardboard Printed Value</div>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{ number_format($count->count_cardboard_printed(), 3) }}</div>
                        <div>Total Cardboard Printed</div>
                    </div>
                </div>
            </div>
            <a href="/inventories/product/cardboard_printed">
                <div class="panel-footer">
                    <span class="pull-left">View All Cardboard Printed</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>


            <div class="col-lg-3 col-md-6">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        <h3 class="product">Supplies</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="huge">${{ number_format($total->get_total_supplies(), 3) }}</div>
                                <div>Supplies Value</div>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ number_format($count->count_supplies(), 3) }}</div>
                                <div>Total Supplies</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/supplies">
                        <div class="panel-footer">
                            <span class="pull-left">View All Supplies</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>




        </div>
                </div>
<!--end row-->

 <!--row-->
            <div class="list">
<div class="row">

    <div class="col-lg-3 col-md-6">
        <div class="panel panel-red">
            <div class="panel-heading">
                <h3 class="product">Materials</h3>
                <hr />
                <div class="row">
                    <div class="col-xs-3">
                        <div class="huge">${{ number_format($total->get_total_materials(), 3) }}</div>
                        <div>Materials Value</div>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{ number_format($count->count_materials(), 3) }}</div>
                        <div>Total Materials</div>
                    </div>
                </div>
            </div>
            <a href="/inventories/product/materials">
                <div class="panel-footer">
                    <span class="pull-left">View All Materials</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>

            <div class="col-lg-3 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="product">Follmer</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="huge">${{ number_format($total->get_total_follmer(), 3) }}</div>
                                <div>Follmer Value</div>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ number_format($count->count_follmer(), 3) }}</div>
                                <div>Total Follmer</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/follmer">
                        <div class="panel-footer">
                            <span class="pull-left">View All Follmer</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>


            <div class="col-lg-3 col-md-6">
                <div class="panel panel-green">
                    <div class="panel-heading">
                        <h3 class="product">Flavors</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="huge">${{ number_format($total->get_total_flavors(), 3) }}</div>
                                <div>Flavors Value</div>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ number_format($count->count_flavors(), 3) }}</div>
                                <div>Total Flavors</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/flavors">
                        <div class="panel-footer">
                            <span class="pull-left">View All Flavors</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>


            <div class="col-lg-3 col-md-6">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        <h3 class="product">Flavors For Samples</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="huge">${{ number_format($total->get_total_flavors_for_samples(), 3) }}</div>
                                <div>Flavors for samples Value</div>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ number_format($count->count_flavors_for_samples(), 3) }}</div>
                                <div>Total Flavors For Samples</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/flavors_for_samples">
                        <div class="panel-footer">
                            <span class="pull-left">View All Flavors For Samples</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-lg-3 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="product">Customer Oils</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="huge">{{ number_format($count->count_customers_oils(), 3) }}</div>
                                <div>Total Customer Oils</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/customers_oils">
                        <div class="panel-footer">
                            <span class="pull-left">View All Flavors For Samples</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>

            
        </div>
                </div>


<!--end row-->

    </div><!--end-->

    </div><!--inventory id-->

@stop

{{--footer--}}
@section('footer')
    <script type="text/javascript">
        var options = {
            valueNames: [ 'product'],
            // pagination
//            page: 10,
            plugins: [
                ListPagination({})
            ]
        };

        var inventoryList = new List('inventory', options);
    </script>
@stop