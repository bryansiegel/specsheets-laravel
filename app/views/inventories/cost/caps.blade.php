@extends('layouts.admin')

@section('content')

    <div id="lotlogs">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    caps
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/inventories">Inventory</a> > <a href="/inventories/product/caps">caps</a>
                    </li>
                </ol>
            </div>
        </div>

        {{--sessions--}}
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif

        <table class="table table-stripped table-responsive table-hover">
            <thead>
            <tr>
                <td><strong>Bottle(s) Used In</strong></td>
                <td><strong>Description</strong></td>
                <td><strong>Price</strong></td>
                <td><strong>Lot#</strong></td>
                <td><strong>Area</strong></td>
                <td><strong>Supplier</strong></td>
                <td><strong>manufacturer_part_number</strong></td>
                <td></td>
            </tr>
            </thead>
            <tbody class="list">

            @foreach($caps as $cap)
                @if($cap->discontinued != 1)

                <tr>
                    <td class="lot">{{ $cap->size }}</td>
                    <td class="so">{{ $cap->oil_type }}</td>
                    {{ Form::model($cap, array('method' => 'PATCH', 'route' => array('capsupdates.update', $cap->id))) }}
                    {{ Form::hidden('id', $cap->id) }}
                    {{ Form::hidden('update', 'true') }}
                    {{ Form::hidden('size') }}
                    {{ Form::hidden('oil_type') }}
                    {{ Form::hidden('lot') }}
                    {{ Form::hidden('area') }}
                    {{ Form::hidden('supplier') }}
                    {{ Form::hidden('manufacturer_part_number') }}
                    {{ Form::hidden('username',Auth::user()->username) }}

                    <td>{{ Form::text('caps_price', $cap->caps_price,['class' => 'form-control'])}}</td>
                    <td class="originallot">{{ $cap->lot }}</td>
                    <td>{{ $cap->area }}</td>
                    <td>{{ $cap->supplier }}</td>
                    <td>{{ $cap->manufacturer_part_number }}</td>
                    <td>{{ Form::submit('Update', ['class' => 'btn btn-update']) }}</td>
                </tr>
                {{ Form::close() }}
                @endif
            @endforeach
            <tr>
              <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><strong>
            </strong></td>
            </tr>
            </tbody>
        </table>
        <ul class="pagination"></ul>
    </div>
@stop
{{--end content--}}


{{--footer--}}
@section('footer')
    <script type="text/javascript">
        var options = {
            valueNames: [ 'lot', 'so', 'originallot', 'oiltype', 'supplier', 'origin', 'qty', 'size', 'customer', 'date'],
            // pagination
            // page: 10,
            plugins: [
                ListPagination({})
            ]
        };

        var lotlogList = new List('lotlogs', options);
    </script>
@stop
