@extends('layouts.admin')

@section('content')

    <div id="lotlogs">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    website_vinegars
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/inventories">Inventory</a> > <a href="/inventories/product/website_vinegars">website_vinegars</a>
                    </li>
                </ol>
            </div>
        </div>

        {{--sessions--}}
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif
        <table class="table table-stripped table-responsive table-hover">
            <thead>
            <tr>
                <td><strong>Size</strong></td>
                <td><strong>Description</strong></td>
                <td><strong>Cost Per</strong></td>
                <td><strong>Lot#</strong></td>
                <td><strong>Area</strong></td>
                <td></td>
            </tr>
            </thead>
            <tbody class="list">

            @foreach($website_vinegar as $drum)
              @if($drum->discontinued != 1)

                <tr>
                    <td class="lot">{{ $drum->size }}</td>
                    <td class="so">{{ $drum->oil_type }}</td>
                    {{ Form::model($drum, array('method' => 'PATCH', 'route' => array('website_vinegarsupdates.update', $drum->id))) }}
                    {{ Form::hidden('id', $drum->id) }}
                    {{ Form::hidden('update', 'true') }}
                    {{ Form::hidden('size') }}
                    {{ Form::hidden('oil_type') }}
                    {{ Form::hidden('lot') }}
                    {{ Form::hidden('area') }}
                    {{ Form::hidden('expiration_date') }}
                    {{ Form::hidden('username',Auth::user()->username) }}

                    <td>{{ Form::text('website_vinegars_cost', $drum->website_vinegars_cost,['class' => 'form-control'])}}</td>
                    <td class="originallot">{{ $drum->lot }}</td>
                    <td class="originallot">{{ $drum->area }}</td>
                    <td>{{ Form::submit('Update', ['class' => 'btn btn-update']) }}</td>
                </tr>
                {{ Form::close() }}
              @endif
            @endforeach
            <tr>
              <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><strong>
            </strong></td>
            </tr>
            </tbody>
        </table>
    </div>
@stop
{{--end content--}}


{{--footer--}}
@section('footer')
    <script type="text/javascript">
        var options = {
            valueNames: [ 'lot', 'so', 'originallot', 'oiltype', 'supplier', 'origin', 'qty', 'size', 'customer', 'date'],
            // pagination
            // page: 10,
            plugins: [
                ListPagination({})
            ]
        };

        var lotlogList = new List('lotlogs', options);
    </script>
@stop
