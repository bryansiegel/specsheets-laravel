@extends('layouts.admin')

@section('content')

    <div id="lotlogs">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    tanks
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/inventories">Inventory</a> > <a href="/inventories/product/tanks">Tanks</a>
                    </li>
                </ol>
            </div>
        </div>

        {{--sessions--}}
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif

        <table class="table table-stripped table-responsive table-hover">
            <thead>
            <tr>
                <td><strong>Size</strong></td>
                <td><strong>Description</strong></td>
                <td><strong>Tanks Cost</strong></td>
                <td></td>
                            </tr>
            </thead>
            <tbody class="list">
            
            @foreach($tanks as $tank)
                <tr>
                    <td class="lot">{{ $tank->size }}</td>
                    <td class="so">{{ $tank->oil_type }}</td>
                    {{ Form::model($tank, array('method' => 'PATCH', 'route' => array('tankupdates.update', $tank->id))) }}
                    {{ Form::hidden('id', $tank->id) }}
                    {{ Form::hidden('qty') }}
                    {{ Form::hidden('size') }}
                    {{ Form::hidden('oil_type') }}
                    <td>{{ Form::text('tanks_cost_per', $tank->tanks_cost_per,['class' => 'form-control'])}}</td>
                    <td class="originallot">{{ $tank->lot }}</td>
                    <td>{{ Form::submit('Update', ['class' => 'btn btn-update']) }}</td>
                </tr>
                {{ Form::close() }}
            @endforeach
            <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><strong>
            </strong></td>
            </tr>
            </tbody>
        </table>
        <ul class="pagination"></ul>
    </div>
@stop
{{--end content--}}


{{--footer--}}
@section('footer')
    <script type="text/javascript">
        var options = {
            valueNames: [ 'lot', 'so', 'originallot', 'oiltype', 'supplier', 'origin', 'qty', 'size', 'customer', 'date'],
            // pagination
            // page: 10,
            plugins: [
                ListPagination({})
            ]
        };

        var lotlogList = new List('lotlogs', options);
    </script>
@stop