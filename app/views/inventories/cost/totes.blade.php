@extends('layouts.admin')

@section('content')

    <div id="lotlogs">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    totes
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/inventories">Inventory</a> > <a href="/inventories/product/totes">Totes</a>
                    </li>
                </ol>
            </div>
        </div>

        {{--sessions--}}
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif

        <table class="table table-stripped table-responsive table-hover">
            <thead>
            <tr>
                <td><strong>Size</strong></td>
                <td><strong>Description</strong></td>
                <td><strong>Cost Per</strong></td>
                <td><strong>Lot#</strong></td>
                <td>
                  <strong>Area</strong>
                </td>
                <td></td>
            </tr>
            </thead>
            <tbody class="list">

            @foreach($totes as $tote)
              @if($tote->discontinued != 1)

                <tr>
                    <td class="lot">{{ $tote->size }}</td>
                    <td class="so">{{ $tote->oil_type }}</td>
                    {{ Form::model($tote, array('method' => 'PATCH', 'route' => array('toteupdates.update', $tote->id))) }}
                    {{ Form::hidden('id', $tote->id) }}
                    {{ Form::hidden('update', 'true') }}
                    {{ Form::hidden('qty') }}
                    {{ Form::hidden('size') }}
                    {{ Form::hidden('oil_type') }}
                    {{ Form::hidden('lot') }}
                    {{ Form::hidden('area') }}
                    {{ Form::hidden('expiration_date') }}

                    <td>{{ Form::text('totes_cost_per', $tote->totes_cost_per,['class' => 'form-control'])}}</td>
                    <td class="originallot">{{ $tote->lot }}</td>
                    <td>
                      {{ $tote->area }}
                    </td>
                    <td>{{ Form::submit('Update', ['class' => 'btn btn-update']) }}</td>
                </tr>
                {{ Form::close() }}
              @endif
            @endforeach

            <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><strong>
            </strong></td>
            </tr>

            </tbody>
        </table>


        <ul class="pagination"></ul>
    </div>
@stop

{{--end content--}}


{{--footer--}}
@section('footer')
    <script type="text/javascript">
        var options = {
            valueNames: [ 'lot', 'so', 'originallot', 'oiltype', 'supplier', 'origin', 'qty', 'size', 'customer', 'date'],
            // pagination
            // page: 10,
            plugins: [
                ListPagination({})
            ]
        };

        var lotlogList = new List('lotlogs', options);
    </script>
@stop
