@extends('layouts.admin')

@section('content')

    <div id="inventory">
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Inventory History <small>Statistics Overview</small>
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> Dashboard
                    </li>
                </ol>
            </div>
        </div>

        <div class="row">


        


        </div>
      {{--sub nav--}}
    <div class="row">
        <div class="col-lg-12">
            <nav class="navbar navbar-default">

                {{ link_to('inventories/', 'Inventory', ['class' => 'btn btn-success navbar-btn']) }}

                
            </nav>
        </div>
    </div>


<h2>This and Last Month Total Inventory Counts</h2>
<!-- chart -->
<div id="perf_div"></div>

 <?php echo $lava->render('AreaChart', 'History', 'perf_div') ?>
 <!-- end chart -->
    <pre>
    <strong>This Month Count- {{ $month->thisMonthCount() }}</strong> | <strong>Last Month Count {{ $month->lastMonthCount() }}</strong>
    </pre>
 <!-- more history -->
<div class="row">
            <div class="list">
                <div class="product">
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                    <h3 class="product">Drum History</h3>
                    <hr />
                        <div class="row">
                            <div class="col-xs-5">
                            <div class="huge">  {{ number_format($count->count_drums(), 3) }}</div>
                                <div>This Month</div>
                            </div>
                            <div class="col-xs-7 text-right">
                                <div class="huge">{{ number_format($month->last_month_drums(), 3) }}</div>
                                <div>Last Month</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/drums">
                        <div class="panel-footer">
                            <span class="pull-left">View All Drums</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
                </div>
                </div>

            <div class="list">
                <div class="product">
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-green">
                    <div class="panel-heading">
                        <h3 class="product">Totes</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="huge">{{ number_format($count->count_totes(), 3) }}</div>
                                <div>This Month</div>
                            </div>
                            <div class="col-xs-7 text-right">
                                <div class="huge">{{ number_format($month->last_month_totes(), 3) }}</div>
                                <div>Last Month</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/totes">
                        <div class="panel-footer">
                            <span class="pull-left">View All Totes</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
</div>
            </div>

            <div class="list">
                <div class="product">
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        <h3 class="product">Organic</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="huge">{{ number_format($count->count_organics(), 3) }}</div>
                                <div>This Month</div>
                            </div>
                            <div class="col-xs-7 text-right">
                                <div class="huge">{{ number_format($month->last_month_organics(), 3) }}</div>
                                <div>Last Month</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/organic">
                        <div class="panel-footer">
                            <span class="pull-left">View All Organics</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
</div>
                </div>
            <div class="list">
                <div class="product">
            <div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <h3 class="product">Tanks</h3>
                            <hr />
                            <div class="row">
                                <div class="col-xs-5">
                                    <div class="huge">{{ number_format($count->count_tanks(), 3) }}</div>
                                    <div>This Month</div>
                                </div>
                                <div class="col-xs-7 text-right">
                                    <div class="huge">{{ number_format($month->last_month_tanks(), 3) }}</div>
                                    <div>Last Month</div>
                                </div>
                            </div>
                        </div>
                        <a href="/inventories/product/tanks">
                            <div class="panel-footer">
                                <span class="pull-left">View All Tanks</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                </div>
                </div>
                </div>

                <div class="list">
<div class="row">

            <div class="col-lg-3 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="product">Vinegars</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="huge">{{ number_format($count->count_vinegars(), 3) }}</div>
                                <div>This Month</div>
                            </div>
                            <div class="col-xs-7 text-right">
                                <div class="huge">{{ number_format($month->last_month_vinegars(), 3) }}</div>
                                <div>Last Month</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/vinegars">
                        <div class="panel-footer">
                            <span class="pull-left">View All Vinegars</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>


            <div class="col-lg-3 col-md-6">
                <div class="panel panel-green">
                    <div class="panel-heading">
                        <h3 class="product">Railcars</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="huge">{{ number_format($count->count_railcars(), 3) }}</div>
                                <div>This Month</div>
                            </div>
                            <div class="col-xs-7 text-right">
                                <div class="huge">{{ number_format($month->last_month_railcars(), 3) }}</div>
                                <div>Last Month</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/railcars">
                        <div class="panel-footer">
                            <span class="pull-left">View All Railcars</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>


            <div class="col-lg-3 col-md-6">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        <h3 class="product">Specialty</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="huge">{{ number_format($count->count_specialties(), 3) }}</div>
                                <div>This Month</div>
                            </div>
                            <div class="col-xs-7 text-right">
                                <div class="huge">{{ number_format($month->last_month_specialties(), 3) }}</div>
                                <div>Last Month</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/specialty">
                        <div class="panel-footer">
                            <span class="pull-left">View All Specialties</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>


            <div class="col-lg-3 col-md-6">
                <div class="panel panel-red">
                    <div class="panel-heading">
                        <h3 class="product">Infused Oil Drums</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="huge">{{ number_format($count->count_infused_oil_drums(), 3) }}</div>
                                <div>This Month</div>
                            </div>
                            <div class="col-xs-7 text-right">
                                <div class="huge">{{ number_format($month->last_month_infused_oil_drums(), 3)}}</div>
                                <div>Last Month</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/infused_oil_drums">
                        <div class="panel-footer">
                            <span class="pull-left">View All Infused Oil Drums</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>

        </div>
        </div>
</div>

<div class="list">
<div class="row">

            <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="product">Spreads and Olives</h3>
                    <hr />
                    <div class="row">
                        <div class="col-xs-5">
                            <div class="huge">{{ number_format($count->count_spreads_and_olives(), 3) }}</div>
                            <div>This Month</div>
                        </div>
                        <div class="col-xs-7 text-right">
                            <div class="huge">{{ number_format($month->last_month_spreads_and_olives(), 3) }}</div>
                            <div>Last Month</div>
                        </div>
                    </div>
                </div>
                <a href="/inventories/product/spreads_and_olives">
                    <div class="panel-footer">
                        <span class="pull-left">View All Spreads and Olives</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>


            <div class="col-lg-3 col-md-6">
                <div class="panel panel-green">
                    <div class="panel-heading">
                        <h3 class="product">Website Olive Oil</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="huge">{{ number_format($count->count_website_olive_oils(), 3) }}</div>
                                <div>This Month</div>
                            </div>
                            <div class="col-xs-7 text-right">
                                <div class="huge">{{ number_format($month->last_month_website_olive_oils(), 3) }}</div>
                                <div>Last Month</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/website_olive_oils">
                        <div class="panel-footer">
                            <span class="pull-left">View All Website Olive Oils</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>


            <div class="col-lg-3 col-md-6">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        <h3 class="product">Web Flavor and Infused</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="huge">{{ number_format($count->count_web_flavor_and_infused(), 3) }}</div>
                                <div>This Month</div>
                            </div>
                            <div class="col-xs-7 text-right">
                                <div class="huge">{{ number_format($month->last_month_web_flavor_and_infused(), 3) }}</div>
                                <div>Last Month</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/web_flavor_and_infused">
                        <div class="panel-footer">
                            <span class="pull-left">View All Web Flavor and Infused</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>


            <div class="col-lg-3 col-md-6">
        <div class="panel panel-red">
            <div class="panel-heading">
                <h3 class="product">Website Vinegars</h3>
                <hr />
                <div class="row">
                    <div class="col-xs-5">
                        <div class="huge">{{ number_format($count->count_website_vinegars(), 3) }}</div>
                        <div>This Month</div>
                    </div>
                    <div class="col-xs-7 text-right">
                        <div class="huge">{{ number_format($month->last_month_website_vinegars(), 3) }}</div>
                        <div>Last Month</div>
                    </div>
                </div>
            </div>
            <a href="/inventories/product/website_vinegars">
                <div class="panel-footer">
                    <span class="pull-left">View All Website Vinegars</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>

        </div>
        </div>
</div>

<div class="list">
<div class="row">

            <div class="col-lg-3 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="product">Website Other</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="huge">{{ number_format($count->count_website_other(), 3) }}</div>
                                <div>This Month</div>
                            </div>
                            <div class="col-xs-7 text-right">
                                <div class="huge">{{ number_format($month->last_month_website_other(), 3) }}</div>
                                <div>Last Month</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/website_other">
                        <div class="panel-footer">
                            <span class="pull-left">View All Website Other</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>


            <div class="col-lg-3 col-md-6">
                <div class="panel panel-green">
                    <div class="panel-heading">
                        <h3 class="product">Website Totals</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="huge">{{ number_format($count->count_website_totals(), 3) }}</div>
                                <div>This Month</div>
                            </div>

                            <div class="col-xs-7 text-right">
                                <div class="huge"></div>
                                <div>Last Month</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/website_totals">
                        <div class="panel-footer">
                            <span class="pull-left">View All Website Totals</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>


            <div class="col-lg-3 col-md-6">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        <h3 class="product">Finished Product</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="huge">{{ number_format($count->count_finished_product(), 3) }}</div>
                                <div>This Month</div>
                            </div>
                            <div class="col-xs-7 text-right">
                                <div class="huge">{{ number_format($month->last_month_finished_product(), 3) }}</div>
                                <div>Last Month</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/finished_product">
                        <div class="panel-footer">
                            <span class="pull-left">View All Finished Product</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>


            <div class="col-lg-3 col-md-6">
                <div class="panel panel-red">
                    <div class="panel-heading">
                        <h3 class="product">Sprays</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="huge">{{ number_format($count->count_sprays(), 3) }}</div>
                                <div>This Month</div>
                            </div>
                            <div class="col-xs-7 text-right">
                                <div class="huge">{{ number_format($month->last_month_sprays(), 3) }}</div>
                                <div>Last Month</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/sprays">
                        <div class="panel-footer">
                            <span class="pull-left">View All Sprays</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        </div>

        <div class="list">
<div class="row">

            <div class="col-lg-3 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="product">Ready To Ship</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="huge">{{ number_format($count->count_ready_to_ship(), 3) }}</div>
                                <div>This Month</div>
                            </div>
                            <div class="col-xs-7 text-right">


                                <div class="huge">{{ number_format($month->last_month_ready_to_ship(), 3) }}</div>
                                <div>Last Month</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/ready_to_ship">
                        <div class="panel-footer">
                            <span class="pull-left">View All Ready To Ship</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>


            <div class="col-lg-3 col-md-6">
                <div class="panel panel-green">
                    <div class="panel-heading">
                        <h3 class="product">Rework</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="huge">{{ number_format($count->count_rework(), 3) }}</div>
                                <div>This Month</div>
                            </div>
                            <div class="col-xs-7 text-right">
                                <div class="huge">{{ number_format($month->last_month_rework(), 3) }}</div>
                                <div>Last Month</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/rework">
                        <div class="panel-footer">
                            <span class="pull-left">View All Rework</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>


            <div class="col-lg-3 col-md-6">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        <h3 class="product">Labels In House</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="huge">{{ number_format($count->count_labels_in_house(), 3)}}</div>
                                <div>This Month</div>
                            </div>
                            <div class="col-xs-7 text-right">
                                <div class="huge">{{ number_format($month->last_month_labels_in_house(), 3) }}</div>
                                <div>Last Month</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/labels_in_house">
                        <div class="panel-footer">
                            <span class="pull-left">View All Labels In House</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>


            <div class="col-lg-3 col-md-6">
                <div class="panel panel-red">
                    <div class="panel-heading">
                        <h3 class="product">Labels in Labelroom</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="huge">{{ number_format($count->count_labels_in_labelroom(), 3) }}</div>
                                <div>This Month</div>
                            </div>
                            <div class="col-xs-7 text-right">
                                <div class="huge">{{ number_format($month->last_month_labels_in_labelroom(), 3) }}</div>
                                <div>Last Month</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/labels_in_labelroom">
                        <div class="panel-footer">
                            <span class="pull-left">View All Labels in Labelroom</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>

        </div>
<!--end row-->

 <!--row-->
            <div class="list">
<div class="row">

            <div class="col-lg-3 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="product">Customer Labels</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="huge">{{ number_format($count->count_customers_labels(), 3) }}</div>
                                <div>This Month</div>
                            </div>
                            <div class="col-xs-7 text-right">
                                <div class="huge">{{ number_format($month->last_month_customers_labels(), 3) }}</div>
                                <div>Last Month</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/customers_labels">
                        <div class="panel-footer">
                            <span class="pull-left">View All Customer Labels</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>


            <div class="col-lg-3 col-md-6">
                <div class="panel panel-green">
                    <div class="panel-heading">
                        <h3 class="product">Bottles</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="huge">{{ number_format($count->count_bottles(), 3) }}</div>
                                <div>This Month</div>
                            </div>
                            <div class="col-xs-7 text-right">
                                <div class="huge">{{ number_format($month->last_month_bottles(), 3) }}</div>
                                <div>Last Month</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/bottles">
                        <div class="panel-footer">
                            <span class="pull-left">View All Bottles</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>


            <div class="col-lg-3 col-md-6">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        <h3 class="product">Etched Bottles</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="huge">{{ number_format($count->count_etched_bottles(), 3) }}</div>
                                <div>This Month</div>
                            </div>
                            <div class="col-xs-7 text-right">
                                <div class="huge">{{ number_format($month->last_month_etched_bottles(), 3) }}</div>
                                <div>Last Month</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/etched_bottles">
                        <div class="panel-footer">
                            <span class="pull-left">View All Etched Bottles</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>


            <div class="col-lg-3 col-md-6">
                <div class="panel panel-red">
                    <div class="panel-heading">
                        <h3 class="product">Fustis</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="huge">{{ number_format($count->count_fustis(), 3) }}</div>
                                <div>This Month</div>
                            </div>
                            <div class="col-xs-7 text-right">
                                <div class="huge">{{ number_format($month->last_month_fustis(), 3) }}</div>
                                <div>Last Month</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/fustis">
                        <div class="panel-footer">
                            <span class="pull-left">View All Fustis</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>

        </div>
                </div>
<!--end row-->

 <!--row-->
            <div class="list">
<div class="row">

            <div class="col-lg-3 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="product">Caps</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="huge">{{ number_format($count->count_caps(), 3) }}</div>
                                <div>This Month</div>
                            </div>
                            <div class="col-xs-7 text-right">
                                <div class="huge">{{ number_format($month->last_month_caps(), 3) }}</div>
                                <div>Last Month</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/caps">
                        <div class="panel-footer">
                            <span class="pull-left">View All Caps</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>


            <div class="col-lg-3 col-md-6">
                <div class="panel panel-green">
                    <div class="panel-heading">
                        <h3 class="product">Cardboard Blank</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="huge">{{ number_format($count->count_cardboard_blank(), 3) }}</div>
                                <div>This Month</div>
                            </div>
                            <div class="col-xs-7 text-right">
                                <div class="huge">{{ number_format($month->last_month_cardboard_blank(), 3) }}</div>
                                <div>Last Month</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/cardboard_blank">
                        <div class="panel-footer">
                            <span class="pull-left">View All Cardboard Blank</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>

    <div class="col-lg-3 col-md-6">
        <div class="panel panel-green">
            <div class="panel-heading">
                <h3 class="product">Cardboard Printed</h3>
                <hr />
                <div class="row">
                    <div class="col-xs-5">
                        <div class="huge">{{ number_format($count->count_cardboard_printed(), 3) }}</div>
                        <div>This Month</div>
                    </div>
                    <div class="col-xs-7 text-right">
                        <div class="huge">{{ number_format($month->last_month_cardboard_printed(), 3) }}</div>
                        <div>Last Month</div>
                    </div>
                </div>
            </div>
            <a href="/inventories/product/cardboard_printed">
                <div class="panel-footer">
                    <span class="pull-left">View All Cardboard Printed</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>


            <div class="col-lg-3 col-md-6">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        <h3 class="product">Supplies</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="huge">{{ number_format($count->count_supplies(), 3) }}</div>
                                <div>This Month</div>
                            </div>
                            <div class="col-xs-7 text-right">
                                <div class="huge">{{ number_format($month->last_month_supplies(), 3) }}</div>
                                <div>Last Month</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/supplies">
                        <div class="panel-footer">
                            <span class="pull-left">View All Supplies</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>




        </div>
                </div>
<!--end row-->

 <!--row-->
            <div class="list">
<div class="row">

    <div class="col-lg-3 col-md-6">
        <div class="panel panel-red">
            <div class="panel-heading">
                <h3 class="product">Materials</h3>
                <hr />
                <div class="row">
                    <div class="col-xs-5">
                        <div class="huge">{{ number_format($count->count_materials(), 3) }}</div>
                        <div>This Month</div>
                    </div>
                    <div class="col-xs-7 text-right">
                        <div class="huge">{{ number_format($month->last_month_materials(), 3) }}</div>
                        <div>Last Month</div>
                    </div>
                </div>
            </div>
            <a href="/inventories/product/materials">
                <div class="panel-footer">
                    <span class="pull-left">View All Materials</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>

            <div class="col-lg-3 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="product">Follmer</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="huge">{{ number_format($count->count_follmer(), 3) }}</div>
                                <div>This Month</div>
                            </div>
                            <div class="col-xs-7 text-right">
                                <div class="huge">{{ number_format($month->last_month_follmer(), 3) }}</div>
                                <div>Last Month</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/follmer">
                        <div class="panel-footer">
                            <span class="pull-left">View All Follmer</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>


            <div class="col-lg-3 col-md-6">
                <div class="panel panel-green">
                    <div class="panel-heading">
                        <h3 class="product">Flavors</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="huge">{{ number_format($count->count_flavors(), 3) }}</div>
                                <div>This Month</div>
                            </div>
                            <div class="col-xs-7 text-right">
                                <div class="huge">{{ number_format($month->last_month_flavors(), 3) }}</div>
                                <div>Last Month</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/flavors">
                        <div class="panel-footer">
                            <span class="pull-left">View All Flavors</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>


            <div class="col-lg-3 col-md-6">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        <h3 class="product">Flavors For Samples</h3>
                        <hr />
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="huge">{{ number_format($count->count_flavors_for_samples(), 3) }}</div>
                                <div>This Month</div>
                            </div>
                            <div class="col-xs-7 text-right">
                                <div class="huge">{{ number_format($month->last_month_flavors_for_samples(), 3) }}</div>
                                <div>Last Month</div>
                            </div>
                        </div>
                    </div>
                    <a href="/inventories/product/flavors_for_samples">
                        <div class="panel-footer">
                            <span class="pull-left">View All Flavors For Samples</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>

            
        </div>
                </div>


<!--end row-->

    </div>



@stop

{{--footer--}}
@section('footer')
    <script type="text/javascript">
        var options = {
            valueNames: [ 'product'],
            // pagination
//            page: 10,
            plugins: [
                ListPagination({})
            ]
        };

        var inventoryList = new List('inventory', options);
    </script>
@stop