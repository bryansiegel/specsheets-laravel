@extends('layouts.admin')

@section('content')

    <div id="nutritionfacts">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Nutrition Facts
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > Nutrition Facts
                    </li>
                </ol>
            </div>
        </div>

        <!--sessions-->
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif

        <!--sub nav-->
        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">
                    {{ link_to_route('nutritionfacts.create', 'Create A New Nutrition Fact',null, ['class' => 'btn btn-primary navbar-btn']) }}
                    <!--search-->

                    <!--<form class="navbar-form navbar-right" role="search">
                        <div class="form-group">
                            <input type="text" class=" search form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-primary" data-sort="nutritionfact">Submit</button>
                    </form>-->
                </nav>
            </div>
        </div>

        <table id="datatable" class="table table-stripped">
            <thead>
            <tr>
                <td><strong>Title</strong></td>
                <td><strong>Active?</strong></td>
                <td><strong>Category</strong></td>
                <td class="no-sort"></td>
            </tr>
            </thead>
            <tbody class="list">
            @foreach($nutritionfacts as $nutritionfact)
                <tr>
                    <td class="nutritionfact">{{ $nutritionfact->title }}</td>
                    <td>
                        @if($nutritionfact->active === "1")
                            Active
                            @elseif($nutritionfact->active === "2")
                            Disabled
                            @else

                            @endif
                    </td>
                    <td class="category">
                        @if($nutritionfact->category === "olive_oils")
                            Olive Oils
                            @elseif($nutritionfact->category === "vinegars")
                            Vinegars
                            @elseif($nutritionfact->category === "pantry_items")
                            Pantry Items
                            @elseif($nutritionfact->category === "25_star_white")
                            25 Star White Balsamic Vinegar
                            @elseif($nutritionfact->category === "25_star_dark")
                            25 Star Balsamic Vinegar
                            @else

                        @endif
                    </td>
                    <td class="no-sort">
                        <div class="btn-group" role="group" aria-label="">
                            {{ link_to_route('nutritionfacts.show', 'View', [$nutritionfact->id],['class' => 'btn btn-success']) }}
                            @if(Auth::user()->group == 100 || Auth::user()->group == 50)
                            {{ link_to_route('nutritionfacts.edit', 'Edit', [$nutritionfact->id],['class' => 'btn btn-warning']) }}
                            {{ Form::open(array('method' => 'DELETE', 'style' => 'display:inline','route' => array('nutritionfacts.destroy', $nutritionfact->id))) }}
                            {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                            {{ Form::close() }}
                                @endif
                        </div>
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>


        <ul class="pagination"></ul>
    </div>
@stop

<!--end content-->


<!--footer-->
@section('footer')
   <script>
          $(function() {
            $( "#datepicker" ).datepicker();
            $( "#datepicker1" ).datepicker();
            $('#timepicker').timepicker();
            $('#datatable').DataTable({
                paging:true,
                "order": [[ 2, "desc" ]],
                // scrollY:400,
                deferRender:true,
                // scroller:true,
                columnDefs: [
                                { targets: 'no-sort', orderable: false }
                            ],

            });
        });
    </script>

    @stop

