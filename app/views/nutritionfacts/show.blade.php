@extends('layouts.admin')

@section('content')

    <div id="blends">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">View</span>Nutrition Facts
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/nutritionfacts/">Nutrition Facts</a> >
                        View
                    </li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">
                    <a href="{{ url() . "/public/nutritionfacts/" . $nutritionfact->id }}" class="btn btn-primary navbar-btn">View Public Nutrition Fact</a>

                </nav>
            </div>
        </div>


        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <strong>Title:</strong> {{ $nutritionfact->title }}
                            </li>
                            <li class="list-group-item">
                                @if($nutritionfact->active == 1)
                                    <strong>Active</strong>
                                    @elseif($nutritionfact->active == 2)
                                    <strong>Not Active</strong>
                                @endif
                                <strong>Serving size:</strong> {{ $nutritionfact->serving_size }}
                            </li>
                            <li class="list-group-item">
                                <strong>Servings per container:</strong> {{ $nutritionfact->servings_per_container }}
                            </li>
                            <li class="list-group-item">
                                <strong>Calories:</strong> {{ $nutritionfact->calories }}
                            </li>
                            <li class="list-group-item">
                                <strong>Calories from fat:</strong> {{ $nutritionfact->calories_from_fat }}
                            </li>
                            <li class="list-group-item">
                                <strong>Total fat grams:</strong> {{ $nutritionfact->total_fat_grams }}
                            </li>
                            <li class="list-group-item">
                                <strong>Total fat percent:</strong> {{ $nutritionfact->total_fat_percent }}
                            </li>
                            <li class="list-group-item">
                                <strong>Saturated fat grams:</strong> {{ $nutritionfact->saturated_fat_grams }}
                            </li>
                            <li class="list-group-item">
                                <strong>Saturated fat percentage:</strong> {{ $nutritionfact->saturated_fat_percentage }}
                            </li>
                            <li class="list-group-item">
                                <strong>Trans fat:</strong> {{ $nutritionfact->trans_fat }}
                            </li>
                            <li class="list-group-item">
                                <strong>Cholesterol grams:</strong> {{ $nutritionfact->cholesterol_grams}}
                            </li>
                            <li class="list-group-item">
                                <strong>Cholesterol percentage:</strong> {{ $nutritionfact->cholesterol_percentage}}
                            </li>
                            <li class="list-group-item">
                                <strong>Sodium grams:</strong> {{ $nutritionfact->sodium_grams}}
                            </li>
                            <li class="list-group-item">
                                <strong>Sodium percentage:</strong> {{ $nutritionfact->sodium_percentage}}
                            </li>
                            <li class="list-group-item">
                                <strong>Total carbohydrates grams:</strong> {{ $nutritionfact->total_carbohydrates_grams}}
                            </li>
                            <li class="list-group-item">
                                <strong>Total carbohydrates percentage:</strong> {{ $nutritionfact->total_carbohydrates_percentage}}
                            </li>
                            <li class="list-group-item">
                                <strong>Dietary fiber grams:</strong> {{ $nutritionfact->dietary_fiber_grams}}
                            </li>
                            <li class="list-group-item">
                                <strong>Dietary fiber percentage:</strong> {{ $nutritionfact->dietary_fiber_percentage}}
                            </li>
                            <li class="list-group-item">
                                <strong>Sugars grams:</strong> {{ $nutritionfact->sugars_grams}}
                            </li>
                            <li class="list-group-item">
                                <strong>Added Sugars:</strong> {{ $nutritionfact->added_sugars }}
                            </li>
                            <li class="list-group-item">
                                <strong>Protein grams:</strong> {{ $nutritionfact->protein_grams}}
                            </li>
                            <li class="list-group-item">
                                <strong>Vitamin D grams: {{ $nutritionfact->vitamin_d_grams}}</strong>
                            </li>
                            <li class="list-group-item">
                                <strong>Vitamin D percentage: {{ $nutritionfact->vitamin_d_percentage}}</strong>
                            </li>
                            <li class="list-group-item">
                                <strong>Calcium grams: {{ $nutritionfact->calcium_grams }}</strong>
                            </li>
                            <li class="list-group-item">
                                <strong>Calcium percentage: {{ $nutritionfact->calcium_percentage }}</strong>
                            </li>
                            <li class="list-group-item">
                                <strong>Iron grams: {{ $nutritionfact->iron_grams }}</strong>
                            </li>
                            <li class="list-group-item">
                                <strong>Iron percentage: {{ $nutritionfact->iron_percentage }}</strong>
                            </li>
                            <li class="list-group-item">
                                <strong>Potassium grams: {{ $nutritionfact->potassium_grams }}</strong>
                            </li>
                            <li class="list-group-item">
                                <strong>Potassium percentage: {{ $nutritionfact->potassium_percentage }}</strong>
                            </li>
{{--                             <li class="list-group-item">
                                <strong>Blurb:</strong> {{ $nutritionfact->blurb}}
                            </li> --}}
                            <li class="list-group-item">
                                <strong>Ingredients:</strong> {{ $nutritionfact->ingredients}}
                            </li>
                        </ul>

                        <hr/>
                        <div class="alert alert-dismissible alert-success" style="margin-bottom:0px">
                            <strong>Created: </strong><?php echo \Carbon\Carbon::createFromTimestamp(strtotime($nutritionfact->created_at))->diffForHumans(); ?>
                        </div>
                        <div class="alert alert-dismissible alert-warning">
                            <strong>Updated: </strong><?php echo \Carbon\Carbon::createFromTimestamp(strtotime($nutritionfact->updated_at))->diffForHumans(); ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
@stop