@extends('layouts.admin')

@section('content')
    <div id="receivingappointmentlogs">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Create</span> A New Nutrition Fact
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/nutritionfacts/">Nutrition Facts</a> >
                        Create
                    </li>
                </ol>
            </div>
        </div>

        {{--errors--}}
        @if($errors->has())
            <div class="alert alert-dismissible alert-danger">
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
    @endif

        {{--form--}}
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        {{ Form::open(['route' => 'nutritionfacts.store', 'class' => 'form-horizontal']) }}
                        <fieldset>

                            <script>
                                $(document).ready(function () {
                                    $('.defaultnutrition').hide();

                                    $('#form_category').change(function () {
                                        $(this).find("option").each(function () {
                                            $("#" + $(this).val()).hide();
                                            console.log($(this));
                                        });
                                        $("#" + $(this).val()).show();
                                        // hide input fields when they are hidden
                                        $("div").filter(":hidden").children("input[type='text']").attr("disabled", "disabled");
                                        $("div").filter(":hidden").children("textarea[class='form-control']").attr("disabled", "disabled");

                                    });
                                });
                            </script>

                            {{--category dropdown--}}
                            @include('nutritionfacts.form._category')
                            {{--end cateogry--}}

                            {{--blank--}}
                            <div id="blank" class="defaultnutrition">
                                @include('nutritionfacts.form._main')
                            </div>

                            {{--vinegars--}}
                            <div id="vinegars" class="defaultnutrition">
                                @include('nutritionfacts.form.default._vinegars')
                            </div>

                            {{--25 star dark--}}
                            <div id="25_star_dark" class="defaultnutrition">
                                @include('nutritionfacts.form.default._25_star_dark')
                            </div>

                            {{--pantry items--}}
                            <div id="pantry_items" class="defaultnutrition">
                                @include('nutritionfacts.form.default._pantry')
                            </div>

                            {{--olive oils--}}
                            <div id="olive_oils" class="defaultnutrition">
                                @include('nutritionfacts.form.default._olive_oils')
                            </div>

                            {{--25 star white--}}
                            <div id="25_star_white" class="defaultnutrition">
                                @include('nutritionfacts.form.default._25_star_white')
                            </div>
                        </fieldset>
                        {{ Form::close() }}

                    </div><!--end row-->
                </div>
            </div><!--end col lg 8 -->
        </div><!--end container fluid-->



    </div><!--end blends-->
    @stop