@extends('layouts.admin')

@section('content')

    <div id="lotlogs">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Material Lot Logs
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> >Material Lot Logs
                    </li>
                </ol>
            </div>
        </div>

        {{--sessions--}}
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif

        {{--sub nav--}}
        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">

                    {{ link_to_route('materiallotlogs.create', 'Create A New Material Lot Log',null, ['class' => 'btn btn-primary navbar-btn']) }}
                    {{--search

                    <form class="navbar-form navbar-right" role="search">
                        <div class="form-group">
                            <input type="text" class=" search form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-primary" data-sort="nutritionfact">Submit</button>
                    </form>--}}
                </nav>
            </div>
        </div>

        <table id="datatable" class="table table-stripped">
            <thead>
            <tr>
                <th><strong>id</strong></th>
                <th><strong>Lot</strong></th>
                <th><strong>Old Lot</strong></th>
                <th><strong>Supplier</strong></th>
                {{-- <td><strong>SO#</strong></td> --}}
                <td><strong>Description</strong></td>
                {{-- <td><strong>Origin</strong></td> --}}
                {{-- <td><strong>QTY</strong></td> --}}
                {{-- <td><strong>Size</strong></td> --}}
                {{-- <td><strong>Customer</strong></td> --}}
                <th><strong>Date</strong></th>
                <th class="no-sort"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($materiallotlogs as $materiallotlogs)
            
                <tr>
                    <td class="id">{{ $materiallotlogs->id }}</td>
                    <td class="lot">{{ $materiallotlogs->lot }}</td>
                    <td class="originallot">{{ $materiallotlogs->original_lot }}</td>
                    <td class="supplier">{{ $materiallotlogs->supplier }}</td>
                    {{-- <td class="so">{{ $materiallotlogs->so }}</td> --}}
                    <td class="oiltype">{{ $materiallotlogs->oil_type }}</td>
                    {{-- <td class="origin">{{ $materiallotlogs->origin }}</td> --}}
                    {{-- <td class="qty">{{ $materiallotlogs->qty }}</td> --}}
                    {{-- <td class="size">{{ $materiallotlogs->size }}</td> --}}
                    {{-- <td class="customer">{{ $materiallotlogs->customer }}</td> --}}
                    <td class="date">
                    @if(empty($materiallotlogs->date))
                        N/A
                        @else
                        {{ $materiallotlogs->date }}
                    @endif
                    </td>

                    <td>
                        <div class="btn-group" role="group" aria-label="">
                            {{ link_to_route('materiallotlogs.show', 'View', [$materiallotlogs->id],['class' => 'btn btn-success']) }}
                                <a href="/materiallotlogs/label/{{ $materiallotlogs->id}}" class="btn btn-primary">View Label</a>
                            
                            @if(Auth::user()->group == 100 || Auth::user()->group == 50)
                                {{ link_to_route('materiallotlogs.edit', 'Edit', [$materiallotlogs->id],['class' => 'btn btn-warning']) }}
                                {{ Form::open(array('method' => 'DELETE', 'style' => 'display:inline','route' => array('materiallotlogs.destroy', $materiallotlogs->id))) }}
                                {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                                {{ Form::close() }}
                            @endif
                        </div>
                </td>
                </tr>

            @endforeach
            </tbody>
        </table>

    </div>
@stop

{{--end content--}}


{{--footer--}}
@section('footer')
   <script>
          $(function() {
            $( "#datepicker" ).datepicker();
            $( "#datepicker1" ).datepicker();
            $('#timepicker').timepicker();
            $('#datatable').DataTable({
                paging:true,
                "order": [[ 0, "desc" ]],
                // scrollY:400,
                deferRender:true,
                // scroller:true,
                columnDefs: [
                                { targets: 'no-sort', orderable: false }
                            ],

            });
        });
    </script>

    @stop
