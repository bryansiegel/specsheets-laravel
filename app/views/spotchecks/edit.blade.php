@extends('layouts.admin')

@section('content')
    <div id="spotchecks">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Edit</span> A Spotcheck Form
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/spotchecks/">Spotcheck</a> >
                        Edit
                    </li>
                </ol>
            </div>
        </div>

        {{--errors--}}
        @if($errors->has())
            <div class="alert alert-dismissible alert-danger">
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
        @endif

        {{--form--}}
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        {{ Form::model($spotcheck, array('method' => 'PATCH', 'route' =>
                                                       array('spotchecks.update', $spotcheck->id))) }}                           <fieldset>
                            <div class="form-group">
                                {{ Form::label('date', 'Date') }}
                                {{ Form::text('date', null,['class' => 'form-control','id' => 'datepicker', 'placeholder' => 'Date']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('description', 'Description') }}
                                {{ Form::text('description', null,['class' => 'form-control', 'placeholder' => 'Description']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('lot', 'Lot#') }}
                                {{ Form::text('lot', null,['class' => 'form-control', 'placeholder' => 'Lot']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('product_label', 'Product Label') }}
                                <div class="radio">
                                    <label>{{ Form::radio('product_label', 'OK',['class' => 'form-control']) }}OK</label>
                                </div>
                                <div class="radio">
                                    <label>{{ Form::radio('product_label', 'NOT OK',['class' => 'form-control']) }}NOT OK</label>
                                </div>
                                <div class="radio">
                                  <label>{{ Form::radio('product_label', 'N/A',['class' => 'form-control']) }}N/A</label>
                                </div>
                            </div>
                            <hr/>
                            <div class="form-group">
                                {{ Form::label('seals', 'Seals') }}
                                <div class="radio">
                                    <label>{{ Form::radio('seals', 'OK',['class' => 'form-control']) }}OK</label>
                                </div>
                                <div class="radio">
                                    <label>{{ Form::radio('seals', 'NOT OK',['class' => 'form-control']) }}NOT OK</label>
                                </div>
                                <div class="radio">
                                  <label>{{ Form::radio('seals','N/A',['class' => 'form-control']) }}N/A</label>
                                </div>
                            </div>
                            <hr/>
                            <div class="form-group">
                                {{ Form::label('exp_date', 'EXP. Date') }}
                                <div class="radio">
                                    <label>{{ Form::radio('exp_date', 'OK',['class' => 'form-control']) }}OK</label>
                                </div>
                                <div class="radio">
                                    <label>{{ Form::radio('exp_date', 'NOT OK',['class' => 'form-control']) }}NOT OK</label>
                                </div>
                                <div class="radio">
                                  <label>{{ Form::radio('exp_date','N//A',['class' => 'form-control']) }}N/A</label>
                                </div>
                            </div>
                            <hr/>
                            <div class="form-group">
                                {{ Form::label('box_label', 'Box Label') }}
                                <div class="radio">
                                    <label>{{ Form::radio('box_label', 'OK',['class' => 'form-control']) }}OK</label>
                                </div>
                                <div class="radio">
                                    <label>{{ Form::radio('box_label', 'NOT OK',['class' => 'form-control']) }}NOT OK</label>
                                </div>
                                <div class="radio">
                                  <label>{{ Form::radio('box_label', 'N/A',['class' => 'form-control']) }}N/A</label>
                                </div>
                            </div>
                            <hr/>
                            <div class="form-group">
                                {{ Form::label('required_weight', 'Required Weight') }}
                                {{ Form::textarea('required_weight', null,['cols' => '3', 'rows' => '3', 'class' => 'form-control', 'placeholder' => 'Required Weight']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('actual_weight', 'Actual Weight') }}
                                {{ Form::textarea('actual_weight', null,['cols' => '3', 'rows' => '3', 'class' => 'form-control', 'placeholder' => 'Actual Weight']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('by', 'By') }}
                                {{ Form::text('by', null,['class' => 'form-control', 'placeholder' => 'By']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('resolution', 'Resolution') }}
                                {{ Form::textarea('resolution', null,['cols' => '3', 'rows' => '3', 'class' => 'form-control', 'placeholder' => 'Resolution']) }}
                            </div>
                            {{ Form::submit('Update',['class' => 'btn btn-default', 'style' => 'float:right']) }}
                            {{ link_to_route('spotchecks.index', 'Cancel',null,['class' => 'btn btn-primary', 'style' => 'float:right;']) }}

                        </fieldset>
                        {{ Form::close() }}

                    </div><!--end row-->
                </div>
            </div><!--end col lg 8 -->
        </div><!--end container fluid-->



    </div><!--end blends-->
@stop
