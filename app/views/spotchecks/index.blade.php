@extends('layouts.admin')

@section('content')

    <div id="spotchecks">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Product Quality Spot Check Form
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > Spot Check Form
                    </li>
                </ol>
            </div>
        </div>

        {{--sessions--}}
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
            @endif

        {{--sub nav--}}
        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">
                    {{ link_to_route('spotchecks.create', 'Create A New Spot Check Form',null, ['class' => 'btn btn-primary navbar-btn']) }}
                    <a href="/spotchecks/print/" class="btn btn-success navbar-btn">Print</a>

                    {{--search
                    <form class="navbar-form navbar-right" role="search">
                        <div class="form-group">
                            <input type="text" class=" search form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-primary" data-sort="nutritionfact">Submit</button>
                    </form>--}}
                </nav>
            </div>
        </div>

        <table id="datatable" class="table table-stripped">
            <thead>
            <tr>
                <td><strong>Date</strong></td>
                <td><strong>Product Description</strong></td>
                <td><strong>Lot#</strong></td>
               {{--  <td><strong>Product Label</strong></td>
                <td><strong>Seals</strong></td>
                <td><strong>EXP. Date</strong></td>
                <td><strong>Box Label</strong></td>
                <td><strong>Required Weight</strong></td>
                <td><strong>Actual Weight</strong></td>
                <td><strong>By</strong></td>
                <td><strong>Resolution</strong></td> --}}
                <td class="no-sort"></td>
            </tr>
            </thead>
            <tbody class="list">
            @foreach($spotchecks as $spotcheck)
                <tr>
                    <td>{{ $spotcheck->date }}</td>
                    <td>{{ $spotcheck->description }}</td>
                    <td class="lot">{{ $spotcheck->lot }}</td>
                    {{-- <td>{{ $spotcheck->product_label }}</td>
                    <td>{{ $spotcheck->seals }}</td>
                    <td>{{ $spotcheck->exp_date }}</td>
                    <td>{{ $spotcheck->box_label }}</td>
                    <td>{{ $spotcheck->required_weight }}</td>
                    <td>{{ $spotcheck->actual_weight }}</td>
                    <td>{{ $spotcheck->by }}</td>
                    <td>{{ $spotcheck->resolution }}</td>
                     --}}<td>
                        <div class="btn-group" role="group" aria-label="">
                            {{ link_to_route('spotchecks.show', 'View', [$spotcheck->id],['class' => 'btn btn-success']) }}
                            {{ link_to_route('spotchecks.edit', 'Edit', [$spotcheck->id],['class' => 'btn btn-warning']) }}
                            {{ Form::open(array('method' => 'DELETE', 'style' => 'display:inline','route' => array('spotchecks.destroy', $spotcheck->id))) }}
                            {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                            {{ Form::close() }}
                        </div>
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>


        <ul class="pagination"></ul>
    </div>
@stop

{{--end content--}}


{{--footer--}}
@section('footer')
    <script>
          $(function() {
            $( "#datepicker" ).datepicker();
            $( "#datepicker1" ).datepicker();
            $('#timepicker').timepicker();
            $('#datatable').DataTable({
                paging:true,
                "order": [[ 0, "desc" ]],
                // scrollY:400,
                deferRender:true,
                // scroller:true,
                columnDefs: [
                                { targets: 'no-sort', orderable: false }
                            ],

            });
        });
    </script>
@stop
