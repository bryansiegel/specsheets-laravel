@extends('layouts.admin')

@section('content')

<div id="specsheets">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Customer Complaints > QC
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/customercomplaints">Customer Complaints</a> > QC
                </li>
            </ol>
        </div>
    </div>

    {{--sessions--}}
    @if(Session::has('message'))
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
    @endif

    {{--sub nav--}}
    <div class="row">
        <div class="col-lg-12">
            <nav class="navbar navbar-default">
              {{ link_to_route('customercomplaints.create', 'Create A New Customer Complaint',null, ['class' => 'btn btn-primary navbar-btn']) }}                {{--search--}}
              <a href="/customercomplaints/in_progress" class="btn btn-yellow navbar-btn">In Progress</a>
              <a href="/customercomplaints/complete" class="btn btn-success navbar-btn">Complete</a>

              <a href="/customercomplaints/customer_service" class="btn btn-customer-service navbar-btn">Customer Service</a>
              <a href="/customercomplaints/production" class="btn btn-production navbar-btn">Production</a>
              <a href="/customercomplaints/qc" class="btn btn-warning navbar-btn">Q.C.</a>
              <a href="/customercomplaints/technology" class="btn btn-technology navbar-btn">Technology</a>

                {{-- <form class="navbar-form navbar-right" role="search">
                    <div class="form-group">
                        <input type="text" class=" search form-control" placeholder="Search">
                    </div>
                    <button type="submit" class="btn btn-primary" data-sort="nutritionfact">Submit</button>
                </form> --}}
            </nav>
        </div>
    </div>

    <table class="table table-stripped table-responsive table-hover" id="datatable">
        <thead>
        <tr>
          <td><strong>Status</strong></td>
           <td><strong>Case Number</strong>
           <td><strong>Date Of Complaint</strong></td>
           <td><strong>Corrective Action By</strong></td>
           <td><strong>Company Name</strong></td>
           <td><strong>Customer Contact</strong></td>
           <td><strong>Order Number</strong></td>
           <td class="no-sort"></td>
        </tr>
        </thead>
        <tbody class="list">


        @foreach($qc as $customercomplaint)

        @if($customercomplaint->corrective_action_by == 'QC')
            @if($customercomplaint->status == 'in_progress')
            <tr style="background-color:#faff8c;font-weight:bold;">
            @elseif($customercomplaint->status == 'complete')
            <tr style="background-color:#abcca1;font-weight:bold;">
            @else
              <tr>
            @endif
            <td class="status">
              @if($customercomplaint->status == 'in_progress')
                In Progress
              @elseif($customercomplaint->status == 'complete')
                Complete
              @endif

            </td>
            <td class="casenumber">
            <?php $customer_number = $customercomplaint->id + 200; ?>
            {{ $customer_number }}
            </td>
            <td>{{ $customercomplaint->date_of_complaint }}</td>
            @if($customercomplaint->corrective_action_by == 'QC')
            <td style="background: orange;border:3px solid black; color: white;">
            @elseif($customercomplaint->corrective_action_by == 'Production')
            <td style="background: brown;border:3px solid black;color: white">
            @elseif($customercomplaint->corrective_action_by == 'Technology')
            <td style="background: grey;border:3px solid black; color: white;">
            @elseif($customercomplaint->corrective_action_by == 'Customer Service')
            <td style="background: pink; border: 3px solid black">
            @else
            <td>
            @endif
            {{ $customercomplaint->corrective_action_by }}</td>

             <td class="isdone">{{ $customercomplaint->customer_name }}</td>
             <td>{{ $customercomplaint->customer_contact }}</td>
             <td class="ordernumber">{{ $customercomplaint->order_number }}</td>
             <td class="no-sort">
                 <div class="btn-group" role="group" aria-label="">
                 <a href="/customercomplaints/{{ $customercomplaint->id }}/products" class="btn btn-primary">Products</a>
                     {{ link_to_route('customercomplaints.show', 'View', [$customercomplaint->id],['class' => 'btn btn-success']) }}
                     @if(Auth::user()->group == 100)
                     {{ link_to_route('customercomplaints.edit', 'Edit', [$customercomplaint->id],['class' => 'btn btn-warning']) }}

                     {{ Form::open(array('method' => 'DELETE', 'style' => 'display:inline','route' => array('customercomplaints.destroy', $customercomplaint->id))) }}
                     {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                     {{ Form::close() }}
                     @endif
                 </div>
             </td>
            </tr>
            @endif
        @endforeach

        </tbody>
    </table>


<ul class="pagination"></ul>
</div>
    @stop

{{--end content--}}


{{--footer--}}
@section('footer')
    <script>
              $(function() {
            $( "#datepicker" ).datepicker();
            $( "#datepicker1" ).datepicker();
            $('#timepicker').timepicker();
            $('#datatable').DataTable({
                paging:true,
                "order": [[ 1, "desc" ]],
                // scrollY:400,
                deferRender:true,
                // scroller:true,
                columnDefs: [
                                { targets: 'no-sort', orderable: false }
                            ],

            });
        });

    </script>
    @stop
