@extends('layouts.pdf_specsheets')

@section('content')
    
    <div>

{{-- top --}}
<table width="125%">
	<tr>
		<td><h1><u>Customer Concern Report</u></h1></td>
		<td width="400px"></td>
		<td width="400px"></td>
		<td width="400px"></td>
		<td width="400px"></td>
		<td><h2>Case # {{ $concern_report->id + 200 }}</h2></td>
	</tr>
</table>

<table border="0" width="75%">
	<tr>
		<td><strong>Customer Name:</strong></td>
		<td style="border-bottom: 1px solid black;">{{ $concern_report->customer_name }}</td>
		<td width="20px;"></td>
		<td width="110px;"><strong>Date of Complaint:</strong></td>
		<td style="border-bottom: 1px solid black;">{{ $concern_report->date_of_complaint }}</td>
	</tr>
	<tr>
		<td width="110px;"><strong>Customer Contact:</strong></td>
		<td style="border-bottom: 1px solid black;">{{ $concern_report->customer_contact }}</td>
		<td></td>
		<td><strong>Time of Complaint:</strong></td>
		<td style="border-bottom: 1px solid black;">{{ $concern_report->time_of_complaint }}</td>
	</tr>
	<tr>
		<td><strong>Customer's Number:</strong></td>
		<td style="border-bottom: 1px solid black;">{{ $concern_report->customer_phone }}</td>
		<td></td>
		<td><strong>Received By:</strong></td>
		<td style="border-bottom: 1px solid black;">{{ $concern_report->complaint_received_by }}</td>
	</tr>
	<tr>
		<td><strong>Customer's E-mail:</strong></td>
		<td style="border-bottom: 1px solid black;">{{ $concern_report->customer_email }}</td>
		<td></td>
		<td><strong>Recieved Via:</strong></td>
		<td style="border-bottom: 1px solid black;">{{ $concern_report->complaint_received_via }}</td>
	</tr>
	<tr>
		<td><strong>Order#</strong></td>
		<td style="border-bottom: 1px solid black;">{{ $concern_report->order_number }}</td>
		<td></td>
		<td><strong></strong></td>
		<td></td>
	</tr>
	<tr>
		<td><strong>Ship Date:</strong></td>
		<td style="border-bottom: 1px solid black;">{{ $concern_report->ship_date}}</td>
		<td></td>
		<td><strong></strong></td>
		<td></td>
	</tr>
</table>

{{-- details of complaint --}}
<br><br>
<span style="font-size:11px;"><strong>Details of Complaint:</strong></span>

<table width="75%" style="table-layout: fixed">
	<tr>
		<td style="word-wrap: break-word;">{{ $concern_report->details_of_complaint}}</td>
	</tr>
</table>

{{-- 1st action taken --}}
<br>
<span style="font-size:11px;"><strong>1st action taken: (By)</strong> <u>{{ $concern_report->first_action_taken_by}}</u> <strong>Date:</strong> <u>{{ $concern_report->first_action_taken_date}}</u> <strong>Time:</strong> <u>{{ $concern_report->first_action_taken_time}}</u></span>

<table width="75%" style="table-layout: fixed">
	<tr>
		<td style="word-wrap: break-word;">{{ $concern_report->first_action_taken_notes}}</td>
	</tr>
</table>

{{-- Corrective Action --}}
<br>
<span style="font-size:11px;"><strong>Corrective Action: (By)</strong> <u>{{ $concern_report->corrective_action_by}}</u> <strong>Date:</strong> <u>{{ $concern_report->corrective_action_date }}</u> <strong>Time:</strong> <u>{{ $concern_report->corrective_action_time}}</u></span>

<table width="75%" style="table-layout: fixed">
	<tr>
		<td style="word-wrap: break-word;">{{ $concern_report->corrective_action_notes}}</td>
	</tr>
</table>

{{-- Credit/Return/Replacement --}}
<br>
<span style="font-size:11px;"><strong>Credit/Return/Replacement Authorized: <u>{{ $concern_report->credit_return_replacement_authorization}}</u> (By)</strong> <u>{{ $concern_report->credit_return_replacement_authorization_by}}</u></span>

{{-- product description --}}
<br><br>
<table border="1" cellpadding="0" cellspacing="0" width="75%">
	<tr>
	<th><strong>Product Description</strong></th>
	<th><strong>Quantity</strong></th>
	<th><strong>Credit Amount</strong></th>
	</tr>
	@foreach($concern_report->products as $product)
	<tr>
		<td>{{ $product->product_description }}</td>
        <td>{{ $product->product_quantity }}</td>
        <td>{{ $product->credit_amount }}</td>
	</tr>
	@endforeach
		
</table>

{{-- fees --}}
<br>
<span><strong>15% Restocking Fee:</strong> <u>{{ $concern_report->fifteen_restocking_fee }}</u> $<u>{{ $concern_report->fifteen_restocking_fee_amount }}</u></span> 
<span style="float:right;"><strong>UPS or Freight cost to re-ship:</strong>  $<u>{{ $concern_report->ups_freight_refund }}</u></span>
<br>
<span><strong>Return received:</strong> <u>{{ $concern_report->return_received }}</u> <strong>By:</strong> <u>{{ $concern_report->received_by }}</u></span>

</div>


@stop