<h1>RMA</h1>
<h2>Customer Details</h2>
<div class="form-group">
  {{ Form::label('rma_address', 'Address') }}
  {{ Form::text('rma_address', null, ['class' => 'form-control', 'placeholder' => 'Address']) }}
</div>
<div class="form-group">
  {{ Form::label('rma_city', 'City') }}
  {{ Form::text('rma_city', null, ['class' => 'form-control', 'placeholder' => 'City']) }}
</div>
<div class="form-group">
  {{ Form::label('rma_state', 'State') }}
  {{ Form::text('rma_state', null, ['class' => 'form-control', 'placeholder' => 'State']) }}
</div>
<h2>Product Details</h2>
<h3>Create A Product</h3>
{{ Form::open() }}


{{ Form::close() }}

<hr>
<table class="table">
  <thead>
  <tr>
    <th>Product</th>
    <th>Lot Number</th>
    <th>Qty</th>
    <th>Reason For Return Credit</th>
    <th>Sales Order</th>
    <th>Date Shipped</th>
  </tr>
  </thead>
  <tbody>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  </tbody>
</table>

<h2>For Internal Use</h2>
<div class="form-group">
  {{ Form::label('rma_number') }}
  {{ Form::text('rma_number', null, ['class' => 'form-control', 'placeholder' => 'RMA Number'])}}
</div>
