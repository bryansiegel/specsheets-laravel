@extends('layouts.admin')

@section('content')

    <div id="blends">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">View</span>Customer Complaints
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/customercomplaints/">Customer Complaints</a> >
                        View
                    </li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">
                    <a href="{{ url()  . "/customercomplaints"}}" class="btn btn-danger navbar-btn"><< Back</a>
                </nav>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
              <div class="col-md-6">
                    <div class="col-lg-8">
                    <h2>Customer Complaint</h2>
                        <ul class="list-group">
                            {{--main--}}
                            <li class="list-group-item">
                                <strong>Case Number</strong> 
                                    <?php $case_number = $customercomplaint->id + 200; ?>
                                {{ $case_number }}
                            </li>
                            <li class="list-group-item">
                                <strong>Order Number</strong> {{ $customercomplaint->order_number }}
                            </li>
                            <li class="list-group-item">
                                <strong>Corrective Action By</strong> {{ $customercomplaint->corrective_action_by }}
                            </li>
                            <li class="list-group-item">
                                <strong>Company Name</strong> {{ $customercomplaint->customer_name }}
                            </li>
                            <li class="list-group-item">
                                <strong>Customer Contact</strong> {{ $customercomplaint->customer_contact }}
                            </li>
                            <li class="list-group-item">
                                <strong>Customers Email</strong> {{ $customercomplaint->customer_email }}
                            </li>

                            <li class="list-group-item">
                                <strong>Date Of Complaint</strong> {{ $customercomplaint->date_of_complaint }}
                            </li>
                            <li class="list-group-item">
                              <strong>Time Of Complaint</strong> {{ $customercomplaint->time_of_complaint }}
                            </li>
                            <li class="list-group-item">
                                <strong>Ship Date</strong> {{ $customercomplaint->ship_date }}
                            </li>
                            <li class="list-group-item">
                                <strong>Received By</strong> {{ $customercomplaint->received_by }}
                            </li>
                            <li class="list-group-item">
                                <strong>Complaint Received Via</strong> {{ $customercomplaint->complaint_received_via }}
                            </li>
                            <li class="list-group-item">
                                <strong>Details Of Complaint</strong> {{ $customercomplaint->details_of_complaint }}
                            </li>
                            <li class="list-group-item">
                                <strong>Action Taken By</strong> {{ $customercomplaint->first_action_taken_by }}
                            </li>
                            <li class="list-group-item">
                                <strong>Action Taken Date</strong> {{ $customercomplaint->first_action_taken_date }}
                            </li>                            
                            <li class="list-group-item">
                                <strong>Action Taken Time</strong> {{ $customercomplaint->first_action_taken_time }}
                            </li>

                            <li class="list-group-item">
                                <strong>Action Taken Notes</strong> {{ $customercomplaint->first_action_taken_notes }}
                            </li>

                            <li class="list-group-item">
                                <strong>Corrective Action Date</strong> {{ $customercomplaint->corrective_action_date }}
                            </li>                            
                            <li class="list-group-item">
                                <strong>Corrective Action Time</strong> {{ $customercomplaint->corrective_action_time }}
                            </li>
                            <li class="list-group-item">
                                <strong>15% Restocking Fee</strong> {{ $customercomplaint->fifteen_restocking_fee }}
                            </li>
                            <li class="list-group-item">
                                <strong>Returned</strong> {{ $customercomplaint->return_received }}
                            </li>
                            <li class="list-group-item">
                                <strong>Freight Damage UPs</strong> {{ $customercomplaint->freight_damage_ups }}
                            </li>
                            <li class="list-group-item">
                                <strong>Freight Damage Other</strong> {{ $customercomplaint->freight_damage_other }}
                            </li>
                            <li class="list-group-item">
                                <strong>Quality Of Product</strong> {{ $customercomplaint->quality_of_product }}
                            </li>
                            <li class="list-group-item">
                                <strong>Wrong Missing Product</strong> {{ $customercomplaint->wrong_missing_product }}
                            </li>
                            <li class="list-group-item">
                                <strong>Labeling Documentation</strong> {{ $customercomplaint->labeling_documentation }}
                            </li>
                            <li class="list-group-item">
                                <strong>Other</strong> {{ $customercomplaint->other }}
                            </li>
                            <li class="list-group-item">
                                <strong>UPS or Freight Refund</strong> {{ $customercomplaint->ups_freight_refund }}
                            </li>
                            <li class="list-group-item">
                                <strong>Lot Number</strong> {{ $customercomplaint->lot_number }}
                            </li>
                            <li class="list-group-item">
                                <strong>Notes</strong> {{ $customercomplaint->notes }}
                            </li>
                          
                          </ul>



                    </div>
                </div>

                {{--right--}}
                <div class="col-md-6">
                    <h2>Products</h2>
                    <table class="table table-stripped table-responsive table-hover">
                    <thead>
                        <tr>
                            <td><strong>Description</strong></td>
                            <td><strong>Qty</strong></td>
                            <td><strong>Credit Amount</strong></td>
                        </tr>
                    </thead>
                        @foreach($customercomplaint->products as $product)
                            <tr>
                                <td>{{ $product->description }}</td>
                                <td>{{ $product->quantity }}</td>
                                <td>{{ $product->credit_amount }}</td>
                            </tr>
                        @endforeach
                    </table>

                    <hr>
                    <h2>Add A Product</h2>
                    <form method="POST" action="/customercomplaints/{{ $customercomplaint->id }}/products">
                        <input type="hidden" name="id" value="{{ $customercomplaint->id }}">
                        <div class="form-group">
                            <label>Description</label>
                            <input type="text" name="description" class="form-control" placeholder="Description">
                        </div>                        
                        <div class="form-group">
                            <label>QTY</label>
                            <input type="text" name="quantity" class="form-control" placeholder="QTY">
                        </div>
                        <div class="form-group">
                            <label>Credit Amount</label>
                            <input type="text" name="credit_amount" class="form-control" placeholder="Credit Amount">
                        </div>
                        <button type="submit" class="btn btn-primary">Add</button>                        
                    </form>

                 </div><!--end right-->



                </div>
            <hr/>
            <div class="alert alert-dismissible alert-success" style="margin-bottom:0px">
                <strong>Created: </strong><?php echo \Carbon\Carbon::createFromTimestamp(strtotime($customercomplaint->created_at))->diffForHumans(); ?>
            </div>
            <div class="alert alert-dismissible alert-warning">
                <strong>Updated: </strong><?php echo \Carbon\Carbon::createFromTimestamp(strtotime($customercomplaint->updated_at))->diffForHumans(); ?>
            </div>
        </div>
@stop
